package fabnum.metiis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import fabnum.metiis.domain.enumerations.Roles;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Classe utilistaire pour les tests.
 */
public final class TestsUtils {

    /**
     * Mot de passe de socle.
     */
    public final static String PWD = "Azerty123-";

    /**
     * E-mail de socle (ne correspond pas au mail par défaut dans le liquibase.).
     */
    public final static String EMAIL = "noreply@socle.fr";

    /**
     * Salt permettant à génération d'un token (doit être cohérent avec la valeur dans application-socle.yml).
     */
    private final static String SECRET_KEY = "secret";

    /**
     * Temporisation de 1 milliseconde.
     */
    public static void await() {
        await(TimeUnit.MILLISECONDS, 1L);
    }

    /**
     * Temporisation.
     *
     * @param unit    {@link TimeUnit} : unité de temps.
     * @param waitFor {@link Long} : temps d'attente d'attente.
     */
    public static void await(TimeUnit unit, Long waitFor) {
        try {
            unit.sleep(waitFor);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Mot de passe encodé.
     */
    public static String password() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(PWD);
    }

    /**
     * Pemret de créer un contexte pour simuler une connexion.
     *
     * @param username {@link String}
     */
    public static void applySecurityContextWith(String username) {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        securityContext.setAuthentication(new UsernamePasswordAuthenticationToken(username, username));
        SecurityContextHolder.setContext(securityContext);
    }

    /**
     * génération du résultat en chaîne de caractères d'un objet en Json.
     *
     * @param arg0 {@link Object} : l'object à Jsoniser :)
     * @return stream
     */
    public static String toJsonString(Object arg0) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
            return mapper.writer().writeValueAsString(arg0);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Création d'un jeton JWT.
     *
     * @param username {@link String} : username sujet du jeton.
     * @param roles    {@link Roles} : liste des roles du user.
     * @return String
     */
    public static String createToken(String username, Roles... roles) {
        return createToken(ChronoUnit.HOURS, username, roles);
    }

    /**
     * Création d'un jeton JWT.
     *
     * @param addValidityUnit {@link TemporalUnit} : unité de valeur pour l'ajout de la validité du jeton.
     * @param username        {@link String} : username sujet du jeton.
     * @param roles           {@link Roles} : liste des roles du user.
     * @return String
     */
    public static String createToken(TemporalUnit addValidityUnit, String username, Roles... roles) {
        // Récupération des roles pour en faire des authorities spring.
        Collection<GrantedAuthority> authorities = Arrays.stream(roles)
                .map(Roles::name).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("authorities", authorities);
        JwtBuilder jwt = Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date.from(Instant.now().plus(1, addValidityUnit)))
                .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString(SECRET_KEY.getBytes()));

        return String.join(" ", "Bearer", jwt.compact());
    }

    public static String createAdminToken() {
        return createToken("admin", Roles.values());
    }
}
