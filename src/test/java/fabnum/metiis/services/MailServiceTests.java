package fabnum.metiis.services;

import fabnum.metiis.TestsUtils;
import fabnum.metiis.config.ApplicationProperties;
import fabnum.metiis.services.mail.MailService;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.internet.MimeMessage;

/**
 * Classe de socle du service d'envoi de mail.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTests {

    /**
     * Proprietés de l'application.
     */
    @Autowired
    private ApplicationProperties appProps;


    /**
     * Object gérant l'envoi de mail. Est mocké en partie d'où le @Spi.
     */
    @Spy
    private JavaMailSenderImpl javaMailSender;

    /**
     * Va permettre d'attraper le message avant qu'il ne soit envoyé.
     */
    @Captor
    private ArgumentCaptor<MimeMessage> messageCaptor;

    /**
     * Service à tester.
     */
    private MailService service;

    /***/
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        Mockito.doNothing().when(javaMailSender).send(Mockito.any(MimeMessage.class));
        appProps.getMail().setEnabled(true); // Activation de l'envoi pour permettre de tester.
       // service = new MailService(javaMailSender, appProps);
    }

    /**
     * Envoi d'un message synchrone.
     */
    @Test
    public void sendText() throws Exception {
        service.sendText("testSubject", "This is a socle", TestsUtils.EMAIL);
        Mockito.verify(javaMailSender).send(messageCaptor.capture());
        MimeMessage message = messageCaptor.getValue();
        Assertions.assertThat(message.getSubject()).isEqualTo("testSubject");
        Assertions.assertThat(message.getAllRecipients()[0].toString()).isEqualTo(TestsUtils.EMAIL);
        Assertions.assertThat(message.getFrom()[0].toString()).isEqualTo("socle@localhost");
        Assertions.assertThat(message.getContent()).isInstanceOf(String.class);
        Assertions.assertThat(message.getContent().toString()).isEqualTo("This is a socle");
        Assertions.assertThat(message.getDataHandler().getContentType()).isEqualTo("text/html;charset=UTF-8");
    }

    /**
     * Envoi d'un message asynchrone.
     */
    @Test
    public void sendAsyncText() throws Exception {
        service.sendAsyncText("testSubject", "This is a socle", TestsUtils.EMAIL);
        Mockito.verify(javaMailSender).send(messageCaptor.capture());
        MimeMessage message = messageCaptor.getValue();
        Assertions.assertThat(message.getSubject()).isEqualTo("testSubject");
        Assertions.assertThat(message.getAllRecipients()[0].toString()).isEqualTo(TestsUtils.EMAIL);
        Assertions.assertThat(message.getFrom()[0].toString()).isEqualTo("socle@localhost");
        Assertions.assertThat(message.getContent()).isInstanceOf(String.class);
        Assertions.assertThat(message.getContent().toString()).isEqualTo("This is a socle");
        Assertions.assertThat(message.getDataHandler().getContentType()).isEqualTo("text/html;charset=UTF-8");
    }
}
