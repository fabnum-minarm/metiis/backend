package fabnum.metiis.services.administration;

import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.exceptions.ConflictException;
import fabnum.metiis.exceptions.NotFoundException;
import fabnum.metiis.repository.ProfilRepositoryTests;
import fabnum.metiis.services.exceptions.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

/**
 * Classe de socle du service des profils.
 * Attention, pas de rollback à la fin de chaque socle.
 * Pas de socle des méthodes de recherche, d'insertion de modification ou de suppression valides car testées avec
 * {@link ProfilRepositoryTests}
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfilServiceTests {

    /** Service à tester. */
    @Autowired
    private ProfilService service;

    /** Test de recherche de profil via un id inexistant. */
    @Test(expected = NotFoundException.class)
    public void notFindById() {
        this.service.findById(0L);
    }

    /** Test d'insertion de profil sans code. */
    @Test(expected = ServiceException.class)
    public void noCodeSave() {
        Profil p = Profil.builder()
                .description("Technique")
                .roles(new HashSet<>(Collections.singleton(Roles.ROLE_TECH)))
                .build();
        this.service.saveOrUpdate(p);
    }

    /** Test d'insertion de profil sans roles. */
    @Test(expected = ServiceException.class)
    public void noRolesSave() {
        Profil p = Profil.builder()
                .code("tech2")
                .build();
        this.service.saveOrUpdate(p);
    }

    /** Test d'insertion de profil sans code déjà existant. */
    @Test(expected = ConflictException.class)
    public void existingCodeSave() {
        Profil p = Profil.builder()
                .code("user")
                .roles(new HashSet<>(Collections.singleton(Roles.ROLE_TECH)))
                .build();
        this.service.saveOrUpdate(p);
    }

    /** Test de modification de profil avec une référence d'id inconnu. */
    @Test(expected = NotFoundException.class)
    public void idNotFoundUpdate() {
        Profil p = this.service.findById(-2L);
        this.service.update(0L, p);
    }

    /** Test de suppression de profil avec une référence d'id inconnu. */
    @Test(expected = NotFoundException.class)
    public void idNotFoundDelete() {
        this.service.delete(0L);
    }

    /** Test de suppression de profil avec des users associés. */
    @Test(expected = ServiceException.class)
    public void UsersFoundDelete() {
        this.service.delete(-1L);
    }

    /** Test de suppression de profils avec dont un à des users associés. */
    @Test(expected = DataIntegrityViolationException.class)
    public void UsersFoundDeletes() {
        this.service.delete(Arrays.asList(-1L, -2L));
    }
}
