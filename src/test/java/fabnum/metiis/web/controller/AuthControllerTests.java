package fabnum.metiis.web.controller;

import fabnum.metiis.TestsUtils;
import fabnum.metiis.config.ApplicationProperties;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.AuthenticationRequestDto;
import fabnum.metiis.dto.administration.ChangePasswordDto;
import fabnum.metiis.dto.administration.ResetPasswordDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Classe de tests du controller d'authentification et de tout ce qui gravite autour d'un utilisateur connecté.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles({"test", "auth"})
public class AuthControllerTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ApplicationProperties appProps;

    /**
     * Test de connexion à l'application.
     */
    @Test
    public void signin() throws Exception {
        AuthenticationRequestDto dto = new AuthenticationRequestDto("admin", TestsUtils.PWD);

        this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/signin")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("username").value("admin"))
                .andExpect(MockMvcResultMatchers.jsonPath("token").exists());
    }

    /**
     * Test de connexion à l'application avec une authentification erronée.
     */
    @Test
    public void wrongSignin() throws Exception {
        AuthenticationRequestDto dto = new AuthenticationRequestDto("admin", "admin");

        this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/signin")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(dto)))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("signin.error")))
                .andExpect(MockMvcResultMatchers.jsonPath("details").value(Tools.message("signin.error")));
    }

    /**
     * Test de connexion à l'application avec une authentification erronée.
     */
    @Test
    public void wrongSigninAndLock() throws Exception {
        AuthenticationRequestDto dto = new AuthenticationRequestDto("testAuth", "pouet");

        for (int i = 0; i < this.appProps.getPassword().getAttempts() - 1; i++) {
            this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/signin")
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(TestsUtils.toJsonString(dto)))
                    .andExpect(MockMvcResultMatchers.status().isForbidden())
                    .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("signin.error")));
        }

        this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/signin")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(dto)))
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("user.locked")));
    }

    /**
     * Test de déconnexion sans authentification préalable.
     */
    @Test
    public void logoutWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/logout"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de déconnexion.
     */
    @Test
    public void logout() throws Exception {
        System.out.println(TestsUtils.createToken("admin", Roles.ROLE_ADMIN));
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/auth/logout")
                .header("Authorization", TestsUtils.createToken("admin", Roles.ROLE_ADMIN)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("logout.ok", "admin")));
    }

    /**
     * Test de récupération du user connecté sans jeton.
     */
    @Test
    public void meWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/auth/me"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de récupération du user connecté.
     */
    @Test
    public void me() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/auth/me")
                .header("Authorization", TestsUtils.createToken("admin", Roles.ROLE_ADMIN)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("username").value("admin"));
    }

    /**
     * Test de récupération du user connecté avec un jeton expiré (Test unique car celà reste le même comportement pour les autres méthodes).
     */
    @Test
    public void meWithExpiredToken() throws Exception {
        String token = TestsUtils.createToken(ChronoUnit.MILLIS, "admin", Roles.ROLE_ADMIN);
        TestsUtils.await(TimeUnit.MILLISECONDS, 5L);
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/auth/me")
                .header("Authorization", token))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de changement de mot de passe sans jeton.
     */
    @Test
    public void changePasswordWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/changePassword"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de changement de mot de passe.
     */
    @Test
    public void changePassword() throws Exception {
        ChangePasswordDto dto = new ChangePasswordDto(TestsUtils.PWD, "Azerty456-", "Azerty456-");
        this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/changePassword")
                .header("Authorization", TestsUtils.createToken("testChangePwd", Roles.ROLE_ADMIN))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("password.change")));
    }

    /**
     * Test de réinitialisation de mot de passe.
     */
    @Test
    public void resetPassword() throws Exception {
        ResetPasswordDto dto = new ResetPasswordDto("testResetPwd", "noreply@metiis.fr");
        this.mvc.perform(MockMvcRequestBuilders.post("/v1/auth/resetPassword")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(dto)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("password.reset")));
    }

    /**
     * Test de récupération des préférences du user connecté sans jeton.
     */
    @Test
    public void currentUserPreferencesWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/auth/me/preferences"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de récupération des préférences du user connecté sans jeton.
     */
    @Test
    public void currentUserPreferences() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/auth/me/preferences")
                .header("Authorization", TestsUtils.createToken("admin", Roles.ROLE_ADMIN)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("pref1").value("value1"))
                .andExpect(MockMvcResultMatchers.jsonPath("pref2").value("value2"));
    }

    /**
     * Test de modification des préférences du user connecté sans jeton.
     */
    @Test
    public void updateUserPreferencesWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.patch("/v1/auth/me/preferences"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de modification des préférences du user connecté sans jeton.
     */
    @Test
    public void updateUserPreferences() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("prefTest", "testValue");
        this.mvc.perform(MockMvcRequestBuilders.patch("/v1/auth/me/preferences")
                .header("Authorization", TestsUtils.createToken("user", Roles.ROLE_USER))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(map)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("prefTest").value("testValue"));
    }
}
