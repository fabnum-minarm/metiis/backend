package fabnum.metiis.web.controller;

import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcBuilderCustomizer;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.ConfigurableMockMvcBuilder;

@Component
public class MockMvcBuilderCustomizeHeaders implements MockMvcBuilderCustomizer {

    @Override
    public void customize(ConfigurableMockMvcBuilder<?> builder) {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("any")
                .header("Origin", "*");
        builder.defaultRequest(requestBuilder);
    }
}
