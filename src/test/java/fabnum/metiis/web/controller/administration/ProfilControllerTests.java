package fabnum.metiis.web.controller.administration;

import fabnum.metiis.TestsUtils;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.administration.Profil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Classe de tests du controller des profils.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProfilControllerTests {

    @Autowired
    private MockMvc mvc;

    /**
     * Test de recherche des profils sans token.
     */
    @Test
    public void allWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/profils"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de recherche des profils.
     */
    @Test
    public void all() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/profils")
                .header("Authorization", TestsUtils.createAdminToken()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Test de recherche des utilisateurs pour un profil sans token.
     */
    @Test
    public void allUserByProfilWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/profils/%d/utilisateurs", -1L)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());

    }

    /**
     * Test de recherche des utilisateurs pour un profil.
     */
    @Test
    public void allUserByProfil() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/profils/%d/utilisateurs", -1L))
                .header("Authorization", TestsUtils.createAdminToken())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].username").value("admin"));
    }

    /**
     * Test de recherche d'un profil sans token.
     */
    @Test
    public void getWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/profils/%d", -2L)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de recherche d'un profil.
     */
    @Test
    public void get() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/profils/%d", -2L))
                .header("Authorization", TestsUtils.createAdminToken())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("code").value("user"));
    }

    /**
     * Test de recherche des profils sans token.
     */
    @Test
    public void createProfilWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.post("/v1/profils").accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }


    /**
     * Test de création des profils.
     */
    @Test
    public void createProfil() throws Exception {
        JSONObject json = new JSONObject();
        json.put("code", "TestCreate").accumulate("roles", "ROLE_TECH");

        this.mvc.perform(MockMvcRequestBuilders.post("/v1/profils")
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json.toString()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("code").value("TestCreate"));
    }

    /**
     * Test de modification d'un profil avec un identifiant inconnu.
     */
    @Test
    public void updateProfilWithUnknownId() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.put(String.format("/v1/profils/%d", 0L))
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(new Profil())))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("default.notfound.error")));
    }

    /**
     * Test de modification d'un profil avec un code null oy vide.
     */
    @Test
    public void updateProfilWithEmptyCode() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.put(String.format("/v1/profils/%d", -2L))
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(new Profil())))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("profil.missing.code")));
    }

    /**
     * Test de modification d'un profil avec un code déjà existant.
     */
    @Test
    public void updateProfilWithExistingCode() throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", "-1");
        json.put("code", "user");

        this.mvc.perform(MockMvcRequestBuilders.put(String.format("/v1/profils/%d", -1L))
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json.toString()))
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("profil.code.exist")));
    }

    /**
     * Test de modification d'un profil sans rôle associé.
     */
    @Test
    public void updateProfilWithoutRoles() throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", "-2");
        json.put("code", "user");

        this.mvc.perform(MockMvcRequestBuilders.put(String.format("/v1/profils/%d", -2L))
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json.toString()))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("profil.missing.role")));
    }

    /**
     * Test de suppression d'un profil avec un identifiant inconnu.
     */
    @Test
    public void deleteProfilWithUnknownId() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.delete(String.format("/v1/profils/%d", 0L))
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(new Profil())))
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("default.notfound.error")));
    }

    /**
     * Test de suppression d'un profil avec des utilisateurs associés.
     */
    @Test
    public void deleteProfilWithUsers() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.delete(String.format("/v1/profils/%d", -1L))
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(new Profil())))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("profil.users")));
    }

    /**
     * Test de suppression de plusieurs profils avec des erreurs.
     */
    @Test
    public void deleteProfilsInError() throws Exception {
        final Long[] ids = {-1L, -2L};

        this.mvc.perform(MockMvcRequestBuilders.delete("/v1/profils/")
                .header("Authorization", TestsUtils.createAdminToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(TestsUtils.toJsonString(ids)))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("default.error.message")));
    }
}
