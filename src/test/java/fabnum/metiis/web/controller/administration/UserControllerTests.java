package fabnum.metiis.web.controller.administration;

import fabnum.metiis.TestsUtils;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Classe de tests du controller des profils.
 * La plupart des tests ayant été effectués au niveau de repository et/ou du service, il va contenir, principalement les tests sans données.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTests {

    @Autowired
    private MockMvc mvc;

    /**
     * Test de recherche des utilisateurs sans token.
     */
    @Test
    public void allWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/utilisateurs"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de recherche des utilisateurs.
     */
    @Test
    public void all() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/v1/utilisateurs")
                .header("Authorization", TestsUtils.createAdminToken()))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * Test de recherche d'un utilisateur sans token.
     */
    @Test
    public void getWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/utilisateurs/%d", -1L)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de recherche d'un utilisateur.
     */
    @Test
    public void get() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get(String.format("/v1/utilisateurs/%d", -1L))
                .header("Authorization", TestsUtils.createAdminToken())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("username").value("admin"));
    }

    /**
     * Test d'enregistrement d'un utilisateur vide.
     */
    @Test
    public void registerWithoutUser() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.post("/v1/utilisateurs/register"))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    /**
     * Test de modification d'un utilisateur en put.
     */
    @Test
    public void updateIsNotPutMapping() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.put(String.format("/v1/utilisateurs/%d", -2L))
                .header("Authorization", TestsUtils.createAdminToken()))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    /**
     * Test de modification d'un utilisateur sans token.
     */
    @Test
    public void updateWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.patch(String.format("/v1/utilisateurs/%d", -2L)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de modification d'un utilisateur vide.
     */
    @Test
    public void updateWithoutUser() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.patch(String.format("/v1/utilisateurs/%d", -2L))
                .header("Authorization", TestsUtils.createAdminToken()))
                .andExpect(MockMvcResultMatchers.status().isInternalServerError());
    }

    /**
     * Test de suppression d'un utilisateur sans token.
     */
    @Test
    public void deleteWithoutToken() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.delete(String.format("/v1/utilisateurs/%d", -2L)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    /**
     * Test de suppression d'un utilisateur.
     */
    @Test
    public void delete() throws Exception {
        JSONObject json = new JSONObject();
        json.put("username", "testDelete");
        json.put("password", TestsUtils.PWD);
        json.put("confirm", TestsUtils.PWD);
        json.put("lastname", "TEST");
        json.put("firstname", "Delete");
        json.put("email", TestsUtils.EMAIL);

        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.post("/v1/utilisateurs/register")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json.toString()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        JSONObject jsonReturn = new JSONObject(result.getResponse().getContentAsString());

        this.mvc.perform(MockMvcRequestBuilders.delete(String.format("/v1/utilisateurs/%d", jsonReturn.get("id")))
                .header("Authorization", TestsUtils.createAdminToken()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("message").value(Tools.message("user.deleted")));
    }

    /**
     * Test de récupération des roles.
     */
    @Test
    public void getRoles() throws Exception {
        MvcResult result = this.mvc.perform(MockMvcRequestBuilders.get("/v1/utilisateurs/roles")
                .header("Authorization", TestsUtils.createAdminToken()))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        JSONArray jsonReturn = new JSONArray(result.getResponse().getContentAsString());

        Assert.assertEquals(Roles.values().length, jsonReturn.length());
    }
}
