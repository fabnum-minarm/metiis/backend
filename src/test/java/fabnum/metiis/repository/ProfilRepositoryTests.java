package fabnum.metiis.repository;

import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.repository.administration.ProfilRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Classe de socle du repository pour la classe Profil.
 * Un rollback est effectué après chaque socle.
 * Test des méthodes utilisée dans l'application.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class ProfilRepositoryTests {

    /** Profil 'all' inséré à chaque début de socle. */
    private Profil pAll;

    /** Profil 'user' inséré à chaque début de socle. */
    private Profil pUser;

    /** Manager d'entité de socle. */
    @Autowired
    private TestEntityManager entityManager;

    /** Répository à tester. */
    @Autowired
    private ProfilRepository repository;

    /** Méthode d'initialisation avant chaque socle. Créé le profil à tester.*/
    @Before
    public void setUp() {
        pAll = this.entityManager.find(Profil.class, -1L);
        pUser = this.entityManager.find(Profil.class, -2L);

        this.entityManager.detach(pAll);
        this.entityManager.detach(pUser);
    }

    /** Test de recherche de profil via un id existant. */
    @Test
    public void findById() {
        Assert.assertEquals(pAll,
                this.repository.findById(pAll.getId()).orElseGet(Profil::new));
        Assert.assertEquals(pUser,
                this.repository.findById(pUser.getId()).orElseGet(Profil::new));
    }

    /** Test de recherche de profil via un id inexistant. */
    @Test
    public void notFindById() {
        Long notExistingId = pAll.getId()/2;
        Assert.assertNull(this.repository.findById(notExistingId).orElse(null));
    }

    /** Test d'existance de profil via un id existant. */
    @Test
    public void existsById() {
        Assert.assertTrue(this.repository.existsById(pAll.getId()));
    }

    /** Test d'existance de profil via un id inexistant. */
    @Test
    public void notExistsById() {
        Assert.assertFalse(this.repository.existsById(pAll.getId()+100));
    }

    /** Test de recherche de profil via un code existant. */
    @Test
    public void findByCode() {
        Assert.assertEquals(pAll,
                this.repository.findByCode(pAll.getCode()).orElseGet(Profil::new));
        Assert.assertEquals(pUser,
                this.repository.findByCode(pUser.getCode()).orElseGet(Profil::new));
    }

    /** Test de recherche de profil via un code inexistant. */
    @Test
    public void notFindByCode() {
        final String notExistingCode = String.join("_", pAll.getCode(), pUser.getCode());
        Assert.assertNull(this.repository.findByCode(notExistingCode).orElse(null));
    }

    /** Test de recherche de tous les profils. */
    @Test
    public void findAll() {
        List<Profil> list = this.repository.findAll();
        Assert.assertEquals(3, list.size());
    }

    /** Test de recherche des profils via ids existant. */
    @Test
    public void findAllById() {
        List<Profil> list = this.repository.findAllById(Arrays.asList(pAll.getId(), pUser.getId()));
        Assert.assertEquals(2, list.size());
    }

    /** Test de recherche des profils via ids inexistant. */
    @Test
    public void notFindAllById() {
        List<Profil> list = this.repository.findAllById(Arrays.asList(pAll.getId()+100, pUser.getId()+100));
        Assert.assertEquals(0, list.size());
    }

    /** Test de sauvegarde d'un nouveau profil. */
    @Test
    public void save() {
        Profil p = Profil.builder()
            .code("tech")
            .description("Technique")
            .roles(new HashSet<>(Collections.singleton(Roles.ROLE_TECH)))
            .build();
        this.repository.save(p);
        this.repository.flush();
        p = this.repository.findByCode("tech").orElseGet(Profil::new);
        Assert.assertEquals("tech", p.getCode());
        Assert.assertEquals("Technique", p.getDescription());
        Assert.assertEquals(Collections.singleton(Roles.ROLE_TECH), p.getRoles());
        Assert.assertEquals(p.getCreatedBy(), p.getLastModifiedBy());
        Assert.assertEquals(p.getCreatedDate(), p.getLastModifiedDate());
        Assert.assertEquals(Long.valueOf(0L), p.getVersion());
    }

    /** Test de sauvegarde d'un nouveau profil avec une mise à jour automation de la description. */
    @Test
    public void saveAutoDescription() {
        Profil p = Profil.builder()
                .code("tech")
                .roles(new HashSet<>(Collections.singleton(Roles.ROLE_TECH)))
                .build();
        this.repository.save(p);
        this.repository.flush();
        Assert.assertEquals("tech", p.getDescription());
    }

    /** Test de sauvegarde d'un nouveau profil avec un code déjà existant. */
    @Test(expected = DataIntegrityViolationException.class)
    public void saveExistingCode() {
        Profil p = Profil.builder()
                .code(pUser.getCode())
                .roles(new HashSet<>(Collections.singleton(Roles.ROLE_TECH)))
                .build();
        this.repository.save(p);
        this.repository.flush();
    }

    /** Test de modification du profil user. */
    @Test
    public void update() {
        pUser.setCode("userUpd");
        pUser.setRoles(new HashSet<>(Collections.singleton(Roles.ROLE_TECH)));
        this.repository.save(pUser);
        this.repository.flush();
        Profil p = this.repository.findByCode("userUpd").orElseGet(Profil::new);
        Assert.assertEquals("userUpd", p.getCode());
        Assert.assertEquals("user", p.getDescription());
        Assert.assertEquals(Collections.singleton(Roles.ROLE_TECH), p.getRoles());
        Assert.assertNotEquals(p.getCreatedDate(), p.getLastModifiedDate());
        Assert.assertEquals(Optional.of(pUser.getVersion() + 1).get(), p.getVersion());
    }

    /** Test de modification du profil user avec une mise à jour automatique de la description. */
    @Test
    public void updateAutoDescription() {
        pUser.setCode("userUpd");
        pUser.setDescription("");
        this.repository.save(pUser);
        this.repository.flush();
        Profil p = this.repository.findByCode("userUpd").orElseGet(Profil::new);
        Assert.assertEquals("userUpd", p.getCode());
        Assert.assertEquals("userUpd", p.getDescription());
        Assert.assertNotEquals(p.getCreatedDate(), p.getLastModifiedDate());
        Assert.assertEquals(Optional.of(pUser.getVersion() + 1).get(), p.getVersion());
    }

    /** Test de modification du code du profil user avec le code du profil all. */
    @Test(expected = DataIntegrityViolationException.class)
    public void updateExistingCode() {
        pUser.setCode(pAll.getCode());
        this.repository.save(pUser);
        this.repository.flush();
    }

    /** Test de modification du profil user après qu'une modification ait été réalisée par une tiers méthode. */
    @Test(expected = ObjectOptimisticLockingFailureException.class)
    public void updateConcurrent() {
        pUser.setCode("userUpd");
        this.repository.save(pUser);
        this.repository.flush();
        pUser.setCode("userUpd2");
        this.repository.save(pUser);
        this.repository.flush();
    }

    /** Test de suppression du profil user. */
    @Test
    public void delete() {
        this.repository.deleteById(pUser.getId());
        this.repository.flush();
        Assert.assertEquals(2, this.repository.findAll().size());
    }

    /** Test de suppression d'un profil inexistant. */
    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteNonExist() {
        Long notExistingId = pAll.getId()/2;
        this.repository.deleteById(notExistingId);
        this.repository.flush();
    }

    /** Test de suppression du profil user via la méthode de suppression multiple. */
    @Test
    public void deleteAllById() {
        this.repository.deleteByIdIn(Arrays.asList(pUser.getId(), -3L));
        this.repository.flush();
        Assert.assertEquals(1, this.repository.findAll().size());
    }
}
