package fabnum.metiis.repository;

import fabnum.metiis.TestsUtils;
import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.administration.UserProfil;
import fabnum.metiis.repository.administration.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Classe de socle du repository pour la classe User.
 * Un rollback est effectué après chaque socle.
 * Test des méthodes utilisée dans l'application.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class UserRepositoryTest {

    /**
     * Profil 'all' inséré à chaque début de socle.
     */
    private Profil pAll;

    /**
     * Profil 'user' inséré à chaque début de socle.
     */
    private Profil pUser;

    /**
     * Profil 'user' inséré à chaque début de socle.
     */
    private UserProfil upUser;


    /**
     * User 'uAdmin' inséré à chaque début de socle.
     */
    private User uAdmin;

    /**
     * User 'uUser' inséré à chaque début de socle.
     */
    private User uUser;

    /**
     * Manager d'entité de socle.
     */
    @Autowired
    private TestEntityManager entityManager;

    /**
     * Répository à tester.
     */
    @Autowired
    private UserRepository repository;

    /**
     * Méthode d'initialisation avant chaque socle. Créé le profil à tester.
     */
    @Before
    public void setUp() {
        pAll = entityManager.find(Profil.class, -1L);
        pUser = entityManager.find(Profil.class, -2L);

        upUser = entityManager.find(UserProfil.class, -2L);

        uAdmin = entityManager.find(User.class, -1L);
        uUser = entityManager.find(User.class, -2L);

        entityManager.detach(pAll);
        entityManager.detach(pUser);
        entityManager.detach(uAdmin);
        entityManager.detach(uUser);
    }

    /**
     * Test de recherches de user via un id existant.
     */
    @Test
    public void findById() {
        Assert.assertEquals(uAdmin,
                repository.findById(uAdmin.getId()).orElseGet(User::new));
        Assert.assertEquals(uUser,
                repository.findById(uUser.getId()).orElseGet(User::new));
    }

    /**
     * Test de recherche de users via un id inexistant.
     */
    @Test
    public void notFindById() {
        Long notExistingId = uAdmin.getId() / 2;
        Assert.assertNull(repository.findById(notExistingId).orElse(null));
    }

    /**
     * Test d'existance de users via un id existant.
     */
    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsById(uAdmin.getId()));
    }

    /**
     * Test d'existance de users via un id inexistant.
     */
    @Test
    public void notExistsById() {
        Assert.assertFalse(repository.existsById(uAdmin.getId() + 100));
    }


    /**
     * Test de recherche de users via l'id du profil pAll.
     */
    @Test
    public void findByProfilsId() {
        Assert.assertEquals(1, repository.findByUserProfilProfilId(pAll.getId()).size());
    }

    /**
     * Test de recherche de users infructueuse via l'id du profil pUser.
     */
    @Test
    public void notFindByProfilsId() {
        Assert.assertEquals(0, repository.findByUserProfilProfilId(pUser.getId()).size());
    }

    /**
     * Test d'existance de users via l'id du profil pAll.
     */
    @Test
    public void existsUsersByProfilsId() {
        Assert.assertTrue(repository.existsUsersByUserProfilProfilId(pAll.getId()));
    }

    /**
     * Test de non existance de users via l'id du profil pUser.
     */
    @Test
    public void notExistsUsersByProfilsId() {
        Assert.assertFalse(repository.existsUsersByUserProfilProfilId(pUser.getId()));
    }

    /**
     * Test d'existance de users pour un id autre que celui de uAdmin et pour le même username que celui de pUser.
     */
    @Test
    public void existsByIdNotAndUsername() {
        Assert.assertTrue(repository.existsByIdNotAndUsernameIgnoreCase(uAdmin.getId(), uUser.getUsername()));
    }

    /**
     * Test de non existance de user pour un id autre que celui de uAdmin et pour le même username que celui de uAdmin.
     */
    @Test
    public void notExistsByIdNotAndUsername() {
        Assert.assertFalse(repository.existsByIdNotAndUsernameIgnoreCase(uAdmin.getId(), uAdmin.getUsername()));
    }

    /**
     * Test de recherche de user via un username existant.
     */
    @Test
    public void findByUsername() {
        Assert.assertEquals(uAdmin,
                repository.findByUsername(uAdmin.getUsername()).orElseGet(User::new));
        Assert.assertEquals(uUser,
                repository.findByUsername(uUser.getUsername()).orElseGet(User::new));
    }

    /**
     * Test de recherche de user via un username inexistant.
     */
    @Test
    public void notFindByUsername() {
        final String notExistingCode = String.join("_", uAdmin.getUsername(), uUser.getUsername());
        Assert.assertNull(repository.findByUsername(notExistingCode).orElse(null));
    }

    /**
     * Test de recherche de tous les users.
     */
    @Test
    public void findAll() {
        List<User> list = repository.findAll();
        Assert.assertEquals(2, list.size());
    }

    /**
     * Test de recherche des users via ids existant.
     */
    @Test
    public void findAllById() {
        List<User> list = repository.findAllById(Arrays.asList(uAdmin.getId(), uUser.getId()));
        Assert.assertEquals(2, list.size());
    }

    /**
     * Test de recherche des profils via ids inexistant.
     */
    @Test
    public void notFindAllById() {
        List<User> list = repository.findAllById(Arrays.asList(uAdmin.getId() + 100, uUser.getId() + 100));
        Assert.assertEquals(0, list.size());
    }

    /**
     * Test de sauvegarde d'un nouveau user.
     */
    @Test
    public void save() {
        final String pwd = TestsUtils.password();
        User u = User.builder()
                .username("tech")
                .password(pwd)
                .lastname("NICOS")
                .firstname("Tech")
                .email("noreply@metiis.fr")
                .build();
        repository.saveAndFlush(u);
        u = repository.findByUsername("tech").orElseGet(User::new);
        Assert.assertEquals("tech", u.getUsername());
        Assert.assertEquals("NICOS", u.getLastname());
        Assert.assertEquals("Tech", u.getFirstname());
        Assert.assertEquals("noreply@metiis.fr", u.getEmail());
        Assert.assertEquals(pwd, u.getPassword());
        Assert.assertNotEquals(TestsUtils.password(), u.getPassword());
        Assert.assertFalse(u.isEnabled());
        Assert.assertTrue(u.isAccountNonLocked());
        Assert.assertEquals(0, u.getUserProfil().size());
        Assert.assertEquals(0, u.getAuthorities().size());
        Assert.assertEquals(Long.valueOf(0L), u.getVersion());
        Assert.assertEquals(u.getCreatedBy(), u.getLastModifiedBy());
        Assert.assertEquals(u.getCreatedDate(), u.getLastModifiedDate());
    }

    /**
     * Test de sauvegarde d'un nouveau user avec un username déjà existant.
     */
    @Test(expected = DataIntegrityViolationException.class)
    public void saveExistingUsername() {
        final String pwd = TestsUtils.password();
        User u = User.builder()
                .username("user")
                .password(pwd)
                .lastname("TEST")
                .firstname("Doublon")
                .email("noreply@metiis.fr")
                .build();
        repository.saveAndFlush(u);
    }

    /**
     * Test de modification du user uUser.
     */
    @Test
    public void update() {
        final String oldPwd = uUser.getPassword();
        uUser.setUsername("userUpd");
        uUser.setPassword(TestsUtils.password());
        uUser.setLastname("UPDATE");
        uUser.setFirstname("socle");
        uUser.setEmail("noreply@metiis.fr");
        uUser.setUserProfil(new HashSet<>(Collections.singleton(upUser)));
        uUser.setEnabled(true);
        repository.saveAndFlush(uUser);
        entityManager.clear();
        User u = repository.findById(uUser.getId()).orElseGet(User::new);
        Assert.assertEquals("user", u.getUsername());
        Assert.assertEquals("UPDATE", u.getLastname());
        Assert.assertEquals("socle", u.getFirstname());
        Assert.assertEquals("noreply@metiis.fr", u.getEmail());
        Assert.assertNotEquals(oldPwd, u.getPassword());
        Assert.assertTrue(u.isEnabled());
        Assert.assertTrue(u.isAccountNonLocked());
        Assert.assertEquals(1, u.getUserProfil().size());
        Assert.assertEquals(1, u.getAuthorities().size());
        Assert.assertEquals(Optional.of(uUser.getVersion() + 1).get(), u.getVersion());
        Assert.assertNotEquals(u.getCreatedDate(), u.getLastModifiedDate());
    }

    /**
     * Test de modification du user uUser avec une mise à jour automatique de enabled quand il n'a pas de profil associé.
     */
    @Test
    public void updateAutoEnabled() {
        uUser.setEnabled(true);
        repository.saveAndFlush(uUser);
        User u = repository.findById(uUser.getId()).orElseGet(User::new);
        Assert.assertFalse(u.isEnabled());
    }

    /**
     * Test de modification du user uUser après qu'une modification ait été réalisée par une tiers méthode.
     */
    @Test(expected = ObjectOptimisticLockingFailureException.class)
    public void updateConcurrent() {
        uUser.setFirstname("UPDATE");
        repository.saveAndFlush(uUser);
        repository.flush();
        pUser.setCode("REUPDATE");
        repository.saveAndFlush(uUser);
    }

    /**
     * Test de suppression du user uUser.
     */
    @Test
    public void delete() {
        repository.deleteById(uUser.getId());
        repository.flush();
        Assert.assertEquals(1, repository.findAll().size());
    }

    /**
     * Test de suppression du user uAdmin possédant des préférences.
     */
    @Test
    public void deleteWithPreferences() {
        repository.deleteById(uAdmin.getId());
        repository.flush();
        Assert.assertEquals(1, repository.findAll().size());
    }

    /**
     * Test de suppression d'un user inexistant.
     */
    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteNonExist() {
        Long notExistingId = uAdmin.getId() / 2;
        repository.deleteById(notExistingId);
        repository.flush();
    }

    /**
     * Test de recherche un user avec chargement des préférences.
     */
    @Test
    public void getByUsername() {
        User u = repository.getByUsernameIgnoreCase(uAdmin.getUsername()).orElseGet(User::new);
        Assert.assertEquals(1, u.getPreferences().keySet().size());
    }
}
