package fabnum.metiis.dto.tools;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Classe de socle de InfoServiceDto.
 * Test des constructeurs est méthodes autres que getters et setters.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public final class InfoServiceDtoTests {


    /** Test de vérification du statusCode par défaut. */
    @Test
    public void defaultStatusAndStatusCode() {
        HttpStatus s = HttpStatus.NOT_FOUND;
        InfoServiceDto d = new InfoServiceDto();
        Assert.assertEquals(s, d.getStatus());
        Assert.assertEquals(s.value(), d.getStatusCode());
    }

    /** Test de mise à jour du statusCode via set de status. */
    @Test
    public void setStatusAndCheckStatusCode() {
        HttpStatus s = HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS;
        InfoServiceDto d = new InfoServiceDto();
        d.setStatus(s);
        Assert.assertEquals(s, d.getStatus());
        Assert.assertEquals(s.value(), d.getStatusCode());
    }

    /** Test du constructeur pour le retour 200 avec message = details. */
    @Test
    public void okConstructor() {
        InfoServiceDto d = InfoServiceDto.ok("socle message");
        Assert.assertEquals(HttpStatus.OK, d.getStatus());
        Assert.assertEquals(HttpStatus.OK.value(), d.getStatusCode());
        Assert.assertEquals("socle message", d.getMessage());
        Assert.assertEquals("socle message", d.getDetails());
    }

    /** Test du constructeur pour le retour 200 avec message != details. */
    @Test
    public void okWithDetailsConstructor() {
        InfoServiceDto d = InfoServiceDto.ok("socle message", "socle details");
        Assert.assertEquals(HttpStatus.OK, d.getStatus());
        Assert.assertEquals(HttpStatus.OK.value(), d.getStatusCode());
        Assert.assertEquals("socle message", d.getMessage());
        Assert.assertEquals("socle details", d.getDetails());
    }

    /** Test du constructeur pour le retour paramétré avec message == details. */
    @Test
    public void koConstructor() {
        HttpStatus s = HttpStatus.UNAUTHORIZED;
        InfoServiceDto d = InfoServiceDto.ko(s, "socle message");
        Assert.assertEquals(s, d.getStatus());
        Assert.assertEquals(s.value(), d.getStatusCode());
        Assert.assertEquals("socle message", d.getMessage());
        Assert.assertEquals("socle message", d.getDetails());
    }

    /** Test du constructeur pour le retour paramétré avec message != details. */
    @Test
    public void koWithDetailsConstructor() {
        HttpStatus s = HttpStatus.CONFLICT;
        InfoServiceDto d = InfoServiceDto.ko(s, "socle message", "socle details");
        Assert.assertEquals(s, d.getStatus());
        Assert.assertEquals(s.value(), d.getStatusCode());
        Assert.assertEquals("socle message", d.getMessage());
        Assert.assertEquals("socle details", d.getDetails());
    }
}