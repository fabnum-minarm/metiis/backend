package fabnum.metiis.domain;

import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.administration.UserProfil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;

/**
 * Tests unitaire sur la classe User.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTests {

    /**
     * User à tester.
     */
    private User u;

    /**
     * Méthode d'initialisation avant chaque socle.
     */
    @Before
    public void setUp() {
        u = new User();
    }

    /**
     * Vérification de la valeur par défaut de id.
     */
    @Test
    public void testUserId() {
        Assert.assertNull(u.getId());
    }

    /**
     * Vérification de la valeur par défaut de version.
     */
    @Test
    public void testUserVersion() {
        Assert.assertNull(u.getVersion());
    }

    /**
     * Vérification de la valeur par défaut de createdBy.
     */
    @Test
    public void testUserCreatedBy() {
        Assert.assertNull(u.getCreatedBy());
    }

    /**
     * Vérification de la valeur par défaut de createdDate.
     */
    @Test
    public void testUserCreatedDate() {
        Assert.assertNull(u.getCreatedDate());
    }

    /**
     * Vérification de la valeur par défaut de modifiedBy.
     */
    @Test
    public void testUserLastModifiedBy() {
        Assert.assertNull(u.getLastModifiedBy());
    }

    /**
     * Vérification de la valeur par défaut de modifiedDate.
     */
    @Test
    public void testUserLastModifiedDate() {
        Assert.assertNull(u.getLastModifiedDate());
    }

    /**
     * Vérification de la valeur par défaut de profils.
     */
    @Test
    public void testUserDefaultProfils() {
        Assert.assertEquals(new HashSet<UserProfil>(), u.getUserProfil());
    }

    /**
     * Vérification de la valeur par défaut de enabled.
     */
    @Test
    public void testUserDefaultEnabled() {
        Assert.assertFalse(u.isEnabled());
    }

    /**
     * Vérification de la valeur par défaut de accountNonExpired.
     */
    @Test
    public void testUserDefaultAccountNonExpired() {
        Assert.assertTrue(u.isAccountNonExpired());
    }

    /**
     * Vérification de la valeur par défaut de accountNonLocked.
     */
    @Test
    public void testUserDefaultAccountNonLocked() {
        Assert.assertTrue(u.isAccountNonLocked());
    }

    /**
     * Vérification de la valeur par défaut de wrongCredential.
     */
    @Test
    public void testUserDefaultWrongCredential() {
        Assert.assertEquals(0, u.getWrongCredential());
    }

    /**
     * Vérification du fonctionnement des intéractions entre accountNonLocked et wrongCredential.
     */
    @Test
    public void testUserAccountNonLockedAndWrongCredential() {
        u.setWrongCredential(15);
        u.setAccountNonLocked(false);
        Assert.assertEquals(0, u.getWrongCredential());
    }

    /**
     * Vérification de la valeur par défaut de credentialsNonExpired.
     */
    @Test
    public void testUserDefaultCredentialsNonExpired() {
        Assert.assertTrue(u.isCredentialsNonExpired());
    }
}
