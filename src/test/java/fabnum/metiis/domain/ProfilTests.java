package fabnum.metiis.domain;

import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.enumerations.Roles;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashSet;

/**
 * Tests unitaire sur la classe Profil.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfilTests {

    /** Profil à tester. */
    private Profil p;

    /** Méthode d'initialisation avant chaque socle.*/
    @Before
    public void setUp() {
        p = new Profil();
    }

    /** Vérification de la valeur par défaut de id. */
    @Test
    public void testProfilId() {
        Assert.assertNull(p.getId());
    }

    /** Vérification de la valeur par défaut de version. */
    @Test
    public void testProfilVersion() {
        Assert.assertNull(p.getVersion());
    }

    /** Vérification de la valeur par défaut de createdBy. */
    @Test
    public void testProfilCreatedBy() {
        Assert.assertNull(p.getCreatedBy());
    }

    /** Vérification de la valeur par défaut de createdDate. */
    @Test
    public void testProfilCreatedDate() {
        Assert.assertNull(p.getCreatedDate());
    }

    /** Vérification de la valeur par défaut de modifiedBy. */
    @Test
    public void testProfilLastModifiedBy() {
        Assert.assertNull(p.getLastModifiedBy());
    }

    /** Vérification de la valeur par défaut de modifiedDate. */
    @Test
    public void testProfilLastModifiedDate() {
        Assert.assertNull(p.getLastModifiedDate());
    }

    /** Vérification de la valeur par defaut de roles. */
    @Test
    public void testProfilDefaultRoles() {
        Assert.assertEquals(new HashSet<Roles>(), p.getRoles());
    }
}
