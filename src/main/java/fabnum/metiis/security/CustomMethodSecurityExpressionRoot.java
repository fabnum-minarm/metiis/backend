package fabnum.metiis.security;

import fabnum.metiis.web.controller.activite.ActiviteRestController;
import fabnum.metiis.web.controller.rh.UtilisateurRestController;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

import java.util.List;

public class CustomMethodSecurityExpressionRoot
        extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    @Getter
    @Setter
    private Object filterObject;

    @Getter
    @Setter
    private Object returnObject;

    @Getter
    @Setter
    private Object target;

    public CustomMethodSecurityExpressionRoot(Authentication authentication) {
        super(authentication);
    }

    @SuppressWarnings("unchecked")
    public boolean isOrganismeAllowed(Long idOrganisme) {
        List<Long> organismeIdsAllowed = (List<Long>) this.getAuthentication().getDetails();
        return organismeIdsAllowed.contains(idOrganisme);
    }

    @SuppressWarnings("unchecked")
    public boolean isActivityAllowed(Long idActivite) {
        Long idOrganisme = ((ActiviteRestController) target).getService().getIdOrganisme(idActivite);
        List<Long> organismeIdsAllowed = (List<Long>) this.getAuthentication().getDetails();
        return organismeIdsAllowed.contains(idOrganisme);
    }

    @SuppressWarnings("unchecked")
    public boolean isUtilisateurAllowed(Long idUtilisateur) {
        List<Long> organismeIdsAllowed = (List<Long>) this.getAuthentication().getDetails();
        Long idOrganisme = ((UtilisateurRestController) target).getService().getIdOrganisme(idUtilisateur);
        if (idOrganisme == null) {
            idOrganisme = 0L;
        }
        return organismeIdsAllowed.contains(idOrganisme);
    }

    public void setThis(Object target) {
        this.target = target;
    }

    @Override
    public Object getThis() {
        return target;
    }
}