package fabnum.metiis.security.jwt;

import fabnum.metiis.security.CustomAuthentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

//mport lombok.extern.slf4j.Slf4j;
//import javax.servlet.http.Cookie;

//@Slf4j
public class JwtTokenFilter extends GenericFilterBean {

    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException {
        //log.debug("///////////////////////////////////////////////////////////////");
        //HttpServletRequest httpReq = (HttpServletRequest) req;
        //Cookie[] cookies = httpReq.getCookies();
        //if(cookies != null) {
        //    for (Cookie c : cookies) {
        //        log.debug("-----------------------------------------------------------");
        //        log.debug(c.getName() + " : " + c.getValue());
        //        log.debug("-----------------------------------------------------------");
        //    }
        //}

        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
        if (token != null && jwtTokenProvider.validateToken(token)) {
            CustomAuthentication auth = jwtTokenProvider.getAuthentication(token);
            if (auth != null) {
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        }
        filterChain.doFilter(req, res);
    }

}
