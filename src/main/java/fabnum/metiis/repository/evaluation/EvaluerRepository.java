package fabnum.metiis.repository.evaluation;

import fabnum.metiis.domain.evaluation.Evaluer;
import fabnum.metiis.dto.evaluation.EvaluerDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EvaluerRepository extends AbstractRepository<Evaluer, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.evaluation.EvaluerDto( " +
            " e.id , e.createdDate ,  e.noteUtilisation , e.noteBesoin , e.commentaire , " +
            " u.id , u.nom , u.prenom " +
            " ) " +
            "" +
            " FROM Evaluer e " +
            " JOIN e.utilisateur u ";

    String ORDER_BY = "ORDER BY e.createdDate DESC ";

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    Page<EvaluerDto> findDtoPageableAll(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    List<EvaluerDto> findDtoAll();

    @Query(value = "Select AVG(e.noteUtilisation) from Evaluer e")
    Double moyenneNoteUtilisation();

    @Query(value = "Select AVG(e.noteBesoin) from Evaluer e")
    Double moyenneNoteBesoin();

    @Query(value = "SELECT " + NEW_DTO + " where e.id = :id")
    EvaluerDto findDtoById(Long id);
}
