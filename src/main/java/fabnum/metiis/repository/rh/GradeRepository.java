package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.Grade;
import fabnum.metiis.dto.rh.GradeDto;
import fabnum.metiis.repository.abstraction.AbstractRepositoryWithCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface GradeRepository extends AbstractRepositoryWithCode<Grade, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.rh.GradeDto( " +
            " g.id , g.code , g.libelle , g.ordre , " +
            " c.id , c.lettre , c.code , c.libelle , c.ordre )" +
            " FROM Grade g " +
            " JOIN g.corps c";

    String ORDER_BY = " ORDER BY c.ordre , g.ordre ";


    @Query(value = "SELECT " + NEW_DTO +
            " WHERE g.id = :idGrade ")
    Optional<GradeDto> findDtoUsingId(Long idGrade);

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    List<GradeDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    Page<GradeDto> findDtoAll(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE c.id = :idCorps" +
            ORDER_BY)
    List<GradeDto> findDtoAllUsingIdCoprs(Long idCorps);
}
