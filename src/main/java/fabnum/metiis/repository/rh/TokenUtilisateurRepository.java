package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.TokenUtilisateur;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.rh.ChangementMotDePasseDto;
import fabnum.metiis.dto.rh.ConfirmationInscriptionEmployeDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;


public interface TokenUtilisateurRepository extends AbstractRepository<TokenUtilisateur, Long> {


    String NEW_CONFIRMATION_DTO = " new fabnum.metiis.dto.rh.ConfirmationInscriptionEmployeDto( " +
            " tu.token , tu.prescription , " +
            " ut.email , " +
            " o.id ,  o.code , o.libelle , o.credo ," +
            " op.id , op.code , op.libelle ," +
            " utc.id , utc.nom , utc.prenom " +
            " ) " +
            "" +
            " FROM TokenUtilisateur tu " +
            " JOIN tu.utilisateur ut " +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " JOIN af.organisme o " +
            " LEFT JOIN o.parent op " +
            " LEFT JOIN tu.createur utc ";

    String NEW_CHMT_PWD = " new fabnum.metiis.dto.rh.ChangementMotDePasseDto( " +
            " tu.token , ut.email, tu.prescription " +
            " ) " +
            " FROM TokenUtilisateur tu " +
            " JOIN tu.utilisateur ut ";

    String ORDER_BY = " ORDER BY ut.email , ut.id ";
    String SEARCH_EMAIL = " AND LOWER(ut.email) like LOWER(trim(:search)) ";

    Optional<TokenUtilisateur> findByToken(String token);


    @Query(value = "SELECT " + NEW_CONFIRMATION_DTO +
            " WHERE tu.token = :token " +
            " AND tu.forInsciption = true ")
    Optional<ConfirmationInscriptionEmployeDto> findConfirmationDtoUsingToken(String token);

    @Query(value = "SELECT " + NEW_CHMT_PWD +
            " WHERE tu.token = :token " +
            " AND tu.forPassword = true ")
    Optional<ChangementMotDePasseDto> findChangementMotDePasseDtoUsingToken(String token);

    @Query(value = "SELECT " + NEW_CHMT_PWD +
            " WHERE tu.token = :token " +
            " AND tu.forInfos = true ")
    Optional<ChangementMotDePasseDto> findChangementMotDePasseDtoInfosUsingToken(String token);

    @Modifying
    @Query(value = " DELETE FROM TokenUtilisateur tu " +
            " WHERE tu.utilisateur.id = :idUtilisateur " +
            " AND tu.forInsciption = true ")
    void deleteAllTokenInscriptionByIdUtilisateur(Long idUtilisateur);

    @Modifying
    @Query(value = " DELETE FROM TokenUtilisateur tu " +
            " WHERE tu.utilisateur.id = :idUtilisateur " +
            " AND tu.forInfos = true ")
    void deleteAllTokenModificationInfosPersoByIdUtilisateur(Long idUtilisateur);

    @Modifying
    @Query(value = " DELETE FROM TokenUtilisateur tu " +
            " WHERE tu.utilisateur.id = :idUtilisateur " +
            " AND tu.forPassword = true ")
    void deleteAllTokenRenewPasswordByIdUtilisateur(Long idUtilisateur);

    @Modifying
    @Query(value = " DELETE FROM TokenUtilisateur tu " +
            " WHERE tu.prescription < :now " +
            " AND tu.forPassword = true ")
    void supprimerTokenPerimerForRenewPassword(Instant now);

    @Modifying
    @Query(value = " DELETE FROM TokenUtilisateur tu " +
            " WHERE tu.prescription < :now " +
            " AND tu.forInsciption = true ")
    void supprimerTokenPerimerForInscription(Instant now);

    @Query(value = " SELECT tu.utilisateur.id FROM TokenUtilisateur tu " +
            " WHERE tu.prescription < :now " +
            " AND tu.forInsciption = true ")
    List<Long> getIdsUtilisateurTokenPerimerForInscription(Instant now);

    @Modifying
    @Query(value = " DELETE FROM TokenUtilisateur tu " +
            " WHERE tu.prescription < :now " +
            " AND tu.forInfos = true ")
    void supprimerTokenPerimerForModifInfosPerso(Instant now);

    @Query(value = " SELECT tu.utilisateur FROM TokenUtilisateur tu " +
            " WHERE tu.prescription < :now " +
            " AND tu.forInfos = true ")
    List<Utilisateur> getUtilisateursTokenPerimerForModifInfosPerso(Instant now);

    @Query(value = "SELECT " + NEW_CONFIRMATION_DTO +
            " WHERE tu.forInsciption = true " +
            ORDER_BY)
    List<ConfirmationInscriptionEmployeDto> findAllDto();

    @Query(value = "SELECT " + NEW_CONFIRMATION_DTO +
            " WHERE tu.forInsciption = true " +
            " AND o.id = :idOrganisme " +
            ORDER_BY)
    List<ConfirmationInscriptionEmployeDto> getListDtoUsingIdOrganisme(Long idOrganisme);

    @Query(value = "SELECT " + NEW_CONFIRMATION_DTO +
            " WHERE tu.forInsciption = true " +
            " AND o.id = :idOrganisme " +
            SEARCH_EMAIL +
            ORDER_BY)
    List<ConfirmationInscriptionEmployeDto> rechercheInvitation(Long idOrganisme, String search);

    @Query(value = "SELECT " + NEW_CONFIRMATION_DTO +
            " WHERE tu.forInsciption = true " +
            SEARCH_EMAIL +
            ORDER_BY)
    List<ConfirmationInscriptionEmployeDto> rechercheInvitation(String search);

    @Query(value = "from TokenUtilisateur tu WHERE tu.forInsciption = true And tu.utilisateur.id = :idUtilisateur ")
    Optional<TokenUtilisateur> findTokenInscritionByIdUtilisateur(Long idUtilisateur);
}
