package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.GroupePersonnaliser;
import fabnum.metiis.dto.rh.GroupePersonnaliserDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface GroupePersonnaliserRepository extends AbstractRepository<GroupePersonnaliser, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.rh.GroupePersonnaliserDto( " +
            " gp.id , gp.nom , " +
            " ut.id , ut.nom , ut.prenom , ut.email , ut.telephone , " +
            " c.id , c.lettre , c.code , c.libelle , c.ordre " +
            " )" +
            " FROM GroupePersonnaliser gp " +
            " JOIN gp.proprietaire ut " +
            " JOIN ut.corps c ";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE ut.id = :idUtilisateur ")
    List<GroupePersonnaliserDto> findAllDtoUsingIdProprietaire(Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE gp.id = :idGroupePersonnaliser ")
    Optional<GroupePersonnaliserDto> findDtoUsingId(Long idGroupePersonnaliser);

    @Query(value = "SELECT distinct " + NEW_DTO +
            " JOIN gp.groupesPersonnaliserDetails gpd " +
            " JOIN gpd.utilisateur gu " +
            " WHERE ut.id = :idUtilisateurProprietaire " +
            " AND gu.id = :idUtilisateur ")
    List<GroupePersonnaliserDto> findAllDtoUsingIdProprietaireWitchIdUtilisateurInGroupe(Long idUtilisateurProprietaire, Long idUtilisateur);
}
