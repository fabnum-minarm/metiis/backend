package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.TypeOrganisme;
import fabnum.metiis.dto.rh.TypeOrganismeDto;
import fabnum.metiis.repository.abstraction.AbstractRepositoryWithCode;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface TypeOrganismeRepository extends AbstractRepositoryWithCode<TypeOrganisme, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.rh.TypeOrganismeDto(" +
            " to.id , to.code , to.libelle , to.ordre , to.pourAffectation , top.id ) " +
            " FROM TypeOrganisme to " +
            " LEFT JOIN to.parent top ";

    List<TypeOrganisme> findAllByOrderByOrdreAsc();

    @Query(value = "SELECT " + NEW_DTO)
    List<TypeOrganismeDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE to.id = :id")
    Optional<TypeOrganismeDto> findDtoUsingId(Long id);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE top.id = :idTypeOrganismeParent ")
    List<TypeOrganismeDto> findDtoListUsingParent(Long idTypeOrganismeParent);
}
