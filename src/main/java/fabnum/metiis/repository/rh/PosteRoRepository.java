package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.PosteRo;
import fabnum.metiis.dto.rh.PosteRoDto;
import fabnum.metiis.repository.abstraction.AbstractRepositoryWithCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface PosteRoRepository extends AbstractRepositoryWithCode<PosteRo, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.rh.PosteRoDto( " +
            " p.id , p.code , p.libelle , p.ordre " +
            " )" +
            " FROM PosteRo p ";

    String ORDER_BY = " ORDER BY p.ordre , p.code , p.id ";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE p.id = :idPoste ")
    Optional<PosteRoDto> findDtoUsingId(Long idPoste);

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    Page<PosteRoDto> findAllDto(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    List<PosteRoDto> findAllDto();
}
