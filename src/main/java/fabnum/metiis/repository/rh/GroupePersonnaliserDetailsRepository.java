package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.GroupePersonnaliserDetails;
import fabnum.metiis.dto.rh.GroupePersonnaliserDetailsDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;


public interface GroupePersonnaliserDetailsRepository extends AbstractRepository<GroupePersonnaliserDetails, Long> {


    String NEW_DTO = " new fabnum.metiis.dto.rh.GroupePersonnaliserDetailsDto( " +
            " gpd.id ," +
            " gp.id , " +
            UtilisateurRepository.CHAMP_UTILISATEUR_COMPLET +
            " )" +
            " FROM GroupePersonnaliserDetails gpd " +
            " JOIN gpd.groupePersonnaliser gp" +
            " JOIN gpd.utilisateur ut " +
            " JOIN ut.user u " +
            " LEFT JOIN u.userProfil up with up.selected = true " +
            " LEFT JOIN up.profil pf  " +
            " JOIN ut.corps c " +
            " LEFT JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr  " +
            " LEFT JOIN af.organisme o  " +
            " LEFT JOIN o.parent op ";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE gp.id = :idGroupePersonnaliser ")
    List<GroupePersonnaliserDetailsDto> findAllDtoUsingIdGroupePersonnaliser(Long idGroupePersonnaliser);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE gpd.id = :idGroupePersonnaliserDetails ")
    Optional<GroupePersonnaliserDetailsDto> findDtoUsingId(Long idGroupePersonnaliserDetails);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE gp.id in :idsGroupePersonnaliser ")
    List<GroupePersonnaliserDetailsDto> findAllDtoUsingIdsGroupePersonnaliser(Collection<Long> idsGroupePersonnaliser);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN gp.proprietaire p" +
            " WHERE p.id = :idUtilisateur ")
    List<GroupePersonnaliserDetailsDto> findAllDtoUsingIdUtilisateur(Long idUtilisateur);

    @Modifying
    @Query(value = " DELETE FROM GroupePersonnaliserDetails dts " +
            " WHERE dts.groupePersonnaliser.id = :idGroupePersonnaliser ")
    void deleteByGroupePersonnaliserId(Long idGroupePersonnaliser);
}

