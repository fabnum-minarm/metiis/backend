package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.Corps;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.repository.abstraction.AbstractRepositoryWithCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface CorpsRepository extends AbstractRepositoryWithCode<Corps, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.rh.CorpsDto( " +
            " c.id , c.lettre, c.code , c.libelle , c.ordre )" +
            " FROM Corps c ";

    String ORDER_BY = " ORDER BY c.ordre ASC ";

    /**
     * Liste des corps trié par ordre.
     *
     * @return Corps
     */
    List<Corps> findAllByOrderByOrdreAsc();


    @Query(value = "SELECT " + NEW_DTO +
            " WHERE c.id = :idCorps ")
    Optional<CorpsDto> findDtoUsingId(Long idCorps);


    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    List<CorpsDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    Page<CorpsDto> findDtoAll(Pageable pageable);
}
