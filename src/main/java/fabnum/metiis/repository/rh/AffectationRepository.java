package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.Affectation;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface AffectationRepository extends AbstractRepository<Affectation, Long> {

    @Query(value = "SELECT af FROM Affectation af WHERE af.utilisateur.id = :idUtilisateur AND af.dateFinAffectation is null ")
    Optional<Affectation> findDerniereAffectation(Long idUtilisateur);
}
