package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.activite.UtilisateurActiviteDetailsDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.dto.statistique.UtilisateurStatistiqueCountDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;


public interface UtilisateurRepository extends AbstractRepository<Utilisateur, Long> {


    String CHAMP_UTILISATEUR_COMPLET = "" +
            " ut.id ,  ut.nom , ut.prenom , ut.email , ut.telephone , ut.newEmail , ut.newTelephone , " +
            " c.id ,  c.lettre , c.code ,  c.libelle , c.ordre , " +
            " u.enabled , u.accountNonLocked , u.username , " +
            " af.id , af.dateAffectation , af.dateFinAffectation ," +
            " pr.id , pr.code , pr.libelle , pr.ordre , " +
            " o.id ,  o.code , o.libelle , o.credo ," +
            " op.id , op.code , op.libelle ," +
            " pf.id , pf.code , pf.description , pf.isdefault ";

    String NEW_DTO = " new fabnum.metiis.dto.rh.UtilisateurCompletDto( " +
            CHAMP_UTILISATEUR_COMPLET +
            " ) " +
            "" +
            " FROM Utilisateur ut " +
            " JOIN ut.user u " +
            " LEFT JOIN u.userProfil up with up.selected = true " +
            " LEFT JOIN up.profil pf  " +
            " JOIN ut.corps c " +
            " LEFT JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr  " +
            " LEFT JOIN af.organisme o  " +
            " LEFT JOIN o.parent op ";

    String NEW_DTO2 = " new fabnum.metiis.dto.rh.UtilisateurCompletDto( " +
            CHAMP_UTILISATEUR_COMPLET +
            " ) " +
            "" +
            " FROM Utilisateur ut " +
            " JOIN ut.user u " +
            " LEFT JOIN u.userProfil up " +
            " LEFT JOIN up.profil pf  " +
            " JOIN ut.corps c " +
            " LEFT JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr  " +
            " LEFT JOIN af.organisme o  " +
            " LEFT JOIN o.parent op ";

    String NEW_DTO_DASHBOARD = " new fabnum.metiis.dto.rh.UtilisateurCompletDto( " +
            " ut.id ,  ut.nom , ut.prenom , ut.email , ut.telephone , " +
            " c.id ,  c.lettre , c.code ,  c.libelle , c.ordre , " +
            " u.enabled , u.accountNonLocked , u.username  " +
            " ) " +
            "" +
            " FROM Utilisateur ut " +
            " JOIN ut.user u " +
            " LEFT JOIN u.userProfil up with up.selected = true " +
            " LEFT JOIN up.profil pf  " +
            " JOIN ut.corps c ";

    String NEW_DTO_DISPO = NEW_DTO +
            " JOIN ut.participers p " +
            " JOIN p.activite a " +
            " JOIN p.dernierAvancementParticipation dap " +
            " JOIN dap.choix ch " +
            " JOIN ch.statut s " +
            " JOIN ch.typeDisponibilite td " +

            " WHERE o.id = :idOrganisme " +
            " AND a.dateDebut <=  :fin " +
            " AND a.dateFin >= :debut ";

    String NEW_DTO_ACTIVITE_DETAILS = " new fabnum.metiis.dto.activite.UtilisateurActiviteDetailsDto( " +
            " ut.id ,  ut.nom , ut.prenom , ut.email , ut.telephone , " +
            " c.id ,  c.lettre , c.code ,  c.libelle , c.ordre , " +
            " ap.commentaire , ap.dateValidation ,  " +
            " ch.id , ch.code , ch.nom , ch.lettre , ch.actionPrincipale , ch.actionSuppression , ch.canActiviteWithOtherActivite, " +
            " s.id , s.code , s.nom , s.nomSingulier , s.action , " +
            " sd.id , sd.code , sd.disponibilite , " +
            " a.id , " +
            " af.id , " +
            " pr.id , pr.code , pr.libelle , pr.ordre , " +
            " uv.id ,  uv.nom , uv.prenom , " +
            " cuv.id ,  cuv.lettre , cuv.code ,  cuv.libelle , cuv.ordre , " +
            " afv.id , " +
            " prv.id , prv.code , prv.libelle , prv.ordre " +
            " ) " +
            "" +
            " FROM Utilisateur ut " +
            " JOIN ut.corps c " +
            " JOIN ut.participers p " +
            " JOIN p.activite a " +
            " LEFT JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr ";

    String NEW_DTO_ACTIVITE_DETAILS_GROUPE = " new fabnum.metiis.dto.activite.UtilisateurActiviteDetailsDto( " +
            " ut.id ,  ut.nom , ut.prenom , ut.email , ut.telephone , " +
            " c.id ,  c.lettre , c.code ,  c.libelle , c.ordre , " +
            " ap.commentaire , ap.dateValidation ,  " +
            " ch.id , ch.code , ch.nom , ch.lettre , ch.actionPrincipale , ch.actionSuppression , ch.canActiviteWithOtherActivite, " +
            " s.id , s.code , s.nom , s.nomSingulier , s.action , " +
            " sd.id , sd.code , sd.disponibilite , " +
            " a.id , " +
            " af.id , " +
            " pr.id , pr.code , pr.libelle , pr.ordre , " +
            " uv.id ,  uv.nom , uv.prenom , " +
            " cuv.id ,  cuv.lettre , cuv.code ,  cuv.libelle , cuv.ordre , " +
            " afv.id , " +
            " prv.id , prv.code , prv.libelle , prv.ordre " +
            " ) " +
            "" +
            " FROM GroupePersonnaliserDetails gp" +
            " JOIN gp.utilisateur ut " +
            " JOIN gp.groupePersonnaliser g" +
            " JOIN ut.corps c " +
            " JOIN ut.participers p " +
            " JOIN p.activite a " +
            " LEFT JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr ";

    String ACTIVITE_DETAILS_JOIN = "" +
            " JOIN ap.choix ch " +
            " JOIN ch.statut s " +
            " LEFT JOIN s.statutDisponibilite sd " +
            " LEFT JOIN ap.utilisateurValidateur uv " +
            " LEFT JOIN uv.corps cuv " +
            " LEFT JOIN uv.affectations afv with afv.dateFinAffectation is null " +
            " LEFT JOIN afv.posteRo prv  ";

    String SEARCH_UTILISATEUR = "( " +
            " lower(trim(ut.nom || ' ' || ut.prenom)) like lower(trim(:search)) " +
            " OR lower(trim(ut.prenom || ' ' || ut.nom)) like lower(trim(:search)) " +
            " OR lower(trim(ut.nom )) like lower(trim(:search)) " +
            " OR lower(trim(ut.prenom )) like lower(trim(:search)) " +
            " OR lower(trim(pr.code || ' ' || ut.nom || ' ' || ut.prenom)) like lower(trim(:search)) " +
            " OR lower(trim(pr.code || ' ' || ut.prenom || ' ' || ut.nom)) like lower(trim(:search)) " +
            " ) ";


    String SQL_COUNT = "select  ut.id  as utid ,  ut.nom , ut.prenom ,  " +
            " c.id as cid , c.code as ccode, c.ordre  as cordre, " +
            " af.id as afid , " +
            " pr.id as pid , pr.code  as prcode, pr.ordre as prordre , " +
            " u.last_connexion_date , coalesce(dispo.nbdispo , 0) nbdispo ," +
            " coalesce(activite.nbactivite , 0) nbactivite ," +
            " coalesce(participer.nbparticipation , 0) nbparticipation , " +
            " coalesce(dispook.nbdispook , 0) nbdispook " +
            " from utilisateur ut " +
            " join users u on ut.iduser = u.id" +
            " join corps c on ut.idcorps = c.id " +
            " join affectation af on af.idutilisateur = ut.id and af.datefinaffectation is null " +
            " left join postero pr on af.idpostero = pr.id " +
            " join organisme o on af.idorganisme = o.id and o.id = :idOrganisme " +
            " left join (" +
            "       select d.idutilisateur , count(d.id)  as nbdispo" +
            "       from affectation a " +
            "       join disponibilite d on a.idutilisateur = d.idutilisateur" +
            "       where a.datefinaffectation is null" +
            "       and d.datedebut between :debutDispo and :finDispo " +
            "       and a.idorganisme = :idOrganisme " +
            "       group by d.idutilisateur) dispo on dispo.idutilisateur = ut.id " +
            " left join (" +
            "       select a.idutilisateur , count(a.id)  as nbactivite" +
            "       from activite a " +
            "       WHERE a.dateDebut <=  :finActivite " +
            "       AND a.dateFin >= :debutActivite " +
            "       and a.idorganisme = :idOrganisme " +
            "       group by a.idutilisateur) activite on activite.idutilisateur = ut.id " +
            " left join (" +
            "       select p.idutilisateur , count(p.id)  as nbparticipation" +
            "       from activite a " +
            "       join participer p on p.idactivite = a.id " +
            "       WHERE a.dateDebut <=  :finActivite " +
            "       AND a.dateFin >= :debutActivite " +
            "       and a.idorganisme = :idOrganisme " +
            "       group by p.idutilisateur ) participer on participer.idutilisateur = ut.id " +
            " left join (" +
            "       select d.idutilisateur , count(d.id)  as nbdispook" +
            "       from affectation a " +
            "       join disponibilite d on a.idutilisateur = d.idutilisateur" +
            "       join typedisponibilite td on d.idtypedisponibilite = td.id and td.estdisponible = true " +
            "       where a.datefinaffectation is null" +
            "       and d.datedebut between :debutDispo and :finDispo " +
            "       and a.idorganisme = :idOrganisme " +
            "       group by d.idutilisateur) dispook on dispook.idutilisateur = ut.id ";


    String GROUPE_JOIN = "" +
            " JOIN ut.groupesPersonnaliserDetails gpd " +
            " JOIN gpd.groupePersonnaliser gp " +
            " WHERE gp.id = :idGroupePersonnaliser ";

    String ORDER_BY = " ORDER BY pr.ordre ,  c.ordre asc , ut.nom ";

    String ORDER_BY_AVANCEMENT_DATE = " ORDER BY ap.dateValidation desc,  cuv.ordre asc , uv.nom , ap.id ";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE LOWER(u.username) = LOWER(:username) ")
    Optional<UtilisateurCompletDto> findDtoUsingUsername(String username);

    Optional<Utilisateur> findByUserUsernameIgnoreCase(String userName);

    Optional<Utilisateur> findByEmail(String email);

    boolean existsByIdNotAndEmail(Long id, String Email);

    @Query(value = "SELECT " + NEW_DTO
            + ORDER_BY)
    List<UtilisateurCompletDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    Page<UtilisateurCompletDto> findDtoAll(Pageable pageable);


    @Query(value = "SELECT " + NEW_DTO +
            " WHERE ut.id = :id ")
    Optional<UtilisateurCompletDto> findDtoUsingId(Long id);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE ut.id in (:ids) " +
            ORDER_BY)
    List<UtilisateurCompletDto> findDtoUsingIdIn(List<Long> ids);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE ut.user.id = :idUser ")
    Optional<UtilisateurCompletDto> findDtoUsingIdUser(Long idUser);

    @Query(value = "SELECT distinct " + NEW_DTO_DISPO +
            " AND ( " +
            "    td.estDisponible = false " +
            "    OR a.id = :idActiviteNotIn " +
            " )" +
            ORDER_BY)
    List<UtilisateurCompletDto> findAllParticipantActiviteNonDisponibleBetween(Long idOrganisme, Instant debut, Instant fin, Long idActiviteNotIn);

    @Query(value = "SELECT distinct " + NEW_DTO_DISPO +
            " AND td.estDisponible = false " +
            ORDER_BY)
    List<UtilisateurCompletDto> findAllParticipantActiviteNonDisponibleBetween(Long idOrganisme, Instant debut, Instant fin);


    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            " AND u.enabled=true" +
            ORDER_BY)
    List<UtilisateurCompletDto> findDtoAllUsingOrganisme(Long idOrganisme);

    @Query(value = "SELECT " + NEW_DTO2 +
            " WHERE o.id = :idOrganisme " +
            " AND pf.code IN ('employe', 'super_employe') " +
            " AND u.enabled=true" +
            ORDER_BY)
    List<UtilisateurCompletDto> findDtoAllEmployeUsingOrganisme(Long idOrganisme);

    @Query(value = "SELECT " + NEW_DTO2 +
            " WHERE o.id = :idOrganisme " +
            " AND u.enabled=true " +
            " AND pf.id= :idProfil")
    List<UtilisateurCompletDto> findDtoAllUsingOrganismeAndProfil(Long idOrganisme, Long idProfil);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            ORDER_BY)
    List<UtilisateurCompletDto> findDtoAllUsingOrganismeWithNoEnabled(Long idOrganisme);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            " AND u.accountNonLocked = false " +
            ORDER_BY)
    List<UtilisateurCompletDto> findDtoAllBloqueUsingOrganisme(Long idOrganisme);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            " AND u.enabled = false " +
            ORDER_BY)
    List<UtilisateurCompletDto> findDtoAllAValiderUsingOrganisme(Long idOrganisme);

    @Query(value = "SELECT " + NEW_DTO_ACTIVITE_DETAILS +
            " JOIN p.dernierAvancementParticipation ap " +
            ACTIVITE_DETAILS_JOIN +
            " WHERE a.id = :idActivite " +
            " AND s.id = :idStatut " +
            ORDER_BY)
    List<UtilisateurActiviteDetailsDto> findParticipantActiviteUsingStatut(Long idActivite, Long idStatut);

    @Query(value = "SELECT DISTINCT" + NEW_DTO_ACTIVITE_DETAILS_GROUPE +
            " JOIN p.dernierAvancementParticipation ap " +
            ACTIVITE_DETAILS_JOIN +
            " WHERE a.id = :idActivite " +
            " AND g.id in (:idsGroupe) " +
            ORDER_BY)
    List<UtilisateurActiviteDetailsDto> findParticipantActiviteDetail(Long idActivite, List<Long> idsGroupe);

    @Query(value = "SELECT " + NEW_DTO_ACTIVITE_DETAILS +
            " JOIN p.avancementParticipations ap " +
            ACTIVITE_DETAILS_JOIN +
            " WHERE a.id = :idActivite " +
            " AND ap.commentaire is not null " +
            " AND (:idUtilisateur is null or ut.id = :idUtilisateur ) " +
            ORDER_BY_AVANCEMENT_DATE)
    List<UtilisateurActiviteDetailsDto> getListCommentaireActivite(Long idActivite, Optional<Long> idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            " AND " + SEARCH_UTILISATEUR +
            ORDER_BY)
    List<UtilisateurCompletDto> searchDtoUsingOrganisme(Long idOrganisme, String search);

    @Query(value = "SELECT ut " +
            " FROM Utilisateur ut " +
            " JOIN FETCH ut.user u" +
            " WHERE ut.id = :idUtilisateur ")
    Optional<Utilisateur> findByIdWithUser(Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            GROUPE_JOIN +
            ORDER_BY)
    List<UtilisateurCompletDto> findDtoAllUsingIdGroupePersonnaliser(Long idGroupePersonnaliser);

    @Query(value = SQL_COUNT + ORDER_BY, nativeQuery = true)
    List<?> getStatistiques(Long idOrganisme, Instant debutDispo, Instant finDispo, Instant debutActivite, Instant finActivite);

    @Query(value = "SELECT distinct" + NEW_DTO + " WHERE ( :enabled is null or u.enabled = :enabled) " +
            "AND (UPPER(ut.nom) like %:filter% OR UPPER(ut.email) like %:filter%)")
    Page<UtilisateurCompletDto> findDtoDashboardAllSort(Pageable pageable, String filter, Boolean enabled);

    @Query(value = "SELECT " + NEW_DTO_ACTIVITE_DETAILS +
            " JOIN p.avancementParticipations ap " +
            " JOIN ap.etape e " +
            ACTIVITE_DETAILS_JOIN +
            " WHERE a.id = :idActivite " +
            " AND ap.commentaire is not null " +
            " AND uv.id = :idUtilisateur and ut.id = :idUtilisateur" +
            " AND e.id = :idEtape " +
            ORDER_BY_AVANCEMENT_DATE)
    List<UtilisateurActiviteDetailsDto> getListCommentaireActivite(Long idActivite, Long idUtilisateur, Long idEtape);

    @Query(value = "SELECT distinct" + NEW_DTO +
            " WHERE ( :enabled is null or u.enabled = :enabled) " +
            " AND (UPPER(ut.nom) like %:filter% OR UPPER(ut.email) like %:filter%) " +
            " AND o.id IN :idsOrganisme")
    Page<UtilisateurCompletDto> findDtoDashboardAllSort(List<Long> idsOrganisme, Pageable pageable, String filter, Boolean enabled);

    @Query(nativeQuery = true,
            value = "SELECT  ut.nb as utnb , tu.nb as tunb   , a.nb as anb , d.nb  as dnb , p.nb as pnb, o.nb as onb " +
                    " FROM" +
                    " (SELECT count(ut.id) nb " +
                    "      FROM utilisateur ut " +
                    "      join affectation af on af.idutilisateur = ut.id and af.datefinaffectation is null " +
                    "      WHERE not exists (SELECT tu.idutilisateur " +
                    "           FROM tokenutilisateur tu" +
                    "           WHERE tu.idutilisateur = ut.id )  " +
                    " ) ut, " +
                    " ( SELECT count(tu.id) nb " +
                    "   FROM tokenutilisateur tu " +
                    "   WHERE tu.forinsciption = true " +
                    " ) tu, " +
                    " (SELECT count(a.id) nb " +
                    "      FROM activite a " +
                    " ) a, " +
                    " (SELECT count(d.id) nb " +
                    "      FROM disponibilite d " +
                    "      JOIN typedisponibilite td on d.idtypedisponibilite = td.id and td.estDisponible = true " +
                    " ) d , " +
                    " (SELECT count(p.id) nb " +
                    "      FROM participer p " +
                    " ) p , " +
                    " ( SELECT count(o.id) nb " +
                    "   FROM organisme o " +
                    "    )o "
    )
    Optional<?> getStatistiquesGeneral();

    @Query(value = " SELECT ut " +
            " FROM Utilisateur ut " +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " JOIN af.organisme o  " +
            " WHERE o.id = :idOrganisme " +
            " AND ut.user.enabled = true ")
    List<Utilisateur> findAllUsingOrganisme(Long idOrganisme);

    @Query(value = " SELECT ut " +
            " FROM Utilisateur ut " +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " JOIN af.organisme o " +
            " JOIN o.activites a " +
            " WHERE a.id = :idActivite " +
            " AND ut.user.enabled = true " +
            " AND ut.id not in ( " +
            "   SELECT p.utilisateur.id " +
            "   FROM Participer p " +
            "   WHERE p.activite.id = :idActivite " +
            ")")
    List<Utilisateur> findNonParticipantActivite(Long idActivite);

    @Query(value = " SELECT ut " +
            " FROM Utilisateur ut " +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " JOIN af.organisme o " +
            " JOIN o.activites a " +
            " WHERE a.id = :idActivite " +
            " AND ut.user.enabled = true " +
            " AND ut.id in ( " +
            "   SELECT p.utilisateur.id " +
            "   FROM Participer p " +
            "   JOIN p.dernierAvancementParticipation ap " +
            "   JOIN ap.choix ch " +
            "   WHERE p.activite.id = :idActivite " +
            "   AND ch.code='convoquer' " +
            ")")
    List<Utilisateur> findParticipantActivite(Long idActivite);

    @Query(value = " SELECT new fabnum.metiis.dto.statistique.UtilisateurStatistiqueCountDto( " + CHAMP_UTILISATEUR_COMPLET + ", u.lastConnexionDate )" +
            " FROM Utilisateur ut " +
            " JOIN ut.user u " +
            " LEFT JOIN u.userProfil up with up.selected = true " +
            " LEFT JOIN up.profil pf  " +
            " JOIN ut.corps c " +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr  " +
            " JOIN af.organisme o  " +
            " JOIN o.parent op " +
            " WHERE u.lastConnexionDate != null " +
            " ORDER BY u.lastConnexionDate desc ")
    List<UtilisateurStatistiqueCountDto> dernieresConnexion(Pageable pageable);

    @Query(value = "select count(ut) " +
            " from Utilisateur ut " +
            " join ut.affectations a with a.dateFinAffectation is null" +
            " where a.organisme.id = :idOrganisme and ut.corps.id = :idCorps and ut.user.enabled = true")
    Long compterNombrePaxUnite(Long idOrganisme, Long idCorps);

    @Query(value = " SELECT o.id " +
            " FROM Utilisateur ut " +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " JOIN af.organisme o  " +
            " WHERE ut.id = :idUtilisateur ")
    Long getIdOrganisme(Long idUtilisateur);
}
