package fabnum.metiis.repository.rh;

import fabnum.metiis.domain.rh.Organisme;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.OrganismeHiearchieDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;


public interface OrganismeRepository extends AbstractRepository<Organisme, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.rh.OrganismeDto( o.id ," +
            "       o.code , o.libelle , o.credo ," +
            "       to.id , to.libelle , to.pourAffectation ," +
            "       op.id , op.code , op.libelle ) " +
            " FROM Organisme o " +
            " JOIN o.typeOrganisme to " +
            " LEFT JOIN o.parent op ";

    String NEW_DTO_HIEARCHIE = " new fabnum.metiis.dto.rh.OrganismeHiearchieDto( o.id ," +
            "       o.code , o.libelle , o.credo ," +
            "       to.id , to.libelle , " +
            "        op.id ) " +
            " FROM Organisme o " +
            " JOIN o.typeOrganisme to " +
            " LEFT JOIN o.parent op ";

    String RECUSSIVE_REQ = " " +
            " WITH RECURSIVE organisme_rec(id , code , libelle ,  credo , idtypeorganisme , fullname , idparent ) AS (" +
            "    SELECT id , code , libelle , credo, idtypeorganisme , '' || code as fullname  , idparent" +
            "    FROM organisme " +
            "    WHERE id= :idOrganismeParent " +
            "    UNION ALL " +
            "    SELECT o.id , o.code ,o.libelle , o.credo , o.idtypeorganisme , op.fullname || '/'|| o.code as fullname , o.idparent " +
            "    FROM organisme_rec op " +
            "    JOIN organisme o ON 	o.idparent = op.id " +
            " ) " +
            "    SELECT o.id as oid , o.code as ocode , o.libelle as olibelle , o.credo as ocredo ," +
            "           tor.id as torid , tor.libelle , " +
            "           op.id as opid , op.code as opcode , op.libelle as oplibelle , op.credo as opcredo , " +
            "           o.fullname " +
            "    FROM organisme_rec o " +
            "    JOIN typeorganisme tor on o.idtypeorganisme = tor.id " +
            "    LEFT JOIN organisme op on o.idparent = op.id " +
            "    WHERE tor.pouraffectation = true " +
            "    order by o.fullname ";

    String RECUSSIVE_REQ2 = " " +
            " WITH RECURSIVE organisme_rec(id , code , libelle ,  credo , idtypeorganisme , fullname , idparent ) AS (" +
            "    SELECT id , code , libelle , credo, idtypeorganisme , '' || code as fullname  , idparent" +
            "    FROM organisme " +
            "    WHERE id= :idOrganisme " +
            "    UNION ALL " +
            "    SELECT o.id , o.code ,o.libelle , o.credo , o.idtypeorganisme , op.fullname || '/'|| o.code as fullname , o.idparent " +
            "    FROM organisme_rec op " +
            "    JOIN organisme o ON 	o.id = op.idparent " +
            " ) " +
            "    SELECT o.id as oid , o.code as ocode , o.libelle as olibelle , o.credo as ocredo ," +
            "           tor.id as torid , tor.libelle , " +
            "           op.id as opid , op.code as opcode , op.libelle as oplibelle , op.credo as opcredo , " +
            "           o.fullname " +
            "    FROM organisme_rec o " +
            "    JOIN typeorganisme tor on o.idtypeorganisme = tor.id " +
            "    LEFT JOIN organisme op on o.idparent = op.id " +
            "    WHERE o.idtypeorganisme= :idTypeOrganisme ";

    String SQL_COUNT = "select o.id , o.code  , op.code as parentCode ," +
            "  aff.nbutilisateur ," +
            " coalesce(dispo.nbdispo , 0) nbdispo ," +
            " coalesce(activite.nbactivite , 0) nbactivite ," +
            " coalesce(dispook.nbdispo , 0) nbdispook ," +
            " coalesce(invitation.nbinvitation , 0) nbinvitation " +
            " from organisme o" +
            " left join organisme op on o.idparent = op.id" +
            " join (" +
            "       select a.idorganisme , count(a.idutilisateur)  as nbutilisateur" +
            "       from affectation a" +
            "       where a.datefinaffectation is null" +
            "       group by a.idorganisme) aff on aff.idorganisme = o.id" +
            " left join (" +
            "       select a.idorganisme , count(d.id)  as nbdispo" +
            "       from affectation a" +
            "       join disponibilite d on a.idutilisateur = d.idutilisateur" +
            "       where a.datefinaffectation is null" +
            "       and d.datedebut between :debutDispo and :finDispo " +
            "       group by a.idorganisme) dispo on dispo.idorganisme = o.id " +

            " left join (" +
            "       select a.idorganisme , count(a.id)  as nbactivite" +
            "       from activite a " +
            "       WHERE a.dateDebut <=  :finActivite " +
            "       AND a.dateFin >= :debutActivite " +
            "       group by a.idorganisme) activite on activite.idorganisme = o.id " +
            " left join (" +
            "       select a.idorganisme , count(d.id)  as nbdispo" +
            "       from affectation a" +
            "       join disponibilite d on a.idutilisateur = d.idutilisateur " +
            "       join typedisponibilite td on d.idtypedisponibilite = td.id and td.estdisponible = true" +
            "       where a.datefinaffectation is null" +
            "       and d.datedebut between :debutDispo and :finDispo " +
            "       group by a.idorganisme) dispook on dispook.idorganisme = o.id " +
            " left join (" +
            "       select a.idorganisme , count(a.idutilisateur) as nbinvitation " +
            "       from tokenutilisateur tu " +
            "       join affectation a on tu.idutilisateur = a.idutilisateur " +
            "       where a.datefinaffectation is null " +
            "       and tu.forinsciption is true " +
            "       group by a.idorganisme) invitation on invitation.idorganisme = o.id";


    Optional<Organisme> findByCredo(String credo);

    Optional<Organisme> findByIdNotAndCredo(Long id, String credo);

    @Query(value = "SELECT " + NEW_DTO)
    List<OrganismeDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO)
    Page<OrganismeDto> findDtoAll(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :id")
    Optional<OrganismeDto> findDtoUsingId(Long id);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE to.id = :idTypeOrganisme")
    List<OrganismeDto> findDtoUsingIdTypeOrganisme(Long idTypeOrganisme);

    @Query(value = "SELECT " + NEW_DTO_HIEARCHIE +
            " WHERE to.id = :idTypeOrganisme " +
            " ORDER BY o.code")
    List<OrganismeHiearchieDto> findDtoHiearchieUsingIdTypeOrganisme(Long idTypeOrganisme);

    @Query(value = "SELECT " + NEW_DTO_HIEARCHIE +
            " WHERE o.id = :idOrganisme " +
            " ORDER BY o.code")
    List<OrganismeHiearchieDto> findDtoHiearchieUsingId(Long idOrganisme);

    @Query(value = "SELECT o " +
            " FROM Organisme o " +
            " JOIN o.affectations af " +
            " JOIN af.utilisateur u " +
            " WHERE u.id = :idUtilisateur " +
            " AND af.dateFinAffectation is null")
    Optional<Organisme> findAffectationByIdUtilisateur(Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN o.affectations af " +
            " JOIN af.utilisateur u " +
            " WHERE u.id = :idUtilisateur " +
            " AND af.dateFinAffectation is null")
    Optional<OrganismeDto> findAffectationDtoByIdUtilisateur(Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN op.typeOrganisme top " +
            " WHERE to.parent.id = top.id " +
            " AND op.id = :idOrganismeParent ")
    List<OrganismeDto> findDtoListUsingParent(Long idOrganismeParent);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN op.typeOrganisme top " +
            " WHERE to.parent.id = top.id " +
            " AND o.id = :idOrganisme ")
    List<OrganismeDto> findDtoListUsingid(Long idOrganisme);

    @Query(value = SQL_COUNT, nativeQuery = true)
    List<?> countAllUserAndDispoAndActivite(Instant debutDispo, Instant finDispo, Instant debutActivite, Instant finActivite);

    @Query(value = RECUSSIVE_REQ, nativeQuery = true)
    List<?> findAllOrganismeAffectationUsingIdParent(Long idOrganismeParent);

    @Query(value = RECUSSIVE_REQ2, nativeQuery = true)
    Optional<?> findOrganismeParentByTypeOrganisme(Long idOrganisme, Long idTypeOrganisme);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE to.pourAffectation = true")
    List<OrganismeDto> findDtoAllAffectable();
}
