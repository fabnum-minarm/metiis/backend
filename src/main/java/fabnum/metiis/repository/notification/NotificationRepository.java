package fabnum.metiis.repository.notification;

import fabnum.metiis.domain.notification.Notification;
import fabnum.metiis.dto.notification.NotificationDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface NotificationRepository extends AbstractRepository<Notification, Long> {

    String NEW_DTO = "new fabnum.metiis.dto.notification.NotificationDto("
            + " no.id , no.notifAppActiviteCree , no.notifAppActiviteModif , no.notifAppActiviteRappel ,"
            + " no.notifAppActiviteSupp , no.notifAppCandidatRetenu , no.notifAppCandidatNonRetenu , no.notifAppCandidatConvoque ,"
            + " no.notifAppCandidatNonConvoque , no.notifAppPartageActiviteAutreUnite , no.notifAppCreaActiviteAutreUnite ,"
            + " no.notifAppModifActivitePartage , no.notifAppSuppActivitePartage , no.notifMailActiviteCree ,"
            + " no.notifMailActiviteModif , no.notifMailActiviteRappel , no.notifMailActiviteSupp ,"
            + " no.notifMailCandidatRetenu , no.notifMailCandidatNonRetenu , no.notifMailCandidatConvoque , no.notifMailCandidatNonConvoque ,"
            + " no.notifMailPartageActiviteAutreUnite , no.notifMailCreaActiviteAutreUnite , no.notifMailModifActivitePartage,"
            + " no.notifMailSuppActivitePartage, no.utilisateur.id , no.typeActivite.id)"
            + " FROM Notification no ";

    String ORDER_BY = " ORDER by no.typeActivite.id DESC";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE no.utilisateur.id = :idUtilisateur " + ORDER_BY)
    List<NotificationDto> findDtoUsingIdUtilisateur(Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE no.utilisateur.id = :idUtilisateur " +
            " AND no.typeActivite.id = :idTypeActivite ")
    Optional<NotificationDto> findNotifUsingIdUtilisateurAndIdTypeActivite(Long idUtilisateur, Long idTypeActivite);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE no.id = :idNotification ")
    Optional<NotificationDto> findDtoUsingId(Long idNotification);
}
