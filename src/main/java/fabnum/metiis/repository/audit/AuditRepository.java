package fabnum.metiis.repository.audit;

import fabnum.metiis.dto.audit.CreatedUpdatedByDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Optional;

@Repository
public class AuditRepository {

    @PersistenceContext
    private EntityManager entityManager;

    private static final String NEW_DTO = "" +
            " new fabnum.metiis.dto.audit.CreatedUpdatedByDto( " +
            " t.createdDate , t.lastModifiedDate , t.createdBy , t.lastModifiedBy , " +
            " utc.id ,  utc.nom , utc.prenom , " +
            " utu.id ,  utu.nom , utu.prenom " +
            " ) ";

    public Optional<CreatedUpdatedByDto> getById(String table, Long id) {
        Query query = entityManager.createQuery("" +
                "SELECT " + NEW_DTO + " FROM " + table + " t " +
                " left join User uc ON t.createdBy = uc.username " +
                " left join Utilisateur utc ON uc.id = utc.user.id " +
                " left join User uu ON t.lastModifiedBy = uu.username " +
                " left join Utilisateur utu ON uu.id = utu.user.id " +
                "WHERE t.id = :id");

        query.setParameter("id", id);

        return Optional.ofNullable((CreatedUpdatedByDto) query.getSingleResult());

    }
}
