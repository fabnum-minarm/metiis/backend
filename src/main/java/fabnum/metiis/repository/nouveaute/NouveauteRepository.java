package fabnum.metiis.repository.nouveaute;

import fabnum.metiis.domain.nouveaute.Nouveaute;
import fabnum.metiis.dto.nouveaute.NouveauteDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface NouveauteRepository extends AbstractRepository<Nouveaute, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.nouveaute.NouveauteDto( " +
            " n.id , n.date , n.nom  " +
            " ) " +
            "" +
            " FROM Nouveaute n ";

    @Query(value = " SELECT " + NEW_DTO +
            " WHERE NOT EXISTS (" +
            "  SELECT nu.id " +
            "  FROM NouveauteUtilisateur nu " +
            "  WHERE nu.utilisateur.id = :idUtilisateur " +
            "  AND nu.nouveaute.id = n.id " +
            " )")
    List<NouveauteDto> findUsingIdUtilisateur(Long idUtilisateur);


    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO nouveauteutilisateur (idutilisateur , idnouveaute , lu) " +
                    " SELECT :idUtilisateur , n.id , true " +
                    " FROM nouveaute n " +
                    " WHERE NOT EXISTS (" +
                    "  SELECT nu.id " +
                    "  FROM nouveauteutilisateur nu " +
                    "  WHERE nu.idutilisateur = :idUtilisateur " +
                    "  AND nu.idnouveaute = n.id " +
                    " )")
    void readAll(Long idUtilisateur);
}
