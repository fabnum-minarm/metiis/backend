package fabnum.metiis.repository.disponibilite;

import fabnum.metiis.domain.disponibilite.Disponibilite;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;

public interface DisponibiliteRepository extends AbstractRepository<Disponibilite, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.disponibilite.DisponibiliteDto( d.id ," +
            " d.dateDebut , d.dateFin , d.commentaire , " +
            " ut.id , ut.nom , ut.prenom , ut.email , ut.telephone , " +
            " c.id , c.lettre , c.code , c.libelle , c.ordre , " +
            " td.id , td.code , td.libelle , td.estDisponible , " +
            " af.id , " +
            " pr.id , pr.code , pr.libelle , pr.ordre  " +
            " ) " +
            "" +
            " FROM Disponibilite d " +
            " JOIN d.utilisateur ut " +
            " JOIN ut.corps c " +
            " JOIN d.typeDisponibilite td " +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr ";

    String ORDER_BY = " ORDER BY d.dateDebut ";

    @Query("select d " +
            " from Disponibilite d " +
            " join d.utilisateur u " +
            " where u.id = :idUtilisateur " +
            " and d.dateDebut >=  :debut " +
            " and d.dateFin <= :fin " +
            ORDER_BY)
    List<Disponibilite> findListBeetween(Long idUtilisateur, Instant debut, Instant fin);

    @Query("select d " +
            " from Disponibilite d " +
            " join d.utilisateur u " +
            " JOIN d.typeDisponibilite td " +
            " where u.id = :idUtilisateur " +
            " and d.dateDebut >=  :debut " +
            " and d.dateFin <= :fin " +
            " and td.estDisponible = :estDisponible " +
            ORDER_BY)
    List<Disponibilite> findListEstDisponibleBeetween(Long idUtilisateur, Instant debut, Instant fin, Boolean estDisponible);

    @Query(" DELETE FROM Disponibilite d " +
            " where d.utilisateur.id = :idUtilisateur " +
            " and d.dateDebut >=  :debut " +
            " and d.dateFin <= :fin ")
    @Modifying
    void deleteListBeetween(Long idUtilisateur, Instant debut, Instant fin);


    @Query(value = "SELECT " + NEW_DTO +
            " where ut.id = :idUtilisateur " +
            " and d.dateDebut >=  :debut " +
            " and d.dateFin <= :fin " +
            ORDER_BY)
    List<DisponibiliteDto> findDtoListBeetweenUsingUtilisateur(Long idUtilisateur, Instant debut, Instant fin);

    @Query(value = "SELECT " + NEW_DTO +
            " where d.id in (:idsDisponibilite) " +
            ORDER_BY)
    List<DisponibiliteDto> findDtoListIn(List<Long> idsDisponibilite);


    @Query(value = "SELECT " + NEW_DTO +
            " where ut.id = :idUtilisateur " +
            ORDER_BY)
    List<DisponibiliteDto> findAllUsingIdUtilisateur(Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN ut.affectations af with af.dateFinAffectation is null " +
            " JOIN af.organisme o" +
            " where o.id = :idOrganisme " +
            " and d.dateDebut >=  :debut " +
            " and d.dateFin <= :fin " +
            " and (:estDisponible is null or td.estDisponible = :estDisponible) " +
            ORDER_BY)
    List<DisponibiliteDto> findDtoListEstDisponibleBeetweenUsingOrganisme(Long idOrganisme, Instant debut, Instant fin, Boolean estDisponible);

    @Query(value = "SELECT count(d) " +
            " from Disponibilite d " +
            " join d.typeDisponibilite td " +
            " where d.utilisateur.id = :idUtilisateur " +
            " and td.estDisponible = true " +
            " and d.dateDebut >=  :dateDebut " +
            " and d.dateFin <= :dateFin  ")
    Long countDisponibilite(Instant dateDebut, Instant dateFin, Long idUtilisateur);

    @Query(value = "SELECT count(d) " +
            " from Disponibilite d " +
            " join d.typeDisponibilite td " +
            " where d.utilisateur.id = :idUtilisateur " +
            " and td.estDisponible = false " +
            " and d.dateDebut >=  :dateDebut " +
            " and d.dateFin <= :dateFin  ")
    Long countIndisponibilite(Instant dateDebut, Instant dateFin, Long idUtilisateur);

    @Query("select d " +
            " from Disponibilite d " +
            " join d.utilisateur u " +
            " JOIN d.typeDisponibilite td " +
            " where u.id = :idUtilisateur " +
            " and d.dateDebut >=  :debut " +
            " and d.dateFin <= :fin " +
            ORDER_BY)
    List<Disponibilite> findListAllDisponibleBeetween(Long idUtilisateur, Instant debut, Instant fin);
}
