package fabnum.metiis.repository.disponibilite;

import fabnum.metiis.domain.disponibilite.TypeDisponibilite;
import fabnum.metiis.dto.disponibilite.TypeDisponibiliteDto;
import fabnum.metiis.repository.abstraction.AbstractRepositoryWithCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TypeDisponibiliteRepository extends AbstractRepositoryWithCode<TypeDisponibilite, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.disponibilite.TypeDisponibiliteDto( " +
            " td.id , td.code , td.libelle , td.estDisponible)" +
            " FROM TypeDisponibilite td ";

    @Query(value = "SELECT " + NEW_DTO)
    Page<TypeDisponibiliteDto> findDtoPageableAll(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO)
    List<TypeDisponibiliteDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE td.id = :id ")
    Optional<TypeDisponibiliteDto> findDtoUsingId(Long id);

    @Override
    @Query(value = "SELECT " + NEW_DTO +
            " WHERE td.code = :code ")
    Optional<TypeDisponibilite> findByCode(String code);
}
