package fabnum.metiis.repository.sondage;

import fabnum.metiis.domain.sondage.Sondage;
import fabnum.metiis.dto.sondage.ReponseSondageDto;
import fabnum.metiis.dto.sondage.SondageDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import fabnum.metiis.repository.rh.UtilisateurRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SondageRepository extends AbstractRepository<Sondage, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.sondage.SondageDto( " +
            " s.id , s.question " +
            " ) " +
            "" +
            " FROM Sondage s ";

    String NEW_DTO_REPONSE = " new fabnum.metiis.dto.sondage.ReponseSondageDto( " +
            UtilisateurRepository.CHAMP_UTILISATEUR_COMPLET +
            " , su.reponse, su.createdDate ) " +
            " FROM SondageUtilisateur su" +
            " LEFT JOIN su.utilisateur ut " +
            " JOIN ut.user u " +
            " LEFT JOIN u.userProfil up with up.selected = true " +
            " LEFT JOIN up.profil pf  " +
            " JOIN ut.corps c " +
            " LEFT JOIN ut.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr  " +
            " LEFT JOIN af.organisme o  " +
            " LEFT JOIN o.parent op ";

    @Query(value = "SELECT " + NEW_DTO + " where s.id = :id")
    SondageDto findDtoById(Long id);

    @Query(value = "SELECT s FROM Sondage s WHERE s.id = (SELECT max(s2.id) FROM Sondage s2 )")
    Sondage findDernierSondage();

    @Query(value = "SELECT " + NEW_DTO_REPONSE +
            " WHERE su.sondage.id = :idSondage " +
            " AND (UPPER(ut.nom) like %:filter% OR UPPER(ut.email) like %:filter%) " +
            " AND o.id IN :idsOrganisme" +
            " AND (:reponse ='' OR su.reponse=:reponse)")
    Page<ReponseSondageDto> findDtoDashboardAllSort(List<Long> idsOrganisme, Pageable pageable, String filter, Long idSondage, String reponse);

    @Query(value = "SELECT " + NEW_DTO_REPONSE +
            " WHERE su.sondage.id = :idSondage " +
            " AND (UPPER(ut.nom) like %:filter% OR UPPER(ut.email) like %:filter%) " +
            " AND (:reponse ='' OR su.reponse=:reponse)")
    Page<ReponseSondageDto> findDtoDashboardAllSort(Pageable pageable, String filter, Long idSondage, String reponse);

    @Query(value = "SELECT DISTINCT su.reponse " +
            " FROM  Sondage s " +
            " JOIN s.sondageUtilisateurs su" +
            " where s.id = :id")
    List<String> findDistictReponses(Long id);
}
