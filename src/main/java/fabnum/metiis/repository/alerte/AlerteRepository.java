package fabnum.metiis.repository.alerte;

import fabnum.metiis.domain.alerte.Alerte;
import fabnum.metiis.dto.alerte.AlerteDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface AlerteRepository extends AbstractRepository<Alerte, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.alerte.AlerteDto( "
            + "    al.id, al.dateAlerte, al.lu, al.titre, al.description, al.commentaire , al.partage , "
            + "    em.id, em.nom, em.prenom, "
            + "    de.id, de.nom, de.prenom, "
            + "    ac.id, ac.token, ac.nom, ac.description, ac.dateDebut, ac.dateFin, "
            + "    ta.id, ta.code, ta.libelle, ta.codeCss, ta.codeCssIcone, ta.ordre "
            + "    ) "
            + " FROM Alerte al "
            + " JOIN al.destinataire de "
            + " LEFT JOIN al.emetteur em "
            + " LEFT JOIN al.activite ac "
            + " LEFT JOIN al.typeActivite ta "; // On prend le typeActivite de ALERTE et pas celui de ACTIVITE !

    String ORDER_BY = " ORDER BY al.dateAlerte DESC , al.id";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE al.id = :id")
    Optional<AlerteDto> findDtoUsingId(Long id);

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    Page<AlerteDto> findDtoAll(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE al.destinataire.id = :idDestinataire " +
            ORDER_BY)
    List<AlerteDto> findDtoUsingIdDestinataire(Long idDestinataire, Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE al.destinataire.id = :idDestinataire " +
            ORDER_BY)
    List<AlerteDto> findDtoUsingIdDestinataire(Long idDestinataire);


    @Query(value = "SELECT count(al) " +
            " FROM Alerte al " +
            " WHERE al.lu = false " +
            " AND al.destinataire.id = :idDestinataire")
    Long alerteTotalNonLuUsingIdDestinataire(Long idDestinataire);

    Optional<Alerte> findByIdAndDestinataireId(Long id, Long idDestinataire);

    @Modifying
    @Query(value = "UPDATE Alerte al " +
            " SET al.activite = NULL " +
            " WHERE al.activite.id = :idActivite")
    void supprimerReferencesAUneActiviteDansLesAlertes(Long idActivite);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description , a.idutilisateur , ut.id , a.id , a.idtypeactivite " +
                    " FROM utilisateur ut " +
                    " JOIN users u on ut.iduser = u.id " +
                    " JOIN affectation af on ut.id = af.idutilisateur and af.datefinaffectation is null " +
                    " JOIN activite a on af.idorganisme = a.idorganisme " +
                    " JOIN notification n on ut.id = n.idutilisateur" +
                    " WHERE a.id = :idActivite " +
                    " AND u.enabled=true " +
                    " AND ut.id != a.idutilisateur " +
                    " AND n.notifappactivitecree=true" +
                    " AND n.idtypeactivite = a.idtypeactivite")
    void creerAlerteActiviteUnite(Long idActivite, String titre, String description);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , commentaire , idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description ,  :commentaire, :idEmetteur , ut.id , a.id , a.idtypeactivite " +
                    " FROM utilisateur ut " +
                    " JOIN participer p on ut.id = p.idutilisateur " +
                    " JOIN activite a on p.idactivite = a.id " +
                    " WHERE a.id = :idActivite " +
                    " AND (SELECT no.notifAppActiviteModif" +
                    "      FROM notification no" +
                    "      WHERE no.idutilisateur = ut.id" +
                    "      AND no.idtypeactivite = a.idtypeactivite ) = true ")
    void creerAlertActiviteModifierAllParticipant(Long idActivite, Long idEmetteur, String titre, String description, String commentaire);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , commentaire , idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description ,  :commentaire, :idEmetteur , ut.id , a.id , a.idtypeactivite " +
                    " FROM utilisateur ut " +
                    " JOIN participer p on ut.id = p.idutilisateur " +
                    " JOIN activite a on p.idactivite = a.id " +
                    " WHERE a.id = :idActivite ")
    void creerAlertActiviteSupprimerAllParticipant(Long idActivite, Long idEmetteur, String titre, String description, String commentaire);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , idemetteur , iddestinataire , idactivite , idtypeactivite, partage) " +
                    " SELECT now() , false , :titre, :description , :idEmetteur, :idDestinataire, a.id, a.idtypeactivite, 'true' " +
                    " FROM activite a " +
                    " WHERE a.id = :idActivite ")
    void creerAlertDemanderenfort(Long idActivite, Long idEmetteur, Long idDestinataire, String titre, String description);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description , :idEmetteur, :idDestinataire, a.id, a.idtypeactivite " +
                    " FROM activite a " +
                    " WHERE a.id = :idActivite ")
    void creerAlertCreationActivitePartage(Long idActivite, Long idEmetteur, Long idDestinataire, String titre, String description);

    @Modifying
    @Query(value = " DELETE FROM Alerte a " +
            " WHERE a.lu = true " +
            " AND a.dateAlerte < :date ")
    void deleteAlerteLuBeforeDate(Instant date);

    @Modifying
    @Query(value = " DELETE FROM Alerte al " +
            " WHERE al.destinataire.id = :idUtilisateur ")
    void deleteAlerteUtilisateur(Long idUtilisateur);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , commentaire, idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description , :commentaire, :idEmetteur, ut.id , a.id , a.idtypeactivite " +
                    " FROM activite a " +
                    " JOIN utilisateur ut on a.idutilisateur = ut.id " +
                    " WHERE a.id = :idActivite " +
                    " AND a.idutilisateur != :idEmetteur ")
    void creerAlertActiviteCreateur(Long idActivite, Long idEmetteur, String titre, String description, String commentaire);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , commentaire, idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description , :commentaire, :idEmetteur, :idDestinataire , a.id , a.idtypeactivite " +
                    " FROM activite a " +
                    " WHERE a.id = :idActivite ")
    void creerAlertSuppressionActivitePartage(Long idActivite, Long idEmetteur, Long idDestinataire, String titre, String description, String commentaire);

    @Modifying
    @Query(value = "UPDATE Alerte al " +
            " SET al.lu = true " +
            " WHERE al.destinataire.id = :idUtilisateur")
    void lireAlertes(Long idUtilisateur);

    @Query(value = " SELECT al " +
            " FROM Alerte al " +
            " WHERE al.activite.id = :idActivite " +
            " AND al.statut.id = :idStatut ")
    Optional<Alerte> findExistanteByActiviteAndStatut(Long idActivite, Long idStatut);

    @Query(value = " SELECT al " +
            " FROM Alerte al " +
            " WHERE al.activite.id = :idActivite " +
            " AND al.statut.id = :idStatut " +
            " AND al.emetteur.id = :idEmetteur ")
    Optional<Alerte> findExistanteByActiviteStatutAndEmetteur(Long idActivite, Long idStatut, Long idEmetteur);


    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description , a.idutilisateur , ut.id , a.id , a.idtypeactivite " +
                    " FROM utilisateur ut " +
                    " JOIN users u on ut.iduser = u.id " +
                    " JOIN affectation af on ut.id = af.idutilisateur and af.datefinaffectation is null " +
                    " JOIN activite a on af.idorganisme = a.idorganisme " +
                    " WHERE a.id = :idActivite " +
                    " AND u.enabled=true " +
                    " AND ut.id not in ( " +
                    "   SELECT p.idutilisateur " +
                    "   FROM participer p " +
                    "   WHERE p.idactivite = :idActivite " +
                    ")")
    void creerAlertRappelActivite(Long idActivite, String titre, String description);

    @Modifying
    @Query(nativeQuery = true,
            value = " INSERT INTO Alerte (dateAlerte , lu , titre , description , idemetteur , iddestinataire , idactivite , idtypeactivite) " +
                    " SELECT now() , false , :titre, :description , a.idutilisateur , ut.id , a.id , a.idtypeactivite " +
                    " FROM utilisateur ut " +
                    " JOIN affectation af on ut.id = af.idutilisateur and af.datefinaffectation is null " +
                    " JOIN activite a on af.idorganisme = a.idorganisme " +
                    " WHERE a.id = :idActivite " +
                    " AND ut.id in ( " +
                    "   SELECT p.idutilisateur " +
                    "   FROM participer p " +
                    "   JOIN avancementparticipation ap on p.idavancementparticipation = ap.id " +
                    "   JOIN choix ch on ap.idchoix = ch.id " +
                    "   WHERE p.idactivite = :idActivite " +
                    "   AND ch.code='convoquer' " +
                    ")")
    void creerAlertRappelParticipationActivite(Long idActivite, String titre, String description);
}
