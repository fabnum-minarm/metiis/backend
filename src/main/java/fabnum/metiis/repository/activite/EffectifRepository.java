package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.Effectif;
import fabnum.metiis.dto.activite.EffectifDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EffectifRepository extends AbstractRepository<Effectif, Long> {


    String NEW_DTO = " new fabnum.metiis.dto.activite.EffectifDto( " +
            " e.id , e.nombre , " +
            " a.id , " +
            " c.id , c.lettre , c.code , c.libelle , c.ordre " +
            " ) " +
            "" +
            " FROM Effectif e " +
            " JOIN e.activite a " +
            " JOIN e.corps c ";

    String ORDER_BY = " ORDER BY c.ordre asc ";

    @Query(value = " SELECT " + NEW_DTO +
            " WHERE a.id in :idsActivite " +
            ORDER_BY)
    List<EffectifDto> findAllUsedActiviteIdIn(List<Long> idsActivite);

    @Query(value = " SELECT " + NEW_DTO +
            " WHERE a.id = :idActivite " +
            ORDER_BY)
    List<EffectifDto> findAllUsedActiviteId(Long idActivite);


}
