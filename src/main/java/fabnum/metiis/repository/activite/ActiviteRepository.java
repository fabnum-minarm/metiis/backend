package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.dto.activite.ActiviteCorpsStatutCountDto;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.ActiviteSimpleDto;
import fabnum.metiis.dto.activite.RechercheParticipantDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import fabnum.metiis.repository.rh.UtilisateurRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface ActiviteRepository extends AbstractRepository<Activite, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.activite.ActiviteDto( " +
            " a.id , a.token, a.nom , a.description , a.dateDebut , a.dateFin ," +
            " a.envoyerMailCreation ," +
            " a.rappelNbJourAvantDebut , a.dateEnvoieRappel ," +
            " a.rappelInApp , a.rappelEmail ," +

            " a.rappelParticipationNbJourAvantDebut , a.dateEnvoieRappelParticipation ," +
            " a.rappelParticipationInApp , a.rappelParticipationEmail ," +

            " ta.id , ta.code , ta.libelle , ta.codeCss , ta.codeCssIcone , ta.ordre , " +
            " u.id ," +
            " o.id , o.code ,  o.libelle , " +
            " a.parent.id) " +
            "" +
            " FROM Activite a " +
            " LEFT JOIN a.createur u " +
            " JOIN a.organisme o " +
            " JOIN a.typeActivite ta " +
            " JOIN a.organisme o";

    String NEW_DTO_FROM_PARENT = " new fabnum.metiis.dto.activite.ActiviteDto( " +
            " a.id , a.token, a.nom , a.description , a.dateDebut , a.dateFin ," +
            " ta.id , ta.code , ta.libelle , ta.codeCss , ta.codeCssIcone, ta.ordre , " +
            " o.id , o.code ,  o.libelle)" +
            " FROM Activite a " +
            " JOIN a.organisme o " +
            " JOIN a.typeActivite ta";

    String NEW_DTO_SIMPLE = " new fabnum.metiis.dto.activite.ActiviteSimpleDto( " +
            " a.id , a.nom , a.description , a.dateDebut , a.dateFin , a.parent.id , " +
            " ta.id , ta.code , ta.libelle , ta.codeCss , ta.codeCssIcone , ta.ordre ) " +
            "" +
            " FROM Activite a " +
            " JOIN a.typeActivite ta " +
            " JOIN a.organisme o";

    String ORDER_BY = " ORDER BY a.dateDebut, a.id ";

    String NEW_DTO_COUNT_PARTICIPANT = " new fabnum.metiis.dto.activite.ActiviteCorpsStatutCountDto( " +
            " a.id , " +
            " c.id , " +
            " s.id ," +
            " count(ut.id)  " +
            " ) " +
            "" +
            " FROM Activite a " +
            " JOIN a.participers p " +
            " JOIN p.avancementParticipations ap with ap.estRealiser = true " +
            " JOIN ap.choix ch " +
            " JOIN ch.statut s with s.forCompletion = true " +
            " JOIN p.utilisateur ut " +
            " JOIN ut.corps c ";

    String GROUP_BY_COUNT_PARTICIPANT = " GROUP BY a.id , c.id , s.id  ";

    String NEW_DTO_RECHERCHE_PARTICIPANT = " new fabnum.metiis.dto.activite.RechercheParticipantDto( " +
            " a.id , " +
            " p.id , " +
            " c.id , c.code , c.ordre , " +
            " ut.id , ut.nom , ut.prenom , " +
            " s.id , s.code , s.nom , s.nomSingulier , s.action, " +
            " e.id , " +
            " af.id , " +
            " pr.id , pr.code ,  pr.ordre , " +
            " count(d.id)  " +
            " ) " +
            "" +
            " FROM Activite a " +
            " JOIN a.organisme o " +
            " JOIN o.affectations af with af.dateFinAffectation is null " +
            " LEFT JOIN af.posteRo pr" +
            " JOIN af.utilisateur ut " +
            " JOIN ut.user us " +
            " JOIN us.userProfil up " +
            " JOIN up.profil pf " +
            " JOIN ut.corps c " +
            " LEFT JOIN ut.participers p with p.activite.id = a.id " +
            " LEFT JOIN p.dernierAvancementParticipation ap " +
            " LEFT JOIN ap.etape e " +
            " LEFT JOIN ap.choix ch " +
            " LEFT JOIN ch.statut s " +
            " LEFT JOIN ut.disponibilites d with d.dateDebut >= :debut AND d.dateFin <= :fin " +
            " LEFT JOIN d.typeDisponibilite td with td.estDisponible is true " +
            " WHERE NOT EXISTS ( " +
            "  SELECT ut2.id " +
            "  FROM Participer p2 " +
            "  JOIN p2.dernierAvancementParticipation ap2" +
            "  JOIN ap2.choix c2 " +
            "  JOIN c2.typeDisponibilite td2 with td2.estDisponible is false " +
            "  JOIN p2.activite a2 with a2.id != a.id AND a2.dateDebut <= a.dateFin AND a2.dateFin >= a.dateDebut " +
            "  JOIN p2.utilisateur ut2 with ut2.id = ut.id " +
            " ) ";

    String GROUP_BY_ORDER_BY_RECHERCHE_PARTICIPANT = "" +
            " GROUP BY a.id , p.id , c.id , s.id , ut.id , e.id , pr.id , af.id " +
            " ORDER BY pr.ordre , c.ordre , ut.nom , ut.prenom , ut.id ";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            " AND a.dateDebut <= :fin " +
            " AND a.dateFin >= :debut " +
            " AND (:filtre is null or lower(trim(a.nom)) LIKE %:filtre%) " +
            ORDER_BY)
    List<ActiviteDto> findDtoListBeetween(Long idOrganisme, Instant debut, Instant fin, Optional<String> filtre);

    @Query(value = "SELECT " + NEW_DTO_SIMPLE +
            " WHERE o.id = :idOrganisme " +
            " AND a.dateDebut <= :fin " +
            " AND a.dateFin >= :debut " +
            ORDER_BY)
    List<ActiviteSimpleDto> findDtoSimpleListBeetween(Long idOrganisme, Instant debut, Instant fin);

    @Query(value = "SELECT " + NEW_DTO_SIMPLE +
            " WHERE o.id = :idOrganisme " +
            " AND a.dateDebut <= :fin " +
            " AND a.dateFin >= :debut " +
            " AND ta.id IN :idsTypeActivite" +
            ORDER_BY)
    List<ActiviteSimpleDto> findDtoSimpleListBeetween(Long idOrganisme, Instant debut, Instant fin, List<Long> idsTypeActivite);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            " AND a.dateDebut <=  :fin " +
            " AND a.dateFin >= :debut " +
            " AND ta.id IN :idsTypeActivite" +
            " AND (:filtre is null or lower(trim(a.nom)) LIKE %:filtre%) " +
            ORDER_BY)
    List<ActiviteDto> findDtoListBeetween(Long idOrganisme, Instant debut, Instant fin, List<Long> idsTypeActivite, Optional<String> filtre);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE a.id = :idActivite ")
    Optional<ActiviteDto> findDtoUsingId(Long idActivite);

    @Query(value = "SELECT " + NEW_DTO_FROM_PARENT +
            " WHERE a.token = :token ")
    Optional<ActiviteDto> findDtoUsingIdFromParent(String token);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE a.parent.id = :idActiviteParent " +
            " AND o.id = :idOrganisme")
    Optional<ActiviteDto> findDtoUsingOrganismeAndActiviteParent(Long idOrganisme, Long idActiviteParent);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            ORDER_BY)
    Page<ActiviteDto> findDtoAllUsingIdOrganisme(Long idOrganisme, Pageable page);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE a.parent.id = :idActivite " +
            ORDER_BY)
    List<ActiviteDto> findDtoAllEnfantsUsingId(Long idActivite);

    @Query(value = "SELECT a from Activite a" +
            " WHERE a.parent.id = :idActivite ")
    List<Activite> findAllEnfantsUsingId(Long idActivite);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE o.id = :idOrganisme " +
            " and ta.id in :idsTypeActivite" +
            ORDER_BY)
    Page<ActiviteDto> findDtoAllUsingIdOrganisme(Long idOrganisme, List<Long> idsTypeActivite, Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO_COUNT_PARTICIPANT +
            " WHERE a.id = :idActivite " +
            GROUP_BY_COUNT_PARTICIPANT)
    List<ActiviteCorpsStatutCountDto> compterParticipantParStatutCompletion(Long idActivite);

    @Query(value = "SELECT " + NEW_DTO_COUNT_PARTICIPANT +
            " WHERE a.id in (:idsActivite) " +
            GROUP_BY_COUNT_PARTICIPANT)
    List<ActiviteCorpsStatutCountDto> compterParticipantParStatutCompletion(List<Long> idsActivite);

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    Page<ActiviteDto> findDtoAll(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO_RECHERCHE_PARTICIPANT +
            " AND  " +
            UtilisateurRepository.SEARCH_UTILISATEUR +
            " AND a.id = :idActivite " +
            " AND us.enabled = true " +
            " AND pf.code IN ('employe', 'super_employe') " +
            GROUP_BY_ORDER_BY_RECHERCHE_PARTICIPANT)
    List<RechercheParticipantDto> rechercherParticipants(Long idActivite, String search, Instant debut, Instant fin);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN a.participers p " +
            " WHERE o.id = :idOrganisme " +
            " AND p.utilisateur.id = :idUtilisateur " +
            ORDER_BY)
    Page<ActiviteDto> findDtoActiviteWithParticipation(Long idUtilisateur, Long idOrganisme, Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN a.participers p " +
            " WHERE p.utilisateur.id = :idUtilisateur " +
            " AND a.dateDebut <= :fin " +
            " AND a.dateFin >= :debut " +
            ORDER_BY)
    Page<ActiviteDto> findDtoActiviteWithParticipationEntre(Long idUtilisateur, Instant debut, Instant fin, Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN a.participers p " +
            " WHERE o.id = :idOrganisme " +
            " AND p.utilisateur.id = :idUtilisateur " +
            " AND a.id <> :idActivite " +
            " AND a.dateDebut <= :fin " +
            " AND a.dateFin >= :debut " +
            ORDER_BY)
    List<ActiviteDto> findDtoActiviteWithParticipationAutreActivite(Long idUtilisateur, Long idOrganisme, Long idActivite, Instant debut, Instant fin);

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN a.participers p " +
            " JOIN p.dernierAvancementParticipation dap " +
            " JOIN dap.choix ch " +
            " JOIN ch.typeDisponibilite td " +
            " WHERE o.id = :idOrganisme " +
            " AND p.utilisateur.id = :idUtilisateur " +
            " AND td.estDisponible = false " +
            " AND a.dateDebut <=  :fin " +
            " AND a.dateFin >= :debut " +
            ORDER_BY)
    List<ActiviteDto> findDtoActiviteWithUtilisateurParticipeNonDispo(Long idUtilisateur, Long idOrganisme, Instant debut, Instant fin);

    @Query(value = "SELECT a FROM Activite a " +
            " where a.dateEnvoieRappel = :date ")
    List<Activite> findByDateRappel(Instant date);

    @Query(value = "SELECT o.id " +
            " FROM Activite a " +
            " JOIN a.organisme o " +
            " where a.id = :idActivite ")
    Long getIdOrganisme(Long idActivite);

    @Query(value = "select count (a) from Activite  a " +
            " join a.typeActivite ta " +
            " join a.participers p " +
            " join p.avancementParticipations ap " +
            " join ap.choix c " +
            " where p.utilisateur.id = :idUtilisateur " +
            " and a.dateDebut <=  :fin " +
            " and a.dateFin >= :debut " +
            " and c.code = 'activite-realisee' " +
            " and ta.id = :idTypeActivite ")
    Long compterActiviteRealise(Instant debut, Instant fin, Long idUtilisateur, Long idTypeActivite);

    @Query(value = "select count (a) from Activite  a " +
            " join a.typeActivite ta " +
            " join a.participers p " +
            " join p.avancementParticipations ap " +
            " join ap.choix c " +
            " where p.utilisateur.id = :idUtilisateur " +
            " and a.dateDebut <=  :fin " +
            " and a.dateFin >= :debut " +
            " and (c.code = 'convoquer' OR c.code = 'annuler-non-convoques') " +
            " and ta.id = :idTypeActivite ")
    Long compterActiviteConvoque(Instant debut, Instant fin, Long idUtilisateur, Long idTypeActivite);

    @Query(value = "select count (a) from Activite  a " +
            " join a.participers p " +
            " join p.avancementParticipations ap " +
            " join ap.choix c " +
            " where p.utilisateur.id = :idUtilisateur " +
            " and a.dateDebut <=  :fin " +
            " and a.dateFin >= :debut " +
            " and c.code = 'candidater' ")
    Long compterCandidature(Instant debut, Instant fin, Long idUtilisateur);

    @Query(value = "select count (a) from Activite  a " +
            " join a.participers p " +
            " join p.avancementParticipations ap " +
            " join ap.choix c " +
            " where p.utilisateur.id = :idUtilisateur " +
            " and a.dateDebut <=  :fin " +
            " and a.dateFin >= :debut " +
            " and (c.code = 'convoquer' OR c.code = 'annuler-non-convoques') ")
    Long compterConvoque(Instant debut, Instant fin, Long idUtilisateur);

    @Query(value = "select " + NEW_DTO +
            " join a.participers p " +
            " join p.avancementParticipations ap " +
            " join ap.choix c " +
            " where p.utilisateur.id = :idUtilisateur " +
            " and a.dateDebut <=  :fin " +
            " and a.dateFin >= :debut " +
            " and c.code = 'activite-realisee' ")
    List<ActiviteDto> findDTOAllActiviteRealise(Instant debut, Instant fin, Long idUtilisateur);

    @Query(value = "select  " + NEW_DTO +
            " join a.participers p " +
            " join p.avancementParticipations ap " +
            " join ap.choix c " +
            " where p.utilisateur.id = :idUtilisateur " +
            " and (c.code = 'convoquer' OR c.code = 'annuler-non-convoques') " +
            " and a.typeActivite.id = :idTypeActivite " +
            " and a.dateDebut = " +
            " (select max(a2.dateDebut) from Activite a2" +
            " join a2.participers p2 " +
            " join p2.avancementParticipations ap2  " +
            " join ap2.choix c2 " +
            " where p2.utilisateur.id = :idUtilisateur " +
            " and (c2.code = 'convoquer' OR c2.code = 'annuler-non-convoques') " +
            " and a2.typeActivite.id = :idTypeActivite )")
    ActiviteDto findLastActivite(Long idUtilisateur, Long idTypeActivite);

    @Query(value = "select " + NEW_DTO +
            " join a.participers p " +
            " join p.avancementParticipations ap " +
            " join ap.choix c " +
            " where p.utilisateur.id = :idUtilisateur " +
            " and a.dateDebut <=  :fin " +
            " and a.dateFin >= :debut " +
            " and (c.code = 'convoquer' OR c.code = 'annuler-non-convoques') ")
    List<ActiviteDto> findDTOAllActiviteConvoque(Instant debut, Instant fin, Long idUtilisateur);


}
