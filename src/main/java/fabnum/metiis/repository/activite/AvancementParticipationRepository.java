package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.AvancementParticipation;
import fabnum.metiis.dto.activite.AvancementParticipationDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AvancementParticipationRepository extends AbstractRepository<AvancementParticipation, Long> {


    String ETAPE_FIELD = " e.id , e.code , e.nom , e.nomPluriel, e.ordre , e.description , e.descriptionPluriel";

    String NEW_DTO = " new fabnum.metiis.dto.activite.AvancementParticipationDto( " +
            " ap.id , ap.commentaire , ap.dateValidation , ap.estRealiser ,  " +
            " p.id , " +
            ETAPE_FIELD + " , " +

            ChoixRepository.CHOIX_FIELD + " , " +
            " s.id , s.code , s.nom , s.nomSingulier , s.action , " +
            " sd.id , sd.code , sd.disponibilite , " +

            " tec.id , tec.code , tec.nom  , tec.finAvancement ,  " +

            " ut.id , ut.nom , ut.prenom , " +
            " c.id , c.lettre , c.code , c.libelle , c.ordre " +

            " ) " +
            "" +
            " FROM AvancementParticipation ap " +
            " JOIN ap.participer p " +
            " JOIN ap.etape e " +

            " LEFT JOIN ap.choix ch " +
            " LEFT JOIN ch.statut s " +
            " LEFT JOIN s.statutDisponibilite sd " +
            " LEFT JOIN ch.typeEtatChoix tec " +

            " LEFT JOIN ap.utilisateurValidateur ut " +
            " LEFT JOIN ut.corps c ";


    String NEW_DTO_VIDE = " new fabnum.metiis.dto.activite.AvancementParticipationDto( " +
            ETAPE_FIELD +
            " ) " +
            "" +
            " FROM Etape e ";

    String ORDER_BY = " ORDER BY e.ordre asc";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE p.id = :idParticiper " +
            " AND ap.dateAnnulation is null " +
            ORDER_BY)
    List<AvancementParticipationDto> findAllDtoUsingIdParticiper(Long idParticiper);

    @Modifying
    @Query(value = "UPDATE AvancementParticipation set estRealiser = true " +
            " where id in ( :idAvancementARealiser ) ")
    void updateAvancementRealiserByIds(List<Long> idAvancementARealiser);

    @Query(value = "SELECT " + NEW_DTO_VIDE +
            ORDER_BY)
    List<AvancementParticipationDto> findAllDtoUsingIdParticiperVide();

    @Query(" DELETE FROM AvancementParticipation ap " +
            " where ap.participer.id = :idParticiper ")
    @Modifying
    void deleteByIdParticiper(Long idParticiper);
}
