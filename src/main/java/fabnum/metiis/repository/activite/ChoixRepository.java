package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.Choix;
import fabnum.metiis.repository.abstraction.AbstractRepository;

public interface ChoixRepository extends AbstractRepository<Choix, Long> {

    String CHOIX_FIELD = " ch.id , ch.code , ch.nom , ch.lettre ,  ch.actionPrincipale , ch.actionSuppression , ch.canActiviteWithOtherActivite ";


}
