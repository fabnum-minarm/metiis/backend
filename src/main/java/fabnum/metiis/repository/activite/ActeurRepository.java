package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.Acteur;
import fabnum.metiis.repository.abstraction.AbstractRepository;

public interface ActeurRepository extends AbstractRepository<Acteur, Long> {


}
