package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.ChoixEtapeAction;
import fabnum.metiis.dto.activite.ChoixEtapeActionDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChoixEtapeActionRepository extends AbstractRepository<ChoixEtapeAction, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.activite.ChoixEtapeActionDto( " +
            " cea.id ," +
            " e.id , e.code , e.nom , e.nomPluriel, e.ordre , e.description , e.descriptionPluriel , " +
            ChoixRepository.CHOIX_FIELD + " , " +
            " td.id , td.code , td.libelle , td.estDisponible , " +
            " er.id , er.code  , er.nom , er.nomPluriel, er.ordre , er.description , er.descriptionPluriel , " +
            " es.id , es.code , es.nom , es.nomPluriel , es.ordre , es.description , es.descriptionPluriel  " +
            " ) " +
            "" +
            " FROM ChoixEtapeAction cea " +
            " JOIN cea.etape e " +
            " JOIN cea.choix ch " +
            " JOIN ch.typeDisponibilite td " +
            " JOIN cea.etapeARemplir er " +
            " LEFT JOIN cea.etapeSuivante es ";

    String ORDER_BY = " ORDER BY e.ordre asc";

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    List<ChoixEtapeActionDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN ch.acteur ac " +
            " WHERE e.id = :idEtape " +
            " AND ac.participant = true " +
            ORDER_BY)
    List<ChoixEtapeActionDto> findMyAction(Long idEtape);
}
