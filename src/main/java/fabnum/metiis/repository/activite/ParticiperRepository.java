package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.Participer;
import fabnum.metiis.dto.activite.IndisponibiliteActiviteDto;
import fabnum.metiis.dto.activite.ParticiperDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface ParticiperRepository extends AbstractRepository<Participer, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.activite.ParticiperDto( " +
            " p.id , p.dateInscription , " +
            " a.id , " +
            " ut.id , ut.nom , ut.prenom , " +
            " c.id , c.lettre , c.code , c.libelle , c.ordre , " +
            " dap.id , dap.commentaire , dap.dateValidation , dap.estRealiser , " +
            ChoixRepository.CHOIX_FIELD + " , " +
            " st.id , st.code , st.nomSingulier , " +
            " td.id , td.code , td.libelle , td.estDisponible , " +
            " e.id , e.code , e.nom , e.nomPluriel, e.ordre , e.description , e.descriptionPluriel   " +
            " )" +
            " FROM Participer p " +
            " JOIN p.activite a " +
            " JOIN p.utilisateur ut " +
            " JOIN ut.corps c " +
            " LEFT JOIN p.dernierAvancementParticipation dap " +
            " LEFT JOIN dap.choix ch " +
            " LEFT JOIN ch.statut st " +
            " LEFT JOIN ch.typeDisponibilite td " +
            " LEFT JOIN dap.etape e ";

    String NEW_INDISPO_ACTIVITE_DTO = " new fabnum.metiis.dto.activite.IndisponibiliteActiviteDto( " +
            " a.id , a.dateDebut , a.dateFin , " +
            " p.id , " +
            " ut.id " +
            " ) " +
            " FROM Activite a " +
            " JOIN a.organisme o " +
            " JOIN a.participers p " +
            " JOIN p.utilisateur ut " +
            " JOIN p.dernierAvancementParticipation dap " +
            " JOIN dap.choix ch" +
            " JOIN ch.typeDisponibilite td" +
            " WHERE td.estDisponible is false ";


    Optional<Participer> findByActiviteIdAndUtilisateurId(Long idActivite, Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE p.id = :idParticiper ")
    Optional<ParticiperDto> findDtoUsingId(Long idParticiper);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE a.id = :idActivite " +
            " AND ut.id = :idUtilisateur ")
    Optional<ParticiperDto> findDtoUsingIdActiviteAndIdUtilisateur(Long idActivite, Long idUtilisateur);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE a.id in :idsActivites " +
            " AND ut.id = :idUtilisateur ")
    List<ParticiperDto> findDtoUsingIdsActivitesAndIdUtilisateur(List<Long> idsActivites, Long idUtilisateur);

    @Query(value = "SELECT " + NEW_INDISPO_ACTIVITE_DTO +
            " AND o.id = :idOrganisme " +
            " AND a.dateDebut <= :fin " +
            " AND a.dateFin >= :debut ")
    List<IndisponibiliteActiviteDto> getIndisponibliteActivite(Long idOrganisme, Instant debut, Instant fin);

    @Query(value = "SELECT " + NEW_INDISPO_ACTIVITE_DTO +
            " AND ut.id = :idUtilisateur " +
            " AND a.dateDebut <= :fin " +
            " AND a.dateFin >= :debut " +
            " AND a.id != :idActiviteExclus ")
    List<IndisponibiliteActiviteDto> getIndisponibliteActiviteUtilisateur(Long idUtilisateur, Long idActiviteExclus, Instant debut, Instant fin);
}
