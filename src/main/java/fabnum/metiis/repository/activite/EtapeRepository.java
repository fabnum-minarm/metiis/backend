package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.Etape;
import fabnum.metiis.dto.activite.EtapeDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EtapeRepository extends AbstractRepository<Etape, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.activite.EtapeDto( " +
            " e.id , e.code , e.nom , e.nomPluriel , e.ordre , e.description , e.descriptionPluriel " +
            " ) " +
            " FROM Etape e ";

    String ORDER_BY = " ORDER BY e.ordre asc";

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    List<EtapeDto> findDtoAll();


    @Query(value = "SELECT " + NEW_DTO +
            " WHERE e.id = :idEtape ")
    EtapeDto findDtoById(Long idEtape);
}
