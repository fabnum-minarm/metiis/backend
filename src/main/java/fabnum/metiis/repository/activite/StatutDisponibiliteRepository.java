package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.StatutDisponibilite;
import fabnum.metiis.repository.abstraction.AbstractRepository;

public interface StatutDisponibiliteRepository extends AbstractRepository<StatutDisponibilite, Long> {


}
