package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.Statut;
import fabnum.metiis.dto.activite.StatutDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface StatutRepository extends AbstractRepository<Statut, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.activite.StatutDto( " +
            " s.id , s.code , s.nom  , s.nomSingulier , s.action ,   " +
            " sd.id , sd.code , sd.disponibilite " +
            " ) " +
            " FROM Statut s " +
            " LEFT JOIN s.statutDisponibilite sd ";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE s.id = :idStatut ")
    Optional<StatutDto> findDtoUsingId(Long idStatut);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE s.forCompletion = true ")
    Optional<StatutDto> findDtoForCompletion();

    @Query(value = "SELECT " + NEW_DTO +
            " JOIN s.etapeStatuts es" +
            " JOIN es.etape e " +
            " WHERE s.forDetails = true " +
            " ORDER BY e.ordre ")
    List<StatutDto> findDtoForDetails();
}
