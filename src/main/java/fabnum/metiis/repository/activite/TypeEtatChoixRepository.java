package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.TypeEtatChoix;
import fabnum.metiis.repository.abstraction.AbstractRepository;

public interface TypeEtatChoixRepository extends AbstractRepository<TypeEtatChoix, Long> {


}
