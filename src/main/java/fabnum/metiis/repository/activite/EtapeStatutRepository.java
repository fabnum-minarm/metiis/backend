package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.EtapeStatut;
import fabnum.metiis.dto.activite.EtapeStatutDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface EtapeStatutRepository extends AbstractRepository<EtapeStatut, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.activite.EtapeStatutDto( " +
            " es.id , es.ordreDansGroupe, " +
            " e.id , " +
            " s.id , s.code , s.nom , s.nomSingulier , s.action , " +
            " sd.id , sd.code , sd.disponibilite" +
            ") " +
            " FROM EtapeStatut es " +
            " JOIN es.etape e " +
            " JOIN es.statut s " +
            " LEFT JOIN s.statutDisponibilite sd";

    String ORDER_BY = " ORDER BY e.ordre ASC, es.ordreDansGroupe ASC";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE :forVisu is null or s.forVisu = :forVisu " +
            ORDER_BY)
    List<EtapeStatutDto> findDtoAll(Optional<Boolean> forVisu);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE e.id = :idEtape" +
            " AND :forVisu is null or s.forVisu = :forVisu " +
            ORDER_BY)
    List<EtapeStatutDto> findDtoAllUsingIdEtape(Long idEtape, Optional<Boolean> forVisu);


    @Query(value = "SELECT " + NEW_DTO +
            " WHERE s.statutDisponibilite.id is not null " +
            " AND :forVisu is null or s.forVisu = :forVisu " +
            ORDER_BY)
    List<EtapeStatutDto> findDtoWithStatutDisponible(Optional<Boolean> forVisu);
}
