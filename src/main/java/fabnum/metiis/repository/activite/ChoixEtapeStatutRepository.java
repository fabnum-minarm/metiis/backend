package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.ChoixEtapeStatut;
import fabnum.metiis.dto.activite.ChoixEtapeStatutDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ChoixEtapeStatutRepository extends AbstractRepository<ChoixEtapeStatut, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.activite.ChoixEtapeStatutDto( " +
            " ces.id , ces.ordreDansGroupe,    " +
            " es.id , " +
            ChoixRepository.CHOIX_FIELD + " , " +
            " td.id , td.code , td.libelle , td.estDisponible " +
            ") " +
            " FROM ChoixEtapeStatut ces " +
            " JOIN ces.etapeStatut es " +
            " JOIN es.etape e " +
            " JOIN ces.choix ch " +
            " JOIN ch.typeDisponibilite td ";

    String ORDER_BY = " ORDER BY e.ordre , es.ordreDansGroupe ASC, ces.ordreDansGroupe asc";

    @Query(value = "SELECT " + NEW_DTO +
            ORDER_BY)
    List<ChoixEtapeStatutDto> findDtoAll();
}
