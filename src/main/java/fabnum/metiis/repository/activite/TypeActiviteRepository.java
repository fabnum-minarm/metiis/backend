package fabnum.metiis.repository.activite;

import fabnum.metiis.domain.activite.TypeActivite;
import fabnum.metiis.dto.activite.TypeActiviteDto;
import fabnum.metiis.repository.abstraction.AbstractRepositoryWithCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TypeActiviteRepository extends AbstractRepositoryWithCode<TypeActivite, Long> {
    String NEW_DTO = " new fabnum.metiis.dto.activite.TypeActiviteDto( " +
            " ta.id , ta.code , ta.libelle , ta.codeCss , ta.codeCssIcone , ta.ordre  )" +
            " FROM TypeActivite ta ";

    String ORDER_BY = " ORDER by ta.ordre ";


    @Override
    @Query(value = "SELECT ta FROM TypeActivite ta" + ORDER_BY)
    List<TypeActivite> findAll();

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    List<TypeActiviteDto> findDtoAllOrder();

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE ta.id = :idTypeActivite ")
    Optional<TypeActiviteDto> findDtoUsingId(Long idTypeActivite);

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    List<TypeActiviteDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    Page<TypeActiviteDto> findDtoAll(Pageable pageable);

    @Query(value = "SELECT ta FROM TypeActivite ta " + ORDER_BY)
    List<TypeActivite> findAllOrder();
}
