package fabnum.metiis.repository.administration;

import fabnum.metiis.domain.administration.UserProfil;
import fabnum.metiis.dto.administration.UserProfilDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserProfilRepository extends AbstractRepository<UserProfil, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.administration.UserProfilDto( " +
            " up.id ,  up.selected , " +
            " u.id , " +
            " ut.id , " +
            " p.id , p.code , p.description , p.isdefault , " +
            " to.id, to.code, to.libelle " +
            " ) " +
            " FROM Utilisateur ut " +
            " JOIN ut.user u " +
            " JOIN u.userProfil up " +
            " JOIN up.profil p " +
            " LEFT JOIN p.typeOrganisme to";

    String ORDER_BY = "ORDER BY p.code";

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE ut.id = :idUtilisateur " +
            ORDER_BY)
    List<UserProfilDto> findDtoAllUsingIdUtilisateur(Long idUtilisateur);

    @Query(value = "SELECT up " +
            " From Utilisateur ut " +
            " JOIN ut.user u " +
            " JOIN u.userProfil up " +
            " JOIN FETCH up.profil p" +
            " WHERE ut.id = :idUtilisateur ")
    List<UserProfil> findAllUsingIdUtilisateur(Long idUtilisateur);

    @Modifying
    void deleteAllByUserId(Long idUser);
}
