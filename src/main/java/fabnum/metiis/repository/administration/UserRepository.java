package fabnum.metiis.repository.administration;

import fabnum.metiis.domain.administration.User;
import fabnum.metiis.dto.administration.UserCompteActifDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * Classe permettant de gérer la persistance des users.
 */
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    /**
     * Recherche d'un user par son username.
     *
     * @param username {@link String}
     * @return Optional
     */
    @Query("Select u" +
            " FROM User u" +
            " JOIN FETCH u.userProfil up" +
            " JOIN FETCH up.profil p" +
            " JOIN FETCH p.roles r " +
            " WHERE LOWER(u.username) = LOWER(:username)")
    Optional<User> findByUsername(String username);

    /**
     * Permet de savoir si des user sont liés à un profil via son identifiant.
     *
     * @param id id {@link Long} : identifiant du profil.
     * @return List
     */
    boolean existsUsersByUserProfilProfilId(Long id);

    /**
     * Permet de rechercher les user liés à un profil via son identifiant.
     *
     * @param id {@link Long} : identifiant du profil.
     * @return List
     */
    List<User> findByUserProfilProfilId(Long id);

    /**
     * Permet de rechercher si un user existe pour un autre identifiant et pour le même code.
     *
     * @param id       {@link Long} : identifiant du user à ne pas prendre en compte.
     * @param username {@link String} : username à rechercher.
     * @return boolean
     */
    boolean existsByIdNotAndUsernameIgnoreCase(Long id, String username);

    /**
     * Récupération d'un utilisateur avec ses préférences chargées.
     *
     * @param username {@link String} : username du user à récupérer.
     * @return Optional
     */
    @EntityGraph(attributePaths = {"preferences"})
    Optional<User> getByUsernameIgnoreCase(String username);

    @Query(" SELECT u " +
            " FROM Utilisateur ut " +
            " JOIN ut.user u " +
            " WHERE ut.id = :idUtilisateur")
    Optional<User> findByIdUtilisateur(Long idUtilisateur);

    String DATE_ACTIF = " CASE WHEN (u.lastModifiedUnlock is null) " +
            " THEN u.lastModifiedEnabled ELSE" +
            " CASE WHEN (u.lastModifiedEnabled > u.lastModifiedUnlock) THEN u.lastModifiedEnabled ELSE u.lastModifiedUnlock END END";

    @Query(value = " SELECT new fabnum.metiis.dto.administration.UserCompteActifDto( " +
            " u.username , u.lastname, u.firstname , u.email," + DATE_ACTIF +
            " )" +
            " FROM User u" +
            " WHERE u.enabled=true" +
            " AND u.accountNonLocked=true" +
            " AND (u.lastModifiedEnabled is not null OR u.lastModifiedUnlock is not null) " +
            " ORDER BY " + DATE_ACTIF + " DESC ")
    Page<UserCompteActifDto> findComptesActifs(Pageable pageable);
}
