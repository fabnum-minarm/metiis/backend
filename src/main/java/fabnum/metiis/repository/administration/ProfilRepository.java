package fabnum.metiis.repository.administration;

import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.repository.abstraction.AbstractRepositoryWithCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Classe permettant de gérer la persistance des profils.
 */
public interface ProfilRepository extends AbstractRepositoryWithCode<Profil, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.administration.ProfilDto( " +
            " p.id , p.code , p.description , p.isdefault, p.ordre, " +
            " to.id, to.code, to.libelle)" +
            " FROM Profil p " +
            " LEFT JOIN p.typeOrganisme to ";

    String PROFIL_CREABLE = " JOIN p.droitCreer pdc" +
            " JOIN pdc.profilCreateur pc " +
            " JOIN pc.userProfil up " +
            " JOIN up.user u " +
            " WHERE u.username = :username ";

    String ORDER_BY = " ORDER BY p.ordre asc";

    @Query(value = "FROM Profil p  WHERE p.isdefault = true")
    Optional<Profil> findDefault();

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE p.id = :idProfil ")
    Optional<ProfilDto> findDtoUsingId(Long idProfil);

    @Query(value = "SELECT " + NEW_DTO +
            " WHERE p.code = :codeProfil ")
    Optional<ProfilDto> findDtoUsingCode(String codeProfil);

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    List<ProfilDto> findDtoAll();

    @Query(value = "SELECT " + NEW_DTO + ORDER_BY)
    Page<ProfilDto> findDtoAll(Pageable pageable);

    @Query(value = "SELECT distinct " + NEW_DTO +
            PROFIL_CREABLE + ORDER_BY
    )
    List<ProfilDto> findListDtoCreableUsingUserName(String username);

    @Query(value = "SELECT distinct p" +
            " FROM Profil p " +
            PROFIL_CREABLE + ORDER_BY
    )
    List<Profil> findListCreableUsingUserName(String username);

    @Query(value = "Select r " +
            " FROM Profil p " +
            " join p.roles r  " +
            " WHERE p.id = :idProfil")
    Set<Roles> findRolesUsingIdProfil(Long idProfil);
}
