package fabnum.metiis.repository.administration;

import fabnum.metiis.domain.administration.DroitCreationProfil;
import fabnum.metiis.dto.administration.DroitCreationProfilDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface DroitCreationProfilRepository extends AbstractRepository<DroitCreationProfil, Long> {

    String NEW_DTO = "new fabnum.metiis.dto.administration.DroitCreationProfilDto(" +
            " dcp.id ,  " +
            " pc.id , pc.code , pc.description, pc.isdefault , " +
            " p.id , p.code, p.description , p.isdefault" +
            ")" +
            " From DroitCreationProfil dcp " +
            " join dcp.profilCreateur pc " +
            " join dcp.profilCree p ";


    @Query(value = " Select " + NEW_DTO)
    List<DroitCreationProfilDto> findDtoAll();

    @Query(value = " Select " + NEW_DTO +
            " where dcp.id = :id ")
    Optional<DroitCreationProfilDto> findDtoUsingId(Long id);

    @Query(value = " Select " + NEW_DTO +
            " where dcp.profilCreateur.id = :idCreateur " +
            " order by dcp.profilCree.id ")
    List<DroitCreationProfilDto> findAllCreateurDto(Long idCreateur);

    @Query(value = " Select dcp from DroitCreationProfil  dcp " +
            " where dcp.profilCreateur.id = :idProfilCreateur and dcp.profilCree.id = :idProfilCree ")
    Optional<DroitCreationProfil> findByCreateurAndCree(Long idProfilCreateur, Long idProfilCree);


}
