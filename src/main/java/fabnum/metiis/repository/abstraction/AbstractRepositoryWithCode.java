package fabnum.metiis.repository.abstraction;

import fabnum.metiis.domain.abstractions.AbstractEntity;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface AbstractRepositoryWithCode<T extends AbstractEntity, ID> extends AbstractRepository<T, ID> {

    /**
     * Recherche d'une entité par son code.
     * @param code {@link String}
     * @return Optional
     */
    Optional<T> findByCode(String code);

    /**
     * Permet de rechercher un entité ayant le code passé en paramètre mais pas le même id
     * @param id {@link Long} : identifiant du corps à ne pas prendre en compte.
     * @param code {@link String} : code à rechercher.
     * @return Optional
     */
    Optional<T> findByIdNotAndCode(ID id, String code);
}
