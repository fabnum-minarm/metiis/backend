package fabnum.metiis.repository.abstraction;

import fabnum.metiis.domain.abstractions.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface AbstractRepository<T extends AbstractEntity, ID> extends JpaRepository<T, ID> {

    /**
     * Permet de supprimer des entity.
     * @param ids {@link Iterable} : identifiants technique des entity à supprimer.
     */
    void deleteByIdIn(Iterable<ID> ids);
}
