package fabnum.metiis.repository.contacter;

import fabnum.metiis.domain.contacter.Contact;
import fabnum.metiis.dto.contacter.ContactDto;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ContactRepository extends AbstractRepository<Contact, Long> {

    String NEW_DTO = " new fabnum.metiis.dto.contacter.ContactDto( " +
            " c.id , c.nom , c.prenom , c.email , c.objet , c.message , c.createdDate , c.lu , c.traite" +
            " ) " +
            "" +
            " FROM Contact c ";
    String ORDER_BY = " ORDER BY c.createdDate desc nulls last ";


    @Query(value = "SELECT " + NEW_DTO +
            " where c.lu = true " +
            " and c.traite = true " +
            ORDER_BY)
    Page<ContactDto> findOld(Pageable pageable);

    @Query(value = "SELECT " + NEW_DTO + " where c.id = :id ")
    Optional<ContactDto> findDtoUsingId(Long id);

    @Query(value = "SELECT " + NEW_DTO +
            " where c.lu = false " +
            ORDER_BY)
    List<ContactDto> findDtoNonLu();

    @Query(value = "SELECT " + NEW_DTO +
            " where c.lu = true " +
            " and c.traite = false " +
            ORDER_BY)
    List<ContactDto> findDtoNonTraite();

}
