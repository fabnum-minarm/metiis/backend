package fabnum.metiis.domain.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.administration.Profil;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "typeorganisme", uniqueConstraints = {
        @UniqueConstraint(columnNames = "code")
}, indexes = {
        @Index(columnList = "idparent")
})
public class TypeOrganisme extends AbstractEntityIdLong {

    @NotEmpty
    @Column(nullable = false)
    private String code;

    @NotEmpty
    @Column(nullable = false)
    private String libelle;

    @NotNull
    @Column(nullable = false)
    private Long ordre;

    @Column(name = "pouraffectation", nullable = false, columnDefinition = "BOOLEAN NOT NULL DEFAULT false")
    private Boolean pourAffectation = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idparent")
    @JsonIgnore
    private TypeOrganisme parent;

    @CheckBeforeDelete(message = "type-organisme.attached.profil")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "typeOrganisme")
    @JsonIgnore
    private Set<Profil> profils = new HashSet<>();

    @CheckBeforeDelete(message = "type-organisme.attached.organisme")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "typeOrganisme")
    @JsonIgnore
    private Set<Organisme> organismes = new HashSet<>();

    @CheckBeforeDelete(message = "type-organisme.attached.type-organisme")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    @JsonIgnore
    private Set<TypeOrganisme> enfants = new HashSet<>();
}
