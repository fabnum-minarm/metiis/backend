package fabnum.metiis.domain.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.activite.Activite;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "organisme", uniqueConstraints = {
        @UniqueConstraint(columnNames = "credo")
}, indexes = {
        @Index(columnList = "idtypeorganisme"),
        @Index(columnList = "idparent")
})
public class Organisme extends AbstractEntityIdLong {

    private static final long serialVersionUID = -2934660699844764086L;
    
    @NotEmpty
    @Column(nullable = false)
    private String code;

    @NotEmpty
    @Column(nullable = false)
    private String credo;

    @NotEmpty
    @Column(nullable = false)
    private String libelle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idtypeorganisme")
    private TypeOrganisme typeOrganisme = null;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idparent")
    @JsonIgnore
    private Organisme parent = null;

    @CheckBeforeDelete(message = "organisme.attached.affectation")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organisme")
    @JsonIgnore
    private Set<Affectation> affectations = new HashSet<>();

    @CheckBeforeDelete(message = "organisme.attached.organisme")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    @JsonIgnore
    private Set<Organisme> enfants = new HashSet<>();

    @CheckBeforeDelete(message = "organisme.attached.activite")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "organisme")
    @JsonIgnore
    private Set<Activite> activites = new HashSet<>();
}
