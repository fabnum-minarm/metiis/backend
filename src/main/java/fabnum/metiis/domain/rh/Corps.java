package fabnum.metiis.domain.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "corps", uniqueConstraints = {
        @UniqueConstraint(columnNames = "code")
})
public class Corps extends AbstractEntityIdLong {
    private static final long serialVersionUID = 1L;

    @NotEmpty
    @Column(nullable = false)
    private String lettre;

    @NotEmpty
    @Column(nullable = false)
    private String code;

    @NotEmpty
    @Column(nullable = false)
    private String libelle;

    @NotNull
    @Column(nullable = false)
    private Long ordre;

    @CheckBeforeDelete(message = "corps.attached.grade")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "corps")
    @JsonIgnore
    private Set<Grade> grades = new HashSet<>();

    @CheckBeforeDelete(message = "grade.attached.utilisateur")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "corps")
    @JsonIgnore
    private Set<Utilisateur> utilisateurs = new HashSet<>();


}
