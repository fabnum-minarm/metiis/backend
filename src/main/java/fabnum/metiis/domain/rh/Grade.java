package fabnum.metiis.domain.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "grade", uniqueConstraints = {
        @UniqueConstraint(columnNames = "code")
}, indexes = {
        @Index(columnList = "idcorps")
})
public class Grade extends AbstractEntityIdLong {
    private static final long serialVersionUID = 1L;

    @TruncateIfTooLong
    @Column(nullable = false)
    private String code;

    @TruncateIfTooLong
    @Column(nullable = false)
    private String libelle;

    @Column(nullable = false)
    private Long ordre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idcorps", nullable = false)
    private Corps corps;

    @CheckBeforeDelete(message = "grade.attached.utilisateur")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "grade")
    @JsonIgnore
    private Set<Utilisateur> utilisateurs = new HashSet<>();
}
