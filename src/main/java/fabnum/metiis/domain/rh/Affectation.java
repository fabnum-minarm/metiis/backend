package fabnum.metiis.domain.rh;

import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name="affectation",
    indexes = {
        @Index(columnList = "idutilisateur") ,
        @Index(columnList = "idorganisme") ,
        @Index(columnList = "idpostero")
    })
public class Affectation extends AbstractEntityIdLong {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idutilisateur" , nullable = false)
    private Utilisateur utilisateur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="idorganisme" , nullable = false)
    private Organisme organisme;

    @Column(name = "dateaffectation", nullable = false, columnDefinition = "timestamp")
    private Instant dateAffectation;

    @Column(name = "datefinaffectation", columnDefinition = "timestamp")
    private Instant dateFinAffectation = null;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idpostero" )
    private PosteRo posteRo;

}
