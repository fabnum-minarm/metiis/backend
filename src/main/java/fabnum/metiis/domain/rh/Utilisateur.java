package fabnum.metiis.domain.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.abstractions.IAuditEntity;
import fabnum.metiis.domain.activite.Participer;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.alerte.Alerte;
import fabnum.metiis.domain.disponibilite.Disponibilite;
import fabnum.metiis.domain.nouveaute.NouveauteUtilisateur;
import fabnum.metiis.security.SecurityUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "utilisateur", uniqueConstraints = {
        @UniqueConstraint(columnNames = "iduser")
}, indexes = {
        @Index(columnList = "idgrade"),
        @Index(columnList = "idcorps")
})
public class Utilisateur extends AbstractEntityIdLong implements IAuditEntity {
    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String nom;

    @Column(nullable = false)
    private String prenom;

    @Column(nullable = false)
    @Email
    private String email;

    @Column
    private String telephone;

    @Column(name = "new_email")
    @Email
    private String newEmail;

    @Column(name = "new_telephone")
    private String newTelephone;

    @Column(name = "accordinscription", nullable = false, columnDefinition = "boolean default false")
    private Boolean accordInscription = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idgrade")
    private Grade grade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idcorps")
    private Corps corps;

    /**
     * Correspond à une @oneToOne mais n'est pas mappé comme cela
     * pour eviter du mauvais eager loading.
     */
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "iduser", nullable = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<Affectation> affectations = new HashSet<>();

    @CheckBeforeDelete(message = "utilisateur.attached.disponibilite")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur")
    @JsonIgnore
    private Set<Disponibilite> disponibilites = new HashSet<>();

    @CheckBeforeDelete(message = "utilisateur.attached.participer")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur")
    @JsonIgnore
    private Set<Participer> participers = new HashSet<>();


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "proprietaire", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<GroupePersonnaliser> groupesPersonnaliser = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<GroupePersonnaliserDetails> groupesPersonnaliserDetails = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<TokenUtilisateur> inscriptions = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "createur", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<TokenUtilisateur> createurToken = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "utilisateur", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<NouveauteUtilisateur> nouveauteUtilisateurs = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "destinataire", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<Alerte> alertes = new HashSet<>();

    @CreatedBy
    @Column(name = "created_by", updatable = false)
    @JsonIgnore
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date", updatable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant createdDate;

    @LastModifiedBy
    @Column(name = "last_modified_by")
    @JsonIgnore
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date", columnDefinition = "timestamp")
    @JsonIgnore
    private Instant lastModifiedDate;

    @Override
    public String toString() {
        return "Utilisateur{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", id=" + id +
                '}';
    }


    private void preMerge() {
        email = Tools.toLowerCase(email);
    }

    @PrePersist
    private void prePersist() {
        createdBy = SecurityUtils.getCurrentUserLogin();
        createdDate = Instant.now();
        preMerge();
    }

    @PreUpdate
    private void preUpdate() {
        lastModifiedBy = SecurityUtils.getCurrentUserLogin();
        lastModifiedDate = Instant.now();
        preMerge();
    }


}
