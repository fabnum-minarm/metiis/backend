package fabnum.metiis.domain.rh;

import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "groupepersonnaliserdetails", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"idgroupepersonnaliser", "idutilisateur"})
}, indexes = {
        @Index(columnList = "idgroupepersonnaliser"),
        @Index(columnList = "idutilisateur")
})
public class GroupePersonnaliserDetails extends AbstractEntityIdLong {
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idgroupepersonnaliser", nullable = false)
    private GroupePersonnaliser groupePersonnaliser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false)
    private Utilisateur utilisateur;

}
