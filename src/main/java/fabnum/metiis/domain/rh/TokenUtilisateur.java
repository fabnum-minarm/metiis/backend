package fabnum.metiis.domain.rh;

import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "tokenutilisateur", indexes = {
        @Index(columnList = "idutilisateur")
})
public class TokenUtilisateur extends AbstractEntityIdLong {
    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String token;

    @Column(nullable = false, columnDefinition = "timestamp")
    private Instant creation = Instant.now();

    @ManyToOne
    @JoinColumn(name = "idutilisateurcreateur")
    private Utilisateur createur;

    @Column(nullable = false, columnDefinition = "timestamp")
    private Instant prescription;

    @Column(nullable = false, name = "forinsciption")
    private Boolean forInsciption = Boolean.FALSE;

    @Column(nullable = false, name = "forpassword")
    private Boolean forPassword = Boolean.FALSE;

    @Column(nullable = false, name = "forinfos")
    private Boolean forInfos = Boolean.FALSE;

    @ManyToOne
    @JoinColumn(name = "idutilisateur", nullable = false)
    private Utilisateur utilisateur;

}
