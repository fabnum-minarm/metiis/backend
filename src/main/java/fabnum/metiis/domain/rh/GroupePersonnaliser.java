package fabnum.metiis.domain.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "groupepersonnaliser", indexes = {
        @Index(columnList = "idutilisateur")
})
public class GroupePersonnaliser extends AbstractEntityIdLong {
    private static final long serialVersionUID = 1L;

    @TruncateIfTooLong
    @Column(nullable = false, length = 25)
    private String nom;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false, updatable = false)
    private Utilisateur proprietaire;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "groupePersonnaliser", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<GroupePersonnaliserDetails> groupesPersonnaliserDetails = new HashSet<>();

}
