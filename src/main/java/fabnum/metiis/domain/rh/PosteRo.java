package fabnum.metiis.domain.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "postero", uniqueConstraints = {
        @UniqueConstraint(columnNames = "code")
})
public class PosteRo extends AbstractEntityIdLong {
    private static final long serialVersionUID = 1L;

    @TruncateIfTooLong
    @Column(nullable = false, length = 4)
    private String code;

    @TruncateIfTooLong
    @Column(nullable = false)
    private String libelle;

    @Column
    private Long ordre = 100L;

    @CheckBeforeDelete(message = "posteRo.attached.affectation")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "posteRo")
    @JsonIgnore
    private Set<Affectation> affectations = new HashSet<>();

}
