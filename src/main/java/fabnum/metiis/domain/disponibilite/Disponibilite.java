package fabnum.metiis.domain.disponibilite;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "disponibilite", indexes = {
        @Index(columnList = "idutilisateur"),
        @Index(columnList = "idtypedisponibilite"),
        @Index(columnList = "datedebut"),
        @Index(columnList = "datefin")
})
public class Disponibilite extends AbstractEntityIdLong {

    private static final long serialVersionUID = 1L;

    @Column(name = "datecreation", nullable = false, columnDefinition = "timestamp default now()")
    private Instant dateCreation;

    @Column(name = "datedebut", nullable = false, columnDefinition = "timestamp")
    private Instant dateDebut;

    @Column(name = "datefin", nullable = false, columnDefinition = "timestamp")
    private Instant dateFin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idtypedisponibilite", nullable = false)
    @JsonIgnore
    private TypeDisponibilite typeDisponibilite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false)
    @JsonIgnore
    private Utilisateur utilisateur;

    @TruncateIfTooLong
    @Column(name = "commentaire", length = 1000)
    private String commentaire;

    @PrePersist
    protected void prePersist() {
        this.dateCreation = Instant.now();
    }

}
