package fabnum.metiis.domain.disponibilite;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.activite.Choix;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "typedisponibilite", uniqueConstraints = {
        @UniqueConstraint(columnNames = "code")
})
public class TypeDisponibilite extends AbstractEntityIdLong {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false, name = "code")
    private String code;

    @Column(nullable = false)
    private String libelle;

    @Column(nullable = false, name = "estdisponible", columnDefinition = "boolean default false")
    private Boolean estDisponible;

    @CheckBeforeDelete(message = "typeDisponibilite.attached.disponibilite")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "typeDisponibilite")
    @JsonIgnore
    private Set<Disponibilite> disponibilites = new HashSet<>();

    @CheckBeforeDelete(message = "typeDisponibilite.attached.choix")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "typeDisponibilite")
    @JsonIgnore
    private Set<Choix> choixes = new HashSet<>();


}
