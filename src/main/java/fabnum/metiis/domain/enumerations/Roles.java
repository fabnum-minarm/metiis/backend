package fabnum.metiis.domain.enumerations;

import org.springframework.security.core.GrantedAuthority;

public enum Roles implements GrantedAuthority {
    ROLE_TECH(Code.TECH),
    ROLE_ADMIN(Code.ADMIN),
    ROLE_USER(Code.USER),
    ROLE_READER(Code.READER),
    ROLE_EMPLOYE(Code.EMPLOYE),
    ROLE_EMPLOYEUR(Code.EMPLOYEUR),
    ROLE_ADMIN_LOCAL(Code.ADMIN_LOCAL),
    ;

    private String authority;

    Roles(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }


    public final static class Code {

        public static final String ADMIN = "ROLE_ADMIN";
        public static final String USER = "ROLE_USER";
        public static final String TECH = "ROLE_TECH";
        public static final String READER = "ROLE_READER";
        public static final String EMPLOYE = "ROLE_EMPLOYE";
        public static final String EMPLOYEUR = "ROLE_EMPLOYEUR";
        public static final String ADMIN_LOCAL = "ROLE_ADMIN_LOCAL";
    }
}