package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "choixetapestatut",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"idetapestatut", "idchoix"})
        },
        indexes = {
                @Index(columnList = "idetapestatut"),
                @Index(columnList = "idchoix"),
        })
public class ChoixEtapeStatut extends AbstractEntityIdLong {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idetapestatut", nullable = false)
    @JsonIgnore
    private EtapeStatut etapeStatut;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idchoix", nullable = false)
    @JsonIgnore
    private Choix choix;

    @Column(name = "ordredansgroupe", nullable = false)
    private Long ordreDansGroupe;

}
