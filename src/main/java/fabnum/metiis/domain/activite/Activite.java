package fabnum.metiis.domain.activite;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Organisme;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.interfaces.activite.IActivite;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "activite", indexes = {
        @Index(columnList = "idtypeactivite"),
        @Index(columnList = "idorganisme"),
        @Index(columnList = "idutilisateur"),
        @Index(columnList = "datedebut"),
        @Index(columnList = "datefin"),
        @Index(columnList = "dateenvoierappel"),
        @Index(columnList = "dateenvoierappelparticipation"),
        @Index(columnList = "token")
})
public class Activite extends AbstractEntityIdLong implements IActivite {

    private static final long serialVersionUID = 1L;

    @Column(name = "datecreation", nullable = false, updatable = false, columnDefinition = "timestamp default now()")
    private Instant dateCreation;

    @Column(name = "datemodification", columnDefinition = "timestamp")
    private Instant dateModification;

    @TruncateIfTooLong
    @Column(length = 150)
    private String nom;

    @TruncateIfTooLong
    @Column(length = 10000)
    private String description;

    @Column(name = "datedebut", nullable = false, columnDefinition = "timestamp")
    private Instant dateDebut;

    @Column(name = "datefin", nullable = false, columnDefinition = "timestamp")
    private Instant dateFin;

    @Column(name = "envoyermailcreation", updatable = false, nullable = false, columnDefinition = "boolean default false")
    private Boolean envoyerMailCreation = Boolean.FALSE;

    @Column(name = "rappelnbjouravantdebut")
    private Long rappelNbJourAvantDebut = 0L;

    @Column(name = "dateenvoierappel", columnDefinition = "timestamp")
    private Instant dateEnvoieRappel;

    @Column(name = "rappelinapp", nullable = false, columnDefinition = "boolean default false")
    private Boolean rappelInApp = Boolean.FALSE;

    @Column(name = "rappelemail", nullable = false, columnDefinition = "boolean default false")
    private Boolean rappelEmail = Boolean.FALSE;

    @Column(name = "rappelparticipationnbjouravantdebut")
    private Long rappelParticipationNbJourAvantDebut = 0L;

    @Column(name = "dateenvoierappelparticipation", columnDefinition = "timestamp")
    private Instant dateEnvoieRappelParticipation;

    @Column(name = "rappelparticipationinapp", nullable = false, columnDefinition = "boolean default false")
    private Boolean rappelParticipationInApp = Boolean.FALSE;

    @Column(name = "rappelparticipationemail", nullable = false, columnDefinition = "boolean default false")
    private Boolean rappelParticipationEmail = Boolean.FALSE;

    @Column(nullable = false)
    private String token;

    @ManyToOne
    @JoinColumn(name = "idtypeactivite", nullable = false)
    @JsonIgnore
    private TypeActivite typeActivite;

    // si pas chargé plante lors de l'envoie de mails 'candidater'
    @ManyToOne
    @JoinColumn(name = "idutilisateur", nullable = false, updatable = false)
    @JsonIgnore
    private Utilisateur createur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idorganisme", nullable = false)
    @JsonIgnore
    private Organisme organisme;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idparent")
    @JsonIgnore
    private Activite parent = null;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "activite", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<Effectif> effectifs = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "activite", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<Participer> participers = new HashSet<>();

    @PrePersist
    protected void prePersist() {
        dateCreation = Instant.now();
    }

    @PreUpdate
    private void preUpdate() {
        dateModification = Instant.now();
    }

    @Override
    public String toString() {
        return "Activite{" +
                "nom='" + nom + '\'' +
                ", id=" + id +
                '}';
    }
}
