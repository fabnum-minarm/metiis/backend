package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "statut",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "code")
        },
        indexes = {
                @Index(columnList = "idstatutdisponibilite")
        })
public class Statut extends AbstractEntityIdLong {

    private static final long serialVersionUID = 4007155910118132783L;
    
    @Column(name = "nom", nullable = false, length = 50)
    private String nom;

    @Column(name = "nomsingulier", length = 50)
    private String nomSingulier;

    @Column(name = "code", nullable = false, length = 50)
    private String code;

    @Column(name = "action", nullable = false, length = 100)
    private String action;

    @Column(name = "actionutilisateur", length = 100)
    private String actionUtilisateur;

    @Column(name = "forvisu", nullable = false, columnDefinition = "boolean default false")
    private Boolean forVisu = Boolean.FALSE;

    @Column(name = "forcompletion", nullable = false, columnDefinition = "boolean default false")
    private Boolean forCompletion = Boolean.FALSE;

    @Column(name = "fordetails", nullable = false, columnDefinition = "boolean default false")
    private Boolean forDetails = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idstatutdisponibilite")
    @JsonIgnore
    private StatutDisponibilite statutDisponibilite;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "statut")
    @JsonIgnore
    private Set<Choix> choix = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "statut")
    @JsonIgnore
    private Set<EtapeStatut> etapeStatuts = new HashSet<>();

}
