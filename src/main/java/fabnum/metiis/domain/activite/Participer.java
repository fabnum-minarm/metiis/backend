package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "participer",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"idactivite", "idutilisateur"})
        },
        indexes = {
                @Index(columnList = "idactivite"),
                @Index(columnList = "idutilisateur"),
                @Index(columnList = "idavancementparticipation"),
        })
public class Participer extends AbstractEntityIdLong {

    @Column(name = "dateinscription", nullable = false, columnDefinition = "timestamp default now()")
    private Instant dateInscription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idactivite", nullable = false, updatable = false)
    @JsonIgnore
    private Activite activite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false, updatable = false)
    @JsonIgnore
    private Utilisateur utilisateur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idavancementparticipation")
    @JsonIgnore
    private AvancementParticipation dernierAvancementParticipation;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "participer", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<AvancementParticipation> avancementParticipations = new HashSet<>();

    @PrePersist
    protected void prePersist() {
        dateInscription = Instant.now();
    }


}
