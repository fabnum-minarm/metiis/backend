package fabnum.metiis.domain.activite;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Corps;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "effectif", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"idactivite", "idcorps"})
}, indexes = {
        @Index(columnList = "idactivite"),
        @Index(columnList = "idcorps")
})
public class Effectif extends AbstractEntityIdLong {

    private static final long serialVersionUID = 1L;

    @Column(name = "datecreation", nullable = false, columnDefinition = "timestamp default now()")
    private Instant dateCreation;

    @Column(name = "datemodification", columnDefinition = "timestamp")
    private Instant dateModification;

    @Column(name = "nombre")
    private Long nombre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idactivite", nullable = false)
    @JsonIgnore
    private Activite activite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idcorps", nullable = false)
    @JsonIgnore
    private Corps corps;

    @PrePersist
    protected void prePersist() {
        this.dateCreation = Instant.now();
    }

    @PreUpdate
    private void preUpdate() {
        this.dateModification = Instant.now();
    }

}
