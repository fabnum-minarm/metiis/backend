package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "statutdisponibilite",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "code")
        }
)
public class StatutDisponibilite extends AbstractEntityIdLong {

    @Column(name = "code", nullable = false, length = 50)
    private String code;

    @Column(name = "disponibilite", nullable = false, columnDefinition = "boolean default false")
    private Boolean disponibilite = Boolean.FALSE;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "statutDisponibilite")
    @JsonIgnore
    private Set<Statut> statuts = new HashSet<>();

}
