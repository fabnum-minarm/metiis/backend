package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "etape", uniqueConstraints = {
        @UniqueConstraint(columnNames = "code")
})
public class Etape extends AbstractEntityIdLong {

    @TruncateIfTooLong
    @Column(name = "code", nullable = false, length = 50)
    private String code;

    @TruncateIfTooLong
    @Column(name = "nom", nullable = false, length = 80)
    private String nom;

    @TruncateIfTooLong
    @Column(name = "nompluriel", nullable = false, length = 80)
    private String nomPluriel;

    @Column(name = "ordre", nullable = false)
    private Long ordre;

    @TruncateIfTooLong
    @Column(name = "description")
    private String description;

    @TruncateIfTooLong
    @Column(name = "descriptionpluriel")
    private String descriptionPluriel;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "etape")
    @JsonIgnore
    private Set<ChoixEtapeAction> choixEtapeEtatActions = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "etapeARemplir")
    @JsonIgnore
    private Set<ChoixEtapeAction> choixEtapeEtatActionsARemplir = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "etapeSuivante")
    @JsonIgnore
    private Set<ChoixEtapeAction> choixEtapeEtatActionsSuivants = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "etape")
    @JsonIgnore
    private Set<EtapeStatut> etapeStatuts = new HashSet<>();

}
