package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.disponibilite.TypeDisponibilite;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "choix",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "code")
        },
        indexes = {
                @Index(columnList = "idstatut"),
                @Index(columnList = "idtypedisponibilite"),
                @Index(columnList = "idacteur"),
                @Index(columnList = "idtypeetatchoix")
        }
)
public class Choix extends AbstractEntityIdLong {

    @Column(name = "nom", nullable = false, length = 50)
    private String nom;

    @Column(name = "code", nullable = false, length = 50)
    private String code;

    @Column(name = "lettre", length = 1)
    private String lettre;

    @Column(name = "actionprincipale", nullable = false, columnDefinition = "boolean default false")
    private Boolean actionPrincipale = Boolean.FALSE;

    @Column(name = "actionsuppression", nullable = false, columnDefinition = "boolean default false")
    private Boolean actionSuppression = Boolean.FALSE;

    @Column(name = "canactivitewithotheractivite", nullable = false, columnDefinition = "boolean default false")
    private Boolean canActiviteWithOtherActivite = Boolean.FALSE;

    @ManyToOne
    @JoinColumn(name = "idstatut", nullable = false)
    private Statut statut;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idtypedisponibilite", nullable = false)
    private TypeDisponibilite typeDisponibilite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idacteur", nullable = false)
    private Acteur acteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idtypeetatchoix", nullable = false)
    private TypeEtatChoix typeEtatChoix;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "choix")
    @JsonIgnore
    private Set<ChoixEtapeAction> choixEtapeEtatActions = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "choix")
    @JsonIgnore
    private Set<ChoixEtapeStatut> choixEtapeStatuts = new HashSet<>();

}
