package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "avancementparticipation",
        indexes = {
                @Index(columnList = "idparticiper"),
                @Index(columnList = "idchoix"),
                @Index(columnList = "idutilisateur"),
                @Index(columnList = "idetape")
        }
)
public class AvancementParticipation extends AbstractEntityIdLong {

    @TruncateIfTooLong
    @Column(length = 1000)
    private String commentaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idparticiper", nullable = false, updatable = false)
    @JsonIgnore
    private Participer participer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idchoix")
    @JsonIgnore
    private Choix choix;

    @ManyToOne
    @JoinColumn(name = "idetape", nullable = false, updatable = false)
    @JsonIgnore
    private Etape etape;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur")
    @JsonIgnore
    private Utilisateur utilisateurValidateur;

    @Column(name = "datevalidation", columnDefinition = "timestamp")
    private Instant dateValidation;

    @Column(name = "dateannulation", columnDefinition = "timestamp")
    private Instant dateAnnulation;

    @Column(name = "estrealiser", nullable = false, columnDefinition = "boolean default false")
    private Boolean estRealiser = Boolean.FALSE;


}
