package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "etapestatut",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"idetape", "idstatut"})
        },
        indexes = {
                @Index(columnList = "idetape"),
                @Index(columnList = "idstatut"),
        })
public class EtapeStatut extends AbstractEntityIdLong {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idetape", nullable = false)
    @JsonIgnore
    private Etape etape;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idstatut", nullable = false)
    @JsonIgnore
    private Statut statut;

    @Column(name = "ordredansgroupe", nullable = false)
    private Long ordreDansGroupe;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "etapeStatut")
    @JsonIgnore
    private Set<ChoixEtapeStatut> choixEtapeStatuts = new HashSet<>();

}
