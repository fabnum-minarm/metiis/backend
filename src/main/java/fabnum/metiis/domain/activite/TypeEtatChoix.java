package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "typeetatchoix",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "code")
        }
)
public class TypeEtatChoix extends AbstractEntityIdLong {

    @Column(name = "nom", nullable = false, length = 50)
    private String nom;

    @Column(name = "code", nullable = false, length = 50)
    private String code;

    @Column(name = "finavancement", nullable = false, columnDefinition = "boolean default false")
    private Boolean finAvancement;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "typeEtatChoix")
    @JsonIgnore
    private Set<Choix> choix = new HashSet<>();

}
