package fabnum.metiis.domain.activite;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "choixetapeaction",
        indexes = {
                @Index(columnList = "idetape"),
                @Index(columnList = "idchoix"),
                @Index(columnList = "idetapearemplir"),
                @Index(columnList = "idetapesuivante")
        })
public class ChoixEtapeAction extends AbstractEntityIdLong {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idetape", nullable = false)
    @JsonIgnore
    private Etape etape;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idchoix", nullable = false)
    @JsonIgnore
    private Choix choix;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idetapearemplir", nullable = false)
    @JsonIgnore
    private Etape etapeARemplir;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idetapesuivante")
    @JsonIgnore
    private Etape etapeSuivante;

}
