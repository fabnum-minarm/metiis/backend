package fabnum.metiis.domain.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "typeactivite")
public class TypeActivite extends AbstractEntityIdLong {

    private static final long serialVersionUID = 1L;

    @NotEmpty
    @Column(nullable = false)
    private String code;

    @NotEmpty
    @Column(nullable = false)
    private String libelle;

    @Column(name = "codecss")
    private String codeCss;

    @Column(name = "codecssicone")
    private String codeCssIcone;

    @Column
    private Long ordre;

    @CheckBeforeDelete(message = "typeActivite.attached.activite")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "typeActivite")
    @JsonIgnore
    private Set<Activite> activites = new HashSet<>();

}
