package fabnum.metiis.domain.activite;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "acteur",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "code")
        })
public class Acteur extends AbstractEntityIdLong {

    private static final long serialVersionUID = 1L;

    @Column(name = "nom", length = 50)
    private String nom;

    @Column(name = "code", length = 50)
    private String code;

    @Column(name = "participant", nullable = false, columnDefinition = "boolean default false")
    private Boolean participant;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "acteur")
    @JsonIgnore
    private Set<Choix> choix = new HashSet<>();
}
