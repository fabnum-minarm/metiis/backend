package fabnum.metiis.domain.notification;

import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.activite.TypeActivite;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "notification",
        indexes = {
                @Index(columnList = "idutilisateur")})
public class Notification extends AbstractEntityIdLong {
    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false)
    private Utilisateur utilisateur;

    @ManyToOne
    @JoinColumn(name = "idtypeactivite", nullable = false)
    private TypeActivite typeActivite;

    @Column(name = "notifappactivitecree", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppActiviteCree;

    @Column(name = "notifappactivitemodif", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppActiviteModif;

    @Column(name = "notifappactiviterappel", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppActiviteRappel;

    @Column(name = "notifappactivitesupp", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppActiviteSupp;

    @Column(name = "notifappcandidatretenu", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppCandidatRetenu;

    @Column(name = "notifappcandidatnonretenu", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppCandidatNonRetenu;

    @Column(name = "notifappcandidatconvoque", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppCandidatConvoque;

    @Column(name = "notifappcandidatnonconvoque", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppCandidatNonConvoque;

    @Column(name = "notifapppartageactiviteautreunite", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppPartageActiviteAutreUnite;

    @Column(name = "notifappcreaactiviteautreunite", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppCreaActiviteAutreUnite;

    @Column(name = "notifappmodifactivitepartage", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppModifActivitePartage;

    @Column(name = "notifappsuppactivitepartage", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifAppSuppActivitePartage;

    @Column(name = "notifmailactivitecree", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailActiviteCree;

    @Column(name = "notifmailactivitemodif", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailActiviteModif;

    @Column(name = "notifmailactiviterappel", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailActiviteRappel;

    @Column(name = "notifmailactivitesupp", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailActiviteSupp;

    @Column(name = "notifmailcandidatretenu", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailCandidatRetenu;

    @Column(name = "notifmailcandidatnonretenu", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailCandidatNonRetenu;

    @Column(name = "notifmailcandidatconvoque", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailCandidatConvoque;

    @Column(name = "notifmailcandidatnonconvoque", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailCandidatNonConvoque;

    @Column(name = "notifmailpartageactiviteautreunite", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailPartageActiviteAutreUnite;

    @Column(name = "notifmailcreaactiviteautreunite", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailCreaActiviteAutreUnite;

    @Column(name = "notifmailmodifactivitepartage", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailModifActivitePartage;

    @Column(name = "notifmailsuppactivitepartage", nullable = false, columnDefinition = "boolean default false")
    private Boolean notifMailSuppActivitePartage;
}
