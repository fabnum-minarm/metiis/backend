package fabnum.metiis.domain.alerte;

import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.domain.activite.Statut;
import fabnum.metiis.domain.activite.TypeActivite;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "alerte",
        indexes = {
                @Index(columnList = "idemetteur"),
                @Index(columnList = "iddestinataire"),
                @Index(columnList = "idactivite"),
                @Index(columnList = "idtypeactivite")
        })
public class Alerte extends AbstractEntityIdLong {

    @Column(name = "datealerte", nullable = false, columnDefinition = "timestamp")
    private Instant dateAlerte = Instant.now();

    @Column(name = "lu", nullable = false, columnDefinition = "boolean default false")
    private Boolean lu = Boolean.FALSE;

    @TruncateIfTooLong
    @NotEmpty
    @Column(name = "titre", length = 150, nullable = false) // meme taille que le nom d'une activité
    private String titre;

    @Column(name = "nombre")
    private Long nombre;

    @TruncateIfTooLong
    @NotEmpty
    @Column(name = "description", length = 250, nullable = false)
    private String description;

    @TruncateIfTooLong
    @Column(name = "commentaire", length = 250)
    private String commentaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idemetteur")
    private Utilisateur emetteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idstatut")
    private Statut statut;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "iddestinataire", nullable = false)
    private Utilisateur destinataire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idactivite")
    private Activite activite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idtypeactivite")
    private TypeActivite typeActivite;

    @Column(name = "partage", nullable = false, columnDefinition = "boolean default false")
    private Boolean partage = Boolean.FALSE;

    public void incrementerNombre() {
        if (nombre == null) {
            nombre = 0L;
        }
        nombre++;
    }
}