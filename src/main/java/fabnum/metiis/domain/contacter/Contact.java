package fabnum.metiis.domain.contacter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "contact")
public class Contact extends AbstractEntityIdLong {

    private static final long serialVersionUID = -4979973451630638607L;

    @TruncateIfTooLong
    @Column(length = 100, nullable = false)
    private String nom;

    @TruncateIfTooLong
    @Column(length = 100, nullable = false)
    private String prenom;

    @Email
    @TruncateIfTooLong
    @Column
    private String email;

    @TruncateIfTooLong
    @Column
    private String objet;

    @TruncateIfTooLong
    @Column(name = "message", length = 1000)
    private String message;

    @CreatedDate
    @Column(name = "created_date", updatable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant createdDate;

    @Column(name = "lu", nullable = false, columnDefinition = "boolean default false")
    private Boolean lu = Boolean.FALSE;

    @Column(name = "traite", nullable = false, columnDefinition = "boolean default false")
    private Boolean traite = Boolean.FALSE;

    @PrePersist
    private void prePersist() {
        createdDate = Instant.now();
    }
}
