package fabnum.metiis.domain.evaluation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.MinMax;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "evaluer", indexes = {
        @Index(columnList = "idutilisateur")
})
public class Evaluer extends AbstractEntityIdLong {

    public static final long NOTE_MAX = 10L;

    public static final long NOTE_MIN = 1L;

    @CreatedDate
    @Column(name = "created_date", updatable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant createdDate;

    @MinMax(min = NOTE_MIN, max = NOTE_MAX)
    @Column(name = "noteutilisation")
    private Long noteUtilisation;

    @MinMax(min = NOTE_MIN, max = NOTE_MAX)
    @Column(name = "notebesoin")
    private Long noteBesoin;

    @TruncateIfTooLong
    @Column(name = "commentaire", length = 1000)
    private String commentaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false)
    @JsonIgnore
    private Utilisateur utilisateur;

    @PrePersist
    private void prePersist() {
        createdDate = Instant.now();
    }
}
