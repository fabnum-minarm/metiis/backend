package fabnum.metiis.domain.abstractions;

import java.time.Instant;

public interface IAuditEntity {

    String getCreatedBy();

    String getLastModifiedBy();

    Instant getCreatedDate();

    Instant getLastModifiedDate();
}
