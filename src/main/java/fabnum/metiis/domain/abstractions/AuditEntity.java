package fabnum.metiis.domain.abstractions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.security.SecurityUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

@Getter
@Setter
@MappedSuperclass
public abstract class AuditEntity extends VersionEntity implements IAuditEntity {

    private static final long serialVersionUID = 1L;

    @CreatedBy
    @Column(name = "created_by", nullable = false, updatable = false)
    @JsonIgnore
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant createdDate;

    @LastModifiedBy
    @Column(name = "last_modified_by", nullable = false)
    @JsonIgnore
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date", nullable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant lastModifiedDate;

    @PrePersist
    protected void prePersistAudit() {
        createdBy = lastModifiedBy = SecurityUtils.getCurrentUserLogin();
        createdDate = lastModifiedDate = Instant.now();
    }

    @PreUpdate
    protected void preUpdateAudit() {
        lastModifiedBy = SecurityUtils.getCurrentUserLogin();
        lastModifiedDate = Instant.now();
    }
}
