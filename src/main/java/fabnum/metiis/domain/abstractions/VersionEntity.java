package fabnum.metiis.domain.abstractions;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.io.Serializable;


@MappedSuperclass
public abstract  class VersionEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    @Column(nullable = false)
    @Getter
    private Long version;
}
