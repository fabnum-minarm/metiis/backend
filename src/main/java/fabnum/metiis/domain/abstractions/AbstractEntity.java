package fabnum.metiis.domain.abstractions;

import java.io.Serializable;

public interface AbstractEntity<E> extends Serializable {

    E getId();
    void setId(E id);
}
