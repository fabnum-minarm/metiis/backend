package fabnum.metiis.domain.administration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.abstractions.IAuditEntity;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.security.SecurityUtils;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.*;

import static java.util.stream.Collectors.toList;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"password", "wrongCredential", "preferences"})
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "username")
})
@EqualsAndHashCode(of = {"username"})
public class User implements UserDetails, IAuditEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Version
    @Column(nullable = false)
    private Long version;

    @CreatedBy
    @Column(name = "created_by", nullable = false, updatable = false)
    @JsonIgnore
    private String createdBy;

    @CreatedDate
    @Column(name = "created_date", nullable = false, updatable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant createdDate;

    @LastModifiedBy
    @Column(name = "last_modified_by", nullable = false)
    @JsonIgnore
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "last_modified_date", nullable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant lastModifiedDate;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    @JsonIgnore
    private String password;

    @Column(length = 100, nullable = false)
    private String lastname;

    @Column(length = 100, nullable = false)
    private String firstname;

    @NotEmpty
    @Column(nullable = false)
    @Email
    private String email;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.REMOVE)
    @JsonIgnore
    @Builder.Default
    private Set<UserProfil> userProfil = new HashSet<>();

    @ElementCollection(fetch = FetchType.LAZY)
    @MapKeyColumn(name = "key", length = 20)
    @Column(name = "value", nullable = false)
    @Lob
    @CollectionTable(name = "users_preferences", joinColumns = @JoinColumn(name = "user_id"))
    @Builder.Default
    @JsonIgnore
    private Map<String, String> preferences = new HashMap<>();

    @Column(nullable = false, columnDefinition = "boolean default false")
    @Builder.Default
    private boolean enabled = false;

    @Column(name = "last_modified_enabled", columnDefinition = "timestamp")
    private Instant lastModifiedEnabled;

    @Transient
    @Builder.Default
    private boolean accountNonExpired = true;

    @Column(name = "account_non_locked", nullable = false, columnDefinition = "boolean default true")
    @Builder.Default
    private boolean accountNonLocked = true;

    @Column(name = "last_modified_unlock", columnDefinition = "timestamp")
    private Instant lastModifiedUnlock;

    @Column(name = "last_connexion_date", columnDefinition = "timestamp")
    private Instant lastConnexionDate;

    @Column(name = "wrong_credential", nullable = false, columnDefinition = "integer default 0")
    @Builder.Default
    @JsonIgnore
    private int wrongCredential = 0;

    @Transient
    @Builder.Default
    private boolean credentialsNonExpired = true;

    public void setAccountNonLocked(boolean accountNonLocked) {
        if (accountNonLocked && !this.accountNonLocked) {
            setLastModifiedUnlock(new Date().toInstant());
        }
        this.accountNonLocked = accountNonLocked;
        wrongCredential = 0;
    }

    public void setEnabled(boolean enabled) {
        if (enabled && !this.enabled) {
            setLastModifiedEnabled(new Date().toInstant());
        }
        this.enabled = enabled;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userProfil.stream().map(UserProfil::getProfil).flatMap(p -> p.getRoles().stream())
                .map(Roles::name).distinct().sorted()
                .map(SimpleGrantedAuthority::new).collect(toList());
    }

    @PrePersist
    private void prePersist() {
        createdBy = SecurityUtils.getCurrentUserLogin();
        createdDate = Instant.now();
        preMerge();
    }

    @PreUpdate
    private void preUpdate() {
        preMerge();
    }

    private void preMerge() {
        lastModifiedBy = SecurityUtils.getCurrentUserLogin();
        lastModifiedDate = Instant.now();

        username = Tools.toLowerCase(username);
        email = Tools.toLowerCase(email);
    }
}
