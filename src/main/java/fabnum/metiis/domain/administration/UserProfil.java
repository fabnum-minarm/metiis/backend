package fabnum.metiis.domain.administration;

import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.*;

import javax.persistence.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name="users_profils",
        indexes = {
                @Index(columnList = "user_id") ,
                @Index(columnList = "profil_id")
        })
public class UserProfil extends AbstractEntityIdLong {

    /**
     * User
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_id" , nullable = false)
    private User user;

    /**
     * Profil
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="profil_id" , nullable = false)
    private Profil profil;

    /**
     * Select : boolean pour définir le profil à charger par défaut
     */
    @Column(nullable = false, columnDefinition = "boolean default false")
    @Builder.Default
    private Boolean selected = false;

}
