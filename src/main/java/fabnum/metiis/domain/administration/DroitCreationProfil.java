package fabnum.metiis.domain.administration;

import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "droitcreationprofil",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"idprofilcreateur", "idprofilcree"})
        },
        indexes = {
                @Index(columnList = "idprofilcreateur"),
                @Index(columnList = "idprofilcree")
        })
@Entity
public class DroitCreationProfil extends AbstractEntityIdLong {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idprofilcreateur", nullable = false)
    private Profil profilCreateur;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idprofilcree", nullable = false)
    private Profil profilCree;

}
