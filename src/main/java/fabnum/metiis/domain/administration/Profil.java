package fabnum.metiis.domain.administration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.CheckBeforeDelete;
import fabnum.metiis.domain.abstractions.AbstractEntity;
import fabnum.metiis.domain.abstractions.AuditEntity;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.domain.rh.TypeOrganisme;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "profils", uniqueConstraints = {
        @UniqueConstraint(columnNames = "code")
})
public class Profil extends AuditEntity implements AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    @Column(length = 20, nullable = false)
    protected String code;

    @Column(nullable = false)
    protected String description;

    @Column
    private Long ordre;

    @Column(nullable = false, columnDefinition = "BOOLEAN NOT NULL DEFAULT false")
    @Builder.Default
    private Boolean isdefault = false;

    @ManyToOne
    @JoinColumn(name = "idtypeorganisme")
    private TypeOrganisme typeOrganisme = null;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @Column(name = "roles", nullable = false)
    @CollectionTable(name = "profils_roles", joinColumns = @JoinColumn(name = "profil_id"))
    @Builder.Default
    protected Set<Roles> roles = new HashSet<>();

    @CheckBeforeDelete(message = "profil.users")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "profil")
    @JsonIgnore
    @Builder.Default
    private Set<UserProfil> userProfil = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "profilCreateur", cascade = CascadeType.REMOVE)
    @JsonIgnore
    @Builder.Default
    private Set<DroitCreationProfil> droitsCreateur = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "profilCree", cascade = CascadeType.REMOVE)
    @JsonIgnore
    @Builder.Default
    private Set<DroitCreationProfil> droitCreer = new HashSet<>();

    @PrePersist
    @PreUpdate
    private void prePersistOrMerge() {
        if (this.description == null || this.description.isEmpty()) {
            this.description = this.code;
        }
    }

}