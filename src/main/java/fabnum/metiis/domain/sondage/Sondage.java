package fabnum.metiis.domain.sondage;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "sondage")
public class Sondage extends AbstractEntityIdLong {

    private static final long serialVersionUID = 1L;

    @TruncateIfTooLong
    @Column(name = "nom", length = 50)
    private String nom;

    @TruncateIfTooLong
    @Column(length = 10000)
    private String question;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sondage", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<SondageUtilisateur> sondageUtilisateurs = new HashSet<>();

}
