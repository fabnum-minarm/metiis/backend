package fabnum.metiis.domain.sondage;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "sondageutilisateur", indexes = {
        @Index(columnList = "idsondage"),
        @Index(columnList = "idutilisateur"),
})
public class SondageUtilisateur extends AbstractEntityIdLong {

    private static final long serialVersionUID = 1L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false)
    private Utilisateur utilisateur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idsondage")
    private Sondage sondage;

    @Column(name = "reponse", nullable = false, length = 255)
    private String reponse;

    @CreatedDate
    @Column(name = "created_date", updatable = false, columnDefinition = "timestamp")
    @JsonIgnore
    private Instant createdDate;

    @PrePersist
    private void prePersist() {
        createdDate = Instant.now();
    }
}
