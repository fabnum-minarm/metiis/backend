package fabnum.metiis.domain.nouveaute;


import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.domain.rh.Utilisateur;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "nouveauteutilisateur", indexes = {
        @Index(columnList = "idnouveaute"),
        @Index(columnList = "idutilisateur"),
})
public class NouveauteUtilisateur extends AbstractEntityIdLong {

    private static final long serialVersionUID = 8720549735408010440L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idutilisateur", nullable = false)
    private Utilisateur utilisateur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idnouveaute")
    private Nouveaute nouveaute;

    @Column(name = "lu", nullable = false, columnDefinition = "boolean default false")
    private Boolean lu = Boolean.FALSE;
}
