package fabnum.metiis.domain.nouveaute;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.checkBefore.TruncateIfTooLong;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "nouveaute")
public class Nouveaute extends AbstractEntityIdLong {

    private static final long serialVersionUID = -4359292990861851024L;

    @Column(name = "date", length = 10)
    private String date;

    @TruncateIfTooLong
    @Column(name = "nom", length = 20)
    private String nom;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "nouveaute", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private Set<NouveauteUtilisateur> nouveauteUtilisateurs = new HashSet<>();

}
