package fabnum.metiis.services.abstraction;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.abstractions.AbstractEntity;
import fabnum.metiis.exceptions.NotFoundException;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import fabnum.metiis.services.exceptions.ServiceException;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
public abstract class AbstarctService<R extends AbstractRepository, E extends AbstractEntity , ID> extends AbstarctServiceSimple<R,E,ID>  {

    public AbstarctService(R repositiory) {
        super(repositiory);
    }

    /**
     * Permet de rechercher l'ensemble des entités.
     * @return List
     */
    @SuppressWarnings("unchecked")
    public List<E> findAll() {
        return this.repository.findAll();
    }


    @SuppressWarnings("unchecked")
    public E update(ID id, E entity) {

        if(!id.equals(entity.getId())) {
            throw new ServiceException(Tools.message("default.update.conflit"));
        }
        if(!this.repository.existsById(id) ) {
            throw new NotFoundException();
        }

        this.checkBeforeSaveOrUpdate(entity);

        return (E) this.repository.save(entity);

    }
}
