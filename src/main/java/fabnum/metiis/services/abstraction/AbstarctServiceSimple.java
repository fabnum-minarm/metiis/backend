package fabnum.metiis.services.abstraction;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.checkBefore.MetiisBeforeDelete;
import fabnum.metiis.config.checkBefore.MetiisBeforeSaveOrUpdate;
import fabnum.metiis.domain.abstractions.AbstractEntity;
import fabnum.metiis.exceptions.NotFoundException;
import fabnum.metiis.repository.abstraction.AbstractRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Transactional
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstarctServiceSimple<R extends AbstractRepository, E extends AbstractEntity, ID> {

    protected R repository;

    @SuppressWarnings("unchecked")
    protected String getEntityClassName() {
        return ((Class<E>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1]).getSimpleName();
    }

    /**
     * Permet de rechercher une entité via son identifiant.
     *
     * @param id {@link ID}
     * @return Renvoi une exception si l'entité n'existe pas (erreur 404, doc Cadre API)
     */
    @SuppressWarnings("unchecked")
    public E findById(ID id) {
        return Tools.getValueFromOptionalOrNotFoundException((Optional<E>) repository.findById(id), getEntityClassName());
    }


    public void existId(ID id) {
        findById(id);
    }

    public List<E> findByIds(Collection<ID> ids) {
        List<E> entitys = (List<E>) repository.findAllById(ids);
        if (entitys.size() != ids.size()) {
            throw new NotFoundException(getEntityClassName());
        }
        return entitys;
    }

    public void existIds(Collection<ID> ids) {
        findByIds(ids);
    }

    /**
     * Permet d'enregistrer une entité..
     *
     * @param entity {@link E} : l'entité à créer ou modifier.
     * @return {@link E} : l'entité créé ou modifié.
     */
    @SuppressWarnings("unchecked")
    public E saveOrUpdate(E entity) {
        // Vérification des données du profil manipulé.
        checkBeforeSaveOrUpdate(entity);
        // Enregistrement et renvoi du profil
        return (E) repository.save(entity);
    }


    /**
     * Suppression d'une entity.
     *
     * @param id {@link Long} : identifiant à supprimer.
     */
    @SuppressWarnings("unchecked")
    public void delete(ID id) {
        E entity = findById(id);
        checkBeforeDelete(entity);
        repository.delete(entity);

    }

    /**
     * Suppression d'une entity.
     *
     * @param entity {@link Long} : identifiant à supprimer.
     */
    @SuppressWarnings("unchecked")
    public void delete(E entity) {
        checkBeforeDelete(entity);
        repository.delete(entity);
    }

    /**
     * Suppression de plusieurs entity.
     *
     * @param ids {@link Iterable} : identifiants  à supprimer.
     */
    @SuppressWarnings("unchecked")
    public void delete(Iterable<ID> ids) {
        repository.deleteByIdIn(ids);
    }

    /**
     * Permet de valider une entity
     *
     * @param entity {@link E} : corps à tester.
     */
    protected void checkBeforeSaveOrUpdate(E entity) {
        MetiisBeforeSaveOrUpdate.verifier(entity);
    }

    /**
     * Permet de valider une entity
     *
     * @param entity {@link E} : corps à tester.
     */
    protected void checkBeforeDelete(E entity) {
        MetiisBeforeDelete.verifier(entity);
    }


    /**
     * Teste si un champs texte est null, renvoie une exception avec le message errorMessageKey comme clé.
     *
     * @param value           .
     * @param errorMessageKey .
     */
    protected void testNull(String value, String errorMessageKey) {
        Tools.testerNullite(value, errorMessageKey);
    }

    /**
     * Teste si un champs texte est null, renvoie une exception avec le message errorMessageKey comme clé.
     *
     * @param value           .
     * @param errorMessageKey .
     */
    protected void testNull(Long value, String errorMessageKey) {
        Tools.testerNullite(value, errorMessageKey);
    }

    /**
     * Teste si l'entité exist, renvoie une exception avec le message errorMessageKey comme clé.
     *
     * @param optionalEntity  .
     * @param errorMessageKey .
     */
    protected void testExist(Optional<E> optionalEntity, String errorMessageKey) {
        Tools.testerExistance(optionalEntity, errorMessageKey);
    }


}
