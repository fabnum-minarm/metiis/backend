package fabnum.metiis.services.activite;

import fabnum.metiis.domain.activite.Acteur;
import fabnum.metiis.repository.activite.ActeurRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ActeurService extends AbstarctServiceSimple<ActeurRepository, Acteur, Long> {


    public ActeurService(
            ActeurRepository repository) {
        super(repository);
    }


}
