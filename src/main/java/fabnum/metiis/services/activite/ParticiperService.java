package fabnum.metiis.services.activite;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.domain.activite.AvancementParticipation;
import fabnum.metiis.domain.activite.Choix;
import fabnum.metiis.domain.activite.Participer;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.activite.*;
import fabnum.metiis.dto.alerte.ParticipationDataEntity;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import fabnum.metiis.interfaces.activite.IActivite;
import fabnum.metiis.repository.activite.ParticiperRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.alerte.AlerteService;
import fabnum.metiis.services.exceptions.ServiceException;
import fabnum.metiis.services.mail.MailService;
import fabnum.metiis.services.rh.UtilisateurService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ParticiperService extends AbstarctServiceSimple<ParticiperRepository, Participer, Long> {

    private ActiviteService activiteService;
    private UtilisateurService utilisateurService;
    private AvancementParticipationService avancementParticipationService;
    private ChoixService choixService;
    private ChoixEtapeActionService choixEtapeActionService;
    private AlerteService alerteService;
    private MailService mailService;

    public ParticiperService(
            ActiviteService activiteService,
            UtilisateurService utilisateurService,
            AvancementParticipationService avancementParticipationService,
            ChoixService choixService,
            ChoixEtapeActionService choixEtapeActionService,
            AlerteService alerteService,
            MailService mailService,

            ParticiperRepository repository) {

        super(repository);

        this.activiteService = activiteService;
        this.utilisateurService = utilisateurService;
        this.avancementParticipationService = avancementParticipationService;
        this.choixService = choixService;
        this.choixEtapeActionService = choixEtapeActionService;
        this.alerteService = alerteService;
        this.mailService = mailService;
    }


    private Optional<Participer> findParticipation(Activite activite, Utilisateur utilisateur) {
        return repository.findByActiviteIdAndUtilisateurId(activite.getId(), utilisateur.getId());
    }

    public List<ParticiperDto> getListParticipation(List<Long> idsActivites, Long idUtilisateur) {
        return repository.findDtoUsingIdsActivitesAndIdUtilisateur(idsActivites, idUtilisateur);
    }


    public ParticiperDto creerParticipation(Long idActivite, Long idUtilisateur) {

        Activite activite = activiteService.findById(idActivite);
        Utilisateur utilisateur = utilisateurService.findById(idUtilisateur);

        testExist(findParticipation(activite, utilisateur), "participer.already.exist");

        Participer participer = createEntity(activite, utilisateur);

        return findDtoById(participer.getId());
    }

    private Participer createEntity(Activite activite, Utilisateur utilisateur) {
        Participer participer = new Participer();
        participer.setActivite(activite);
        participer.setUtilisateur(utilisateur);

        saveOrUpdate(participer);

        avancementParticipationService.creerAvacenement(participer);
        return participer;
    }

    public Participer findOrCreate(ChoisirDataEntity choisirDataEntity, UtilisateurActiviteDetailsDto utilisateurActiviteDetailsDto) {
        Participer participer;
        Utilisateur utilisateur = utilisateurService.findById(utilisateurActiviteDetailsDto.getUtilisateur().getId());

        Optional<Participer> optionalParticiper = findParticipation(choisirDataEntity.getActivite(), utilisateur);

        if (optionalParticiper.isEmpty()) {
            participer = createEntity(choisirDataEntity.getActivite(), utilisateur);
        } else {
            participer = optionalParticiper.get();
        }

        return participer;
    }

    public ParticiperDto findDtoByIdActiviteAndIdUtilisateur(Long idActivite, Long idUtilisateur) {
        ParticiperDto participer;
        Optional<ParticiperDto> optionalDto = repository.findDtoUsingIdActiviteAndIdUtilisateur(idActivite, idUtilisateur);

        if (optionalDto.isPresent()) {
            participer = optionalDto.get();
            remplirAvancementParticipationDto(participer);
        } else {
            participer = new ParticiperDto(idActivite, idUtilisateur);
            remplirAvancementParticipationVide(participer);
        }

        return participer;
    }

    private void remplirAvancementParticipationVide(ParticiperDto participer) {
        List<AvancementParticipationDto> avancements = avancementParticipationService.findDtoAllByIdParticiperVide();
        participer.setAvancements(avancements);
    }

    public ParticiperDto findDtoById(Long idParticiper) {
        ParticiperDto dto = findDtoOnly(idParticiper);
        remplirAvancementParticipationDto(dto);
        return dto;
    }

    public ParticiperDto findDtoOnly(Long idParticiper) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idParticiper),
                getEntityClassName());
    }

    private void remplirAvancementParticipationDto(ParticiperDto participer) {
        List<AvancementParticipationDto> avancements = avancementParticipationService.findDtoAllByIdParticiper(participer.getId());
        participer.setAvancements(avancements);
    }

    public void choisir(ChoisirDto choisir) {
        ChoisirDataEntity choisirDataEntity = createOrUpdateParticipation(choisir);
        informerParticipant(choisirDataEntity);
    }

    public void choisirEmploye(ChoisirDto choisir) {
        ChoisirDataEntity choisirDataEntity = createOrUpdateParticipation(choisir);
        informerCreateurActivite(choisirDataEntity);
    }

    @NotNull
    private ChoisirDataEntity createOrUpdateParticipation(ChoisirDto choisir) {
        ChoisirDataEntity choisirDataEntity = getChoisirEntity(choisir);
        choisir.getSelectionnees().forEach(choisirUtilisateurCommentaire ->
                choisir(choisirUtilisateurCommentaire, choisirDataEntity)
        );
        return choisirDataEntity;
    }

    @NotNull
    private ChoisirDataEntity getChoisirEntity(ChoisirDto choisir) {
        MetiisValidation.verifier(choisir, ContextEnum.CHOISIR);

        Activite activite = activiteService.findById(choisir.getActivite().getId());
        Utilisateur validateur = utilisateurService.findById(choisir.getValidateur().getId());
        Choix choix = choixService.findById(choisir.getChoix().getId());
        List<ChoixEtapeActionDto> etapesActions = choixEtapeActionService.getDtoAll();

        return new ChoisirDataEntity(activite, validateur, choix, etapesActions);
    }

    private void informerParticipant(ChoisirDataEntity choisirDataEntity) {
        choisirDataEntity.getDataForAlertes().stream().filter(ParticipationDataEntity::haveAvancementParticipation).forEach(participationDataEntity -> {
                    alerteService.informerParticipation(choisirDataEntity, participationDataEntity);
                    mailService.envoyerMailParticipation(choisirDataEntity, participationDataEntity);
                }
        );

    }

    private void informerCreateurActivite(ChoisirDataEntity choisirDataEntity) {
        choisirDataEntity.getDataForAlertes().stream().filter(ParticipationDataEntity::haveAvancementParticipation).forEach(participationDataEntity -> {
                    alerteService.informerMaParticipation(choisirDataEntity, participationDataEntity);
                    mailService.envoyerMailMaParticipation(choisirDataEntity);
                }
        );
    }

    private void choisir(UtilisateurActiviteDetailsDto choisirUtilisateurCommentaire, ChoisirDataEntity choisirDataEntity) {
        MetiisValidation.verifier(choisirUtilisateurCommentaire, ContextEnum.CHOISIR_PERSONNE);

        if (!choisirDataEntity.getChoix().getCanActiviteWithOtherActivite()) {
            verifierDisponibilite(choisirUtilisateurCommentaire.getUtilisateur(), choisirDataEntity.getActivite());
        }
        Participer participer = findOrCreate(choisirDataEntity, choisirUtilisateurCommentaire);
        choisirDataEntity.newCurrentParticipationData();
        choisirDataEntity.addCurrentParticiper(participer);
        choisirDataEntity.addCurrentAvancementsParticipationsDto(avancementParticipationService.findDtoAllByIdParticiper(participer.getId()));
        Optional<AvancementParticipationDto> avancementParticipationDtoOptional = avancementParticipationService.
                searchAvancementParticipationAModifier(choisirDataEntity);
        avancementParticipationDtoOptional.ifPresent(avancementParticipationDto ->
                mettreAJourAvancementParticipation(avancementParticipationDto, choisirUtilisateurCommentaire, choisirDataEntity)
        );
        if (choisirDataEntity.getChoix().getCode().equals("annuler-retenir") ||
                choisirDataEntity.getChoix().getCode().equals("annuler-convoquer")) {
            Long idParticiper = participer.getId();
            delete(participer);
            avancementParticipationService.deleteByIdParticiper(idParticiper);
        }
    }

    private void verifierDisponibilite(UtilisateurCompletDto utilisateur, IActivite activite) {
        var indisponibiliteActiviteDtoList = indisponibiliteActivite(utilisateur, activite);
        if (!indisponibiliteActiviteDtoList.isEmpty()) {
            throw new ServiceException(Tools.message("utilisateur.participer.autre.activite", utilisateur.getNom(), utilisateur.getPrenom()));
        }
    }

    private List<IndisponibiliteActiviteDto> indisponibiliteActivite(UtilisateurDto utilisateur, IActivite activite) {
        return repository.getIndisponibliteActiviteUtilisateur(utilisateur.getId(), activite.getId(), activite.getDateDebut(), activite.getDateFin());
    }

    public List<IndisponibiliteActiviteDto> searchIndisponibiliteInOtherActivite(Long idActivite, Long idUtilisateur) {
        var activite = activiteService.findDtoById(idActivite);
        var utilisateur = utilisateurService.findDtoById(idUtilisateur);
        return indisponibiliteActivite(utilisateur, activite);
    }

    private void mettreAJourAvancementParticipation(AvancementParticipationDto avancementParticipationDto, UtilisateurActiviteDetailsDto choisirUtilisateurCommentaire, ChoisirDataEntity choisirDataEntity) {

        AvancementParticipation avancement;

        if (choisirDataEntity.getChoix().getActionSuppression()) {
            avancement = avancementParticipationService.annulerAvancementParticipationAndCreateNew(avancementParticipationDto, choisirUtilisateurCommentaire, choisirDataEntity);
        } else {
            avancement = avancementParticipationService.realiserAvancementParticipation(avancementParticipationDto, choisirUtilisateurCommentaire, choisirDataEntity);
        }

        avancementParticipationService.updateAvancementRealiserBefore(choisirDataEntity.getCurrent().getAvancementsParticipationsDto(), avancementParticipationDto);

        choisirDataEntity.addCurrentAvancementParticipation(avancement);

        choisirDataEntity.getCurrent().getParticiper().setDernierAvancementParticipation(avancement);
        saveOrUpdate(choisirDataEntity.getCurrent().getParticiper());
    }

    public List<IndisponibiliteActiviteDto> getIndisponibliteActivite(Long idOrganisme, Instant debut, Instant fin) {
        return repository.getIndisponibliteActivite(idOrganisme, debut, fin);
    }


    public List<ChoixEtapeActionDto> findMyAction(ParticiperDto participer) {
        List<ChoixEtapeActionDto> choixEtapeActions = new ArrayList<>();
        Optional<AvancementParticipationDto> avancementParticipationDtoOptional = avancementParticipationService.findFirstEtapeNonRealiser(participer.getAvancements());
        if (avancementParticipationDtoOptional.isPresent()) {
            AvancementParticipationDto avancementParticipation = avancementParticipationDtoOptional.get();
            choixEtapeActions = choixEtapeActionService.findMyAction(avancementParticipation.getEtape().getId());
        }
        return choixEtapeActions;
    }

    public MaParticipationDto getMaParticipationByActivite(Long idActivite) {
        var maParticipationDto = new MaParticipationDto();
        var utilisateurConnecter = utilisateurService.findDtoByUserName(SecurityUtils.getCurrentUserLogin());

        maParticipationDto.setParticiper(findDtoByIdActiviteAndIdUtilisateur(idActivite, utilisateurConnecter.getId()));
        maParticipationDto.setMesActions(findMyAction(maParticipationDto.getParticiper()));
        return maParticipationDto;
    }

    public List<IndisponibiliteActiviteDto> searchMesIndisponibiliteInOtherActivite(Long idActivite) {
        var activite = activiteService.findDtoById(idActivite);
        var utilisateur = utilisateurService.findDtoByUserName(SecurityUtils.getCurrentUserLogin());
        return indisponibiliteActivite(utilisateur, activite);
    }
}
