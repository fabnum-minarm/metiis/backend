package fabnum.metiis.services.activite;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.activite.Etape;
import fabnum.metiis.dto.activite.EtapeDto;
import fabnum.metiis.dto.activite.EtapeStatutDto;
import fabnum.metiis.repository.activite.EtapeRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EtapeService extends AbstarctServiceSimple<EtapeRepository, Etape, Long> {

    private EtapeStatutService etapeStatutService;


    public EtapeService(
            EtapeStatutService etapeStatutService,

            EtapeRepository repository) {

        super(repository);
        this.etapeStatutService = etapeStatutService;

    }

    @Cacheable("AllEtape")
    public List<Etape> findAll() {
        return repository.findAll(Sort.by(Sort.Direction.ASC, "ordre"));
    }

    public List<EtapeDto> findDtoAllForEmployeurAction() {
        return loadEtapesWithEtapeStatut(null, true);
    }

    private List<EtapeDto> loadEtapesWithEtapeStatut(Boolean forVisu, boolean loadChoixEtapeStatut) {
        List<EtapeDto> etapes = findDtoAll();
        List<EtapeStatutDto> etapeStatuts = etapeStatutService.findDtoAllAndLoadChoixEtapeStatut(forVisu, loadChoixEtapeStatut);
        etapes.forEach(groupe -> groupe.remplirStatut(etapeStatuts));
        return etapes;
    }

    public List<EtapeDto> findDtoAllForEmployer() {
        return loadEtapesWithEtapeStatut(true, false);
    }

    @Cacheable("AllEtapeDto")
    public List<EtapeDto> findDtoAll() {
        return repository.findDtoAll();
    }

    @Caching(evict = {
            @CacheEvict(value = "AllEtapeDto", allEntries = true),
            @CacheEvict(value = "AllEtape", allEntries = true)
    })
    public EtapeDto modifier(Long idEtape, EtapeDto etapeDto) {
        MetiisValidation.verifier(etapeDto, ContextEnum.UPDATE_ETAPE);

        Etape etape = findById(idEtape);
        etape.setCode(etapeDto.getCode());
        etape.setNom(etapeDto.getNom());
        etape.setNomPluriel(etapeDto.getNomPluriel());
        etape.setOrdre(etapeDto.getOrdre());
        etape.setDescription(etapeDto.getDescription());
        etape.setDescriptionPluriel(etapeDto.getDescriptionPluriel());

        saveOrUpdate(etape);

        return repository.findDtoById(idEtape);
    }

    public EtapeDto findDtoByIdOnly(Long id) {
        return repository.findDtoById(id);
    }
}
