package fabnum.metiis.services.activite;

import fabnum.metiis.domain.activite.ChoixEtapeStatut;
import fabnum.metiis.dto.activite.ChoixEtapeStatutDto;
import fabnum.metiis.repository.activite.ChoixEtapeStatutRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ChoixEtapeStatutService extends AbstarctServiceSimple<ChoixEtapeStatutRepository, ChoixEtapeStatut, Long> {

    public ChoixEtapeStatutService(
            ChoixEtapeStatutRepository repository) {
        super(repository);
    }

    @Cacheable("AllChoixEtapeStatutDto")
    public List<ChoixEtapeStatutDto> findDtoAll() {
        return repository.findDtoAll();
    }

}
