package fabnum.metiis.services.activite;

import fabnum.metiis.domain.activite.Choix;
import fabnum.metiis.repository.activite.ChoixRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ChoixService extends AbstarctServiceSimple<ChoixRepository, Choix, Long> {


    public ChoixService(
            ChoixRepository repository) {

        super(repository);

    }

    @Cacheable("Choix")
    @Override
    public Choix findById(Long id) {
        return super.findById(id);
    }

}
