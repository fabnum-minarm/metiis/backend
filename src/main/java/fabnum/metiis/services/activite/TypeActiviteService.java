package fabnum.metiis.services.activite;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.activite.TypeActivite;
import fabnum.metiis.dto.activite.TypeActiviteDto;
import fabnum.metiis.repository.activite.TypeActiviteRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TypeActiviteService extends AbstarctService<TypeActiviteRepository, TypeActivite, Long> {

    public TypeActiviteService(TypeActiviteRepository repository) {
        super(repository);
    }

    public List<TypeActivite> getListById(List<Long> idsTypeActivite) {
        List<Long> idsTypeActiviteSearch = new ArrayList<>();
        if (!CollectionUtils.isEmpty(idsTypeActivite)) {
            idsTypeActiviteSearch.addAll(idsTypeActivite);
        }

        return findByIds(idsTypeActiviteSearch);
    }

    public List<TypeActiviteDto> findDtoAll() {
        return repository.findDtoAll();
    }

    public List<TypeActiviteDto> findDtoAllOder() {
        return repository.findDtoAllOrder();
    }

    public List<TypeActivite> findAllOrder() {
        return repository.findAllOrder();
    }

    public TypeActiviteDto creerFromDto(TypeActiviteDto typeActiviteDto) {
        MetiisValidation.verifier(typeActiviteDto, ContextEnum.CREATE_TYPE_ACTIVITE);
        TypeActivite typeActivite = new TypeActivite();
        rempliTypeActivite(typeActivite, typeActiviteDto);
        saveOrUpdate(typeActivite);
        return findDtoByIdOnly(typeActivite.getId());
    }

    public TypeActiviteDto modifierFromDto(Long idTypeActivite, TypeActiviteDto typeActiviteDto) {
        MetiisValidation.verifier(typeActiviteDto, ContextEnum.UPDATE_TYPE_ACTIVITE);
        TypeActivite typeActivite = findById(idTypeActivite);
        rempliTypeActivite(typeActivite, typeActiviteDto);
        saveOrUpdate(typeActivite);
        return findDtoByIdOnly(typeActivite.getId());
    }

    private void rempliTypeActivite(TypeActivite typeActivite, TypeActiviteDto typeActiviteDto) {
        typeActivite.setCode(typeActiviteDto.getCode());
        typeActivite.setLibelle(typeActiviteDto.getLibelle());
        typeActivite.setCodeCss(typeActiviteDto.getCodeCss());
        typeActivite.setCodeCssIcone(typeActiviteDto.getCodeCssIcone());
        typeActivite.setOrdre(typeActiviteDto.getOrdre());
    }

    public TypeActiviteDto findDtoByIdOnly(Long idTypeActivite) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idTypeActivite), getEntityClassName());
    }

    public Page<TypeActiviteDto> findDtoAll(Pageable pageable) {
        return repository.findDtoAll(pageable);
    }


}
