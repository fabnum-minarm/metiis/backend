package fabnum.metiis.services.activite;

import fabnum.metiis.domain.activite.ChoixEtapeAction;
import fabnum.metiis.dto.activite.ChoixEtapeActionDto;
import fabnum.metiis.repository.activite.ChoixEtapeActionRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ChoixEtapeActionService extends AbstarctServiceSimple<ChoixEtapeActionRepository, ChoixEtapeAction, Long> {

    public ChoixEtapeActionService(

            ChoixEtapeActionRepository repository) {
        super(repository);

    }

    @Cacheable("AllChoixEtapeActionDto")
    public List<ChoixEtapeActionDto> getDtoAll() {
        return repository.findDtoAll();
    }


    public List<ChoixEtapeActionDto> findMyAction(Long idEtape) {
        return repository.findMyAction(idEtape);
    }
}
