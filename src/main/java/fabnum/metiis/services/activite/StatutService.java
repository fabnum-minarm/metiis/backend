package fabnum.metiis.services.activite;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.activite.Statut;
import fabnum.metiis.dto.activite.StatutDto;
import fabnum.metiis.repository.activite.StatutRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StatutService extends AbstarctServiceSimple<StatutRepository, Statut, Long> {


    public StatutService(

            StatutRepository repository) {

        super(repository);


    }


    public StatutDto findDtoById(Long idStatut) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idStatut), getEntityClassName());
    }

    @Cacheable("StatutDtoForCompletion")
    public StatutDto findDtoForCompletion() {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoForCompletion(), getEntityClassName());
    }

    public List<StatutDto> findDtoForDetails() {
        return repository.findDtoForDetails();
    }
}
