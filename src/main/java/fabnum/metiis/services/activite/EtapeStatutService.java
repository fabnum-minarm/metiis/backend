package fabnum.metiis.services.activite;

import fabnum.metiis.domain.activite.EtapeStatut;
import fabnum.metiis.dto.activite.ChoixEtapeStatutDto;
import fabnum.metiis.dto.activite.EtapeStatutDto;
import fabnum.metiis.repository.activite.EtapeStatutRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EtapeStatutService extends AbstarctServiceSimple<EtapeStatutRepository, EtapeStatut, Long> {


    private ChoixEtapeStatutService choixEtapeStatutService;


    public EtapeStatutService(
            ChoixEtapeStatutService choixEtapeStatutService,
            EtapeStatutRepository repository) {
        super(repository);
        this.choixEtapeStatutService = choixEtapeStatutService;
    }

    public List<EtapeStatutDto> findDtoAll(Boolean forVisu) {
        return repository.findDtoAll(Optional.ofNullable(forVisu));
    }

    public List<EtapeStatutDto> findDtoAllByIdEtape(Long idEtape, Boolean forVisu) {
        List<EtapeStatutDto> etapeStatutDtos = repository.findDtoAllUsingIdEtape(idEtape, Optional.ofNullable(forVisu));
        loadChoixEtapeStatut(etapeStatutDtos);
        return etapeStatutDtos;
    }

    @Cacheable("EtapeStatutDtoWithStatutDisponible")
    public List<EtapeStatutDto> findDtoWithStatutDisponible(Boolean forVisu) {
        return repository.findDtoWithStatutDisponible(Optional.ofNullable(forVisu));
    }

    public List<EtapeStatutDto> findDtoAllAndLoadChoixEtapeStatut(Boolean forVisu, boolean loadChoixEtapeStatut) {
        List<EtapeStatutDto> etapesStatuts = findDtoAll(forVisu);
        if (loadChoixEtapeStatut) {
            loadChoixEtapeStatut(etapesStatuts);
        }
        return etapesStatuts;
    }

    private void loadChoixEtapeStatut(List<EtapeStatutDto> etapesStatuts) {
        List<ChoixEtapeStatutDto> choixEtapesStatuts = choixEtapeStatutService.findDtoAll();
        etapesStatuts.forEach(statutGroupeStatutDto -> statutGroupeStatutDto.remplirChoix(choixEtapesStatuts));
    }
}
