package fabnum.metiis.services.activite;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.activite.AvancementParticipation;
import fabnum.metiis.domain.activite.Choix;
import fabnum.metiis.domain.activite.Etape;
import fabnum.metiis.domain.activite.Participer;
import fabnum.metiis.dto.activite.*;
import fabnum.metiis.repository.activite.AvancementParticipationRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Transactional
public class AvancementParticipationService extends AbstarctServiceSimple<AvancementParticipationRepository, AvancementParticipation, Long> {

    private EtapeService etapeService;

    public AvancementParticipationService(
            EtapeService etapeService,

            AvancementParticipationRepository repository) {

        super(repository);

        this.etapeService = etapeService;
    }


    public void creerAvacenement(Participer participer) {
        if (participer.getAvancementParticipations().isEmpty()) {
            List<Etape> etapes = etapeService.findAll();
            etapes.forEach(etape -> create(participer, etape));
        }
    }

    private void create(Participer participer, Etape etape) {
        AvancementParticipation avancementParticipation = new AvancementParticipation();
        avancementParticipation.setEtape(etape);
        avancementParticipation.setParticiper(participer);

        saveOrUpdate(avancementParticipation);
    }

    public List<AvancementParticipationDto> findDtoAllByIdParticiper(Long idParticiper) {
        return repository.findAllDtoUsingIdParticiper(idParticiper);
    }


    public Optional<AvancementParticipationDto> findFirstEtapeNonRealiser(List<AvancementParticipationDto> avancementParticipations) {

        AvancementParticipationDto trouver = null;

        for (AvancementParticipationDto avancementParticipationDto : avancementParticipations) {
            if (avancementParticipationDto.estFinit()) {
                break;
            }
            if (!avancementParticipationDto.getEstRealiser()) {
                trouver = avancementParticipationDto;
                break;
            }
        }

        return Optional.ofNullable(trouver);
    }

    public Optional<AvancementParticipationDto> searchAvancementParticipationAModifier(ChoisirDataEntity choisirDataEntity) {

        Optional<AvancementParticipationDto> avancementParticipationOptional = findAvancementByChoix(choisirDataEntity.getCurrent().getAvancementsParticipationsDto(), choisirDataEntity.getChoix());

        if (avancementParticipationOptional.isPresent()) {
            return searchAvancementParticipationARemplirWithAvancementAndChoix(choisirDataEntity, avancementParticipationOptional.get());
        }
        return Optional.empty();

    }

    public AvancementParticipation annulerAvancementParticipationAndCreateNew(AvancementParticipationDto avancementParticipationDto, UtilisateurActiviteDetailsDto choisirUtilisateurCommentaire, ChoisirDataEntity choixEntity) {
        AvancementParticipation avancementAAnnuler = annulerAvancement(avancementParticipationDto);
        var avancementParticipation = dupliquerApresAnnulation(avancementAAnnuler);
        remplirAndSave(choisirUtilisateurCommentaire, choixEntity, avancementParticipation);
        return avancementParticipation;
    }

    @NotNull
    private AvancementParticipation annulerAvancement(AvancementParticipationDto avancementParticipationDto) {
        var avancementAAnnuler = findById(avancementParticipationDto.getId());
        avancementAAnnuler.setDateAnnulation(SuperDate.now());
        saveOrUpdate(avancementAAnnuler);
        return avancementAAnnuler;
    }

    @NotNull
    private AvancementParticipation dupliquerApresAnnulation(AvancementParticipation aDupliquer) {
        AvancementParticipation nouveau = new AvancementParticipation();
        nouveau.setParticiper(aDupliquer.getParticiper());
        nouveau.setEtape(aDupliquer.getEtape());
        saveOrUpdate(nouveau);
        return nouveau;
    }

    @NotNull
    public AvancementParticipation realiserAvancementParticipation(AvancementParticipationDto avancementParticipationDto, UtilisateurActiviteDetailsDto choisirUtilisateurCommentaire, ChoisirDataEntity choixEntity) {
        AvancementParticipation avancementParticipation = findById(avancementParticipationDto.getId());

        remplirAndSave(choisirUtilisateurCommentaire, choixEntity, avancementParticipation);

        return avancementParticipation;
    }

    private void remplirAndSave(UtilisateurActiviteDetailsDto choisirUtilisateurCommentaire, ChoisirDataEntity choixEntity, AvancementParticipation avancementParticipation) {
        avancementParticipation.setChoix(choixEntity.getChoix());
        avancementParticipation.setUtilisateurValidateur(choixEntity.getValidateur());
        avancementParticipation.setDateValidation(SuperDate.now());
        avancementParticipation.setEstRealiser(Boolean.TRUE);

        if (Tools.testerNulliteString(choisirUtilisateurCommentaire.getCommentaire())) {
            avancementParticipation.setCommentaire(choisirUtilisateurCommentaire.getCommentaire());
        }

        saveOrUpdate(avancementParticipation);
    }

    public void updateAvancementRealiserBefore(List<AvancementParticipationDto> avancementParticipations, AvancementParticipationDto avancementParticipationRealiser) {
        List<Long> idAvancementARealiser = rechercherIdAvancementAvantRealiser(avancementParticipations, avancementParticipationRealiser);
        if (!idAvancementARealiser.isEmpty()) {
            repository.updateAvancementRealiserByIds(idAvancementARealiser);
        }
    }

    private List<Long> rechercherIdAvancementAvantRealiser(List<AvancementParticipationDto> avancementParticipations, AvancementParticipationDto avancementParticipationRealiser) {
        return avancementParticipations.stream()
                .filter(Predicate.not(AvancementParticipationDto::getEstRealiser))
                .filter(aRealiser -> aRealiser.isBefore(avancementParticipationRealiser))
                .map(AvancementParticipationDto::getId)
                .collect(Collectors.toList());
    }


    private Optional<AvancementParticipationDto> findAvancementByChoix(List<AvancementParticipationDto> avancementParticipations, Choix choix) {
        Optional<AvancementParticipationDto> avancementParticipationOptional;

        if (choix.getActionSuppression()) {
            avancementParticipationOptional = findLastEtapeRealiser(avancementParticipations);
        } else {
            avancementParticipationOptional = findFirstEtapeNonRealiser(avancementParticipations);
        }
        return avancementParticipationOptional;
    }

    @NotNull
    private Optional<AvancementParticipationDto> findLastEtapeRealiser(List<AvancementParticipationDto> avancementParticipations) {
        return avancementParticipations.stream()
                .sorted(Collections.reverseOrder())
                .filter(AvancementParticipationDto::getEstRealiser)
                .findFirst();
    }

    private Optional<AvancementParticipationDto> searchAvancementParticipationARemplirWithAvancementAndChoix(ChoisirDataEntity choisirDataEntity, AvancementParticipationDto avancementParticipationDto) {

        Optional<EtapeDto> etapeARemplirOptionnal = searchEtapeARemplir(avancementParticipationDto, choisirDataEntity);
        Optional<AvancementParticipationDto> avancementParticipationARemplir = Optional.empty();

        if (etapeARemplirOptionnal.isPresent()) {
            avancementParticipationARemplir = searchAvancementARemplir(choisirDataEntity.getCurrent().getAvancementsParticipationsDto(), etapeARemplirOptionnal
                    .get());
        }
        return avancementParticipationARemplir;
    }


    private Optional<EtapeDto> searchEtapeARemplir(AvancementParticipationDto avancementParticipationDto, ChoisirDataEntity choisirDataEntity) {
        return choisirDataEntity.getEtapesActions().stream()
                .filter(choixEtapeAction -> choixEtapeAction.equalsEtapeChoix(avancementParticipationDto.getEtape()
                        .getId(), choisirDataEntity.getChoix().getId()))
                .findFirst()
                .map(ChoixEtapeActionDto::getEtapeARemplir);

    }

    private Optional<AvancementParticipationDto> searchAvancementARemplir(List<AvancementParticipationDto> avancementParticipations, EtapeDto etapeARemplir) {
        return avancementParticipations.stream()
                .filter(avancementParticipation -> avancementParticipation.equalsEtape(etapeARemplir.getId()))
                .findFirst();
    }


    public List<AvancementParticipationDto> findDtoAllByIdParticiperVide() {
        return repository.findAllDtoUsingIdParticiperVide();
    }

    public void deleteByIdParticiper(Long idParticiper) {
        repository.deleteByIdParticiper(idParticiper);
    }
}