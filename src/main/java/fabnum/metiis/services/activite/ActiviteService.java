package fabnum.metiis.services.activite;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.domain.activite.Participer;
import fabnum.metiis.domain.activite.TypeActivite;
import fabnum.metiis.domain.disponibilite.Disponibilite;
import fabnum.metiis.domain.rh.Corps;
import fabnum.metiis.domain.rh.Organisme;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.activite.*;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.dto.rh.*;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.dto.tools.OptionListFilterCorps;
import fabnum.metiis.repository.activite.ActiviteRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.administration.ProfilService;
import fabnum.metiis.services.alerte.AlerteService;
import fabnum.metiis.services.disponibilite.DisponibiliteDate;
import fabnum.metiis.services.disponibilite.DisponibiliteService;
import fabnum.metiis.services.exceptions.ServiceException;
import fabnum.metiis.services.mail.MailService;
import fabnum.metiis.services.rh.CorpsService;
import fabnum.metiis.services.rh.FilterService;
import fabnum.metiis.services.rh.OrganismeService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class ActiviteService extends AbstarctServiceSimple<ActiviteRepository, Activite, Long> {

    private UtilisateurService utilisateurService;
    private TypeActiviteService typeActiviteService;
    private OrganismeService organismeService;
    private EffectifService effectifService;
    private DisponibiliteService disponibiliteService;
    private StatutService statutService;
    private ParticiperService participerService;
    private AlerteService alerteService;
    private EtapeStatutService etapeStatutService;
    private FilterService filterService;
    private MailService mailService;
    private CorpsService corpsService;
    private ProfilService profilService;

    public ActiviteService(UtilisateurService utilisateurService,
                           TypeActiviteService typeActiviteService,
                           OrganismeService organismeService,
                           EffectifService effectifService,
                           DisponibiliteService disponibiliteService,
                           StatutService statutService,
                           CorpsService corpsService,
                           @Lazy AlerteService alerteService, // le lazy evite les dependece circulaire
                           @Lazy ParticiperService participerService, // le lazy evite les dependece circulaire
                           @Lazy EtapeStatutService etapeStatutService, // le lazy evite les dependece circulaire
                           @Lazy FilterService filterService,
                           MailService mailService,
                           @Lazy ProfilService profilService,
                           ActiviteRepository repository) {

        super(repository);

        this.utilisateurService = utilisateurService;
        this.typeActiviteService = typeActiviteService;
        this.organismeService = organismeService;
        this.effectifService = effectifService;
        this.disponibiliteService = disponibiliteService;
        this.filterService = filterService;
        this.statutService = statutService;
        this.participerService = participerService;
        this.etapeStatutService = etapeStatutService;
        this.alerteService = alerteService;
        this.mailService = mailService;
        this.corpsService = corpsService;
        this.profilService = profilService;
    }


    public ActiviteDto creer(ActiviteDto activiteDto, String currentUserName) {

        MetiisValidation.verifier(activiteDto, ContextEnum.CREATE_ACTIVITE);
        verifierActivitePartage(activiteDto);
        Utilisateur utilisateur = utilisateurService.findByUserName(currentUserName);

        Activite activite = new Activite();
        activite.setCreateur(utilisateur);
        activite.setEnvoyerMailCreation(activiteDto.getEnvoyerMailCreation());
        activite.setToken(UUID.randomUUID().toString());
        remplirCompletementEntity(activiteDto, activite);

        saveOrUpdate(activite);

        creerEffectif(activiteDto, activite);

        alerteService.informerCreationActivite(activite);
        envoyerMailCreation(activite);
        if (activiteDto.getIdParent() != null) {
            Activite activiteParent = findById(activiteDto.getIdParent());
            mailService.envoyerMailCreationActivitePartage(activite, activiteParent);
            alerteService.informerCreationActivitePartage(activite, activiteParent);
        }

        return findDtoById(activite.getId());
    }

    public void verifierActivitePartage(ActiviteDto activiteDto) {
        if (activiteDto.getIdParent() != null) {
            Optional<ActiviteDto> activiteDejaPartagee = repository.findDtoUsingOrganismeAndActiviteParent(
                    activiteDto.getOrganisme().getId(), activiteDto.getIdParent());
            if (activiteDejaPartagee.isPresent()) {
                throw new ServiceException("Il existe déjà une activité liée à cette activité parent dans votre unité.");
            }
            Activite activiteParent = findById(activiteDto.getIdParent());
            if (activiteDto.getOrganisme().getId().equals(activiteParent.getOrganisme().getId())) {
                throw new ServiceException("Vous ne pouvez pas créer une activité partagée dans l'unité de l'activité parent.");
            }
        }
    }

    private void envoyerMailCreation(Activite activite) {
        if (activite.getEnvoyerMailCreation()) {
            List<Utilisateur> destinataires = utilisateurService.findAllByOrganisme(activite.getOrganisme().getId());
            destinataires.forEach(destinataire -> mailService.envoyerMailCreationActivite(activite, destinataire));
        }
    }


    private void remplirCompletementEntity(ActiviteDto activiteDto, Activite activite) {

        SuperDate debut = new SuperDate(activiteDto.getDateDebut()).debutDeLaJournee();
        SuperDate fin = new SuperDate(activiteDto.getDateFin()).debutDeLaJournee();


        if (debut.estApres(fin)) {
            throw new ServiceException(Tools.message("activite.erreur.date"));
        }

        Organisme organisme = organismeService.findById(activiteDto.getOrganisme().getId());

        if (activiteDto.getIdParent() != null) {
            Activite activiteParent = findById(activiteDto.getIdParent());
            activite.setParent(activiteParent);
        }

        activite.setDateDebut(debut.instant());
        activite.setDateFin(fin.instant());
        activite.setOrganisme(organisme);


        remplirPartiellementEntity(activiteDto, activite);
    }


    /**
     * Les dates / createur / organisme ne peuvent pas être modifié.
     *
     * @param activiteDto .
     * @param activite    .
     */
    private void remplirPartiellementEntity(ActiviteDto activiteDto, Activite activite) {
        TypeActivite typeActivite = typeActiviteService.findById(activiteDto.getTypeActivite().getId());
        activite.setNom(activiteDto.getNom());
        activite.setDescription(activiteDto.getDescription());
        activite.setTypeActivite(typeActivite);

        activite.setRappelNbJourAvantDebut(activiteDto.getRappelNbJourAvantDebut());
        activite.setRappelInApp(activiteDto.getRappelInApp());
        activite.setRappelEmail(activiteDto.getRappelEmail());

        if (activite.getRappelNbJourAvantDebut() != null) {
            SuperDate rappel = new SuperDate(activite.getDateDebut());
            rappel.debutDeLaJournee();
            rappel.jourSuivant(-activite.getRappelNbJourAvantDebut());
            activite.setDateEnvoieRappel(rappel.instant());
        }

        activite.setRappelParticipationNbJourAvantDebut(activiteDto.getRappelParticipationNbJourAvantDebut());
        activite.setRappelParticipationInApp(activiteDto.getRappelParticipationInApp());
        activite.setRappelParticipationEmail(activiteDto.getRappelParticipationEmail());

        if (activite.getRappelParticipationNbJourAvantDebut() != null) {
            SuperDate rappelParticipation = new SuperDate(activite.getDateDebut());
            rappelParticipation.debutDeLaJournee();
            rappelParticipation.jourSuivant(-activite.getRappelParticipationNbJourAvantDebut());
            activite.setDateEnvoieRappelParticipation(rappelParticipation.instant());
        }
    }

    private void creerEffectif(ActiviteDto activiteDto, Activite activite) {
        if (activiteDto.getEffectifs() != null) {
            activiteDto.getEffectifs().forEach(effectif -> effectifService.creerEffectif(effectif, activite));
        }
    }

    public List<ActiviteDto> getListByIdOrganismeBeetween(Long idOrganisme, String dateDebut, String dateFin,
                                                          List<Long> idsTypeActivite,
                                                          List<Long> idsGroupe,
                                                          @Nullable String filtre) {

        Organisme organisme = organismeService.findById(idOrganisme);

        List<TypeActivite> typeActivites = typeActiviteService.getListById(idsTypeActivite);

        List<ActiviteDto> activitesDto = getListActiviteEntre(organisme, dateDebut, dateFin, typeActivites, filtre);

        remplirEffectifEtStatutEtAvancement(activitesDto);

        remplirParticipantsGroupe(activitesDto, idsGroupe);

        return activitesDto;
    }

    private void remplirParticipantsGroupe(Collection<ActiviteDto> activites, List<Long> idsGroupe) {
        if (idsGroupe != null) {
            activites.forEach(activiteDto -> activiteDto.setParticipantsGroupe(getParticipants(activiteDto, idsGroupe)));
        }
    }

    public List<MoisActivite> getActiviteEtDispoAnnuel(Long idOrganisme, String dateDebut, List<Long> idsTypeActivite) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee().jourDuMois(1).moisSuivant(-3);


        Organisme organisme = organismeService.findById(idOrganisme);

        List<TypeActivite> typeActivites = typeActiviteService.getListById(idsTypeActivite);

        SuperDate fin = debut.copie().moisSuivant(12).debutDeLaJournee();
        List<ActiviteSimpleDto> activites = getActiviteSimpleAnnuel(idOrganisme, debut, organisme, typeActivites, fin);
        List<DisponibiliteDto> disponibilites = disponibiliteService.getListByUtilisateurConnecterBeetween(debut, fin);

        SuperDate increment = debut.copie();

        List<MoisActivite> resultat = creationMois(fin, increment);

        fin = fin.finDeLaSemaine();
        increment = debut.copie().debutDeLaSemaine();

        List<SemaineActivite> semaines = createSemaine(fin, increment, resultat);

        semaines.forEach(semaineActivite -> semaineActivite.add(activites, disponibilites));


        return resultat;
    }

    private List<ActiviteSimpleDto> getActiviteSimpleAnnuel(Long idOrganisme, SuperDate debut, Organisme organisme, List<TypeActivite> typeActivites, SuperDate fin) {
        List<ActiviteSimpleDto> activites;

        if (CollectionUtils.isEmpty(typeActivites)) {
            activites = repository.findDtoSimpleListBeetween(idOrganisme, debut.instant(), fin.instant());
        } else {
            activites = repository.findDtoSimpleListBeetween(organisme.getId(), debut.instant(), fin.instant(), Tools.getListId(typeActivites));
        }
        return activites;
    }

    @NotNull
    private List<SemaineActivite> createSemaine(SuperDate fin, SuperDate increment, List<MoisActivite> resultat) {
        List<SemaineActivite> semaines = new ArrayList<>();

        while (increment.estAvantOuEgale(fin)) {
            SemaineActivite semaine = new SemaineActivite(increment);
            increment.jourSuivant(7);
            resultat.forEach(moisActivite -> moisActivite.add(semaine));
            semaines.add(semaine);
        }
        return semaines;
    }

    private List<MoisActivite> creationMois(SuperDate fin, SuperDate increment) {
        List<MoisActivite> listMois = new ArrayList<>();
        while (!increment.estEgaleJMA(fin)) {
            MoisActivite mois = new MoisActivite(increment);
            listMois.add(mois);
            increment.moisSuivant();
        }
        return listMois;
    }

    private List<ActiviteCorpsStatutCountDto> compterParticipantParStatutCompletion(List<Long> idsActivite) {
        return repository.compterParticipantParStatutCompletion(idsActivite);
    }

    private void remplirStatutForCompletion(Iterable<ActiviteDto> activitesDto) {
        StatutDto statut = statutService.findDtoForCompletion();
        activitesDto.forEach(
                activites -> activites.setStatutCompletion(statut)
        );

    }

    private void remplirCompletion(Iterable<ActiviteDto> activitesDto, List<ActiviteCorpsStatutCountDto> countCompletion) {
        activitesDto.forEach(
                activites -> activites.ajouterCompletion(countCompletion)
        );
    }

    private void remplirEffectif(Iterable<ActiviteDto> activitesDto, List<EffectifDto> effectifDemander) {
        effectifDemander.forEach(
                effectif -> activitesDto.forEach(
                        activiteDto -> activiteDto.add(effectif)
                )
        );
    }

    private List<ActiviteDto> getListActiviteEntre(Organisme organisme, String dateDebut, String dateFin,
                                                   List<TypeActivite> typeActivites, String filtre) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        List<ActiviteDto> activitesDto;
        Optional<String> filtreOptional = Optional.empty();

        if (Tools.testerNulliteString(filtre)) {
            filtreOptional = Optional.of(filtre.trim().toLowerCase());
        }

        if (CollectionUtils.isEmpty(typeActivites)) {
            activitesDto = repository.findDtoListBeetween(organisme.getId(), debut.instant(), fin.instant(), filtreOptional);
        } else {
            activitesDto = repository.findDtoListBeetween(organisme.getId(), debut.instant(), fin.instant(), Tools.getListId(typeActivites), filtreOptional);
        }
        return activitesDto;
    }


    public List<ActiviteDto> getListByIdUtilisateurBeetween(Long idUtilisateur, String dateDebut, String dateFin,
                                                            List<Long> idsTypeActivite, Boolean isMyActivities, List<Long> idsGroupe, String filtre) {
        UtilisateurCompletDto utilisateur = utilisateurService.findDtoById(idUtilisateur);
        List<ActiviteDto> activites = getListByIdOrganismeBeetween(utilisateur.getAffectation().getOrganisme().getId(), dateDebut, dateFin,
                idsTypeActivite, idsGroupe, filtre);
        remplirMaParticipation(activites, utilisateur);
        if (isMyActivities != null && isMyActivities) {
            activites = filtreMesActivites(activites);
        }
        remplirDispoActivite(activites, getAutresActivites(activites, dateDebut, dateFin, utilisateur), idUtilisateur);
        return activites;
    }

    private List<ActiviteDto> filtreMesActivites(List<ActiviteDto> activites) {
        return activites.stream()
                .filter(activite -> !activite.getParticipants().isEmpty())
                .collect(Collectors.toList());
    }

    private List<ActiviteDto> getListAutresActivitesByIdUtilisateurBeetween(UtilisateurCompletDto utilisateur,
                                                                            String dateDebut, String dateFin, List<Long> idsTypeActivite) {
        List<ActiviteDto> activites = getListByIdOrganismeBeetween(utilisateur.getAffectation().getOrganisme().getId(), dateDebut, dateFin, idsTypeActivite, null, null);
        remplirMaParticipation(activites, utilisateur);
        return activites;
    }

    private List<ActiviteDto> getAutresActivites(List<ActiviteDto> activites, String dateDebut, String dateFin,
                                                 UtilisateurCompletDto utilisateur) {
        SuperDate dateDebutAutreActivite = SuperDate.createFromIsoDateTime(dateDebut);
        SuperDate dateFinAutreActivite = SuperDate.createFromIsoDateTime(dateFin);
        for (ActiviteDto activite : activites) {
            if (activite.getDateDebut().isBefore(dateDebutAutreActivite.instant())) {
                dateDebutAutreActivite = new SuperDate(activite.getDateDebut());
            }
            if (activite.getDateFin().isAfter(dateFinAutreActivite.instant())) {
                dateFinAutreActivite = new SuperDate(activite.getDateFin());
            }
        }
        return getListAutresActivitesByIdUtilisateurBeetween(utilisateur,
                dateDebutAutreActivite.instant().toString(), dateFinAutreActivite.instant().toString(), null);
    }

    private void remplirDispoActivite(List<ActiviteDto> activites, List<ActiviteDto> autresActivites, Long idUtilisateur) {
        for (ActiviteDto activite : activites) {
            if (!participeAActivite(activite) && !participeAAutreActivite(activite, autresActivites)) {
                activite.setEstDispo(isUtilisateurDisponible(activite, idUtilisateur));
            }
        }
    }

    private boolean participeAAutreActivite(ActiviteDto activite, List<ActiviteDto> autresActivites) {
        SuperDate dtDebutActivite = new SuperDate(activite.getDateDebut());
        SuperDate dtFinActivite = new SuperDate(activite.getDateFin());
        for (ActiviteDto autreActivite : autresActivites) {
            if (!autreActivite.getId().equals(activite.getId())) {
                SuperDate dtDebutAutreActivite = new SuperDate(autreActivite.getDateDebut());
                SuperDate dtFinAutreActivite = new SuperDate(autreActivite.getDateFin());
                if ((dtDebutActivite.estAvantOuEgale(dtFinAutreActivite) && dtFinActivite.estApresOuEgale(dtDebutAutreActivite))) {
                    if (!autreActivite.getParticipants().isEmpty()) {
                        if (!autreActivite.getParticipants().get(0).getDernierAvancement().getChoix().getTypeDisponibilite().getEstDisponible()) {
                            activite.setEstDispo(false);
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private boolean participeAActivite(ActiviteDto activite) {
        if (!activite.getParticipants().isEmpty() && activite.getParticipants().get(0).getDernierAvancement() != null) {
            activite.setEstDispo(!activite.getParticipants().get(0).getDernierAvancement().getChoix().getTypeDisponibilite().getEstDisponible());
            return true;
        }
        return false;
    }

    private void remplirMaParticipation(List<ActiviteDto> activites, UtilisateurCompletDto utilisateur) {
        List<Long> idsActivite = Tools.getListIdDto(activites);
        if (!CollectionUtils.isEmpty(idsActivite)) {
            List<ParticiperDto> participations = participerService.getListParticipation(idsActivite, utilisateur.getId());
            activites.forEach(activite -> activite.addParticipants(participations));
        }
    }

    public ActiviteDto findDtoById(Long idActivite) {
        ActiviteDto activiteDto = findDtoByIdOnly(idActivite);
        List<EffectifDto> effectifDemander = effectifService.findEffectifDemander(idActivite);
        activiteDto.setEffectifs(effectifDemander);

        StatutDto statut = statutService.findDtoForCompletion();

        activiteDto.setStatutCompletion(statut);

        List<ActiviteCorpsStatutCountDto> countCompletion = repository.compterParticipantParStatutCompletion(idActivite);
        activiteDto.ajouterCompletion(countCompletion);

        return activiteDto;
    }

    public ActiviteDto findDtoByIdParent(Long idActivite) {
        ActiviteDto activiteDto = findDtoByIdOnly(idActivite);
        ActiviteDto activiteDtoParent = findDtoByIdOnly(activiteDto.getIdParent());
        ProfilDto profil = profilService.findDtoByCode("super_employeur");
        activiteDtoParent.getOrganisme().setContacts((utilisateurService.getUtilisateurByOrganismeAndProfil(
                activiteDtoParent.getOrganisme().getId(), profil.getId())));
        return activiteDtoParent;
    }

    public ActiviteDto findDtoByIdFromParent(String token) {
        ActiviteDto activiteDto = findDtoByIdOnlyFromParent(token);
        activiteDto.setIdParent(activiteDto.getId());
        ProfilDto profil = profilService.findDtoByCode("super_employeur");
        activiteDto.getOrganisme().setContacts((utilisateurService.getUtilisateurByOrganismeAndProfil(
                activiteDto.getOrganisme().getId(), profil.getId())));
        activiteDto.setId(null);
        return activiteDto;
    }

    public ActiviteDto findDtoByIdOnlyFromParent(String token) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingIdFromParent(token), getEntityClassName());
    }

    public ActiviteDto findDtoByIdOnly(Long idActivite) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idActivite), getEntityClassName());
    }

    public ActiviteDto modifierPartiellement(Long idActivite, ActiviteDto activiteDto) {

        MetiisValidation.verifier(activiteDto, ContextEnum.UPDATE_ACTIVITE_PARTIEL);

        Activite activite = findById(idActivite);
        remplirPartiellementEntity(activiteDto, activite);
        saveOrUpdate(activite);

        effectifService.deleteByActivite(activite);

        creerEffectif(activiteDto, activite);

        alerteService.informerActiviteModifiee(activite);

        for (Participer participant: activite.getParticipers()) {
            mailService.envoyerMailModificationActivite(activite, participant.getUtilisateur());
        }

        return findDtoById(idActivite);

    }

    public ActiviteDto modifier(Long idActivite, ActiviteDto activiteDto) {

        MetiisValidation.verifier(activiteDto, ContextEnum.UPDATE_ACTIVITE);

        Activite activite = findById(idActivite);
        remplirCompletementEntity(activiteDto, activite);
        saveOrUpdate(activite);

        effectifService.deleteByActivite(activite);

        creerEffectif(activiteDto, activite);

        alerteService.informerActiviteModifiee(activite);

        for (Participer participant: activite.getParticipers()) {
            mailService.envoyerMailModificationActivite(activite, participant.getUtilisateur());
        }

        return findDtoById(idActivite);
    }

    public void supprimer(Long id, String commentaire) {
        List<Activite> activitesEnfants = repository.findAllEnfantsUsingId(id);
        activitesEnfants.forEach(activiteEnfant -> {
            alerteService.informerActiviteSupprimee(activiteEnfant, commentaire);
            delete(activiteEnfant.getId());
        });
        Activite activite = findById(id);
        if (activite.getParent() != null) {
            Activite activiteParent = findById(activite.getParent().getId());
            alerteService.informerParentActiviteSupprimee(activite, activiteParent, commentaire);
        }
        alerteService.informerActiviteSupprimee(activite, commentaire);

        for (Participer participant: activite.getParticipers()) {
            mailService.envoyerMailSuppressionActivite(activite, participant.getUtilisateur());
        }

        delete(id);
    }

    public List<ActiviteDto> findDtoActiviteWithUtilisateurParticipeNonDispo(DisponibiliteDto disponibilite, String currentUsername) {
        UtilisateurCompletDto utilisateurCompletDto = utilisateurService.findDtoByUserName(currentUsername);
        DisponibiliteDate dates = DisponibiliteDate.getInstance(disponibilite);
        return repository.findDtoActiviteWithUtilisateurParticipeNonDispo(
                utilisateurCompletDto.getId(),
                utilisateurCompletDto.getAffectation().getOrganisme().getId(),
                dates.debutToInstant(),
                dates.finToInstant());
    }

    public Page<ActiviteDto> findDtoAllByIdOrganisme(Long idOrganisme, List<Long> idsTypeActivite, Pageable pageable) {

        Organisme organisme = organismeService.findById(idOrganisme);
        List<TypeActivite> typeActivites = typeActiviteService.getListById(idsTypeActivite);

        Page<ActiviteDto> activites = getListActivite(organisme, typeActivites, pageable);

        remplirEffectifEtStatutEtAvancement(activites.getContent());

        return activites;
    }

    private void remplirEffectifEtStatutEtAvancement(Collection<ActiviteDto> activites) {
        List<Long> idsActivite = Tools.getListIdDto(activites);

        if (!CollectionUtils.isEmpty(idsActivite)) {
            remplirStatutForCompletion(activites);

            List<EffectifDto> effectifDemander = effectifService.findEffectifDemander(idsActivite);
            remplirEffectif(activites, effectifDemander);

            List<ActiviteCorpsStatutCountDto> countCompletion = compterParticipantParStatutCompletion(idsActivite);
            remplirCompletion(activites, countCompletion);

        }

    }

    public Page<ActiviteDto> findDtoAllByIdUtilisateur(Long idUtilisateur, List<Long> idsTypeActivite, Pageable pageable) {
        UtilisateurCompletDto utilisateurDto = utilisateurService.findDtoById(idUtilisateur);
        return findDtoAllByIdOrganisme(utilisateurDto.getAffectation().getOrganisme().getId(), idsTypeActivite, pageable);
    }

    private Page<ActiviteDto> getListActivite(Organisme organisme, List<TypeActivite> typeActivites, Pageable pageable) {

        Page<ActiviteDto> activitesDto;

        if (CollectionUtils.isEmpty(typeActivites)) {
            activitesDto = repository.findDtoAllUsingIdOrganisme(organisme.getId(), pageable);
        } else {
            activitesDto = repository.findDtoAllUsingIdOrganisme(organisme.getId(), Tools.getListId(typeActivites), pageable);
        }
        return activitesDto;
    }


    public ListFilterCorps listePersonnelDisponiblePourUneActiviteFiltrer(Long idActivite, Long idCorps, Long idGroupePersonnaliser) {
        ActiviteDto activiteDto = findDtoByIdOnly(idActivite);

        return filterService.getFilter(OptionListFilterCorps.byCorps(idCorps).andAddGroupes(idGroupePersonnaliser))
                .add(listePersonnelDisponiblePourUneActivite(activiteDto));
    }

    public ListFilterCorps listePersonnelDisponiblePourUneActiviteFiltrer(Long idOrganisme, String dateDebut, String dateFin, Long idCorps) {

        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();

        OrganismeDto organismeDto = organismeService.findDtoById(idOrganisme);
        List<Long> idsUtilisateurs = createFilterDisponibilite(organismeDto.getId(), new SuperDate(debut), new SuperDate(fin), null)
                .getIdsUtilisateurDispoAllDays();

        return filterService.getFilter(OptionListFilterCorps.byCorps(idCorps))
                .add(getUtilisateurCompletDtos(idsUtilisateurs));
    }

    private List<UtilisateurActiviteDetailsDto> listePersonnelDisponiblePourUneActivite(ActiviteDto activiteDto) {
        List<Long> idsUtilisateurs = createFilterDisponibilite(activiteDto)
                .getIdsUtilisateurDispoAllDays();

        return getUtilisateurCompletDtos(idsUtilisateurs);
    }

    private List<UtilisateurActiviteDetailsDto> listePersonnelNonDisponiblePourUneActivite(ActiviteDto activiteDto) {
        List<Long> idsUtilisateurs = createFilterDisponibilite(activiteDto)
                .getIdsUtilisateursNonDisponiblesPourActivite();

        return getUtilisateurCompletDtos(idsUtilisateurs);
    }

    private List<UtilisateurActiviteDetailsDto> getUtilisateurCompletDtos(List<Long> idsUtilisateurs) {
        List<UtilisateurActiviteDetailsDto> resultat = new ArrayList<>();
        if (!idsUtilisateurs.isEmpty()) {
            List<UtilisateurCompletDto> utilisateurCompletDtos = utilisateurService.findDtoByIdIn(idsUtilisateurs);
            utilisateurCompletDtos.forEach(
                    utilisateurCompletDto ->
                            resultat.add(new UtilisateurActiviteDetailsDto(utilisateurCompletDto))
            );
        }
        return resultat;
    }

    private ListActiviteDisponibiliteUtilisateurFiltreDto createFilterDisponibilite(ActiviteDto activiteDto) {

        SuperDate debut = new SuperDate(activiteDto.getDateDebut());
        SuperDate fin = new SuperDate(activiteDto.getDateFin());

        //log.debug("actvite  : " + activiteDto);

        return createFilterDisponibilite(activiteDto.getOrganisme().getId(), debut, fin, activiteDto.getId());


    }

    private ListActiviteDisponibiliteUtilisateurFiltreDto createFilterDisponibilite(Long idOrganisme, SuperDate debut, SuperDate fin, Long idActivite) {

        List<DisponibiliteDto> disponibiliteDtos = disponibiliteService.getListByIdOrganismeBeetween(idOrganisme, debut, fin);

        List<UtilisateurCompletDto> participantsActivites = utilisateurService.findDtoAllParticipantActiviteNonDisponibleBetween(idOrganisme, debut, fin, idActivite);

        List<UtilisateurCompletDto> utilisateursDeLUnite = utilisateurService.findDtoAllEmployeByOrganisme(idOrganisme);

        ListActiviteDisponibiliteUtilisateurFiltreDto filtre = new ListActiviteDisponibiliteUtilisateurFiltreDto(debut, fin, null);

        filtre.addUtilisateurOrganisme(utilisateursDeLUnite)
                .addDisponibilite(disponibiliteDtos)
                .addUtilisateurParticipantsActiviteNonDisponible(participantsActivites)
                .filtrer();

        return filtre;

    }


    public ListFilterCorps personnelsTrierParStatutPourUneActivite(Long idActivite, Long idStatut, Long idCorps, Long idGroupePersonnaliser) {

        ActiviteDto activite = findDtoByIdOnly(idActivite);
        StatutDto statut = statutService.findDtoById(idStatut);

        return personnelsTrierParStatutPourUneActivite(activite, statut, idCorps, idGroupePersonnaliser);
    }

    public ListFilterCorps personnelsTrierParStatutPourUneActivite(ActiviteDto activite, StatutDto statut, Long idCorps, Long idGroupePersonnaliser) {
        List<WithCorpsDto> resultats = new ArrayList<>(getParticipants(activite, statut));
        return filterService.getFilter(OptionListFilterCorps.byCorps(idCorps).andAddGroupes(idGroupePersonnaliser)).add(resultats);
    }

    private List<UtilisateurActiviteDetailsDto> getParticipants(ActiviteDto activite, StatutDto statut) {
        List<UtilisateurActiviteDetailsDto> resultats = utilisateurService.findDtoParticipantActiviteByStatut(activite.getId(), statut.getId());
        if (statut.afficherDispoIndispo() != null) {
            resultats.addAll(ajouterDisponibiliteEtIndisponibilite(statut, activite));
        }
        ajouterAutresActivitesEtCommentaires(activite, resultats);
        return resultats;
    }

    private List<UtilisateurActiviteDetailsDto> getParticipants(ActiviteDto activite, List<Long> idsGroupe) {
        return utilisateurService.findDtoParticipantActivite(activite.getId(), idsGroupe);
    }

    private void ajouterAutresActivitesEtCommentaires(ActiviteDto activite, List<UtilisateurActiviteDetailsDto> utilisateurActiviteDetailsDtos) {
        SuperDate debut = new SuperDate(activite.getDateDebut()).debutDeLaJournee();
        SuperDate fin = new SuperDate(activite.getDateFin()).finDeLaJournee();
        utilisateurActiviteDetailsDtos.forEach(utilisateur -> {
            List<ActiviteDto> activiteDtos = repository.findDtoActiviteWithParticipationAutreActivite(utilisateur.getIdUtilisateur(),
                    activite.getOrganisme().getId(), activite.getId(), debut.instant(), fin.instant());
            remplirEffectifEtStatutEtAvancement(activiteDtos);
            remplirMaParticipation(activiteDtos, utilisateur.getUtilisateur());
            if (!activiteDtos.isEmpty()) {
                utilisateur.setAutresActivites(activiteDtos);
            }
            List<UtilisateurActiviteDetailsDto> commentaires = getListeCommentaire(activite.getId(), utilisateur.getIdUtilisateur());
            if (!commentaires.isEmpty()) {
                utilisateur.setCommentaires(commentaires);
            }
        });
    }

    private List<UtilisateurActiviteDetailsDto> ajouterDisponibiliteEtIndisponibilite(StatutDto statut, ActiviteDto activiteDto) {
        if (Boolean.TRUE.equals(statut.afficherDispoIndispo())) {
            return listePersonnelDisponiblePourUneActivite(activiteDto);
        }
        return listePersonnelNonDisponiblePourUneActivite(activiteDto);
    }

    public List<UtilisateurActiviteDetailsDto> getListeCommentaire(Long idActivite) {
        return utilisateurService.getListCommentaireActivite(idActivite);
    }

    public List<UtilisateurActiviteDetailsDto> getListeCommentaire(Long idActivite, String currentUserName) {
        Utilisateur utilisateur = utilisateurService.findByUserName(currentUserName);
        return utilisateurService.getListCommentaireActivite(idActivite, utilisateur.getId());
    }

    public List<UtilisateurActiviteDetailsDto> getListeCommentaire(Long idActivite, Long idUtilisateur) {
        Utilisateur utilisateur = utilisateurService.findById(idUtilisateur);
        return utilisateurService.getListCommentaireActivite(idActivite, utilisateur.getId());
    }

    public Page<ActiviteDto> findDtoAll(Pageable pageable) {
        Page<ActiviteDto> activites = repository.findDtoAll(pageable);
        remplirEffectifEtStatutEtAvancement(activites.getContent());

        return activites;
    }

    public List<ActiviteDto> getEnfants(Long idActivite) {
        List<ActiviteDto> activites = repository.findDtoAllEnfantsUsingId(idActivite);
        remplirEffectifEtStatutEtAvancement(activites);
        remplirContact(activites);
        return activites;
    }

    public void remplirContact(Collection<ActiviteDto> activites) {
        activites.forEach(activiteDto -> {
            ProfilDto profil = profilService.findDtoByCode("super_employeur");
            activiteDto.getOrganisme().setContacts((utilisateurService.getUtilisateurByOrganismeAndProfil(
                    activiteDto.getOrganisme().getId(), profil.getId())));
        });
    }

    public List<EtapeStatutDto> listParticipantByEtape(Long idActivite, Long idEtape, Long idGroupe) {
        ActiviteDto activiteDto = findDtoByIdOnly(idActivite);
        List<EtapeStatutDto> etapeStatutDtos = etapeStatutService.findDtoAllByIdEtape(idEtape, null);

        etapeStatutDtos.forEach(etapeStatutDto ->
                etapeStatutDto.setParticipants(personnelsTrierParStatutPourUneActivite(activiteDto, etapeStatutDto.getStatut(), null, idGroupe)));

        return etapeStatutDtos;
    }

    public List<RechercheParticipantDto> rechercherParticipants(Long idActivite, String search) {
        List<RechercheParticipantDto> resultat = new ArrayList<>();
        if (Tools.testerNulliteString(search)) {

            ActiviteDto activiteDto = findDtoByIdOnly(idActivite);
            SuperDate debut = new SuperDate(activiteDto.getDateDebut()).debutDeLaJournee();
            SuperDate fin = new SuperDate(activiteDto.getDateFin()).finDeLaJournee();

            Long duree = debut.compterNombreDeJourDebutInclus(fin);

            List<EtapeStatutDto> etapeStatuts = etapeStatutService.findDtoWithStatutDisponible(null);

            resultat = repository.rechercherParticipants(idActivite, '%' + search + '%', debut.instant(), fin.instant());
            resultat.forEach(rechercheParticipantDto -> {
                rechercheParticipantDto.setDureeActivite(duree);
                rechercheParticipantDto.setEtapeStatutWithDisponibilite(etapeStatuts);
            });
        }
        return resultat;

    }

    public boolean isUtilisateurDisponible(ActiviteDto activite, Long idUtilisateur) {
        DisponibiliteDate dates = DisponibiliteDate.getInstance(activite.getDateDebut(), activite.getDateFin());
        List<Disponibilite> dispos = disponibiliteService.findListEstDisponibleBeetween(idUtilisateur,
                dates.debutToInstant(), dates.finToInstant(), true);
        return dispos.size() == dates.debut().compterNombreDeJourDebutInclus(dates.fin());
    }

    @Override
    protected void checkBeforeDelete(Activite activite) {
        super.checkBeforeDelete(activite);
        alerteService.supprimerReferencesAUneActiviteDansLesAlertes(activite.getId());
    }

    public Page<ActiviteDto> findDtoActiviteWithParticipation(Long idUtilisateur, Pageable pageable) {
        UtilisateurCompletDto utilisateur = utilisateurService.findDtoById(idUtilisateur);
        Page<ActiviteDto> pages = repository.findDtoActiviteWithParticipation(idUtilisateur, utilisateur.getAffectation().getOrganisme().getId(), pageable);
        remplirEffectifEtStatutEtAvancement(pages.getContent());
        remplirMaParticipation(pages.getContent(), utilisateur);
        return pages;
    }

    public Page<ActiviteDto> findDtoActiviteWithParticipationEntre(Long idUtilisateur, String dateDebut, String dateFin, Pageable pageable) {
        UtilisateurCompletDto utilisateur = utilisateurService.findDtoById(idUtilisateur);
        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);
        Page<ActiviteDto> pages = repository.findDtoActiviteWithParticipationEntre(idUtilisateur,
                dates.debutToInstant(), dates.finToInstant(), pageable);
        remplirEffectifEtStatutEtAvancement(pages.getContent());
        remplirMaParticipation(pages.getContent(), utilisateur);
        return pages;
    }

    public void envoyerRappelActivite() {
        List<Activite> activites = repository.findByDateRappel(new SuperDate().debutDeLaJournee().instant());
        for (Activite activite : activites) {
            envoyerRappelEmail(activite);
            envoyerRappelInApp(activite);
        }
    }

    private void envoyerRappelInApp(Activite activite) {
        if (activite.getRappelInApp()) {
            alerteService.envoyerRappelActivite(activite);
        }
    }

    public void envoyerRappelParticipationActivite() {
        List<Activite> activites = repository.findByDateRappel(new SuperDate().debutDeLaJournee().instant());
        for (Activite activite : activites) {
            envoyerRappelParticipationEmail(activite);
            envoyerRappelParticipationInApp(activite);
        }
    }

    private void envoyerRappelParticipationInApp(Activite activite) {
        if (activite.getRappelParticipationInApp()) {
            alerteService.envoyerRappelParticipationActivite(activite);
        }
    }

    public List<UtilisateurActiviteDetailsDto> getListeCommentaireDeLemploye(Long idActivite, Long idUtilisateur, Long idEtape) {
        Utilisateur utilisateur = utilisateurService.findById(idUtilisateur);
        return utilisateurService.getListeCommentaireDeLemploye(idActivite, utilisateur.getId(), idEtape);
    }


    private void envoyerRappelEmail(Activite activite) {
        if (activite.getRappelEmail()) {
            List<Utilisateur> nonParticipants = utilisateurService.findNonPartipantsActivite(activite.getId());
            nonParticipants.forEach(utilisateur -> mailService.envoyerRappelActivite(utilisateur, activite));
        }
    }

    private void envoyerRappelParticipationEmail(Activite activite) {
        if (activite.getRappelEmail()) {
            List<Utilisateur> participants = utilisateurService.findPartipantsActivite(activite.getId());
            participants.forEach(utilisateur -> mailService.envoyerRappelParticipationActivite(utilisateur, activite));
        }
    }

    public List<StatutParticipantDto> getListeForDetailsParticipation(Long idActivite) {
        ActiviteDto activite = findDtoByIdOnly(idActivite);
        List<StatutDto> statuts = statutService.findDtoForDetails();
        List<StatutParticipantDto> statutParticipants = statuts.stream().map(StatutParticipantDto::new).collect(Collectors.toList());
        statutParticipants.forEach(statutParticipantDto -> statutParticipantDto.setParticipants(getParticipants(activite, statutParticipantDto.getStatut())));
        return statutParticipants;
    }

    public NombrePaxDto compteurNombrePax(Long idOrganisme) {
        List<Corps> corpsList = corpsService.findAll();
        List<Long> nombrePax = new ArrayList<>();
        if (idOrganisme != null) {
            for (Corps corps : corpsList) {
                nombrePax.add(utilisateurService.compterNombrePaxUnite(idOrganisme, corps.getId()));
            }
        }
        return new NombrePaxDto(nombrePax);
    }

    public Long getIdOrganisme(Long idActivite) {
        return repository.getIdOrganisme(idActivite);
    }

    public Long compterActiviteRealise(String dateDebut, String dateFin, Long idUtilisateur, Long idTypeActivite) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        return repository.compterActiviteRealise(debut.instant(), fin.instant(), idUtilisateur, idTypeActivite);
    }

    public Long compterActiviteConvoque(String dateDebut, String dateFin, Long idUtilisateur, Long idTypeActivite) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        return repository.compterActiviteConvoque(debut.instant(), fin.instant(), idUtilisateur, idTypeActivite);
    }

    public Long compterCandidature(String dateDebut, String dateFin, Long idUtilisateur) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        return repository.compterCandidature(debut.instant(), fin.instant(), idUtilisateur);
    }

    public Long compterConvoque(String dateDebut, String dateFin, Long idUtilisateur) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        return repository.compterConvoque(debut.instant(), fin.instant(), idUtilisateur);
    }

    public List<ActiviteDto> findDTOAllActiviteRealise(String dateDebut, String dateFin, Long idUtilisateur) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        return repository.findDTOAllActiviteRealise(debut.instant(), fin.instant(), idUtilisateur);
    }

    public List<ActiviteDto> findDTOAllActiviteConvoque(String dateDebut, String dateFin, Long idUtilisateur) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        return repository.findDTOAllActiviteConvoque(debut.instant(), fin.instant(), idUtilisateur);
    }

    public ActiviteDto findLastActivite(Long idUtilisateur, Long idTypeActivite) {
        return repository.findLastActivite(idUtilisateur, idTypeActivite);
    }

    public ActiviteDto partager(Long idActivite, List<OrganismePartageDto> organismesDto, String currentUserName) {
        ActiviteDto activiteDto = findDtoById(idActivite);
        Activite activite = findById(idActivite);
        ProfilDto profil = profilService.findDtoByCode("super_employeur");
        organismesDto.forEach(organismePartageDto -> {
            List<UtilisateurCompletDto> superEmployeurs = utilisateurService.getUtilisateurByOrganismeAndProfil(
                    organismePartageDto.getId(), profil.getId());
            superEmployeurs.forEach(utilisateurCompletDto -> {
                mailService.envoyerMailPartage(utilisateurCompletDto, activiteDto, organismePartageDto);
                alerteService.informerDemandeRenfort(activiteDto, activite.getCreateur().getId(), utilisateurCompletDto.getId());
            });
        });

        return activiteDto;
    }
}