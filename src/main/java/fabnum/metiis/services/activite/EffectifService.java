package fabnum.metiis.services.activite;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.domain.activite.Effectif;
import fabnum.metiis.domain.rh.Corps;
import fabnum.metiis.dto.activite.EffectifDto;
import fabnum.metiis.repository.activite.EffectifRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.rh.CorpsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EffectifService extends AbstarctService<EffectifRepository, Effectif, Long> {

    private CorpsService corpsService;

    public EffectifService(
            CorpsService corpsService,

            EffectifRepository repository) {

        super(repository);
        this.corpsService = corpsService;
    }


    public void creerEffectif(EffectifDto effectifDto, Activite activite) {
        MetiisValidation.verifier(effectifDto, ContextEnum.CREATE_EFFECTIF);

        Corps corps = corpsService.findById(effectifDto.getCorps().getId());

        Effectif effectif = new Effectif();
        effectif.setActivite(activite);
        effectif.setCorps(corps);
        effectif.setNombre(effectifDto.getNombre());

        saveOrUpdate(effectif);

    }

    public List<EffectifDto> findEffectifDemander(List<Long> idsActivite) {
        return repository.findAllUsedActiviteIdIn(idsActivite);
    }

    public List<EffectifDto> findEffectifDemander(Long idActivite) {
        return repository.findAllUsedActiviteId(idActivite);
    }

    public void deleteByActivite(Activite activite) {
        activite.getEffectifs().forEach(this::delete);
        repository.flush();
    }
}
