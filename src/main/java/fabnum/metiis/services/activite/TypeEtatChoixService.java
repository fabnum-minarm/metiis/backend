package fabnum.metiis.services.activite;

import fabnum.metiis.domain.activite.TypeEtatChoix;
import fabnum.metiis.repository.activite.TypeEtatChoixRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TypeEtatChoixService extends AbstarctServiceSimple<TypeEtatChoixRepository, TypeEtatChoix, Long> {


    public TypeEtatChoixService(
            TypeEtatChoixRepository repository) {

        super(repository);

    }


}
