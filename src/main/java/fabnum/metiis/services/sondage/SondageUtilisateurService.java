package fabnum.metiis.services.sondage;

import fabnum.metiis.constante.Order;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.domain.sondage.Sondage;
import fabnum.metiis.domain.sondage.SondageUtilisateur;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.sondage.ReponseSondageDto;
import fabnum.metiis.dto.sondage.SondageDto;
import fabnum.metiis.repository.sondage.SondageRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.rh.OrganismeService;
import fabnum.metiis.services.rh.UtilisateurService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class SondageUtilisateurService extends AbstarctService<SondageRepository, SondageUtilisateur, Long> {

    private UtilisateurService utilisateurService;
    private SondageService sondageService;
    private OrganismeService organismeService;

    public SondageUtilisateurService(UtilisateurService utilisateurService, SondageService sondageService,
                                     OrganismeService organismeService, SondageRepository repository) {
        super(repository);
        this.utilisateurService = utilisateurService;
        this.sondageService = sondageService;
        this.organismeService = organismeService;
    }

    public void creer(SondageDto sondageDto, String currentUsername) {

        Sondage sondage = sondageService.getDernierSondage();
        Utilisateur utilisateur = utilisateurService.findByUserName(currentUsername);
        SondageUtilisateur sondageUtilisateur = new SondageUtilisateur();
        sondageUtilisateur.setUtilisateur(utilisateur);
        sondageUtilisateur.setSondage(sondage);
        sondageUtilisateur.setReponse(sondageDto.getReponse());

        saveOrUpdate(sondageUtilisateur);
    }

    public Page<ReponseSondageDto> findDtoDashboardAll(Long idOrganisme, Integer page, Integer size, String sortBy, String order, String filtre, String reponse) {
        SondageDto sondageDto = sondageService.getDernierSondageDto();
        if (idOrganisme != null) {
            List<Long> idsOrganisme = new ArrayList<>();
            List<OrganismeDto> lstOrg = organismeService.findAllOrganismeAffectableByParent(idOrganisme);
            lstOrg.forEach(organismeDto -> idsOrganisme.add(organismeDto.getId()));
            return repository.findDtoDashboardAllSort(idsOrganisme, getPaging(page, size, sortBy, order), filtre.toUpperCase(), sondageDto.getId(), reponse);
        } else {
            return repository.findDtoDashboardAllSort(getPaging(page, size, sortBy, order), filtre.toUpperCase(), sondageDto.getId(), reponse);
        }
    }

    private Pageable getPaging(Integer page, Integer size, String sortBy, String order) {
        Pageable paging = PageRequest.of(page, size, Sort.by(sortBy));
        if (Order.ASC.equals(order)) {
            paging = PageRequest.of(page, size, Sort.by(sortBy).ascending());
        }
        if (Order.DESC.equals(order)) {
            paging = PageRequest.of(page, size, Sort.by(sortBy).descending());
        }
        return paging;
    }

    public List<String> getDistintReponses() {
        SondageDto sondageDto = sondageService.getDernierSondageDto();
        return repository.findDistictReponses(sondageDto.getId());
    }
}
