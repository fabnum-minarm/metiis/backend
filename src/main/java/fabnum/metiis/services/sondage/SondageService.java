package fabnum.metiis.services.sondage;

import fabnum.metiis.domain.sondage.Sondage;
import fabnum.metiis.dto.sondage.SondageDto;
import fabnum.metiis.repository.sondage.SondageRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SondageService extends AbstarctService<SondageRepository, Sondage, Long> {

    public SondageService(SondageRepository repository) {
        super(repository);
    }

    public Sondage getDernierSondage() {
        return repository.findDernierSondage();
    }

    public SondageDto getDernierSondageDto() {
        return repository.findDtoById(getDernierSondage().getId());
    }

}
