package fabnum.metiis.services.mail;

public class HTMLMail {

    private static final String OBJET = "METIIS : ";

    private static final String FONT_FAMILY = "font-family: 'Source Sans Pro', sans-serif;";

    public static final String HEAD = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional //EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">" +
            "<html xmlns:v=\"urn:schemas-microsoft-com:vml\">" +
            "<head>" +
            "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\"/>" +
            "<meta name=\"viewport\" content=\"width=device-width; initial-scale=1.0; maximum-scale=1.0;\" />" +
            "<link href=\"https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300&display=swap\" rel=\"stylesheet\" />" +
            "</head>" +
            "<body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">";

    public static final String END = "</body>" +
            "</html>";


    public static final String TR = "<tr>";
    public static final String TR_END = "</tr>";

    public static final String TD = "<td>";
    public static final String TD_END = "</td>";

    public static final String BR = "<br/>";

    public static final String CENTER = "center";

    public static final String HEADER_MSG = "" +
            startTable("", CENTER) +
            TR +
            TD +
            startTable("align=\"" + CENTER + "\" width=\"500px\"") +
            TR +
            tdVide("height=\"52px\"") +
            TR_END +
            TR +
            TD +
            startTable("", CENTER) +
            TR +
            TD + ImageAttachement.LOGO_MIN_ARM.toHtmlImage("left") + TD_END +
            TD + ImageAttachement.LOGO_METIIS.toHtmlImage("right") + TD_END +
            TR_END +
            endTable() +
            TD_END +
            TR_END +
            TR +
            tdVide("height=\"66px\"") +
            TR_END;

    private static final String CORPS = "" +
            TR +
            TD +
            "<h1 style=\"" + FONT_FAMILY + " color:#666ee8; font-size: 34px;  text-align: left;\">" +
            var(Var.TITRE) +
            "</h1> " +
            TD_END +
            TR_END +
            TR +
            tdVide("height=\"15px\"") +
            TR_END +
            TR +
            "<td >" +
            p("60666a", "justify", "15") +
            "Bonjour " + var(Var.PRENOM) + " " + var(Var.NOM) + "," +
            BR + BR +
            var(Var.MESSAGE) +
            BR + BR +
            "À bientôt," +
            BR +
            "L’équipe METIIS." +
            "</p>"
            + TD_END +
            TR_END;

    private static final String BUTTON = "" +
            TR +
            tdVide("height=\"36px\"") +
            TR_END +
            TR +
            "<td style=\"text-align: center;\">" +
            startTable("", CENTER) +
            TR +
            "<td >" +
            "<a href=\"" + var(Var.BASE_URL) + var(Var.URI) + "\" " +
            "style=\"" + FONT_FAMILY + " background-color:#666ee8; padding:10px 40px; width:170px; color:#ffffff; text-align: center; text-decoration: none; \"> " +
            "<b>" +
            var(Var.BOUTTON) +
            "</b> " +
            "</a>" +
            TD_END +
            TR_END +
            endTable() +
            TD_END +
            TR_END +
            TR +
            tdVide("height=\"50px\"") +
            TR_END +
            endTable();

    public static final String BANIERE = "" +
            TR +
            TD +
            startTable("", "") +
            "<tr " + ImageAttachement.BANNIERE.toHtmlBackground() + ">" +
            tdVide() +
            "<td >" +
            p("ffffff", CENTER, "21") +
            "<b>« Un lien qui transforme <br/>" +
            "le potentiel de la réserve en une force <br/>" +
            "pour l’Armée de Terre. »<br/></b>" +
            "</p>" +
            TD_END +
            tdVide() +
            TR_END +
            tdVide("height=\"20px\" style=\"background-color: #fafbfd;\"") +
            tdVide("height=\"20px\" style=\"background-color: #fafbfd;\"") +
            tdVide("height=\"20px\" style=\"background-color: #fafbfd;\"") +
            TR_END +
            TR +
            tdVide("height=\"50px\" style=\"background-color: #fafbfd;\"") +
            "<td height=\"50px\" style=\"background-color: #fafbfd;\"> " +
            p("#5f5f5f", CENTER, "15") +
            "<b>Nous contacter</b><br/>" +
            var(Var.EMAIL_METIIS) +
            "</p>" +
            TD_END +
            tdVide("height=\"50px\" style=\"background-color: #fafbfd;\"") +
            TR_END +
            TR +
            tdVide("height=\"20px\" style=\"background-color: #fafbfd;\"") +
            tdVide("height=\"20px\" style=\"background-color: #fafbfd;\"") +
            tdVide("height=\"20px\" style=\"background-color: #fafbfd;\"") +
            TR_END +
            endTable() +
            TD_END +
            TR_END;

    public static final String FOOTER_MSG = "" +
            TR +
            TD +
            startTable("117px", "") +
            TR +
            tdVide("width=\"35%\"") +
            "<td align=\"center\" >" + ImageAttachement.LOGO_DIRISI.toHtmlImage(CENTER) + TD_END +
            "<td align=\"center\" >" + ImageAttachement.LOGO_FABNUM.toHtmlImage(CENTER) + TD_END +
            "<td align=\"center\" >" + ImageAttachement.LOGO_COM_TN.toHtmlImage(CENTER) + TD_END +
            TR_END +

            endTable() +
            TD_END +
            TR_END +
            endTable();


    private String email = HEAD + HEADER_MSG + CORPS + BUTTON + BANIERE + FOOTER_MSG + END;
    private String objet = OBJET;

    public static String startTable(String height, String align) {
        return startTable("height=\"" + height + "\" align=\"" + align + "\" width=\"100%\"");
    }

    public static String startTable(String param) {
        return "<table " + param + " border=\"0\" cellpadding=\"0\" cellspacing=\"0\">" +
                "<tbody>";
    }

    public static String endTable() {
        return "</tbody>" +
                "</table>";
    }

    public static String tdVide(String param) {
        return "<td " + param + "></td>";
    }

    public static String tdVide() {
        return tdVide("");
    }

    public static String var(String name) {
        return "{{" + name + "}}";
    }


    public static String p(String texteColor, String texteAlign, String texteSize) {
        return "<p style=\"" + FONT_FAMILY +
                " text-align: " + texteAlign +
                "; color:#" + texteColor +
                "; font-size: " + texteSize + "px;\">";
    }

    public HTMLMail addObject(String value) {
        objet += value;
        return this;
    }

    public HTMLMail replace(String code, String value) {
        email = email.replace(var(code), value);
        return this;
    }

    public HTMLMail replace(String code, int value) {
        return replace(code, Integer.toString(value));
    }

    public String message() {
        return email;
    }

    public String objet() {
        return objet;
    }


    public static final class Var {
        public static final String MESSAGE = "message";
        public static final String BASE_URL = "baseurl";
        public static final String URI = "uri";
        public static final String BOUTTON = "boutton";
        public static final String NOM = "nom";
        public static final String PRENOM = "prenom";
        public static final String TITRE = "titre";
        public static final String MDP_TEMP = "motdepassetemp";
        public static final String LOGIN = "login";
        public static final String EMAIL_METIIS = "emailmetiis";
        public static final String DUREE = "duree";
        public static final String NOM_ACTIVITE = "nomactivite";
        public static final String DEBUT = "debut";
        public static final String FIN = "fin";
        public static final String TYPE_ACTIVITE = "typeactivite";
        public static final String ACTION = "action";
        public static final String COMMENTAIRE = "commentaire";
        public static final String NOUVEAU_MAIL = "nouveaumail";
        public static final String UNITE = "unite";
    }

    public static final class Object {

        public static final String BIENVENUE = "Bienvenue sur METIIS";

        public static final String COMPTE_BLOQUE = "Votre compte a été bloqué";
        public static final String COMPTE_DESACTIVE = "Votre compte a été désactivé";
        public static final String COMPTE_ACTIVE = "Votre compte a été réactivé";

        public static final String REINITIALISER_MDP = "Réinitialisation de votre mot de passe ";

        public static final String MDP_MODIFIE = "Votre mot de passe a été modifié";
        public static final String BIENVENUE_INVITATION = BIENVENUE + BR + "Invitation à nous rejoindre.";
        public static final String BIENVENUE_CONFIRMATION_INSCRIPTION = BIENVENUE + BR + "Confirmation de votre inscription.";
        public static final String BIENVENUE_MODIF_INFOS_PERSO_NOUVELLE_ADRESSE = "Confirmer votre adresse e-mail";
        public static final String BIENVENUE_MODIF_INFOS_PERSO_ANCIENNE_ADRESSE = "Votre changement d'adresse e-mail";

        public static final String TENTATIVE_CONNEXION = "Tentative de connexion ";

        public static final String CREATION_ACTIVITE = "Une nouvelle activité a été créée";
        public static final String CREATION_ACTIVITE_PARTAGE = "Une activité liée à la votre a été créée";
        public static final String CREATION_ACTIVITE_PARTAGE_TITRE = "L'unité " + var(Var.UNITE) + " a créé une activité liée à la vôtre";

        public static final String MODIFICATION_ACTIVITE = "Une activité a été modifiée";

        public static final String SUPPRESSION_ACTIVITE = "Une activité a été supprimée";

        public static final String RAPPEL_ACTIVITE = "Rappel : Une activité commence bientôt";
        public static final String RAPPEL_PARTICIPATION_ACTIVITE = "Rappel : N'oubliez pas votre participation à l'activité";

        public static final String PARTAGE_ACTIVITE = "Demande de renfort";
        public static final String PARTAGE_ACTIVITE_TITRE = "L'unité " + var(Var.UNITE) + " a besoin de renfort";
    }

    public static final class Message {


        public static final String INVITATION = "" +
                "<b>Votre OAR, votre Commandant d’Unité ou votre Chef de Section vous invite " + BR +
                " à activer votre compte METIIS en cliquant sur le lien ci-dessous. " + BR +
                "Vous disposez d’un délai de " + var(Var.DUREE) + " jours avant que le lien ne soit inactif.</b>" + BR +
                " METIIS est une plateforme dédiée aux réservistes de l’Armée de Terre qui" + BR +
                " vous permettra de déclarer vos disponibilités et de visualiser les activités" + BR +
                " de votre Unité.";

        public static final String MODIF_INFO_PERSO_NOUVELLE_ADRESSE = "" +
                "Pour confirmer la modification de vos informations personnelles, <b> veuillez " + BR +
                "cliquer sur le lien ci-dessous afin de confirmer votre adresse e-mail.</b>" + BR +
                "En changeant votre adresse e-mail vous avez aussi changé votre identifiant de connexion à METIIS." + BR +
                "Si vos informations de connexion sont pré-enregistrées dans votre navigateur, vous devez aussi les changer." + BR +
                "Vous disposez d’un délai de " + var(Var.DUREE) + " heures avant que ce lien ne soit inactif.";

        public static final String MODIF_INFO_PERSO_ANCIENNE_ADRESSE = "" +
                "Vous avez récemment modifié votre adresse e-mail de votre compte METIIS." + BR +
                "Pour confirmer cette modification, <b>consultez " + var(Var.NOUVEAU_MAIL) + BR +
                "pour vérifier et terminer la mise à jour." + BR + BR +
                "Vous n'avez pas demandé cette modification ?</b>" + BR +
                "Si vous n'avez pas demandé de modification de votre adresse e-mail," + BR +
                "informez-nous immédiatement à l'adresse suivante metiis@fabnum.fr.";

        /**
         * Utilise la variable {{@link HTMLMail.Var}.LOGIN}.
         */
        public static final String COMPTE_VALIDE = "" +
                "Nous vous informons que votre compte a bien été validé." + BR +
                "<b>Vos identifiants de connexion sont les suivants :</b>" + BR +
                "Identifiant : " + var(Var.LOGIN) + BR +
                "Mot de passe : ********" + BR;

        public static final String COMPTE_BLOQUE = "" +
                "À raison du trop grand nombre de tentatives de connexion infructueuses," +
                " votre compte est désormais bloqué." + BR +
                "<b>Pour pouvoir vous connecter de nouveau, vous devez réinitialiser votre" +
                " mot de passe en cliquant sur le lien ci dessous.";

        public static final String COMPTE_ACTIVE = "" +
                "Votre compte METIIS a été réactivé, vous pouvez à nouveau vous connecter à l'application.";

        public static final String COMPTE_DESACTIVE = "" +
                "Votre compte METIIS a été désactivé, vous ne pouvez plus vous connecter à l'application." + BR +
                "Pour le faire réactiver veillez contacter votre hiérarchie.";

        public static final String REINITIALISER_MDP = "" +
                "<b>Vous avez demandé la réinitialisation de votre mot de passe.</b>" + BR +
                "<b>Cliquer sur le lien pour changer votre mot de passe.</b>" + BR +
                "Vous disposez d’un délai de 24h avant que le lien ne soit inactif.</b>" + BR +
                "Si vous n'avez pas fait cette demande vous n'avez rien à faire.";

        public static final String MDP_MODIFIE = "<b>Votre mot de passe a bien été mis à jour.</b>" + BR +
                "Si vous n'avez pas fait cette demande, veuillez réinitialiser votre mot de passe immédiatement";

        public static final String TENTATIVE_CONNEXION = "Un certain nombre de tentatives de connexion infructueuses " +
                "ont été effectuées sur votre compte." + BR +
                "Si vous n'avez pas tenté de vous connecter, veuillez contacter votre " +
                "hiérarchie et/ou l'administrateur.";

        /**
         * Utilise la variable :
         * <ul>
         * <li>{{@link HTMLMail.Var}.TYPE_ACTIVITE}</li>
         * <li>{{@link HTMLMail.Var}.NOM_ACTIVITE}</li>
         * <li>{{@link HTMLMail.Var}.DEBUT}</li>
         * <li>{{@link HTMLMail.Var}.FIN}</li>
         * </ul>
         */

        public static final String NOUVELLE_ACTIVITE = "" +
                "L’activité suivante est désormais disponible :" + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." + BR + BR +
                "N'oubliez pas de déclarer vos disponibilités ou vos indisponibilités depuis votre calendrier. Il vous est également possible de candidater à l'activité.";
        public static final String NOUVELLE_ACTIVITE_PARTAGE = "" +
                "L'unité " + var(Var.UNITE) + " a répondu à votre demande de partage. Elle a créé une activité liée à la vôtre :" + BR + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." + BR + BR +
                "Vous pouvez désormais voir les participants de cette unité sur votre activité.";
        public static final String MODIFICATION_ACTIVITE = "" +
                "L'activité suivante a été modifiée :" + BR +
                "<b> « " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")";
        public static final String SUPPRESSION_ACTIVITE = "" +
                "L'activité suivante a été supprimée :" + BR +
                "<b> « " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")";
        public static final String RAPPEL_ACTIVITE = "" +
                "Cette activité commence bientôt :" + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." + BR + BR +
                "N'oubliez pas de déclarer vos disponibilités ou vos indisponibilités depuis votre calendrier. Il vous est également possible de candidater à l'activité.";

        public static final String RAPPEL_PARTICIPATION_ACTIVITE = "" +
                "N’oubliez pas que vous êtes convoqué à l’activité suivante :" + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." + BR + BR +
                "N'oubliez pas de vous présenter le jour de l'activité.";


        public static final String MA_PARTICIPATION = "" +
                var(Var.PRENOM) + " " + var(Var.NOM) + " " + var(Var.ACTION) + " à l'activité suivante :" + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." +
                var(Var.COMMENTAIRE);

        public static final String PARTICIPATION = "" +
                var(Var.ACTION) + " à l'activité suivante :" + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." +
                var(Var.COMMENTAIRE);

        public static final String PARTICIPATION_REALISE = "" +
                var(Var.ACTION) + " suivante :" + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." +
                var(Var.COMMENTAIRE);

        public static final String PARTAGE = "" +
                "L'unité " + var(Var.UNITE) + " a besoin de renfort sur l'activité suivante :" + BR + BR +
                "<b>« " + var(Var.NOM_ACTIVITE) + " »</b> (" + var(Var.TYPE_ACTIVITE) + ")" + BR +
                "qui débute le <b>" + var(Var.DEBUT) + "</b> et se termine le <b>" + var(Var.FIN) + "</b>." +
                var(Var.COMMENTAIRE) + BR + BR +
                "Pour gérer les participants de votre unité vous devez dupliquer l'activité dans votre calendrier.";

        public static final String CANDIDATURE = "" +
                BR + BR +
                "<b>Vous pouvez choisir de retenir ou non sa candidature dans la page de gestion des participants de l’activité.</b>" + BR + BR +
                "<b>- Le statut « Retenu » lui indique que vous souhaitez qu’il participe à l’activité</b> mais que son dossier est à l’étude. " +
                "Vous pourrez le retrouver dans la colonne « Retenus »." + BR + BR +
                "<b>- Le statut « Non retenu » lui indique que vous ne souhaitez pas qu’il participe à l’activité.</b> " +
                "Vous pourrez le retrouver dans la colonne « Non retenus ». Il vous sera quand même possible de le retenir sous réserve qu’il soit encore disponible.";

        public static final String RETENU = "" +
                BR + BR +
                "<b>Le statut « Retenu » indique que votre employeur souhaite que vous participiez à l’activité mais que votre dossier est encore à l’étude.</b>" + BR + BR +
                "Vous recevrez un mail pour vous informer si vous êtes ou non convoqué (selon si votre dossier le permet), c’est-à-dire si vous participez officiellement à l’activité." + BR + BR +
                "Vous pouvez encore décider d’annuler votre participation directement depuis l’activité en cliquant sur le bouton « Annuler ma participation ». " +
                "Vous devrez alors saisir un motif de désinscription valable.";

        public static final String NON_RETENU = "" +
                BR + BR +
                "<b>Le statut « Non retenu » indique que votre employeur ne souhaite pas que vous participiez à l’activité.</b>";

        public static final String CONVOQUE = "" +
                BR + BR +
                "<b>Le statut « Convoqué » indique que votre participation à l’activité est validée. Votre convocation est en cours de création et en cours d’envoi.</b>" + BR + BR +
                "Si, pour une raison exceptionnelle vous souhaitez annuler votre participation à cette activité, vous devez obligatoirement prendre contact avec votre supérieur hiérarchique.";

        public static final String NON_CONVOQUE = "" +
                BR + BR +
                "<b>Le statut « Non convoqué » indique que vous ne pouvez pas participer à l’activité.</b>";

        public static final String REALISEE = "" +
                BR + BR +
                "<b>Le statut « Activité réalisée » indique que le compte-rendu d’activité (CRA) est en cours de réalisation et que vous allez prochainement percevoir votre solde.</b>";

        public static final String NON_REALISEE = "" +
                BR + BR +
                "<b>Le statut « Activité non réalisée » indique que vous ne comptez pas dans l’effectif ayant participé à l’activité.</b>" + BR + BR +
                "A l’avenir, si pour une raison exceptionnelle vous ne pouvez pas participer à une activité, nous vous conseillons de prendre contact avec votre supérieur hiérarchique.";
    }

    public static final class Boutton {
        public static final String ACCEDER_AU_SITE = "ACCÉDER AU SITE";
        public static final String REINITIALISER_MDP = "RÉINITIALISER MON MOT DE PASSE";
        public static final String ACTIVER_COMPTE = "ACTIVER MON COMPTE";
        public static final String VISUALISER_ACTIVITE = "VISUALISER L’ACTIVITÉ";
        public static final String CONFIRMER_MAIL = "CONFIRMER L'ADRESSE E-MAIL";

    }
}
