package fabnum.metiis.services.mail;

import fabnum.metiis.config.ApplicationProperties;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.domain.activite.Participer;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.contacter.Contact;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.ChoisirDataEntity;
import fabnum.metiis.dto.alerte.ParticipationDataEntity;
import fabnum.metiis.dto.rh.OrganismePartageDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.services.notification.NotificationService;
import fabnum.metiis.services.rh.TokenUtilisateurService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.repository.query.parser.Part;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Service d'envoi de mail.
 * Pour l'envoie de mail asynchrone, il faut que ce soit la methode qui est appeler en dehors de MailService qui soit Async.
 */
@Service
@Slf4j
@AllArgsConstructor
public class MailService {


    private static final String INVITATION_URI = "/invitation/";
    private static final String MODIF_INFOS_PERSO_URI = "/confirmation-infos-perso/";
    private static final String NEW_PWD_URI = "/changement-mdp/";
    private static final String ACTVITE_URI = "/activite-redirect/";
    private static final String ACTVITE_PARTAGE_URI = "/activite-redirect-partage/";
    private JavaMailSender javaMailSender;

    private ApplicationProperties applicationProperties;

    private NotificationService notificationService;


    /**
     * Envoi d'un message simple, de façon asynchnone.
     *
     * @param subject : objet du mail
     * @param content : contenu
     * @param to      : destinataires (ellispe d'adresses)
     */
    @Async
    public void sendAsyncText(String subject, String content, String... to) {
        sendText(subject, content, to);
    }

    /**
     * Envoi d'un message simple, de façon synchnone.
     *
     * @param subject : objet du mail
     * @param content : contenu
     * @param to      : destinataires (ellispe d'adresses)
     */
    public void sendText(String subject, String content, String... to) {
        sendEmail(to, subject, content, false, true);
    }

    /**
     * Methode d'evoie de mail deja cronstruis.
     */
    private void envoieMailPrecree(HTMLMail htmlMail, String... to) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, StandardCharsets.UTF_8.name());

            message.setTo(to);
            message.setFrom(applicationProperties.getMail().getFrom());
            message.setSubject(htmlMail.objet());
            message.setText(htmlMail.message(), true);

            addImages(message);

            javaMailSender.send(mimeMessage);
            //System.out.println(javaMailSender.toString());

            log.debug(Tools.message("mail.send.success", String.join(", ", to)));
        } catch (Exception e) {
            log.error(Tools.message("mail.send.error", String.join(", ", to)), e);
        }
    }


    private void addImages(MimeMessageHelper message) throws IOException, MessagingException {
        for (ImageAttachement imageAttachement : ImageAttachement.values()) {
            message.addInline(imageAttachement.getCid(), new FileSystemResource(new ClassPathResource(imageAttachement.getFullUrl()).getFile()));
        }
    }

    /**
     * Méthode principale d'envoi.
     *
     * @param to          : destinataires
     * @param subject     : objet du mail
     * @param content     : contenu
     * @param isMultipart : permet de savoir si le mail peut transmettre des pièces jointes.
     * @param isHtml      : permet de savoir si le contenu doit être affiché au format HTML
     */
    private void sendEmail(String[] to, String subject, String content, boolean isMultipart, boolean isHtml) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(applicationProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            //System.out.println(javaMailSender.toString());
            javaMailSender.send(mimeMessage);
            log.debug(Tools.message("mail.send.success", String.join(", ", to)));
        } catch (Exception e) {
            log.error(Tools.message("mail.send.error", String.join(", ", to)), e);
        }
    }

    private void sendEmailFrom(String to, String subject, String content, String from, String personnal, boolean isMultipart, boolean isHtml) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(to, personnal);
            message.setReplyTo(from, personnal);
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug(Tools.message("mail.send.success", String.join(", ", to)));
        } catch (Exception e) {
            log.error(Tools.message("mail.send.error", String.join(", ", to)), e);
        }
    }

    private void mail(Utilisateur utilisateur, String objet, String message, String nomBoutton, String uri) {
        HTMLMail htmlMail = getHtmlMail(utilisateur.getNom(), utilisateur.getPrenom(), objet, objet, message, nomBoutton, uri);

        envoieMailPrecree(htmlMail, utilisateur.getEmail());
    }

    private void mail(User user, HTMLMail htmlMail) {
        envoieMailPrecree(htmlMail, user.getEmail());
    }


    private void mail(Utilisateur utilisateur, HTMLMail htmlMail) {
        envoieMailPrecree(htmlMail, utilisateur.getEmail());
    }

    private void mailNouvelleAdresse(Utilisateur utilisateur, HTMLMail htmlMail) {
        envoieMailPrecree(htmlMail, utilisateur.getNewEmail());
    }

    private HTMLMail getHtmlMail(Utilisateur utilisateur, String objet, String titre, String message, String nomBoutton, String uri) {
        return getHtmlMail(utilisateur.getNom(), utilisateur.getPrenom(), objet, titre, message, nomBoutton, uri);
    }

    private HTMLMail getHtmlMail(User user, String objet, String titre, String message, String nomBoutton, String uri) {
        return getHtmlMail(user.getLastname(), user.getFirstname(), objet, titre, message, nomBoutton, uri);
    }

    private HTMLMail getHtmlMail(String nom, String prenom, String objet, String titre, String message, String nomBoutton, String uri) {
        return new HTMLMail()
                .addObject(objet)
                .replace(HTMLMail.Var.TITRE, titre)
                .replace(HTMLMail.Var.NOM, nom)
                .replace(HTMLMail.Var.PRENOM, prenom)
                .replace(HTMLMail.Var.MESSAGE, message)
                .replace(HTMLMail.Var.BOUTTON, nomBoutton)
                .replace(HTMLMail.Var.URI, uri)
                .replace(HTMLMail.Var.BASE_URL, applicationProperties.getFront().getUrl())
                .replace(HTMLMail.Var.EMAIL_METIIS, applicationProperties.getMail().getFrom());
    }


    @Async
    public void mailContact(Contact contact) {
        String message = "Message reçu depuis le formulaire de contact de <a href=\"mailto:" + contact.getEmail() + "\">"
                + contact.getNom() + " " + contact.getPrenom() + " <" + contact.getEmail() + "></a><br><br>"
                + contact.getMessage().replace("\n", "<br>");
        sendEmailFrom(applicationProperties.getMail().getFrom(), contact.getObjet(), message, contact.getEmail(),
                contact.getNom() + " " + contact.getPrenom(), false, true);
    }

    @Async
    public void envoyerMailInvitation(Utilisateur utilisateur, String token) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.BIENVENUE,
                HTMLMail.Object.BIENVENUE_INVITATION,
                HTMLMail.Message.INVITATION,
                HTMLMail.Boutton.ACTIVER_COMPTE, INVITATION_URI + token);
        htmlMail.replace(HTMLMail.Var.DUREE, TokenUtilisateurService.DUREE_VALIDITE_INSCRIPTION);
        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoyerMailModificationInfosPerso(Utilisateur utilisateur, String token) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.BIENVENUE_MODIF_INFOS_PERSO_NOUVELLE_ADRESSE,
                HTMLMail.Object.BIENVENUE_MODIF_INFOS_PERSO_NOUVELLE_ADRESSE,
                HTMLMail.Message.MODIF_INFO_PERSO_NOUVELLE_ADRESSE,
                HTMLMail.Boutton.CONFIRMER_MAIL, MODIF_INFOS_PERSO_URI + token);
        htmlMail.replace(HTMLMail.Var.DUREE, TokenUtilisateurService.DUREE_VALIDITE_MODIF_INFOS_PERSO);
        mailNouvelleAdresse(utilisateur, htmlMail);
    }

    @Async
    public void envoyerMailModificationInfosPersoAncienneAdresse(Utilisateur utilisateur) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.BIENVENUE_MODIF_INFOS_PERSO_ANCIENNE_ADRESSE,
                HTMLMail.Object.BIENVENUE_MODIF_INFOS_PERSO_ANCIENNE_ADRESSE,
                HTMLMail.Message.MODIF_INFO_PERSO_ANCIENNE_ADRESSE,
                HTMLMail.Boutton.ACCEDER_AU_SITE, "");
        htmlMail.replace(HTMLMail.Var.NOUVEAU_MAIL, utilisateur.getNewEmail());
        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoyerMailConfirmationInscription(Utilisateur utilisateur) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.BIENVENUE,
                HTMLMail.Object.BIENVENUE_CONFIRMATION_INSCRIPTION,
                HTMLMail.Message.COMPTE_VALIDE,
                HTMLMail.Boutton.ACCEDER_AU_SITE, "");
        htmlMail.replace(HTMLMail.Var.LOGIN, utilisateur.getEmail());
        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoyerMailCompteBloque(Utilisateur utilisateur) {
        mail(utilisateur,
                HTMLMail.Object.COMPTE_BLOQUE,
                HTMLMail.Message.COMPTE_BLOQUE,
                HTMLMail.Boutton.REINITIALISER_MDP, "");
    }

    @Async
    public void envoyerMailCompteEnabled(Utilisateur utilisateur, boolean isEnabled) {
        if (isEnabled) {
            mail(utilisateur,
                    HTMLMail.Object.COMPTE_ACTIVE,
                    HTMLMail.Message.COMPTE_ACTIVE,
                    HTMLMail.Boutton.ACCEDER_AU_SITE, "");
        } else {
            mail(utilisateur,
                    HTMLMail.Object.COMPTE_DESACTIVE,
                    HTMLMail.Message.COMPTE_DESACTIVE,
                    HTMLMail.Boutton.ACCEDER_AU_SITE, "");
        }
    }

    @Async
    public void envoyerMailParticipation(ChoisirDataEntity choisirDataEntity, ParticipationDataEntity participationDataEntity) {
        String participation = HTMLMail.Message.PARTICIPATION;
        if (choisirDataEntity.getChoix().getCode().equals("activite-realisee") || choisirDataEntity.getChoix().getCode().equals("activite-non-realisee")) {
            participation = HTMLMail.Message.PARTICIPATION_REALISE;
        }
        String messageAdditionnel;
        switch (choisirDataEntity.getChoix().getCode()) {
            case "retenir":
                messageAdditionnel = HTMLMail.Message.RETENU;
                break;
            case "non-retenir":
                messageAdditionnel = HTMLMail.Message.NON_RETENU;
                break;
            case "convoquer":
                messageAdditionnel = HTMLMail.Message.CONVOQUE;
                break;
            case "non-convoquer":
                messageAdditionnel = HTMLMail.Message.NON_CONVOQUE;
                break;
            case "activite-realisee":
                messageAdditionnel = HTMLMail.Message.REALISEE;
                break;
            case "activite-non-realisee":
                messageAdditionnel = HTMLMail.Message.NON_REALISEE;
                break;
            default:
                messageAdditionnel = "";
                break;
        }
        if ((messageAdditionnel.equals(HTMLMail.Message.RETENU) && notificationService.findNotifUsingIdUtilisateurAndIdTypeActivite(participationDataEntity.getParticiper().getUtilisateur().getId(), choisirDataEntity.getActivite().getTypeActivite().getId()).getNotifMailCandidatRetenu())
                || (messageAdditionnel.equals(HTMLMail.Message.NON_RETENU) && notificationService.findNotifUsingIdUtilisateurAndIdTypeActivite(participationDataEntity.getParticiper().getUtilisateur().getId(), choisirDataEntity.getActivite().getTypeActivite().getId()).getNotifMailCandidatNonRetenu())
                || messageAdditionnel.equals(HTMLMail.Message.CONVOQUE) || messageAdditionnel.equals(HTMLMail.Message.NON_CONVOQUE)
                || messageAdditionnel.equals(HTMLMail.Message.REALISEE) || messageAdditionnel.equals(HTMLMail.Message.NON_REALISEE)){
            HTMLMail htmlMail = getHtmlMail(participationDataEntity.getParticiper().getUtilisateur(),
                    choisirDataEntity.getChoix().getStatut().getAction(),
                    choisirDataEntity.getChoix().getStatut().getAction(),
                    participation + messageAdditionnel,
                    HTMLMail.Boutton.VISUALISER_ACTIVITE,
                    "/" + choisirDataEntity.getActivite().getId() + ACTVITE_URI);

            emailParticipation(choisirDataEntity, htmlMail, participationDataEntity.getParticiper().getUtilisateur(), false);
        }
    }

    @Async
    public void envoyerMailModificationActivite(Activite activite, Utilisateur utilisateur) {

        if (notificationService.findNotifUsingIdUtilisateurAndIdTypeActivite(utilisateur.getId(), activite.getTypeActivite().getId()).getNotifMailActiviteModif()) {
            HTMLMail htmlMail = getHtmlMail(utilisateur,
                    HTMLMail.Object.MODIFICATION_ACTIVITE,
                    HTMLMail.Object.MODIFICATION_ACTIVITE,
                    HTMLMail.Message.MODIFICATION_ACTIVITE,
                    HTMLMail.Boutton.ACCEDER_AU_SITE, "");

            parametreActivite(activite, htmlMail);

            mail(utilisateur, htmlMail);
        }
    }

    @Async
    public void envoyerMailSuppressionActivite(Activite activite, Utilisateur utilisateur) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.SUPPRESSION_ACTIVITE,
                HTMLMail.Object.SUPPRESSION_ACTIVITE,
                HTMLMail.Message.SUPPRESSION_ACTIVITE,
                HTMLMail.Boutton.ACCEDER_AU_SITE, "");

        parametreActivite(activite, htmlMail);

        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoyerMailMaParticipation(ChoisirDataEntity choisirDataEntity) {
        String message = Tools.message("mail.ma.participation", choisirDataEntity.getValidateur().getPrenom(),
                choisirDataEntity.getValidateur().getNom(),
                choisirDataEntity.getChoix().getStatut().getActionUtilisateur());
        String messageAdditionnel = "";
        if (choisirDataEntity.getChoix().getCode().equals("candidater")) {
            messageAdditionnel = HTMLMail.Message.CANDIDATURE;
        }
        HTMLMail htmlMail = getHtmlMail(choisirDataEntity.getActivite().getCreateur(),
                message,
                message,
                HTMLMail.Message.MA_PARTICIPATION + messageAdditionnel,
                HTMLMail.Boutton.VISUALISER_ACTIVITE,
                "/" + choisirDataEntity.getActivite().getId() + ACTVITE_URI);

        emailParticipation(choisirDataEntity, htmlMail, choisirDataEntity.getActivite().getCreateur(), true);
    }

    private void emailParticipation(ChoisirDataEntity choisirDataEntity, HTMLMail htmlMail, Utilisateur destinataire, Boolean maParticipation) {
        parametreActivite(choisirDataEntity.getActivite(), htmlMail);
        parametreDataMail(choisirDataEntity, htmlMail, maParticipation);
        mail(destinataire, htmlMail);
    }

    @Async
    public void envoyerMailPartage(UtilisateurCompletDto utilisateurCompletDto, ActiviteDto activiteDto,
                                   OrganismePartageDto organismePartageDto) {
        String partage = HTMLMail.Message.PARTAGE;

        HTMLMail htmlMail = getHtmlMail(utilisateurCompletDto.getNom(), utilisateurCompletDto.getPrenom(),
                HTMLMail.Object.PARTAGE_ACTIVITE, HTMLMail.Object.PARTAGE_ACTIVITE_TITRE, HTMLMail.Message.PARTAGE,
                HTMLMail.Boutton.VISUALISER_ACTIVITE, "/" + activiteDto.getToken() + ACTVITE_PARTAGE_URI);

        parametreMailPartage(activiteDto, utilisateurCompletDto, organismePartageDto, htmlMail);
        envoieMailPrecree(htmlMail, utilisateurCompletDto.getEmail());
    }

    @Async
    public void envoyerMailDemandeNouveauMotDePasse(Utilisateur utilisateur, String token) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.REINITIALISER_MDP,
                HTMLMail.Object.REINITIALISER_MDP,
                HTMLMail.Message.REINITIALISER_MDP,
                HTMLMail.Boutton.REINITIALISER_MDP, NEW_PWD_URI + token);
        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoyerMailMotDePasseChanger(Utilisateur utilisateur) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.MDP_MODIFIE,
                HTMLMail.Object.MDP_MODIFIE,
                HTMLMail.Message.MDP_MODIFIE,
                HTMLMail.Boutton.ACCEDER_AU_SITE, "");
        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoiyerMailTentativeDeConnection(User user) {
        HTMLMail htmlMail = getHtmlMail(user,
                HTMLMail.Object.TENTATIVE_CONNEXION,
                HTMLMail.Object.TENTATIVE_CONNEXION,
                HTMLMail.Message.TENTATIVE_CONNEXION,
                HTMLMail.Boutton.ACCEDER_AU_SITE, "");
        mail(user, htmlMail);
    }

    @Async
    public void envoyerMailCreationActivite(Activite activite, Utilisateur utilisateur) {

        if (notificationService.findNotifUsingIdUtilisateurAndIdTypeActivite(utilisateur.getId(), activite.getTypeActivite().getId()).getNotifMailActiviteCree()){
            HTMLMail htmlMail = getHtmlMail(utilisateur,
                    HTMLMail.Object.CREATION_ACTIVITE,
                    HTMLMail.Object.CREATION_ACTIVITE,
                    HTMLMail.Message.NOUVELLE_ACTIVITE,
                    HTMLMail.Boutton.VISUALISER_ACTIVITE,
                    "/" + activite.getId() + ACTVITE_URI);

            parametreActivite(activite, htmlMail);

            mail(utilisateur, htmlMail);
        }
    }

    @Async
    public void envoyerMailCreationActivitePartage(Activite activite, Activite activiteParent) {
        Utilisateur utilisateur = activiteParent.getCreateur();
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.CREATION_ACTIVITE_PARTAGE,
                HTMLMail.Object.CREATION_ACTIVITE_PARTAGE_TITRE,
                HTMLMail.Message.NOUVELLE_ACTIVITE_PARTAGE,
                HTMLMail.Boutton.VISUALISER_ACTIVITE,
                "/" + activiteParent.getId() + ACTVITE_URI);

        parametreActivite(activiteParent, htmlMail);
        htmlMail.replace(HTMLMail.Var.UNITE, activite.getOrganisme().getLibelle());
        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoyerRappelActivite(Utilisateur utilisateur, Activite activite) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.RAPPEL_ACTIVITE,
                HTMLMail.Object.RAPPEL_ACTIVITE,
                HTMLMail.Message.RAPPEL_ACTIVITE,
                HTMLMail.Boutton.VISUALISER_ACTIVITE,
                "/" + activite.getId() + ACTVITE_URI);


        parametreActivite(activite, htmlMail);

        mail(utilisateur, htmlMail);
    }

    @Async
    public void envoyerRappelParticipationActivite(Utilisateur utilisateur, Activite activite) {
        HTMLMail htmlMail = getHtmlMail(utilisateur,
                HTMLMail.Object.RAPPEL_PARTICIPATION_ACTIVITE,
                HTMLMail.Object.RAPPEL_PARTICIPATION_ACTIVITE,
                HTMLMail.Message.RAPPEL_PARTICIPATION_ACTIVITE,
                HTMLMail.Boutton.VISUALISER_ACTIVITE,
                "/" + activite.getId() + ACTVITE_URI);


        parametreActivite(activite, htmlMail);

        mail(utilisateur, htmlMail);
    }

    private void parametreActivite(Activite activite, HTMLMail htmlMail) {
        htmlMail.replace(HTMLMail.Var.TYPE_ACTIVITE, activite.getTypeActivite().getLibelle())
                .replace(HTMLMail.Var.NOM_ACTIVITE, activite.getNom())
                .replace(HTMLMail.Var.DEBUT, new SuperDate(activite.getDateDebut()).toStringDDMMAAAA())
                .replace(HTMLMail.Var.FIN, new SuperDate(activite.getDateFin()).toStringDDMMAAAA());
    }

    private void parametreMailPartage(ActiviteDto activite, UtilisateurCompletDto utilisateurCompletDto,
                                      OrganismePartageDto organismePartageDto, HTMLMail htmlMail) {
        htmlMail.replace(HTMLMail.Var.TYPE_ACTIVITE, activite.getTypeActivite().getLibelle())
                .replace(HTMLMail.Var.NOM_ACTIVITE, activite.getNom())
                .replace(HTMLMail.Var.DEBUT, new SuperDate(activite.getDateDebut()).toStringDDMMAAAA())
                .replace(HTMLMail.Var.FIN, new SuperDate(activite.getDateFin()).toStringDDMMAAAA())
                .replace(HTMLMail.Var.NOM, utilisateurCompletDto.getNom())
                .replace(HTMLMail.Var.PRENOM, utilisateurCompletDto.getPrenom())
                .replace(HTMLMail.Var.UNITE, activite.getOrganisme().getLibelle());
        if (organismePartageDto.getCommentaire() != null) {
            htmlMail.replace(HTMLMail.Var.COMMENTAIRE, HTMLMail.BR + HTMLMail.BR + "<b>Commentaire :</b> «" +
                    organismePartageDto.getCommentaire() + "».");
        } else {
            htmlMail.replace(HTMLMail.Var.COMMENTAIRE, "");
        }
    }


    private void parametreDataMail(ChoisirDataEntity choisirDataEntity, HTMLMail htmlMail, Boolean maParticipation) {
        htmlMail.replace(HTMLMail.Var.NOM, choisirDataEntity.getValidateur().getNom())
                .replace(HTMLMail.Var.PRENOM, choisirDataEntity.getValidateur().getPrenom());
        if (choisirDataEntity.getDataForAlertes().get(0).getAvancementParticipation().getCommentaire() != null) {
            htmlMail.replace(HTMLMail.Var.COMMENTAIRE, HTMLMail.BR + HTMLMail.BR + "<b>Commentaire :</b> «" +
                    choisirDataEntity.getDataForAlertes().get(0).getAvancementParticipation().getCommentaire() + "».");
        } else {
            htmlMail.replace(HTMLMail.Var.COMMENTAIRE, "");
        }
        if (maParticipation) {
            htmlMail.replace(HTMLMail.Var.ACTION, choisirDataEntity.getChoix().getStatut().getActionUtilisateur());
        } else {
            htmlMail.replace(HTMLMail.Var.ACTION, choisirDataEntity.getChoix().getStatut().getAction());
        }
    }
}