package fabnum.metiis.services.mail;

import lombok.Getter;

public enum ImageAttachement {

    LOGO_MIN_ARM("logoMin", "logo-ministere-des-armees.png", "Logo Ministère", "84", "75"),
    LOGO_METIIS("logoMetiis", "metiis-logotype-2.png", "Logo Ministère", "93", "39"),
    BANNIERE("banniere", "banniere-mail-2.jpg", "Logo Ministère", "768", "230"),
    LOGO_DIRISI("logoDirisi", "logo-dirisi-developpement.png", "Logo Ministère", "81", "38"),
    LOGO_FABNUM("logoFabNum", "logo-fabrique-numerique.png", "Logo Ministère", "127", "41"),
    LOGO_COM_TN("logoSposor", "image-sposor.png", "Logo Ministère", "40", "53"),

    ;

    private static final String PX = "px";
    private static final String WIDTH = "width";
    private static final String HEIGHT = "height";
    private static final String SRC_IMAGES = "images/";

    @Getter
    private String cid;

    @Getter
    private String url;

    @Getter
    private String title;


    private String width;
    private String height;


    ImageAttachement(String cid, String url, String title, String width, String height) {
        this.cid = cid;
        this.url = url;
        this.title = title;
        this.width = width;
        this.height = height;
    }

    private String getMTLCid() {
        return "\"cid:" + cid + "\"";
    }

    private String getHTMLWith() {
        return WIDTH + "=\"" + width + PX + "\"";
    }

    private String getCssWith() {
        return WIDTH + ":" + width + PX + ";";
    }

    private String getHTMLHeight() {
        return HEIGHT + "=\"" + height + PX + "\"";
    }

    private String getCssHeight() {
        return HEIGHT + ":" + height + PX + ";";
    }

    public String getFullUrl() {
        return SRC_IMAGES + url;
    }

    public String toHtmlImage(String cssFloat) {
        return "<img src=" + getMTLCid() + "" +
                " style=\"" + getCssWith() + " " + getCssHeight() + " float: " + cssFloat + ";\"" +
                " border=\"0\"" +
                " alt=\"" + title + "\"/>";
    }

    public String toHtmlBackground() {
        return "background=" + getMTLCid() + "" +
                " " + getHTMLWith() +
                " " + getHTMLHeight() +
                " ";
    }

}
