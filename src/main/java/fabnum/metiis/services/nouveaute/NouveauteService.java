package fabnum.metiis.services.nouveaute;

import fabnum.metiis.domain.nouveaute.Nouveaute;
import fabnum.metiis.dto.nouveaute.NouveauteDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import fabnum.metiis.repository.nouveaute.NouveauteRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.rh.UtilisateurService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class NouveauteService extends AbstarctService<NouveauteRepository, Nouveaute, Long> {

    private UtilisateurService utilisateurService;

    public NouveauteService(NouveauteRepository repository,
                            UtilisateurService utilisateurService) {

        super(repository);
        this.utilisateurService = utilisateurService;
    }

    public List<NouveauteDto> findListDtoByUtilisateurLogin(String login) {
        UtilisateurDto utilisateur = utilisateurService.findDtoOnlyByUserName(login);
        return repository.findUsingIdUtilisateur(utilisateur.getId());
    }

    public void readAll(String login) {
        UtilisateurDto utilisateur = utilisateurService.findDtoOnlyByUserName(login);
        repository.readAll(utilisateur.getId());
    }
}
