package fabnum.metiis.services.rh;

import fabnum.metiis.config.EmailValidation;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.ToolsRepository;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.constante.Order;
import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.administration.UserProfil;
import fabnum.metiis.domain.rh.*;
import fabnum.metiis.dto.AuthenticationRequestDto;
import fabnum.metiis.dto.activite.UtilisateurActiviteDetailsDto;
import fabnum.metiis.dto.administration.ChangePasswordDto;
import fabnum.metiis.dto.administration.UserProfilDto;
import fabnum.metiis.dto.rh.*;
import fabnum.metiis.dto.statistique.StatistiqueGeneralDto;
import fabnum.metiis.dto.statistique.UtilisateurStatistiqueCountDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.dto.tools.OptionListFilterCorps;
import fabnum.metiis.exceptions.ConflictException;
import fabnum.metiis.exceptions.NotFoundException;
import fabnum.metiis.repository.rh.UtilisateurRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.administration.ProfilService;
import fabnum.metiis.services.administration.UserProfilService;
import fabnum.metiis.services.administration.UserService;
import fabnum.metiis.services.disponibilite.DisponibiliteDate;
import fabnum.metiis.services.exceptions.ServiceException;
import fabnum.metiis.services.mail.MailService;
import fabnum.metiis.services.notification.NotificationService;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class UtilisateurService extends AbstarctServiceSimple<UtilisateurRepository, Utilisateur, Long> {

    private UserService userService;
    private CorpsService corpsService;
    private AffectationService affectationService;
    private UserProfilService userProfilService;
    private FilterService filterService;
    private TokenUtilisateurService tokenUtilisateurService;
    private OrganismeService organismeService;
    private ProfilService profilService;
    private MailService mailService;
    private PosteRoService posteRoService;
    private GroupePersonnaliserService groupePersonnaliserService;
    private NotificationService notificationService;

    public UtilisateurService(UserService userService,
                              AffectationService affectationService,
                              UserProfilService userProfilService,
                              @Lazy FilterService filterService,
                              @Lazy TokenUtilisateurService tokenUtilisateurService,
                              @Lazy OrganismeService organismeService,
                              @Lazy ProfilService profilService,
                              MailService mailService,
                              @Lazy CorpsService corpsService,
                              @Lazy PosteRoService posteRoService,
                              @Lazy GroupePersonnaliserService groupePersonnaliserService,
                              @Lazy NotificationService notificationService,
                              UtilisateurRepository repository) {
        super(repository);
        this.userService = userService;
        this.corpsService = corpsService;
        this.affectationService = affectationService;
        this.userProfilService = userProfilService;
        this.filterService = filterService;
        this.organismeService = organismeService;
        this.tokenUtilisateurService = tokenUtilisateurService;
        this.profilService = profilService;
        this.mailService = mailService;
        this.posteRoService = posteRoService;
        this.groupePersonnaliserService = groupePersonnaliserService;
        this.notificationService = notificationService;
    }

    public UtilisateurCompletDto findDtoByUserName(String username) {

        UtilisateurCompletDto utilisateur = findDtoOnlyByUserName(username);

        remplirProfil(utilisateur);

        return utilisateur;
    }

    private void remplirProfil(UtilisateurCompletDto utilisateur) {
        utilisateur.setUserProfils(userProfilService.findDtoAllByIdUtilisateur(utilisateur.getId()));
    }

    public UtilisateurCompletDto findDtoOnlyByUserName(String username) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingUsername(username), getEntityClassName());
    }

    public Utilisateur findByUserName(String username) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findByUserUsernameIgnoreCase(username));
    }

    public List<UtilisateurCompletDto> findDtoAll() {
        return repository.findDtoAll();
    }

    public Page<UtilisateurCompletDto> findDtoAll(Pageable pageable) {
        return repository.findDtoAll(pageable);
    }

    public List<UtilisateurCompletDto> findDtoByIdIn(List<Long> id) {
        return repository.findDtoUsingIdIn(id);
    }

    public UtilisateurCompletDto findDtoById(Long id) {
        UtilisateurCompletDto utilisateurCompletDto =
                Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(id), getEntityClassName());
        remplirProfil(utilisateurCompletDto);

        return utilisateurCompletDto;
    }

    public UtilisateurCompletDto creerFromUtilisateurCompletDto(UtilisateurCompletDto utilisateurCompletDto) {

        MetiisValidation.verifier(utilisateurCompletDto, ContextEnum.CREATE);

        Tools.verifierFormatEtEgaliterMotDePasse(utilisateurCompletDto);

        userService.verifieUserName(utilisateurCompletDto.getUsername());

        Optional<Utilisateur> optionalUtilisateur = repository.findByEmail(utilisateurCompletDto.getEmail());
        Tools.testerExistance(optionalUtilisateur, "utilisateur.email.exist");
        Utilisateur utilisateur = new Utilisateur();
        getEntityFromUtilisateurCompletDto(utilisateur, utilisateurCompletDto);

        saveOrUpdate(utilisateur);

        affectationService.ajouterAffectationUtilisateur(utilisateur, utilisateurCompletDto);

        return findDtoById(utilisateur.getId());
    }

    public UtilisateurCompletDto modifierFromUtilisateurCompletDto(Long idUtilisateur, UtilisateurCompletDto utilisateurCompletDto) {
        MetiisValidation.verifier(utilisateurCompletDto, ContextEnum.UPDATE);
        verifieEmailExist(idUtilisateur, utilisateurCompletDto.getEmail());
        if (Tools.testerNulliteString(utilisateurCompletDto.getPassword())) {
            Tools.verifierFormatEtEgaliterMotDePasse(utilisateurCompletDto.getPassword(), utilisateurCompletDto.getPasswordconfirmation());
        }
        Utilisateur utilisateur = findById(idUtilisateur);
        userService.verifieUserNameExist(utilisateur.getUser().getId(), utilisateurCompletDto.getUsername());
        getEntityFromUtilisateurCompletDto(utilisateur, utilisateurCompletDto);
        saveOrUpdate(utilisateur);
        affectationService.modifierAffectationUtilisateur(utilisateur, utilisateurCompletDto);
        return findDtoById(utilisateur.getId());
    }

    public UtilisateurCompletDto cloturerAffectationUtilisateurCompletDto(Long idUtilisateur, UtilisateurCompletDto utilisateurCompletDto) {
        MetiisValidation.verifier(utilisateurCompletDto, ContextEnum.CLOTURER_AFFECTATION);
        Utilisateur utilisateur = findById(idUtilisateur);
        modifierUser(utilisateurCompletDto, utilisateur);
        affectationService.cloturerAffectation(affectationService.findDerniereAffectation(utilisateur.getId()), utilisateurCompletDto);
        return findDtoById(utilisateur.getId());
    }

    public void verifieEmailExist(long idUser, String email) {
        if (repository.existsByIdNotAndEmail(idUser, email)) {
            ConflictException ex = new ConflictException(Tools.message("utilisateur.email.exist"));
            ex.addHeader(HttpHeaders.LOCATION, idUser);
            throw ex;
        }
    }

    public UtilisateurCompletDto findAdminstrationDtoUsingId(UtilisateurCompletDto Dto) {
        UtilisateurCompletDto utilisateurAdministrationDto = Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(Dto.getId()));
        utilisateurAdministrationDto.setUserProfils(userProfilService.findDtoAllByIdUtilisateur(Dto.getId()));
        return utilisateurAdministrationDto;
    }

    private void verifierAcceptationAccord(Boolean accordInscription) {
        //le not true, gere le null.
        if (!Boolean.TRUE.equals(accordInscription)) {
            throw new ServiceException(Tools.message("utilisateur.missing.accord"));
        }
    }


    private void getEntityFromUtilisateurCompletDto(Utilisateur utilisateur, UtilisateurCompletDto utilisateurCompletDto) {
        Corps corps = corpsService.findById(utilisateurCompletDto.getCorps().getId());
        User user = utilisateur.getUser();
        if (user == null) {
            user = userService.createFromUtilisateurCompletDto(utilisateurCompletDto);
        } else {
            user = userService.modifyFromUtilisateurCompletDto(user, utilisateurCompletDto);
            valider(utilisateurCompletDto);
        }
        rempliInfosPersosUtilisateur(utilisateur, utilisateurCompletDto);
        utilisateur.setUser(user);
        utilisateur.setCorps(corps);

    }

    private void rempliInfosPersosUtilisateur(Utilisateur utilisateur, UtilisateurCompletDto utilisateurCompletDto) {
        utilisateur.setNom(utilisateurCompletDto.getNom());
        utilisateur.setPrenom(utilisateurCompletDto.getPrenom());
        utilisateur.setEmail(utilisateurCompletDto.getEmail());
        utilisateur.setTelephone(utilisateurCompletDto.getTelephone());
    }

    public List<UtilisateurActiviteDetailsDto> findDtoParticipantActiviteByStatut(Long idActivite, Long idStatut) {
        return repository.findParticipantActiviteUsingStatut(idActivite, idStatut);
    }

    public List<UtilisateurActiviteDetailsDto> findDtoParticipantActivite(Long idActivite, List<Long> idsGroupe) {
        return repository.findParticipantActiviteDetail(idActivite, idsGroupe);
    }

    public List<UtilisateurCompletDto> findDtoAllParticipantActiviteNonDisponibleBetween(Long idOrganisme, SuperDate debut, SuperDate fin, Long idActiviteNotIn) {
        List<UtilisateurCompletDto> utilisateurCompletDtos;
        if (idActiviteNotIn != null) {
            utilisateurCompletDtos = repository.findAllParticipantActiviteNonDisponibleBetween(idOrganisme, debut.instant(), fin.instant(), idActiviteNotIn);
        } else {
            utilisateurCompletDtos = repository.findAllParticipantActiviteNonDisponibleBetween(idOrganisme, debut.instant(), fin.instant());
        }
        return utilisateurCompletDtos;
    }

    public List<UtilisateurCompletDto> findDtoAllByOrganisme(Long idOrganisme) {
        return repository.findDtoAllUsingOrganisme(idOrganisme);
    }

    public List<UtilisateurCompletDto> findDtoAllEmployeByOrganisme(Long idOrganisme) {
        return repository.findDtoAllEmployeUsingOrganisme(idOrganisme);
    }

    public List<UtilisateurCompletDto> findDtoAllByOrganismeWithNoEnabled(Long idOrganisme) {
        return repository.findDtoAllUsingOrganismeWithNoEnabled(idOrganisme);
    }

    public List<Utilisateur> findAllByOrganisme(Long idOrganisme) {
        return repository.findAllUsingOrganisme(idOrganisme);
    }

    public UtilisateurCompletDto modifier(UtilisateurCompletDto utilisateurCompletDto) {

        MetiisValidation.verifier(utilisateurCompletDto, ContextEnum.UPDATE_UTILISATEUR);

        Utilisateur utilisateur = findById(utilisateurCompletDto.getId());

        modifierUser(utilisateurCompletDto, utilisateur);
        creerUserProfil(utilisateurCompletDto, utilisateur);
        affectationService.modifierAffectation(utilisateurCompletDto, utilisateur);


        return findAdminstrationDtoUsingId(utilisateurCompletDto);
    }

    private void modifierUser(UtilisateurCompletDto utilisateurCompletDto, Utilisateur utilisateur) {
        if (utilisateurCompletDto.getAffectation() != null) {
            userService.modifierUser(utilisateurCompletDto, utilisateur);
        }
    }

    private void creerUserProfil(UtilisateurCompletDto utilisateurCompletDto, Utilisateur utilisateur) {
        User user = utilisateur.getUser();
        userProfilService.deleteByUser(user);
        if (utilisateurCompletDto.getUserProfils() != null) {
            utilisateurCompletDto.getUserProfils().forEach(userProfilDto -> userProfilService.creerUserProfil(userProfilDto, user));
        }
    }

    public List<UtilisateurActiviteDetailsDto> getListCommentaireActivite(Long idActivite) {
        return repository.getListCommentaireActivite(idActivite, Optional.empty());
    }

    public List<UtilisateurActiviteDetailsDto> getListCommentaireActivite(Long idActivite, Long idUtilisateur) {
        return repository.getListCommentaireActivite(idActivite, Optional.ofNullable(idUtilisateur));
    }


    public ListFilterCorps findDtoAllBloqueByOrganisme(Long idOrganisme, Long idCorps) {
        ListFilterCorps filterCorps = filterService.getFilter(OptionListFilterCorps.byCorps(idCorps));
        filterCorps.add(repository.findDtoAllBloqueUsingOrganisme(idOrganisme));
        return filterCorps;
    }

    public ListFilterCorps findDtoAllAValiderByOrganisme(Long idOrganisme, Long idCorps) {
        ListFilterCorps filterCorps = filterService.getFilter(OptionListFilterCorps.byCorps(idCorps));
        filterCorps.add(repository.findDtoAllAValiderUsingOrganisme(idOrganisme));
        return filterCorps;
    }

    public void valider(UtilisateurCompletDto utilisateurCompletDto) {
        MetiisValidation.verifier(utilisateurCompletDto, ContextEnum.VALIDER);
        if (userService.valider(utilisateurCompletDto.getId(), utilisateurCompletDto.getEnabled())) {
            Utilisateur utilisateur = findByIdWithUser(utilisateurCompletDto.getId());
            mailService.envoyerMailCompteEnabled(utilisateur, utilisateurCompletDto.getEnabled());
        }
    }

    public void debloquer(UtilisateurCompletDto utilisateurCompletDto) {
        MetiisValidation.verifier(utilisateurCompletDto, ContextEnum.DEBLOQUER);
        userService.debloquer(utilisateurCompletDto.getId(), utilisateurCompletDto.getAccountNonLocked());
    }

    public CompteRenduInvitation inscriptionByToken(InscriptionByEmployeurDto inscriptionByEmployeurDto, String currentUserLogin) {
        MetiisValidation.verifier(inscriptionByEmployeurDto, ContextEnum.INSCRIPTION_TOKEN);
        Utilisateur createur = findByUserName(currentUserLogin);
        EmailValidation emailValidation = new EmailValidation(inscriptionByEmployeurDto.getEmails());

        Organisme organisme = organismeService.findById(inscriptionByEmployeurDto.getOrganismeAffectation().getId());
        List<Profil> profils = profilService.findListByDtos(inscriptionByEmployeurDto.getProfils());

        PosteRo posteRo = getPosteRo(inscriptionByEmployeurDto);

        CompteRenduInvitation compteRendu = new CompteRenduInvitation(emailValidation);

        emailValidation.getValid().forEach(email -> createUtilisateurWithToken(email, organisme, profils, createur, posteRo, compteRendu));

        return compteRendu;

    }

    @Nullable
    private PosteRo getPosteRo(InscriptionByEmployeurDto inscriptionByEmployeurDto) {
        PosteRo posteRo = null;
        if (inscriptionByEmployeurDto.getPosteRo() != null && inscriptionByEmployeurDto.getPosteRo().getId() != null) {
            posteRo = posteRoService.findById(inscriptionByEmployeurDto.getPosteRo().getId());
        }
        return posteRo;
    }


    private void createUtilisateurWithToken(String email, Organisme organisme, List<Profil> profils, Utilisateur createur, PosteRo posteRo, CompteRenduInvitation compteRendu) {
        //si try catch rollback de toutes les transaction, on veut juste evité les doublons mais continier le traitement.
        if (userService.findByUsername(email).isEmpty()) {
            Utilisateur utilisateur = new Utilisateur();
            utilisateur.setNom("");
            utilisateur.setPrenom("");
            utilisateur.setEmail(email);
            utilisateur.setUser(userService.createFromUtilisateur(utilisateur, profils));
            saveOrUpdate(utilisateur);
            affectationService.creerAffectation(utilisateur, organisme, posteRo);
            creationMailTokenInscription(utilisateur, createur);

            compteRendu.incrementeCree();
        }
    }

    public ConfirmationInscriptionEmployeDto getConfirmationInscription(String token) {
        ConfirmationInscriptionEmployeDto confirmationInscriptionEmployeDto = tokenUtilisateurService.getConfirmationInscriptionByToken(token);
        confirmationInscriptionEmployeDto.setOrganismes(organismeService.
                findDtoListByEnfantOrderByHierarchie(confirmationInscriptionEmployeDto.getOrganisme().getId()));
        return confirmationInscriptionEmployeDto;
    }

    public ChangementMotDePasseDto getChangementMotDePasse(String token) {
        return tokenUtilisateurService.getChangementMotDePasseByToken(token);
    }

    public ChangementMotDePasseDto getModifInfosPerso(String token) {
        ChangementMotDePasseDto changementMotDePasseDto = tokenUtilisateurService.getChangementMotDePasseInfosByToken(token);
        Utilisateur utilisateur = findByUserName(changementMotDePasseDto.getEmail());
        verifieEmailExist(utilisateur.getId(), utilisateur.getNewEmail());
        User user = utilisateur.getUser();
        userService.verifieUserNameExist(user.getId(), utilisateur.getNewEmail());
        userService.modifierInfosPersos(user, utilisateur.getNewEmail());
        utilisateur.setEmail(utilisateur.getNewEmail());
        utilisateur.setTelephone(utilisateur.getNewTelephone());
        utilisateur.setNewEmail(null);
        utilisateur.setNewTelephone(null);
        saveOrUpdate(utilisateur);
        tokenUtilisateurService.deleteAllTokenForModificationInfosPerso(utilisateur);
        return changementMotDePasseDto;
    }

    public UtilisateurCompletDto confirmerInscription(ConfirmationInscriptionEmployeDto confirmationInscriptionEmploye) {
        MetiisValidation.verifier(confirmationInscriptionEmploye, ContextEnum.CREATE_UTILISATEUR);

        TokenUtilisateur tokenUtilisateur = tokenUtilisateurService.findByTokenNonExpirer(confirmationInscriptionEmploye.getToken());
        Utilisateur utilisateur = tokenUtilisateur.getUtilisateur();

        Corps corps = corpsService.findById(confirmationInscriptionEmploye.getCorps().getId());

        verifierAcceptationAccord(confirmationInscriptionEmploye.getAccordInscription());
        Tools.verifierFormatEtEgaliterMotDePasse(confirmationInscriptionEmploye.getPassword(), confirmationInscriptionEmploye.getPasswordconfirmation());

        utilisateur.setPrenom(confirmationInscriptionEmploye.getPrenom());
        utilisateur.setNom(confirmationInscriptionEmploye.getNom());
        utilisateur.setTelephone(confirmationInscriptionEmploye.getTelephone());

        utilisateur.setCorps(corps);

        utilisateur.setAccordInscription(confirmationInscriptionEmploye.getAccordInscription());

        saveOrUpdate(utilisateur);

        userService.confirmerInscription(utilisateur, confirmationInscriptionEmploye);

        notificationService.creerNotifications(utilisateur);

        tokenUtilisateurService.deleteAllTokenForInscription(utilisateur);

        mailService.envoyerMailConfirmationInscription(utilisateur);

        return findDtoById(utilisateur.getId());
    }

    private void creationMailTokenInscription(Utilisateur utilisateur, Utilisateur createur) {
        String token = tokenUtilisateurService.createTokenForInsctiption(utilisateur, createur);
        mailService.envoyerMailInvitation(utilisateur, token);
    }

    private void creationMailTokenModificationInfosPerso(Utilisateur utilisateur) {
        String token = tokenUtilisateurService.createTokenForModificationInfosPerso(utilisateur);
        mailService.envoyerMailModificationInfosPerso(utilisateur, token);
        if (!utilisateur.getEmail().equals(utilisateur.getNewEmail())) {
            mailService.envoyerMailModificationInfosPersoAncienneAdresse(utilisateur);
        }
    }

    private void creationMailTokenRenewPassword(Utilisateur utilisateur) {
        String token = tokenUtilisateurService.createTokenForRenewPassword(utilisateur);
        mailService.envoyerMailDemandeNouveauMotDePasse(utilisateur, token);
    }

    public void demandeNouveauMotDePasse(AuthenticationRequestDto authenticationRequest) {
        MetiisValidation.verifier(authenticationRequest, ContextEnum.NEW_PWD);
        try {
            Utilisateur utilisateur = findByUserName(authenticationRequest.getUsername());
            Optional<TokenUtilisateur> token = tokenUtilisateurService.findTokenInscritionByIdUtilisateur(utilisateur.getId());

            if (token.isPresent()) {
                creationMailTokenInscription(utilisateur, utilisateur);
            } else {
                creationMailTokenRenewPassword(utilisateur);
            }

        } catch (NotFoundException ignored) {
            /*
             * recupère l'erreur de en cas Utilisateur inconnue
             * pour que dans le front il n'y est pas de retour d'erreur
             */
        }

    }


    public void changerMotDePasse(ChangementMotDePasseDto changementMotDePasse) {
        MetiisValidation.verifier(changementMotDePasse, ContextEnum.NEW_PWD);

        TokenUtilisateur tokenUtilisateur = tokenUtilisateurService.findByTokenNonExpirer(changementMotDePasse.getToken());
        Utilisateur utilisateur = tokenUtilisateur.getUtilisateur();

        Tools.verifierFormatEtEgaliterMotDePasse(changementMotDePasse);
        userService.changerMotDePasse(utilisateur.getUser(), changementMotDePasse.getPassword());
        mailService.envoyerMailMotDePasseChanger(utilisateur);

        tokenUtilisateurService.deleteAllTokenForRenewPassword(utilisateur);
    }

    public void changerMotDePasse(ChangePasswordDto changePassword) {
        MetiisValidation.verifier(changePassword, ContextEnum.NEW_PWD);
        Utilisateur utilisateur = findByUserName(SecurityUtils.getCurrentUserLogin());

        userService.verifierAncienPassword(utilisateur.getUser().getPassword(), changePassword);
        Tools.verifierFormatEtEgaliterMotDePasse(changePassword);

        userService.changerMotDePasse(utilisateur.getUser(), changePassword.getPassword());
    }


    public CompteRenduInvitation renouvellementInscriptionByToken(List<ConfirmationInscriptionEmployeDto> confirmationsInscriptionsEmploye, String currentUserLogin) {
        Utilisateur createur = findByUserName(currentUserLogin);
        MetiisValidation.verifierCollection(confirmationsInscriptionsEmploye, ContextEnum.RENEW_INSCRIPTION);
        CompteRenduInvitation compteRendu = new CompteRenduInvitation(confirmationsInscriptionsEmploye);

        confirmationsInscriptionsEmploye.forEach(confirmationInscriptionEmployeDto -> {

            TokenUtilisateur tokenUtilisateur = tokenUtilisateurService.findByToken(confirmationInscriptionEmployeDto.getToken());
            Utilisateur utilisateur = tokenUtilisateur.getUtilisateur();

            creationMailTokenInscription(utilisateur, createur);

            compteRendu.incrementeCree();
        });

        return compteRendu;
    }

    public CompteRenduInvitation supprimerInscription(List<ConfirmationInscriptionEmployeDto> confirmationsInscriptionsEmploye) {
        CompteRenduInvitation compteRendu = new CompteRenduInvitation(confirmationsInscriptionsEmploye);
        confirmationsInscriptionsEmploye.forEach(confirmationInscriptionEmployeDto -> {
            TokenUtilisateur tokenUtilisateur = tokenUtilisateurService.findByToken(confirmationInscriptionEmployeDto.getToken());
            Utilisateur utilisateur = tokenUtilisateur.getUtilisateur();
            tokenUtilisateurService.deleteAllTokenForInscription(utilisateur);
            delete(utilisateur);
            compteRendu.incrementeSupprime();
        });
        return compteRendu;
    }

    public void supprimerInscriptionsPerimees() {
        delete(tokenUtilisateurService.deleteAllInvitationsPerimees());
    }

    public ListFilterCorps findFilterByOrganisme(Long idOrganisme, Long idCorps, Long idGroupPersonaliser) {
        List<UtilisateurCompletDto> utilisateurs = findDtoAllByIdOrganisme(idOrganisme);
        return addToFilter(utilisateurs, idCorps, idGroupPersonaliser);
    }

    public ListFilterCorps findFilterByOrganismeWithNoEnabled(Long idOrganisme, Long idCorps, Long idGroupPersonaliser) {
        List<UtilisateurCompletDto> utilisateurs = findDtoAllByOrganismeWithNoEnabled(idOrganisme);
        return addToFilter(utilisateurs, idCorps, idGroupPersonaliser);
    }

    private List<UtilisateurCompletDto> findDtoAllByIdOrganisme(Long idOrganisme) {
        OrganismeDto organisme = organismeService.findDtoById(idOrganisme);
        return findDtoAllByOrganisme(organisme.getId());
    }

    private ListFilterCorps addToFilter(List<UtilisateurCompletDto> utilisateurs, Long idCorps, Long idGroupPersonaliser) {
        ListFilterCorps filterCorps = filterService.getFilter(OptionListFilterCorps.byCorps(idCorps).andAddGroupes(idGroupPersonaliser));
        filterCorps.add(utilisateurs);
        return filterCorps;
    }

    public ListFilterCorps findFilterByOrganismeWithGroupe(Long idOrganisme, Long idCorps, Long idGroupPersonaliser) {
        List<UtilisateurCompletDto> utilisateurs = findDtoAllByOrganisme(idOrganisme);
        addGroupes(utilisateurs);
        return addToFilter(utilisateurs, idCorps, idGroupPersonaliser);
    }

    public ListFilterCorps findFilterByOrganismeWithGroupeAndNoEnabled(Long idOrganisme, Long idCorps, Long idGroupPersonaliser) {
        List<UtilisateurCompletDto> utilisateurs = findDtoAllByOrganismeWithNoEnabled(idOrganisme);
        addGroupes(utilisateurs);
        return addToFilter(utilisateurs, idCorps, idGroupPersonaliser);
    }

    private void addGroupes(List<UtilisateurCompletDto> utilisateurs) {
        UtilisateurDto utilisateurConnecter = findDtoByUserName(SecurityUtils.getCurrentUserLogin());
        List<GroupePersonnaliserDto> groupePersonnalisers = groupePersonnaliserService.findAllDtoByIdProprietaireWithUtilisateurs(utilisateurConnecter.getId());
        utilisateurs.forEach(utilisateurCompletDto -> utilisateurCompletDto.addGroupePersonnaliser(groupePersonnalisers));
    }

    public List<UtilisateurCompletDto> searchByOrganisme(Long idOrganisme, String search) {
        List<UtilisateurCompletDto> resultat = new ArrayList<>();
        if (Tools.testerNulliteString(search)) {
            OrganismeDto organisme = organismeService.findDtoById(idOrganisme);

            resultat = repository.searchDtoUsingOrganisme(organisme.getId(), '%' + search + '%');
        }
        return resultat;
    }

    public UtilisateurCompletDto modifierPartiellement(UtilisateurCompletDto utilisateurDto) {
        MetiisValidation.verifier(utilisateurDto, ContextEnum.PATCH_UTILISATEUR);

        Utilisateur utilisateur = findByIdWithUser(utilisateurDto.getId());
        Corps corps = corpsService.findById(utilisateurDto.getCorps().getId());
        valider(utilisateurDto);
        utilisateur.setNom(utilisateurDto.getNom());
        utilisateur.setPrenom(utilisateurDto.getPrenom());
        User user = utilisateur.getUser();
        userService.modifierInfosPersos(user, utilisateurDto);
        utilisateur.setCorps(corps);
        utilisateur.setLastModifiedDate(Instant.now()); //force l'inscription en bdd.
        saveOrUpdate(utilisateur);

        affectationService.modifierPosteAuRo(utilisateurDto);
        userProfilService.modifierProfil(utilisateur, utilisateurDto);

        return findDtoById(utilisateur.getId());
    }

    public UtilisateurCompletDto modifierInfosPersos(UtilisateurCompletDto utilisateurCompletDto) {
        MetiisValidation.verifier(utilisateurCompletDto, ContextEnum.UPDATE_INFOS_PERSOS);
        Utilisateur utilisateur = findByUserName(SecurityUtils.getCurrentUserLogin());
        verifieEmailExist(utilisateur.getId(), utilisateurCompletDto.getEmail());
        User user = utilisateur.getUser();
        userService.verifieUserNameExist(user.getId(), utilisateurCompletDto.getEmail());
        utilisateur.setNewEmail(utilisateurCompletDto.getEmail());
        utilisateur.setNewTelephone(utilisateurCompletDto.getTelephone());
        saveOrUpdate(utilisateur);
        creationMailTokenModificationInfosPerso(utilisateur);
        return findDtoById(utilisateur.getId());
    }

    public void supprimerModifierInfosPersosPerime() {
        tokenUtilisateurService.getIdsUtilisateurTokenModifInfosPersoPerimer().forEach(utilisateur -> {
            utilisateur.setNewEmail(null);
            utilisateur.setNewTelephone(null);
            saveOrUpdate(utilisateur);
        });
        tokenUtilisateurService.supprimerTokenModifInfosPersoPerimer();
    }

    public UtilisateurCompletDto annulerModifierInfosPersos() {
        Utilisateur utilisateur = findByUserName(SecurityUtils.getCurrentUserLogin());
        utilisateur.setNewEmail(null);
        utilisateur.setNewTelephone(null);
        saveOrUpdate(utilisateur);
        return findDtoById(utilisateur.getId());
    }

    public UtilisateurCompletDto renvoyerModifierInfosPersos() {
        Utilisateur utilisateur = findByUserName(SecurityUtils.getCurrentUserLogin());
        creationMailTokenModificationInfosPerso(utilisateur);
        return findDtoById(utilisateur.getId());
    }

    private Utilisateur findByIdWithUser(Long idUtilisateur) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findByIdWithUser(idUtilisateur), getEntityClassName());
    }

    public UtilisateurCompletDto findDtoByIdWithMyGroupe(Long id) {
        UtilisateurCompletDto utilisateur = findDtoById(id);
        addGroupes(utilisateur);
        return utilisateur;
    }

    private void addGroupes(UtilisateurCompletDto utilisateur) {
        UtilisateurDto utilisateurConnecter = findDtoByUserName(SecurityUtils.getCurrentUserLogin());
        List<GroupePersonnaliserDto> groupePersonnalisers = groupePersonnaliserService.findAllDtoByIdProprietaireWithUtilisateursAndIdUtilisateur(utilisateurConnecter.getId(), utilisateur.getId());
        utilisateur.setGroupes(groupePersonnalisers);
    }

    public List<UtilisateurCompletDto> findDtoAllByIdGroupePersonnaliser(Long idGroupePersonnaliser) {
        return repository.findDtoAllUsingIdGroupePersonnaliser(idGroupePersonnaliser);
    }


    public List<OrganismeDto> findDtoListByEnfantOrderByHierarchieByUtilisateur(Long idUtilisateur) {
        UtilisateurCompletDto utilisateur = findDtoById(idUtilisateur);
        return organismeService.findDtoListByEnfantOrderByHierarchie(utilisateur.getAffectation().getOrganisme().getId());
    }

    public List<UtilisateurStatistiqueCountDto> getStatistiqueByOrganisme(Long idOrganisme, String dateDebut, String dateFin) {
        SuperDate debutActivite = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate finActivite = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        DisponibiliteDate disponibiliteDate = DisponibiliteDate.getInstance(dateDebut, dateFin);
        List<?> sqlsData = repository.getStatistiques(idOrganisme, disponibiliteDate.debutToInstant(), disponibiliteDate.finToInstant(), debutActivite.instant(), finActivite.instant());
        List<UtilisateurStatistiqueCountDto> dtos = ToolsRepository.convertir(sqlsData, UtilisateurStatistiqueCountDto.class);
        dtos.forEach(utililisateurStatistiqueCountDto -> utililisateurStatistiqueCountDto.calculNbJour(disponibiliteDate.debut(), disponibiliteDate.fin()));
        return dtos;
    }


    public Page<UtilisateurCompletDto> findDtoDashboardAll(Integer page, Integer size, String sortBy, String order, String filtre, Boolean enabled) {
        return repository.findDtoDashboardAllSort(getPaging(page, size, sortBy, order), filtre.toUpperCase(), enabled);
    }

    public Page<UtilisateurCompletDto> findDtoDashboardAll(Long idOrganisme, Integer page, Integer size, String sortBy, String order, String filtre, Boolean enabled) {
        List<Long> idsOrganisme = new ArrayList<>();
        List<OrganismeDto> lstOrg = organismeService.findAllOrganismeAffectableByParent(idOrganisme);
        lstOrg.forEach(organismeDto -> idsOrganisme.add(organismeDto.getId()));
        return repository.findDtoDashboardAllSort(idsOrganisme, getPaging(page, size, sortBy, order), filtre.toUpperCase(), enabled);
    }

    private Pageable getPaging(Integer page, Integer size, String sortBy, String order) {
        Pageable paging = PageRequest.of(page, size, Sort.by(sortBy));
        if (Order.ASC.equals(order)) {
            paging = PageRequest.of(page, size, Sort.by(sortBy).ascending());
        }
        if (Order.DESC.equals(order)) {
            paging = PageRequest.of(page, size, Sort.by(sortBy).descending());
        }
        return paging;
    }

    public List<UtilisateurActiviteDetailsDto> getListeCommentaireDeLemploye(Long idActivite, Long idUtilisateur, Long idEtape) {
        return repository.getListCommentaireActivite(idActivite, idUtilisateur, idEtape);
    }

    public StatistiqueGeneralDto getStatistique() {
        Optional<?> sqlData = repository.getStatistiquesGeneral();
        Optional<StatistiqueGeneralDto> dtos = ToolsRepository.convertir(sqlData, StatistiqueGeneralDto.class);
        return Tools.getValueFromOptionalOrNotFoundException(dtos, StatistiqueGeneralDto.class.getSimpleName());
    }

    public List<Utilisateur> findNonPartipantsActivite(Long idActivite) {
        return repository.findNonParticipantActivite(idActivite);
    }

    public List<Utilisateur> findPartipantsActivite(Long idActivite) {
        return repository.findParticipantActivite(idActivite);
    }

    public List<UtilisateurStatistiqueCountDto> dernieresConnexion() {
        return repository.dernieresConnexion(PageRequest.of(0, 100));
    }

    public Long compterNombrePaxUnite(Long idOrganise, Long idCorps) {
        return repository.compterNombrePaxUnite(idOrganise, idCorps);
    }

    public Long getIdOrganisme(Long idUtilisateur) {
        return repository.getIdOrganisme(idUtilisateur);
    }

    public List<UtilisateurCompletDto> getUtilisateurByOrganismeAndProfil(Long idOrganisme, Long idProfil) {
        return repository.findDtoAllUsingOrganismeAndProfil(idOrganisme, idProfil);
    }

    public UtilisateurCompletDto modifierUserProfil(UtilisateurCompletDto utilisateurDto) {

        Utilisateur utilisateur = findByUserName(SecurityUtils.getCurrentUserLogin());
        User user = utilisateur.getUser();

        for (UserProfil userProfil : user.getUserProfil()) {
            for (UserProfilDto u : utilisateurDto.getUserProfils()) {
                if (userProfil.getId().equals(u.getId())) {
                    userProfil.setSelected(u.getSelected());
                }
            }
        }
        saveOrUpdate(utilisateur);

        return findDtoById(utilisateur.getId());
    }
}