package fabnum.metiis.services.rh;

import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.dto.tools.OptionListFilterCorps;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class FilterService {

    private CorpsService corpsService;
    private GroupePersonnaliserDetailsService groupePersonnaliserDetailsService;

    public ListFilterCorps getFilter(OptionListFilterCorps option) {
        return new ListFilterCorps(corpsService.findDtoAll(), option)
                .addFiltreGroupe(groupePersonnaliserDetailsService.findAllDtoByIdsGroupePersonnaliser(option.getIdsGroupes()));
    }
}