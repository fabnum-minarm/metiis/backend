package fabnum.metiis.services.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.rh.PosteRo;
import fabnum.metiis.dto.rh.PosteRoDto;
import fabnum.metiis.repository.rh.PosteRoRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PosteRoService extends AbstarctService<PosteRoRepository, PosteRo, Long> {

    public PosteRoService(PosteRoRepository repository) {
        super(repository);
    }


    @Override
    protected void checkBeforeSaveOrUpdate(PosteRo posteRo) {
        testNull(posteRo.getCode(), "posteRo.missing.code");
        testNull(posteRo.getLibelle(), "posteRo.missing.libelle");

        testExist(repository.findByIdNotAndCode(Tools.getValueFromOptional(posteRo.getId()), posteRo.getCode()), "posteRo.code.exist");

    }

    public PosteRoDto findDtoById(Long idPoste) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idPoste), getEntityClassName());
    }

    private void existCode(Long id, String code) {
        if (id != null) {
            testExist(repository.findByIdNotAndCode(id, code), "posteRo.code.exist");
        } else {
            testExist(repository.findByCode(code), "posteRo.code.exist");
        }
    }

    public PosteRoDto creer(PosteRoDto posteRoDto) {
        MetiisValidation.verifier(posteRoDto, ContextEnum.CREATE);

        existCode(null, posteRoDto.getCode());

        PosteRo posteRo = new PosteRo();
        remplirEntity(posteRo, posteRoDto);
        saveOrUpdate(posteRo);

        return findDtoById(posteRo.getId());
    }

    private void remplirEntity(PosteRo posteRo, PosteRoDto posteRoDto) {
        posteRo.setCode(posteRoDto.getCode());
        posteRo.setLibelle(posteRoDto.getLibelle());
        posteRo.setOrdre(posteRoDto.getOrdre());
    }

    public PosteRoDto modifier(Long idPosteRo, PosteRoDto posteRoDto) {
        MetiisValidation.verifier(posteRoDto, ContextEnum.UPDATE);

        existCode(idPosteRo, posteRoDto.getCode());

        PosteRo posteRo = findById(idPosteRo);
        remplirEntity(posteRo, posteRoDto);
        saveOrUpdate(posteRo);

        return findDtoById(posteRo.getId());
    }

    public Page<PosteRoDto> findDtoAll(Pageable pageable) {
        return repository.findAllDto(pageable);
    }

    public List<PosteRoDto> findDtoAll() {
        return repository.findAllDto();
    }
}