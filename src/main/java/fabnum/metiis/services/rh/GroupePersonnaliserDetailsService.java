package fabnum.metiis.services.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.rh.GroupePersonnaliser;
import fabnum.metiis.domain.rh.GroupePersonnaliserDetails;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.rh.GroupePersonnaliserDetailsDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.repository.rh.GroupePersonnaliserDetailsRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class GroupePersonnaliserDetailsService extends AbstarctServiceSimple<GroupePersonnaliserDetailsRepository, GroupePersonnaliserDetails, Long> {

    private UtilisateurService utilisateurService;

    public GroupePersonnaliserDetailsService(UtilisateurService utilisateurService,

                                             GroupePersonnaliserDetailsRepository repository) {
        super(repository);
        this.utilisateurService = utilisateurService;
    }


    public void creerListeFromGroupePersonnaliserDetailsDto(GroupePersonnaliser groupePersonnaliser, List<GroupePersonnaliserDetailsDto> groupesPersonnaliserDetails) {
        deleteAllByIdGroupePersonnaliser(groupePersonnaliser.getId());

        if (!CollectionUtils.isEmpty(groupesPersonnaliserDetails)) {
            MetiisValidation.verifierCollection(groupesPersonnaliserDetails, ContextEnum.CREATE_GROUPE_DETAILS);

            Set<UtilisateurCompletDto> utilisateursDtos = groupesPersonnaliserDetails.stream().map(GroupePersonnaliserDetailsDto::getUtilisateur).collect(Collectors.toSet());
            List<Utilisateur> utilisateurs = utilisateurService.findByIds(Tools.getListIdDto(utilisateursDtos));

            utilisateurs.forEach(
                    utilisateur -> creer(groupePersonnaliser, utilisateur));
        }
    }

    public List<GroupePersonnaliserDetailsDto> findAllDtoByIdGroupePersonnaliser(Long idGroupePersonnaliser) {
        return repository.findAllDtoUsingIdGroupePersonnaliser(idGroupePersonnaliser);
    }

    private void creer(GroupePersonnaliser groupePersonnaliser, Utilisateur utilisateur) {
        GroupePersonnaliserDetails groupePersonnaliserDetails = new GroupePersonnaliserDetails();
        groupePersonnaliserDetails.setGroupePersonnaliser(groupePersonnaliser);
        groupePersonnaliserDetails.setUtilisateur(utilisateur);

        saveOrUpdate(groupePersonnaliserDetails);
    }

    public List<GroupePersonnaliserDetailsDto> findAllDtoByIdsGroupePersonnaliser(Set<Long> idsGroupes) {
        List<GroupePersonnaliserDetailsDto> dtos = new ArrayList<>();
        if (!CollectionUtils.isEmpty(idsGroupes)) {
            dtos = repository.findAllDtoUsingIdsGroupePersonnaliser(idsGroupes);
        }
        return dtos;
    }

    public List<GroupePersonnaliserDetailsDto> findByIdUtilisateur(Long idUtilisateur) {
        return repository.findAllDtoUsingIdUtilisateur(idUtilisateur);
    }

    private void deleteAllByIdGroupePersonnaliser(Long idGroupePersonnaliser) {
        repository.deleteByGroupePersonnaliserId(idGroupePersonnaliser);
        repository.flush();
    }
}