package fabnum.metiis.services.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.rh.Corps;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.repository.rh.CorpsRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CorpsService extends AbstarctServiceSimple<CorpsRepository, Corps, Long> {

    public CorpsService(CorpsRepository repository) {
        super(repository);
    }

    public List<Corps> findAll() {
        return repository.findAllByOrderByOrdreAsc();
    }

    public CorpsDto findDtoById(Long id) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(id), getEntityClassName());
    }

    @Override
    protected void checkBeforeSaveOrUpdate(Corps corps) {
        testExist(repository.findByIdNotAndCode(Tools.getValueFromOptional(corps.getId()), corps.getCode()), "corps.code.exist");
    }

    @CacheEvict(value = "AllCorpsDto", allEntries = true)
    public CorpsDto creerFromDto(CorpsDto corpsDto) {
        MetiisValidation.verifier(corpsDto, ContextEnum.CREATE_CORPS);
        Corps corps = new Corps();
        rempliCorps(corps, corpsDto);
        saveOrUpdate(corps);
        return findDtoById(corps.getId());
    }

    @CacheEvict(value = "AllCorpsDto", allEntries = true)
    public CorpsDto modifierFromDto(Long idCorps, CorpsDto corpsDto) {
        MetiisValidation.verifier(corpsDto, ContextEnum.UPDATE_CORPS);
        Corps corps = findById(idCorps);
        rempliCorps(corps, corpsDto);
        saveOrUpdate(corps);
        return findDtoById(corps.getId());
    }

    private void rempliCorps(Corps corps, CorpsDto corpsDto) {
        corps.setCode(corpsDto.getCode());
        corps.setLibelle(corpsDto.getLibelle());
        corps.setOrdre(corpsDto.getOrdre());
        corps.setLettre(corpsDto.getLettre());
    }

    public Page<CorpsDto> findDtoAll(Pageable pageable) {
        return repository.findDtoAll(pageable);
    }

    @Cacheable("AllCorpsDto")
    public List<CorpsDto> findDtoAll() {
        return repository.findDtoAll();
    }

    @CacheEvict(value = "AllCorpsDto", allEntries = true)
    @Override
    public void delete(Long id) {
        super.delete(id);
    }


}