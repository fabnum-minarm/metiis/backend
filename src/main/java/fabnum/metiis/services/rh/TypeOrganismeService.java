package fabnum.metiis.services.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.rh.TypeOrganisme;
import fabnum.metiis.dto.rh.TypeOrganismeDto;
import fabnum.metiis.repository.rh.TypeOrganismeRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TypeOrganismeService extends AbstarctServiceSimple<TypeOrganismeRepository, TypeOrganisme, Long> {


    public TypeOrganismeService(TypeOrganismeRepository repository) {
        super(repository);
    }


    public List<TypeOrganisme> findAll() {
        return this.repository.findAllByOrderByOrdreAsc();
    }

    public TypeOrganismeDto findDtoById(Long id) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(id));
    }

    public TypeOrganisme findByCode(String code) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findByCode(code));
    }


    @Override
    protected void checkBeforeSaveOrUpdate(TypeOrganisme entite) {
        testExist(this.repository.findByIdNotAndCode(
                Tools.getValueFromOptional(entite.getId()), entite.getCode()),
                "type-organisme.code.exist");
    }

    public TypeOrganismeDto creerFromDto(TypeOrganismeDto typeOrganismeDto) {
        MetiisValidation.verifier(typeOrganismeDto, ContextEnum.CREATE_TYPE_ORGANISME);
        TypeOrganisme typeOrganisme = new TypeOrganisme();
        rempliTypeOrganisme(typeOrganisme, typeOrganismeDto);
        saveOrUpdate(typeOrganisme);
        return findDtoById(typeOrganisme.getId());
    }

    public TypeOrganismeDto modifierFromDto(Long idTypeOrganisme, TypeOrganismeDto typeOrganismeDto) {
        MetiisValidation.verifier(typeOrganismeDto, ContextEnum.UPDATE_TYPE_ORGANISME);
        TypeOrganisme typeOrganisme = findById(idTypeOrganisme);
        rempliTypeOrganisme(typeOrganisme, typeOrganismeDto);
        saveOrUpdate(typeOrganisme);
        return findDtoById(typeOrganisme.getId());
    }

    private void rempliTypeOrganisme(TypeOrganisme typeOrganisme, TypeOrganismeDto typeOrganismeDto) {
        typeOrganisme.setCode(typeOrganismeDto.getCode());
        typeOrganisme.setLibelle(typeOrganismeDto.getLibelle());
        typeOrganisme.setOrdre(typeOrganismeDto.getOrdre());
        typeOrganisme.setPourAffectation(typeOrganismeDto.getPourAffectation());
        if (typeOrganismeDto.getIdTypeOrganismeParent() != null) {
            TypeOrganisme typeOrganismeParent = findById(typeOrganismeDto.getIdTypeOrganismeParent());
            typeOrganisme.setParent(typeOrganismeParent);
        } else {
            typeOrganisme.setParent(null);
        }
    }

    public List<TypeOrganismeDto> findAllDto() {
        return repository.findDtoAll();
    }

    public List<TypeOrganismeDto> findDtoListByParent(Long idTypeOrganismeParent) {
        return repository.findDtoListUsingParent(idTypeOrganismeParent);
    }

    public List<TypeOrganismeDto> findDtoAllByParent(Long idTypeOrganismeParent) {
        List<TypeOrganismeDto> typeOrganismeDtos = new ArrayList<>();
        if (idTypeOrganismeParent == null) {
            return repository.findDtoAll();
        }
        for (TypeOrganismeDto enfant : findDtoListByParent(idTypeOrganismeParent)) {
            typeOrganismeDtos.add(enfant);
            typeOrganismeDtos.addAll(findDtoAllByParent(enfant.getId()));
        }
        return typeOrganismeDtos;
    }
}