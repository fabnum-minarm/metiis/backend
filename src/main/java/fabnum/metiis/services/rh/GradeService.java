package fabnum.metiis.services.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.rh.Corps;
import fabnum.metiis.domain.rh.Grade;
import fabnum.metiis.dto.rh.GradeDto;
import fabnum.metiis.repository.rh.GradeRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class GradeService extends AbstarctService<GradeRepository, Grade, Long> {

    private CorpsService corpsService;


    public GradeService(
            CorpsService corpsService,

            GradeRepository repository) {

        super(repository);
        this.corpsService = corpsService;
    }

    public List<GradeDto> findDtoAll() {
        return repository.findDtoAll();
    }

    public GradeDto creerFromDto(GradeDto gradeDto) {
        MetiisValidation.verifier(gradeDto, ContextEnum.CREATE_GRADE);
        Grade grade = new Grade();
        rempliGrade(grade, gradeDto);
        saveOrUpdate(grade);
        return findDtoByIdOnly(grade.getId());
    }

    public GradeDto modifierFromDto(Long idgrade, GradeDto gradeDto) {
        MetiisValidation.verifier(gradeDto, ContextEnum.UPDATE_GRADE);
        Grade grade = findById(idgrade);
        rempliGrade(grade, gradeDto);
        saveOrUpdate(grade);
        return findDtoByIdOnly(grade.getId());
    }

    private void rempliGrade(Grade grade, GradeDto gradeDto) {
        grade.setCode(gradeDto.getCode());
        grade.setLibelle(gradeDto.getLibelle());
        grade.setOrdre(gradeDto.getOrdre());
        Corps corps = corpsService.findById(gradeDto.getCorps().getId());
        grade.setCorps(corps);
    }

    public GradeDto findDtoByIdOnly(Long idGrade) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idGrade), getEntityClassName());
    }

    public Page<GradeDto> findDtoAll(Pageable pageable) {
        return repository.findDtoAll(pageable);
    }

    public List<GradeDto> findDtoAllByIdCoprs(Long idCorps) {
        return repository.findDtoAllUsingIdCoprs(idCorps);
    }
}