package fabnum.metiis.services.rh;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.rh.TokenUtilisateur;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.rh.ChangementMotDePasseDto;
import fabnum.metiis.dto.rh.ConfirmationInscriptionEmployeDto;
import fabnum.metiis.dto.tools.ListConfirmationInscriptionEmployeDto;
import fabnum.metiis.repository.rh.TokenUtilisateurRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.exceptions.ServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Service
@Transactional
public class TokenUtilisateurService extends AbstarctServiceSimple<TokenUtilisateurRepository, TokenUtilisateur, Long> {

    public static final int DUREE_SUPPRESSION_APRES_EXPIRATION = 30;
    public static final int DUREE_VALIDITE_INSCRIPTION = 15;
    public static final int DUREE_VALIDITE_MODIF_INFOS_PERSO = 24;

    public TokenUtilisateurService(
            TokenUtilisateurRepository repository) {

        super(repository);
    }

    /**
     * @param utilisateur .
     * @param createur    .
     * @return Token
     */
    public String createTokenForInsctiption(Utilisateur utilisateur, Utilisateur createur) {

        deleteAllTokenForInscription(utilisateur);

        TokenUtilisateur tokenUtilisateur = getNewTokenUtilisateur(utilisateur);

        tokenUtilisateur.setForInsciption(Boolean.TRUE);
        tokenUtilisateur.setPrescription(getDateValiditeInvitationInscription());
        tokenUtilisateur.setCreateur(createur);

        saveOrUpdate(tokenUtilisateur);

        return tokenUtilisateur.getToken();
    }

    public String createTokenForModificationInfosPerso(Utilisateur utilisateur) {

        deleteAllTokenForModificationInfosPerso(utilisateur);

        TokenUtilisateur tokenUtilisateur = getNewTokenUtilisateur(utilisateur);

        tokenUtilisateur.setForInfos(Boolean.TRUE);
        tokenUtilisateur.setPrescription(getDateValiditeModificationInfosPerso());
        tokenUtilisateur.setCreateur(utilisateur);

        saveOrUpdate(tokenUtilisateur);

        return tokenUtilisateur.getToken();
    }

    public void deleteAllTokenForInscription(Utilisateur utilisateur) {
        repository.deleteAllTokenInscriptionByIdUtilisateur(utilisateur.getId());
    }

    public void deleteAllTokenForModificationInfosPerso(Utilisateur utilisateur) {
        repository.deleteAllTokenModificationInfosPersoByIdUtilisateur(utilisateur.getId());
    }

    public void deleteAllTokenForRenewPassword(Utilisateur utilisateur) {
        repository.deleteAllTokenRenewPasswordByIdUtilisateur(utilisateur.getId());
    }

    public List<Long> deleteAllInvitationsPerimees() {
        Instant d = getDateSuppressionTokenExpire();
        List<Long> ids = repository.getIdsUtilisateurTokenPerimerForInscription(d);
        repository.supprimerTokenPerimerForInscription(d);
        return ids;
    }

    private TokenUtilisateur getNewTokenUtilisateur(Utilisateur utilisateur) {
        TokenUtilisateur tokenUtilisateur = new TokenUtilisateur();
        tokenUtilisateur.setUtilisateur(utilisateur);
        tokenUtilisateur.setToken(generateToken());
        return tokenUtilisateur;
    }

    public String createTokenForRenewPassword(Utilisateur utilisateur) {
        deleteAllTokenForRenewPassword(utilisateur);
        TokenUtilisateur tokenUtilisateur = getNewTokenUtilisateur(utilisateur);

        tokenUtilisateur.setForPassword(Boolean.TRUE);
        tokenUtilisateur.setPrescription(getDateValiditeRenewPassword());

        saveOrUpdate(tokenUtilisateur);
        return tokenUtilisateur.getToken();
    }

    private Instant getDateValiditeInvitationInscription() {
        SuperDate date = new SuperDate();
        date.finDeLaJournee();
        date.jourSuivant(DUREE_VALIDITE_INSCRIPTION);
        return date.instant();
    }

    private Instant getDateValiditeModificationInfosPerso() {
        SuperDate date = new SuperDate();
        date.finDeLaJournee();
        date.jourSuivant(DUREE_VALIDITE_MODIF_INFOS_PERSO);
        return date.instant();
    }

    private Instant getDateSuppressionTokenExpire() {
        SuperDate date = new SuperDate();
        date.finDeLaJournee();
        date.jourPrecedent(DUREE_SUPPRESSION_APRES_EXPIRATION);
        return date.instant();
    }

    private Instant getDateValiditeRenewPassword() {
        SuperDate date = new SuperDate();
        date.finDeLaJournee();
        date.jourSuivant();
        return date.instant();
    }

    private String generateToken() {
        UUID uuid = UUID.randomUUID();
        Optional<TokenUtilisateur> tokenOption = repository.findByToken(uuid.toString());
        while (tokenOption.isPresent()) {
            uuid = UUID.randomUUID();
            tokenOption = repository.findByToken(uuid.toString());
        }
        return uuid.toString();
    }

    public TokenUtilisateur findByTokenNonExpirer(String token) {
        TokenUtilisateur tokenUtilisateur = findByToken(token);
        hasExpiredException(tokenUtilisateur.getPrescription());
        return tokenUtilisateur;
    }

    public TokenUtilisateur findByToken(String token) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findByToken(token), getEntityClassName());
    }

    public ConfirmationInscriptionEmployeDto getConfirmationInscriptionByToken(String token) {
        ConfirmationInscriptionEmployeDto dto = Tools.getValueFromOptionalOrNotFoundException(repository.findConfirmationDtoUsingToken(token), getEntityClassName());
        hasExpiredException(dto.getPrecription());
        return dto;
    }

    public ListConfirmationInscriptionEmployeDto getList() {
        List<ConfirmationInscriptionEmployeDto> dtos = repository.findAllDto();
        return new ListConfirmationInscriptionEmployeDto(dtos);
    }

    public ListConfirmationInscriptionEmployeDto getListByOrganisme(Long idOrganisme) {
        List<ConfirmationInscriptionEmployeDto> dtos = repository.getListDtoUsingIdOrganisme(idOrganisme);
        return new ListConfirmationInscriptionEmployeDto(dtos);
    }

    public ChangementMotDePasseDto getChangementMotDePasseByToken(String token) {
        ChangementMotDePasseDto dto = Tools.getValueFromOptionalOrNotFoundException(repository.findChangementMotDePasseDtoUsingToken(token), getEntityClassName());
        hasExpiredException(dto.getPrecription());
        return dto;
    }

    public ChangementMotDePasseDto getChangementMotDePasseInfosByToken(String token) {
        ChangementMotDePasseDto dto = Tools.getValueFromOptionalOrNotFoundException(repository.findChangementMotDePasseDtoInfosUsingToken(token), getEntityClassName());
        hasExpiredException(dto.getPrecription());
        return dto;
    }

    private void hasExpiredException(Instant prescription) {
        if (Tools.hasExpired(prescription)) {
            throw new ServiceException(Tools.message("token.expired"));
        }
    }

    public void supprimerTokenRenewPasswordPerimer() {
        SuperDate date = new SuperDate();
        date.jourSuivant(-1);
        repository.supprimerTokenPerimerForRenewPassword(date.instant());
    }

    public void supprimerTokenModifInfosPersoPerimer() {
        SuperDate date = new SuperDate();
        date.jourSuivant(-1);
        repository.supprimerTokenPerimerForModifInfosPerso(date.instant());
    }

    public List<Utilisateur> getIdsUtilisateurTokenModifInfosPersoPerimer() {
        SuperDate date = new SuperDate();
        date.jourSuivant(-1);
        return repository.getUtilisateursTokenPerimerForModifInfosPerso(date.instant());
    }

    public List<ConfirmationInscriptionEmployeDto> rechercheInvitation(Long idOrganisme, String search) {
        List<ConfirmationInscriptionEmployeDto> resultat = new ArrayList<>();
        if (Tools.testerNulliteString(search)) {
            resultat = repository.rechercheInvitation(idOrganisme, '%' + search + '%');
        }
        return resultat;
    }

    public List<ConfirmationInscriptionEmployeDto> rechercheInvitation(String search) {
        List<ConfirmationInscriptionEmployeDto> resultat = new ArrayList<>();
        if (Tools.testerNulliteString(search)) {
            resultat = repository.rechercheInvitation('%' + search + '%');
        }
        return resultat;
    }

    public Optional<TokenUtilisateur> findTokenInscritionByIdUtilisateur(Long idUtilisateur) {
        return repository.findTokenInscritionByIdUtilisateur(idUtilisateur);
    }
}