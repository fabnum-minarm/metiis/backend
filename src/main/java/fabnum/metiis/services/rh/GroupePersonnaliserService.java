package fabnum.metiis.services.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.rh.GroupePersonnaliser;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.rh.GroupePersonnaliserDetailsDto;
import fabnum.metiis.dto.rh.GroupePersonnaliserDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.dto.tools.OptionListFilterCorps;
import fabnum.metiis.repository.rh.GroupePersonnaliserRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GroupePersonnaliserService extends AbstarctServiceSimple<GroupePersonnaliserRepository, GroupePersonnaliser, Long> {

    private GroupePersonnaliserDetailsService groupePersonnaliserDetailsService;
    private UtilisateurService utilisateurService;
    private FilterService filterService;

    public GroupePersonnaliserService(
            GroupePersonnaliserDetailsService groupePersonnaliserDetailsService,
            @Lazy UtilisateurService utilisateurService,
            @Lazy FilterService filterService,

            GroupePersonnaliserRepository repository) {

        super(repository);
        this.groupePersonnaliserDetailsService = groupePersonnaliserDetailsService;
        this.utilisateurService = utilisateurService;
        this.filterService = filterService;
    }


    public List<GroupePersonnaliserDto> findAllDtoByIdProprietaire(Long idUtilisateur) {
        return repository.findAllDtoUsingIdProprietaire(idUtilisateur);
    }

    public List<GroupePersonnaliserDto> findAllDtoByIdProprietaireWithUtilisateurs(Long idUtilisateur) {
        List<GroupePersonnaliserDto> groupes = findAllDtoByIdProprietaire(idUtilisateur);
        List<GroupePersonnaliserDetailsDto> groupesDetails = groupePersonnaliserDetailsService.findByIdUtilisateur(idUtilisateur);
        groupes.forEach(groupePersonnaliserDto -> groupePersonnaliserDto.addGroupeDetails(groupesDetails));
        return groupes;
    }

    public GroupePersonnaliserDto findDtoById(Long idGroupePersonnaliser) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idGroupePersonnaliser), getEntityClassName());
    }

    public GroupePersonnaliserDto findDtoByIdWithUtilisateurs(Long idGroupePersonnaliser) {
        GroupePersonnaliserDto groupe = findDtoById(idGroupePersonnaliser);
        groupe.setGroupesPersonnaliserDetails(groupePersonnaliserDetailsService.findAllDtoByIdGroupePersonnaliser(idGroupePersonnaliser));
        return groupe;
    }

    public GroupePersonnaliserDto creerFromDto(GroupePersonnaliserDto groupePersonnaliserDto) {
        MetiisValidation.verifier(groupePersonnaliserDto, ContextEnum.CREATE_GROUPE);

        GroupePersonnaliser groupePersonnaliser = new GroupePersonnaliser();
        Utilisateur utilisateur = utilisateurService.findById(groupePersonnaliserDto.getProprietaire().getId());
        groupePersonnaliser.setProprietaire(utilisateur);

        return updateCommun(groupePersonnaliserDto, groupePersonnaliser);
    }

    private GroupePersonnaliserDto updateCommun(GroupePersonnaliserDto groupePersonnaliserDto, GroupePersonnaliser groupePersonnaliser) {
        groupePersonnaliser.setNom(groupePersonnaliserDto.getNom());
        saveOrUpdate(groupePersonnaliser);


        groupePersonnaliserDetailsService.creerListeFromGroupePersonnaliserDetailsDto(groupePersonnaliser, groupePersonnaliserDto.getGroupesPersonnaliserDetails());

        return findDtoByIdWithUtilisateurs(groupePersonnaliser.getId());
    }


    public GroupePersonnaliserDto modifierFromDto(Long idGroupePersonnaliser, GroupePersonnaliserDto groupePersonnaliserDto) {
        MetiisValidation.verifier(groupePersonnaliserDto, ContextEnum.CREATE_GROUPE);
        GroupePersonnaliser groupePersonnaliser = findById(idGroupePersonnaliser);

        return updateCommun(groupePersonnaliserDto, groupePersonnaliser);
    }

    public List<GroupePersonnaliserDto> findAllDtoByIdProprietaireWithUtilisateursAndIdUtilisateur(Long idUtilisateurProprietaire, Long idUtilisateur) {
        return repository.findAllDtoUsingIdProprietaireWitchIdUtilisateurInGroupe(idUtilisateurProprietaire, idUtilisateur);
    }

    public ListFilterCorps findUtilisateurGroupeFiltrer(Long idGroupePersonnaliser, Long idCorps, String search) {
        return filterService.getFilter(OptionListFilterCorps.byCorps(idCorps).andSearch(search))
                .add(utilisateurService.findDtoAllByIdGroupePersonnaliser(idGroupePersonnaliser));
    }

}