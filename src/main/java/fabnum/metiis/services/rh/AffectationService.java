package fabnum.metiis.services.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.rh.Affectation;
import fabnum.metiis.domain.rh.Organisme;
import fabnum.metiis.domain.rh.PosteRo;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.rh.AffectationDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.PosteRoDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.repository.rh.AffectationRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.alerte.AlerteService;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;


@Service
@Transactional
public class AffectationService extends AbstarctServiceSimple<AffectationRepository, Affectation, Long> {

    private OrganismeService organismeService;
    private PosteRoService posteRoService;
    private AlerteService alerteService;

    /**
     * je n'ai pas trouvé de moyen d'utilisé longbok pour generer ce constructeur et sans lui le service plante.
     *
     * @param repository .
     */
    public AffectationService(OrganismeService organismeService,
                              PosteRoService posteRoService,
                              @Lazy AlerteService alerteService,
                              AffectationRepository repository) {
        super(repository);
        this.organismeService = organismeService;
        this.posteRoService = posteRoService;
        this.alerteService = alerteService;
    }

    public void ajouterAffectationUtilisateur(Utilisateur utilisateur, UtilisateurCompletDto utilisateurCompletDto) {
        Optional<Affectation> optionalAffectation = repository.findDerniereAffectation(utilisateurCompletDto.getId());
        optionalAffectation.ifPresent(this::cloturerAncienneAffectation);
        Affectation affectation = new Affectation();
        ajoutAffectation(utilisateurCompletDto, utilisateur, affectation);
        alerteService.supprimerAlerteUtilisateur(utilisateur.getId());
    }

    public void modifierAffectationUtilisateur(Utilisateur utilisateur, UtilisateurCompletDto utilisateurCompletDto) {
        if (getDerniereAffectation(utilisateurCompletDto.getId()) == null
                || !getDerniereAffectation(utilisateurCompletDto.getId()).getOrganisme().getId()
                .equals(utilisateurCompletDto.getAffectation().getOrganisme().getId())) {
            if (utilisateurCompletDto.getAffectation().getOrganisme().getId() != null) {
                ajouterAffectationUtilisateur(utilisateur, utilisateurCompletDto);
            }
        } else {
            AffectationDto affectationDto = new AffectationDto();
            affectationDto.setId(utilisateurCompletDto.getAffectation().getId());
            OrganismeDto organismeDto = organismeService.findDtoById(utilisateurCompletDto.getAffectation().getOrganisme().getId());
            PosteRoDto posteRoDto = null;
            if (utilisateurCompletDto.getAffectation().getPosteRo().getId() != null) {
                posteRoDto = posteRoService.findDtoById(utilisateurCompletDto.getAffectation().getPosteRo().getId());
            }
            affectationDto.setIdUtilistateur(utilisateurCompletDto.getId());
            affectationDto.setDateAffectation(utilisateurCompletDto.getAffectation().getDateAffectation());
            affectationDto.setOrganisme(organismeDto);
            affectationDto.setPosteRo(posteRoDto);
            utilisateurCompletDto.setAffectation(affectationDto);
            modifierAffectation(utilisateurCompletDto, utilisateur);
        }
    }

    public void modifierAffectation(UtilisateurCompletDto utilisateurCompletDto, Utilisateur utilisateur) {
        if (utilisateurCompletDto.getAffectation() != null) {
            MetiisValidation.verifier(utilisateurCompletDto.getAffectation(), ContextEnum.UPDATE_AFFECTATION);
            Affectation affectation = findById(utilisateurCompletDto.getAffectation().getId());
            ajoutAffectation(utilisateurCompletDto, utilisateur, affectation);
        }
    }

    public void ajoutAffectation(UtilisateurCompletDto utilisateurCompletDto, Utilisateur utilisateur, Affectation affectation) {
        Organisme unite = organismeService.findById(utilisateurCompletDto.getAffectation().getOrganisme().getId());
        PosteRo posteRo = null;
        if (utilisateurCompletDto.getAffectation().getPosteRo() != null
                && utilisateurCompletDto.getAffectation().getPosteRo().getId() != null) {
            posteRo = posteRoService.findById(utilisateurCompletDto.getAffectation().getPosteRo().getId());
        }
        affectation.setDateAffectation(utilisateurCompletDto.getAffectation().getDateAffectation());
        affectation.setOrganisme(unite);
        affectation.setPosteRo(posteRo);
        affectation.setUtilisateur(utilisateur);

        saveOrUpdate(affectation);
    }

    public void cloturerAncienneAffectation(Affectation ancienneAffectation) {
        ancienneAffectation.setDateFinAffectation(Instant.now());
        saveOrUpdate(ancienneAffectation);
    }

    public void cloturerAffectation(Affectation affectationActuel, UtilisateurCompletDto utilisateurCompletDto) {
        affectationActuel.setDateFinAffectation(utilisateurCompletDto.getAffectation().getDateFinAffectation());
        saveOrUpdate(affectationActuel);
    }


    public Affectation getDerniereAffectation(Long idUtilisateur) {
        Optional<Affectation> optionalAffectation = repository.findDerniereAffectation(idUtilisateur);
        return optionalAffectation.orElse(null);
    }

    public void creerAffectation(Utilisateur utilisateur, Organisme organisme, PosteRo posteRo) {
        Affectation affectation = new Affectation();
        affectation.setDateAffectation(Instant.now());
        affectation.setOrganisme(organisme);
        affectation.setPosteRo(posteRo);
        affectation.setUtilisateur(utilisateur);
        saveOrUpdate(affectation);
    }

    public Affectation findDerniereAffectation(Long idUtilisateur) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDerniereAffectation(idUtilisateur), getEntityClassName());
    }

    public void modifierPosteAuRo(UtilisateurCompletDto utilisateur) {
        Affectation affectation = findDerniereAffectation(utilisateur.getId());
        PosteRo posteRo = null;
        if (utilisateur.getAffectation().getPosteRo() != null
                && utilisateur.getAffectation().getPosteRo().getId() != null) {
            posteRo = posteRoService.findById(utilisateur.getAffectation().getPosteRo().getId());
        }
        affectation.setPosteRo(posteRo);
        saveOrUpdate(affectation);
    }
}