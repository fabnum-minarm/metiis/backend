package fabnum.metiis.services.rh;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.ToolsRepository;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.administration.UserProfil;
import fabnum.metiis.domain.rh.Organisme;
import fabnum.metiis.domain.rh.TypeOrganisme;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.OrganismeHiearchieDto;
import fabnum.metiis.dto.rh.TypeOrganismeDto;
import fabnum.metiis.dto.rh.TypeOrganismeDtoWithOrganisme;
import fabnum.metiis.dto.statistique.OrganismeStatistiqueCountDto;
import fabnum.metiis.repository.rh.OrganismeRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.administration.ProfilService;
import fabnum.metiis.services.disponibilite.DisponibiliteDate;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

@Service
@Transactional
public class OrganismeService extends AbstarctServiceSimple<OrganismeRepository, Organisme, Long> {

    private TypeOrganismeService typeOrganismeService;
    private UtilisateurService utilisateurService;
    private ProfilService profilService;
    private ActiviteService activiteService;

    /**
     * je n'ai pas trouvé de moyen d'utilisé longbok pour generer ce constructeur et sans lui le service plante.
     *
     * @param repository           OrganismeRepository
     * @param typeOrganismeService TypeOrganismeService
     * @param utilisateurService   UtilisateurService
     * @param profilService        ProfilService
     * @param activiteService      ActiviteService
     */
    public OrganismeService(OrganismeRepository repository, TypeOrganismeService typeOrganismeService,
                            @Lazy UtilisateurService utilisateurService, @Lazy ProfilService profilService,
                            @Lazy ActiviteService activiteService) {
        super(repository);
        this.typeOrganismeService = typeOrganismeService;
        this.utilisateurService = utilisateurService;
        this.profilService = profilService;
        this.activiteService = activiteService;
    }

    public List<OrganismeDto> findDtoAll() {
        return repository.findDtoAll();
    }


    public OrganismeDto findDtoById(Long id) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(id), getEntityClassName());
    }

    @Override
    protected void checkBeforeSaveOrUpdate(Organisme entite) {
        testExist(repository.findByIdNotAndCredo(
                Tools.getValueFromOptional(entite.getId()), entite.getCredo()),
                "organisme.credo.exist");

    }

    public Organisme findAffectationByIdUtilisateur(Long idUtilisateur) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findAffectationByIdUtilisateur(idUtilisateur));
    }

    public OrganismeDto creerFromDto(OrganismeDto organismeDto) {
        MetiisValidation.verifier(organismeDto, ContextEnum.CREATE);
        Organisme organisme = new Organisme();
        return organismeToOrganismeDto(rempliOrganisme(organisme, organismeDto));
    }

    public OrganismeDto modifier(Long idOrganisme, OrganismeDto organismeDto) {
        MetiisValidation.verifier(organismeDto, ContextEnum.UPDATE);
        Organisme organisme = findById(idOrganisme);
        return organismeToOrganismeDto(rempliOrganisme(organisme, organismeDto));
    }

    public Page<OrganismeDto> findDtoAll(Pageable pageable) {
        return repository.findDtoAll(pageable);
    }

    private OrganismeDto organismeToOrganismeDto(Organisme organisme) {
        OrganismeDto organismeDto = new OrganismeDto();
        organismeDto.setId(organisme.getId());
        organismeDto.setCode(organisme.getCode());
        organismeDto.setCredo(organisme.getCredo());
        organismeDto.setLibelle(organisme.getLibelle());
        if (organisme.getParent() != null) {
            organismeDto.setIdparent(organisme.getParent().getId());
            organismeDto.setCodeParent(organisme.getParent().getCode());
            organismeDto.setLibelleParent(organisme.getParent().getLibelle());
        }
        organismeDto.setIdTypeOrganisme(organisme.getTypeOrganisme().getId());
        organismeDto.setLibelleTypeOrganisme(organisme.getTypeOrganisme().getLibelle());

        return organismeDto;
    }

    private Organisme rempliOrganisme(Organisme organisme, OrganismeDto organismeDto) {
        organisme.setCode(organismeDto.getCode());
        organisme.setLibelle(organismeDto.getLibelle());
        organisme.setCredo(organismeDto.getCredo());
        if (organismeDto.getIdparent() != null) {
            Organisme parent = findById(organismeDto.getIdparent());
            organisme.setParent(parent);
        } else {
            organisme.setParent(null);
        }
        TypeOrganisme typeOrganisme = typeOrganismeService.findById(organismeDto.getIdTypeOrganisme());
        organisme.setTypeOrganisme(typeOrganisme);
        saveOrUpdate(organisme);
        return organisme;
    }

    public List<OrganismeDto> findDtoAllByTypeOrganisme(Long idTypeOrganisme) {
        return repository.findDtoUsingIdTypeOrganisme(idTypeOrganisme);
    }

    public List<OrganismeDto> findDtoListByParent(Long idOrganismeParent) {
        return repository.findDtoListUsingParent(idOrganismeParent);
    }

    public List<OrganismeDto> findDtoListByEnfantOrderByHierarchie(Long idOrganismeEnfant) {
        List<OrganismeDto> organismesDto = new ArrayList<>();

        OrganismeDto parent = findDtoById(idOrganismeEnfant);
        organismesDto.add(parent);
        while (parent.getIdparent() != null) {
            parent = findDtoById(parent.getIdparent());
            organismesDto.add(0, parent);
        }
        return organismesDto;
    }

    public List<OrganismeDto> findDtoListByParentAndTypeOrganisme(String currentUserLogin, Long idTypeOrganisme) {
        TypeOrganismeDto typeOrganismeDto = typeOrganismeService.findDtoById(idTypeOrganisme);
        OrganismeDto organismeDto = findDtoParent(currentUserLogin);
        if (organismeDto == null) {
            if (typeOrganismeDto.getIdTypeOrganismeParent() != null) {
                return this.findDtoAllByTypeOrganisme(typeOrganismeDto.getIdTypeOrganismeParent());
            } else {
                return new ArrayList<>();
            }
        }
        Long idOrganismeParent = organismeDto.getId();
        OrganismeDto organisme = this.findDtoById(idOrganismeParent);
        List<OrganismeDto> organismesDto = new ArrayList<>();
        if (typeOrganismeDto.getIdTypeOrganismeParent() != null) {
            if (organisme.getIdTypeOrganisme().equals(typeOrganismeDto.getIdTypeOrganismeParent())) {
                organismesDto.add(organisme);
            } else {
                organismesDto.addAll(findDtoListByParentAndTypeOrganismeParent(idOrganismeParent, typeOrganismeDto.getIdTypeOrganismeParent()));
            }
        }
        return organismesDto;
    }

    public List<OrganismeDto> findDtoListByParentAndTypeOrganismeParent(Long idOrganismeParent, Long idTypeOrganisme) {
        List<OrganismeDto> organismesDto = new ArrayList<>();
        for (OrganismeDto enfant : findDtoListByParent(idOrganismeParent)) {
            if (enfant.getIdTypeOrganisme().equals(idTypeOrganisme)) {
                organismesDto.add(enfant);
            } else {
                organismesDto.addAll(findDtoListByParentAndTypeOrganismeParent(enfant.getId(), idTypeOrganisme));
            }
        }
        return organismesDto;
    }

    public List<OrganismeDto> findDtoListAllByParent(Long idOrganismeParent) {
        List<OrganismeDto> organismesDto = new ArrayList<>();
        addDtoByParent(organismesDto, idOrganismeParent);
        return organismesDto;
    }

    public List<OrganismeDto> findDtoListAllByParent(String currentUserLogin) {
        List<OrganismeDto> organismesDto = new ArrayList<>();
        Long idOrganisme = findDtoParent(currentUserLogin).getId();
        addDtoByParent(organismesDto, idOrganisme);
        return organismesDto;
    }

    public OrganismeDto findDtoParent(String currentUserLogin) {
        Utilisateur connectedUtilisateur = utilisateurService.findByUserName(currentUserLogin);
        TypeOrganisme typeOrganisme = findTypeOrganismeAllowedByProfil(currentUserLogin);
        return getOrganismeParentByTypeOrganisme(connectedUtilisateur.getId(), typeOrganisme);
    }

    public TypeOrganisme findTypeOrganismeAllowedByProfil(String currentUserLogin) {
        User connectedUser = utilisateurService.findByUserName(currentUserLogin).getUser();
        boolean aLeProfilSuperAdmin = connectedUser.getUserProfil().stream().map(UserProfil::getProfil)
                .anyMatch(profil -> profil.getTypeOrganisme() == null);
        if (aLeProfilSuperAdmin) {
            return null;
        } else {
            return connectedUser.getUserProfil().stream().map(UserProfil::getProfil)
                    .map(Profil::getTypeOrganisme).min(Comparator.comparingLong(TypeOrganisme::getOrdre))
                    .orElse(null);
        }
    }

    private void addDtoByParent(List<OrganismeDto> organismesDto, Long idOrganismeParent) {
        for (OrganismeDto enfant : findDtoListByParent(idOrganismeParent)) {
            organismesDto.add(enfant);
            organismesDto.addAll(findDtoListAllByParent(enfant.getId()));
        }
    }

    public List<TypeOrganismeDto> findTypeOrganismeDtoAllByOrganismeParent(String currentUserLogin) {
        OrganismeDto organismeDto = this.findDtoParent(currentUserLogin);
        if (organismeDto == null) {
            return typeOrganismeService.findAllDto();
        }
        return typeOrganismeService.findDtoAllByParent(organismeDto.getIdTypeOrganisme());
    }

    public List<TypeOrganismeDto> findTypeOrganismeAllowedByCodeProfil(String codeProfil) {
        ProfilDto profilDto = profilService.findDtoByCode(codeProfil);
        return typeOrganismeService.findDtoAllByParent(profilDto.getTypeOrganisme().getId());
    }

    public List<TypeOrganismeDtoWithOrganisme> findAllDtoTypeOrganismeWithFirsChildOrganisme() {
        List<TypeOrganismeDto> all = typeOrganismeService.findAllDto();
        List<TypeOrganismeDtoWithOrganisme> retour = new ArrayList<>();
        if (!CollectionUtils.isEmpty(all)) {
            all.forEach(typeOrganismeDto -> retour.add(new TypeOrganismeDtoWithOrganisme(typeOrganismeDto)));
            retour.get(0).setOrganismes(findDtoAllByTypeOrganisme(retour.get(0).getId()));
        }

        return retour;
    }

    public List<OrganismeStatistiqueCountDto> getStatistiqueOrganismeDispo(String dateDebut, String dateFin) {
        SuperDate debutActivite = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee();
        SuperDate finActivite = SuperDate.createFromIsoDateTime(dateFin).finDeLaJournee();
        DisponibiliteDate disponibiliteDate = DisponibiliteDate.getInstance(dateDebut, dateFin);
        List<?> sqlsData = repository.countAllUserAndDispoAndActivite(disponibiliteDate.debutToInstant(), disponibiliteDate.finToInstant(), debutActivite.instant(), finActivite.instant());
        List<OrganismeStatistiqueCountDto> dtos = ToolsRepository.convertir(sqlsData, OrganismeStatistiqueCountDto.class);
        dtos.forEach(organismeStatistiqueCountDto -> organismeStatistiqueCountDto.calculNbJour(disponibiliteDate.debut(), disponibiliteDate.fin()));
        return dtos;
    }

    public List<OrganismeDto> findAllOrganismeAffectableByParent(Long idOrganismeParent) {
        var sqlsData = repository.findAllOrganismeAffectationUsingIdParent(idOrganismeParent);
        return ToolsRepository.convertir(sqlsData, OrganismeDto.class);
    }

    private Long getIdOrganismeParent(String currentUserLogin) {
        TypeOrganisme typeOrganisme = this.findTypeOrganismeAllowedByProfil(currentUserLogin);
        Utilisateur connectedUtilisateur = utilisateurService.findByUserName(currentUserLogin);
        if (typeOrganisme == null) {
            return null;
        } else {
            return Objects.requireNonNull(getOrganismeParentByTypeOrganisme(connectedUtilisateur.getId(), typeOrganisme)).getId();
        }
    }

    public List<OrganismeDto> findAllOrganismesAffectableAllowed(String currentUserLogin) {
        Long idOrganisme = this.getIdOrganismeParent(currentUserLogin);
        if (idOrganisme == null) {
            return repository.findDtoAllAffectable();
        } else {
            return this.findAllOrganismeAffectableByParent(idOrganisme);
        }
    }

    public List<OrganismeDto> findAllOrganismesAllowedActivitePartage(Long idActivite) {
        Activite activite = activiteService.findById(idActivite);
        Long idOrganisme = activite.getOrganisme().getId();
        TypeOrganisme typeOrganisme = typeOrganismeService.findByCode("regiment");
        var sqlsData = repository.findOrganismeParentByTypeOrganisme(idOrganisme, typeOrganisme.getId());
        OrganismeDto organismeParent = ToolsRepository.convertir(sqlsData, OrganismeDto.class).get();
        List<OrganismeDto> organismeDtos = this.findAllOrganismeAffectableByParent(organismeParent.getId());
        Iterator<OrganismeDto> i = organismeDtos.iterator();
        while (i.hasNext()) {
            OrganismeDto organismeDto = i.next();
            if (organismeDto.getId().equals(activite.getOrganisme().getId())) {
                i.remove();
            } else {
                ProfilDto profil = profilService.findDtoByCode("super_employeur");
                organismeDto.setContacts(utilisateurService.getUtilisateurByOrganismeAndProfil(
                        organismeDto.getId(), profil.getId()));
            }
        }
        return organismeDtos;
    }

    public List<OrganismeDto> findAllOrganismesAllowed(String currentUserLogin) {
        List<OrganismeDto> organismeDtos;
        Long idOrganisme = this.getIdOrganismeParent(currentUserLogin);
        Utilisateur connectedUtilisateur = utilisateurService.findByUserName(currentUserLogin);
        if (idOrganisme == null) {
            List<OrganismeDto> listAdmin = repository.findDtoAll();
            listAdmin.add(new OrganismeDto(0L));
            return listAdmin;
        } else {
            organismeDtos = this.findDtoListAllByParent(idOrganisme);
            if (organismeDtos.isEmpty()) {
                Optional<OrganismeDto> optionalOrganismeDto = repository.findAffectationDtoByIdUtilisateur(connectedUtilisateur.getId());
                optionalOrganismeDto.ifPresent(organismeDtos::add);
            }
        }
        return organismeDtos;
    }

    private OrganismeDto getOrganismeParentByTypeOrganisme(Long idUtilisateur, TypeOrganisme typeOrganisme) {
        if (typeOrganisme == null) {
            return null;
        }
        Organisme organisme = this.findAffectationByIdUtilisateur(idUtilisateur);
        var sqlsData = repository.findOrganismeParentByTypeOrganisme(organisme.getId(), typeOrganisme.getId());
        return ToolsRepository.convertir(sqlsData, OrganismeDto.class).get();
    }

    public List<OrganismeHiearchieDto> getAllOrganismeHiearchie(String currentUserLogin) {
        Utilisateur connectedUtilisateur = utilisateurService.findByUserName(currentUserLogin);
        TypeOrganisme typeOrganisme = typeOrganismeService.findByCode("brigade");
        TypeOrganisme typeOrganismeAllowed = this.findTypeOrganismeAllowedByProfil(currentUserLogin);
        OrganismeDto dto = getOrganismeParentByTypeOrganisme(connectedUtilisateur.getId(), typeOrganisme);
        Long idOrganisme = this.getIdOrganismeParent(currentUserLogin);
        List<OrganismeHiearchieDto> brigades;
        if (idOrganisme == null) {
            brigades = repository.findDtoHiearchieUsingIdTypeOrganisme(typeOrganisme.getId());
        } else {
            brigades = repository.findDtoHiearchieUsingId(dto.getId());
        }
        brigades.forEach(brigade -> {
            if (typeOrganismeAllowed != null && typeOrganismeAllowed.getCode().equals("regiment")) {
                brigade.setOrganismesEnfants(organismeHiearchieEnfants(repository.findDtoListUsingid(idOrganisme)));
            } else {
                brigade.setOrganismesEnfants(organismeHiearchieEnfants(findDtoListByParent(brigade.getId())));
            }
            brigade.getOrganismesEnfants().forEach(regiment -> {
                regiment.setOrganismesEnfants(organismeHiearchieEnfants(findDtoListByParent(regiment.getId())));
            });
        });
        return brigades;
    }

    public List<OrganismeHiearchieDto> organismeHiearchieEnfants(List<OrganismeDto> organismeDtos) {
        List<OrganismeHiearchieDto> enfants = new ArrayList<>();
        organismeDtos.forEach(organismeDto -> enfants.add(new OrganismeHiearchieDto(organismeDto)));
        return enfants;
    }
}