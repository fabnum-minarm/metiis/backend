package fabnum.metiis.services.audit;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.abstractions.IAuditEntity;
import fabnum.metiis.dto.audit.CreatedUpdatedByDto;
import fabnum.metiis.repository.audit.AuditRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class AuditService {

    private AuditRepository repository;

    public CreatedUpdatedByDto getById(Class<? extends IAuditEntity> classe, Long id) {
        String table = classe.getSimpleName();
        return Tools.getValueFromOptionalOrNotFoundException(repository.getById(table, id), table);
    }

}
