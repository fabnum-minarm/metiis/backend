package fabnum.metiis.services.exceptions;

public final class ServiceConstraintException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ServiceConstraintException() {
    }

    public ServiceConstraintException(String message) {
        super(message);
    }

    public ServiceConstraintException(Throwable cause) {
        super(cause);
    }

    public ServiceConstraintException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceConstraintException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}