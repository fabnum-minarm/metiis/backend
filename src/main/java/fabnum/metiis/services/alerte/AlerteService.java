package fabnum.metiis.services.alerte;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.activite.Activite;
import fabnum.metiis.domain.alerte.Alerte;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.ChoisirDataEntity;
import fabnum.metiis.dto.alerte.AlerteDto;
import fabnum.metiis.dto.alerte.AlertesDto;
import fabnum.metiis.dto.alerte.ParticipationDataEntity;
import fabnum.metiis.repository.alerte.AlerteRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.notification.NotificationService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class AlerteService extends AbstarctServiceSimple<AlerteRepository, Alerte, Long> {

    private final UtilisateurService utilisateurService;
    private final NotificationService notificationService;

    public AlerteService(
            UtilisateurService utilisateurService,
            AlerteRepository repository, NotificationService notificationService) {
        super(repository);

        this.utilisateurService = utilisateurService;
        this.notificationService = notificationService;
    }


    public AlertesDto findDtoByUserNameWithNonLue(String username, Integer limit) {
        Utilisateur utilisateur = utilisateurService.findByUserName(username);

        List<AlerteDto> alertes;
        if (limit != null) {
            alertes = repository.findDtoUsingIdDestinataire(utilisateur.getId(), PageRequest.of(0, limit));
        } else {
            alertes = repository.findDtoUsingIdDestinataire(utilisateur.getId());
        }
        return new AlertesDto(alertes, totalNonLuByUserName(utilisateur.getId()));
    }

    public Long totalNonLuByUserName(Long idUtilisateur) {
        return repository.alerteTotalNonLuUsingIdDestinataire(idUtilisateur);
    }

    public void lireAlertes(String username) {
        Utilisateur utilisateur = utilisateurService.findByUserName(username);
        repository.lireAlertes(utilisateur.getId());
    }

    public void supprimerReferencesAUneActiviteDansLesAlertes(Long idActivite) {
        repository.supprimerReferencesAUneActiviteDansLesAlertes(idActivite);
    }

    public void informerCreationActivite(Activite activite) {
        SuperDate debut = new SuperDate(activite.getDateDebut());
        SuperDate fin = new SuperDate(activite.getDateFin());
        repository.creerAlerteActiviteUnite(activite.getId(), activite.getNom(), Tools.message("alerte.creation.activite", debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA()));
    }

    public void informerDemandeRenfort(ActiviteDto activite, Long idEmeteur, Long idDestinataire) {
        SuperDate debut = new SuperDate(activite.getDateDebut());
        SuperDate fin = new SuperDate(activite.getDateFin());
        repository.creerAlertDemanderenfort(
                activite.getId(),
                idEmeteur,
                idDestinataire,
                activite.getNom(),
                Tools.message("alerte.activite.renfort", activite.getOrganisme().getCode(), debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA())
        );
    }

    public void informerCreationActivitePartage(Activite activite, Activite activiteParent) {
        SuperDate debut = new SuperDate(activite.getDateDebut());
        SuperDate fin = new SuperDate(activite.getDateFin());
        repository.creerAlertCreationActivitePartage(
                activiteParent.getId(),
                activite.getCreateur().getId(),
                activiteParent.getCreateur().getId(),
                activite.getNom(),
                Tools.message("alerte.activite.renfort.cree", activite.getOrganisme().getCode(), debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA())
        );
    }

    public void informerParticipation(ChoisirDataEntity choisirDataEntity, ParticipationDataEntity participationDataEntity) {
        if ((choisirDataEntity.getChoix().getStatut().getCode().equals("retenus") && notificationService.findNotifUsingIdUtilisateurAndIdTypeActivite(participationDataEntity.getParticiper().getUtilisateur().getId(), choisirDataEntity.getActivite().getTypeActivite().getId()).getNotifAppCandidatRetenu())
                || (choisirDataEntity.getChoix().getStatut().getCode().equals("non-retenus") && notificationService.findNotifUsingIdUtilisateurAndIdTypeActivite(participationDataEntity.getParticiper().getUtilisateur().getId(), choisirDataEntity.getActivite().getTypeActivite().getId()).getNotifAppCandidatNonRetenu())
                || choisirDataEntity.getChoix().getStatut().getCode().equals("convoques") || choisirDataEntity.getChoix().getStatut().getCode().equals("realises")
                || choisirDataEntity.getChoix().getStatut().getCode().equals("non-realises") || choisirDataEntity.getChoix().getStatut().getCode().equals("indisponibles")
                || choisirDataEntity.getChoix().getStatut().getCode().equals("candidats") || choisirDataEntity.getChoix().getStatut().getCode().equals("non-convoques")) {

            var alerte = getAlerteParticipation(choisirDataEntity);
            var message = choisirDataEntity.getChoix().getStatut().getAction() + ".";
            var commentaire = participationDataEntity.getParticiper().getDernierAvancementParticipation().getCommentaire();
            if (Tools.testerNulliteString(commentaire)) {
                alerte.setCommentaire(commentaire);
            }
            alerte.setDescription(message);
            alerte.setDestinataire(participationDataEntity.getParticiper().getUtilisateur());
            saveOrUpdate(alerte);

        }
    }

    private Alerte getAlerteMaParticipation(ChoisirDataEntity choisirDataEntity, ParticipationDataEntity participationDataEntity) {
        Alerte alerte = getAlerteParticipation(choisirDataEntity);
        alerte.setStatut(choisirDataEntity.getStatut());
        alerte.setDestinataire(choisirDataEntity.getActivite().getCreateur());
        alerte.setNombre(0L);
        return alerte;
    }

    private Alerte getAlerteParticipation(ChoisirDataEntity choisirDataEntity) {
        Alerte alerte = new Alerte();
        alerte.setActivite(choisirDataEntity.getActivite());
        alerte.setTitre(choisirDataEntity.getActivite().getNom());
        alerte.setEmetteur(choisirDataEntity.getValidateur());
        alerte.setTypeActivite(choisirDataEntity.getActivite().getTypeActivite());
        return alerte;
    }

    public void informerMaParticipation(ChoisirDataEntity choisirDataEntity, ParticipationDataEntity participationDataEntity) {
        Optional<Alerte> alerteOptional = repository.findExistanteByActiviteAndStatut(choisirDataEntity.getActivite().getId(), choisirDataEntity.getStatut().getId());
        Optional<Alerte> alerteOptionalEmetteur = repository.findExistanteByActiviteStatutAndEmetteur(
                choisirDataEntity.getActivite().getId(),
                choisirDataEntity.getStatut().getId(),
                choisirDataEntity.getValidateur().getId());
        Alerte alerte = alerteOptional.orElseGet(() -> getAlerteMaParticipation(choisirDataEntity, participationDataEntity));

        String message = Tools.message("alerte.ma.participation", choisirDataEntity.getValidateur().getNom(),
                choisirDataEntity.getValidateur().getPrenom(),
                choisirDataEntity.getStatut().getActionUtilisateur());
        alerte.setEmetteur(choisirDataEntity.getValidateur());
        alerte.setCommentaire(participationDataEntity.getAvancementParticipation().getCommentaire());

        if (alerteOptionalEmetteur.isEmpty()) {
            alerte.incrementerNombre();
        }

        if (alerte.getNombre() > 2) {
            message += " " + Tools.message("alerte.ma.participation.ainsi", alerte.getNombre() - 1L);
        }
        if (alerte.getNombre() == 2) {
            message += " " + Tools.message("alerte.ma.participation.ainsi.1");
        }

        alerte.setDateAlerte(Instant.now());
        alerte.setLu(false);
        alerte.setDescription(message);


        saveOrUpdate(alerte);
    }


    public void supprimerAlertePerimer() {
        SuperDate date = new SuperDate();
        date.anneePrecedente().debutDeLaJournee(); // on garde 1 ans d'historique.
        repository.deleteAlerteLuBeforeDate(date.instant());
    }

    public void supprimerAlerteUtilisateur(Long idUtilisateur) {
        repository.deleteAlerteUtilisateur(idUtilisateur);
    }

    public void informerActiviteModifiee(Activite activite) {
        informerParticipantsActiviteModifiee(activite);
    }


    public void informerParticipantsActiviteModifiee(Activite activite) {
        Utilisateur currentUtilisateur = utilisateurService.findByUserName(SecurityUtils.getCurrentUserLogin());
        SuperDate debut = new SuperDate(activite.getDateDebut());
        SuperDate fin = new SuperDate(activite.getDateFin());
        repository.creerAlertActiviteModifierAllParticipant(activite.getId(), currentUtilisateur.getId(), activite.getNom(),
                Tools.message("alerte.service.modification.activite.participant",
                        debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA()), null);

        repository.creerAlertActiviteCreateur(activite.getId(), currentUtilisateur.getId(), activite.getNom(),
                Tools.message("alerte.service.modification.activite.createur",
                        debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA()), null);
    }


    public void informerActiviteSupprimee(Activite activite, String commentaire) {
        Utilisateur currentUtilisateur = utilisateurService.findByUserName(SecurityUtils.getCurrentUserLogin());
        SuperDate debut = new SuperDate(activite.getDateDebut());
        SuperDate fin = new SuperDate(activite.getDateFin());

        String messageParticipant = Tools.message("alerte.service.suppression.activite.participant",
                debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA());
        String messageCreateur = Tools.message("alerte.service.suppression.activite.createur",
                debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA());

        repository.creerAlertActiviteSupprimerAllParticipant(activite.getId(), currentUtilisateur.getId(), activite.getNom(), messageParticipant, commentaire);

        repository.creerAlertActiviteCreateur(activite.getId(), currentUtilisateur.getId(), activite.getNom(),
                messageCreateur, commentaire);
    }

    public void informerParentActiviteSupprimee(Activite activite, Activite activiteParent, String commentaire) {
        Utilisateur currentUtilisateur = utilisateurService.findByUserName(SecurityUtils.getCurrentUserLogin());

        String message = Tools.message("alerte.service.suppression.activite.partage", activite.getOrganisme().getCode());

        repository.creerAlertSuppressionActivitePartage(activiteParent.getId(), currentUtilisateur.getId(),
                activiteParent.getCreateur().getId(), activite.getNom(), message, commentaire);
    }

    public void envoyerRappelActivite(Activite activite) {
        SuperDate debut = new SuperDate(activite.getDateDebut());
        SuperDate fin = new SuperDate(activite.getDateFin());
        String description = Tools.message("alerte.service.activite.rappel",
                debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA());

        repository.creerAlertRappelActivite(activite.getId(), activite.getNom(), description);
    }

    public void envoyerRappelParticipationActivite(Activite activite) {
        SuperDate debut = new SuperDate(activite.getDateDebut());
        SuperDate fin = new SuperDate(activite.getDateFin());
        String description = Tools.message("alerte.service.activite.rappel.participation",
                debut.toStringDDMMAAAA(), fin.toStringDDMMAAAA());

        repository.creerAlertRappelParticipationActivite(activite.getId(), activite.getNom(), description);
    }
}
