package fabnum.metiis.services.notification;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.activite.TypeActivite;
import fabnum.metiis.domain.notification.Notification;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.notification.NotificationDto;
import fabnum.metiis.repository.notification.NotificationRepository;
import fabnum.metiis.services.abstraction.AbstarctServiceSimple;
import fabnum.metiis.services.activite.TypeActiviteService;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class NotificationService extends AbstarctServiceSimple<NotificationRepository, Notification, Long> {
    private TypeActiviteService typeActiviteService;

    public NotificationService(@Lazy TypeActiviteService typeActiviteService,
                               NotificationRepository repository) {

        super(repository);
        this.typeActiviteService = typeActiviteService;
    }

    public List<NotificationDto> findAllUsingIdUtilisateur(Long idUtilisateur) {
        return repository.findDtoUsingIdUtilisateur(idUtilisateur);
    }

    public NotificationDto findNotifUsingIdUtilisateurAndIdTypeActivite(Long idUtilisateur, Long idTypeActivite) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findNotifUsingIdUtilisateurAndIdTypeActivite(idUtilisateur, idTypeActivite));
    }

    public NotificationDto findDtoById(Long id) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(id));
    }

    @Override
    public Notification findById(Long aLong) {
        return super.findById(aLong);
    }

    public NotificationDto modifierFromDto(Long idNotification, NotificationDto notificationDto) {
        MetiisValidation.verifier(notificationDto, ContextEnum.UPDATE_NOTIF);
        Notification notification = findById(idNotification);
        modifierNotification(notification, notificationDto);
        saveOrUpdate(notification);
        return findDtoById(idNotification);
    }

    private void modifierNotification(Notification notification, NotificationDto notificationDto) {
        notification.setNotifAppActiviteCree(notificationDto.getNotifAppActiviteCree());
        notification.setNotifAppActiviteModif(notificationDto.getNotifAppActiviteModif());
        notification.setNotifAppActiviteRappel(notificationDto.getNotifAppActiviteRappel());
        notification.setNotifAppActiviteSupp(notificationDto.getNotifAppActiviteSupp());
        notification.setNotifAppCandidatRetenu(notificationDto.getNotifAppCandidatRetenu());
        notification.setNotifAppCandidatNonRetenu(notificationDto.getNotifAppCandidatNonRetenu());
        notification.setNotifAppCandidatConvoque(notificationDto.getNotifAppCandidatConvoque());
        notification.setNotifAppCandidatNonConvoque(notificationDto.getNotifAppCandidatNonConvoque());
        notification.setNotifAppPartageActiviteAutreUnite(notificationDto.getNotifAppPartageActiviteAutreUnite());
        notification.setNotifAppCreaActiviteAutreUnite(notificationDto.getNotifAppCreaActiviteAutreUnite());
        notification.setNotifAppModifActivitePartage(notificationDto.getNotifAppModifActivitePartage());
        notification.setNotifAppSuppActivitePartage(notificationDto.getNotifAppSuppActivitePartage());
        notification.setNotifMailActiviteCree(notificationDto.getNotifMailActiviteCree());
        notification.setNotifMailActiviteModif(notificationDto.getNotifMailActiviteModif());
        notification.setNotifMailActiviteRappel(notificationDto.getNotifMailActiviteRappel());
        notification.setNotifMailActiviteSupp(notificationDto.getNotifMailActiviteSupp());
        notification.setNotifMailCandidatRetenu(notificationDto.getNotifMailCandidatRetenu());
        notification.setNotifMailCandidatNonRetenu(notificationDto.getNotifMailCandidatNonRetenu());
        notification.setNotifMailCandidatConvoque(notificationDto.getNotifMailCandidatConvoque());
        notification.setNotifMailCandidatNonConvoque(notificationDto.getNotifMailCandidatNonConvoque());
        notification.setNotifMailPartageActiviteAutreUnite(notificationDto.getNotifMailPartageActiviteAutreUnite());
        notification.setNotifMailCreaActiviteAutreUnite(notificationDto.getNotifMailCreaActiviteAutreUnite());
        notification.setNotifMailModifActivitePartage(notificationDto.getNotifMailModifActivitePartage());
        notification.setNotifMailSuppActivitePartage(notificationDto.getNotifMailSuppActivitePartage());
    }

    public void creerNotification(Utilisateur utilisateur, TypeActivite typeActivite) {
        Notification newNotification = new Notification();
        newNotification.setTypeActivite(typeActivite);
        newNotification.setUtilisateur(utilisateur);
        newNotification.setNotifAppActiviteCree(true);
        newNotification.setNotifAppActiviteModif(true);
        newNotification.setNotifAppActiviteRappel(true);
        newNotification.setNotifAppActiviteSupp(true);
        newNotification.setNotifAppCandidatConvoque(true);
        newNotification.setNotifAppCandidatNonConvoque(true);
        newNotification.setNotifAppCandidatRetenu(true);
        newNotification.setNotifAppCandidatNonRetenu(true);
        newNotification.setNotifAppCreaActiviteAutreUnite(false);
        newNotification.setNotifAppModifActivitePartage(false);
        newNotification.setNotifAppPartageActiviteAutreUnite(false);
        newNotification.setNotifAppSuppActivitePartage(false);
        newNotification.setNotifMailActiviteCree(true);
        newNotification.setNotifMailActiviteModif(true);
        newNotification.setNotifMailActiviteRappel(true);
        newNotification.setNotifMailActiviteSupp(true);
        newNotification.setNotifMailCandidatConvoque(true);
        newNotification.setNotifMailCandidatNonConvoque(true);
        newNotification.setNotifMailCandidatRetenu(true);
        newNotification.setNotifMailCandidatNonRetenu(true);
        newNotification.setNotifMailCreaActiviteAutreUnite(false);
        newNotification.setNotifMailModifActivitePartage(false);
        newNotification.setNotifMailPartageActiviteAutreUnite(false);
        newNotification.setNotifMailSuppActivitePartage(false);
        saveOrUpdate(newNotification);
    }

    public void creerNotifications(Utilisateur utilisateur) {
        List<TypeActivite> typeActivites = typeActiviteService.findAll();
        typeActivites.forEach(typeActivite -> {
            creerNotification(utilisateur, typeActivite);
        });
    }
}
