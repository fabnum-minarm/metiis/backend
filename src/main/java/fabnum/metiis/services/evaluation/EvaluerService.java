package fabnum.metiis.services.evaluation;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.evaluation.Evaluer;
import fabnum.metiis.dto.evaluation.EvaluerDto;
import fabnum.metiis.repository.evaluation.EvaluerRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.rh.UtilisateurService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class EvaluerService extends AbstarctService<EvaluerRepository, Evaluer, Long> {

    private UtilisateurService utilisateurService;

    public EvaluerService(UtilisateurService utilisateurService,
                          EvaluerRepository repository) {
        super(repository);
        this.utilisateurService = utilisateurService;
    }

    public Page<EvaluerDto> findDtoPageableAll(Pageable pageable) {
        return this.repository.findDtoPageableAll(pageable);
    }

    public List<EvaluerDto> findDtoAll() {
        return this.repository.findDtoAll();
    }


    public EvaluerDto creer(EvaluerDto evaluerDto) {

        MetiisValidation.verifier(evaluerDto, ContextEnum.CREATE_EVALUATION);

        Evaluer evaluer = new Evaluer();

        evaluer.setNoteUtilisation(evaluerDto.getNoteUtilisation());
        evaluer.setNoteBesoin(evaluerDto.getNoteBesoin());
        evaluer.setCommentaire(evaluerDto.getCommentaire());
        evaluer.setUtilisateur(utilisateurService.findById(evaluerDto.getUtilisateur().getId()));

        saveOrUpdate(evaluer);

        return repository.findDtoById(evaluer.getId());
    }

    public Double moyenneUtilisation() {
        return this.repository.moyenneNoteUtilisation();
    }

    public Double moyenneBesoin() {
        return this.repository.moyenneNoteBesoin();
    }
}
