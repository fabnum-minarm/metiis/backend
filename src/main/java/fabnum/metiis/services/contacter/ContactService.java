package fabnum.metiis.services.contacter;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.contacter.Contact;
import fabnum.metiis.dto.contacter.ContactDto;
import fabnum.metiis.repository.contacter.ContactRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.mail.MailService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ContactService extends AbstarctService<ContactRepository, Contact, Long> {

    private MailService mailService;

    public ContactService(MailService mailService,
                          ContactRepository repository) {
        super(repository);
        this.mailService = mailService;
    }

    public Page<ContactDto> findDtoOld(Pageable pageable) {
        return repository.findOld(pageable);
    }

    public void creer(ContactDto contactDto) {

        MetiisValidation.verifier(contactDto, ContextEnum.CONTACT);

        Contact contact = new Contact();
        contact.setNom(contactDto.getNom());
        contact.setPrenom(contactDto.getPrenom());
        contact.setEmail(contactDto.getEmail());
        contact.setObjet(contactDto.getObjet());
        contact.setMessage(contactDto.getMessage());

        if (!SecurityUtils.getCurrentUserLogin().equals(SecurityUtils.ANONYMOUS_USER)) {
            saveOrUpdate(contact);
        }

        mailService.mailContact(contact);
    }

    public ContactDto findDtoByIdOnly(Long idContact) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idContact), getEntityClassName());
    }

    public void lu(Long idContact) {
        Contact contact = findById(idContact);
        contact.setLu(true);
        saveOrUpdate(contact);
    }

    public void traite(Long idContact) {
        Contact contact = findById(idContact);
        contact.setLu(true);
        contact.setTraite(true);
        saveOrUpdate(contact);
    }

    public List<ContactDto> findDtoNonLu() {
        return repository.findDtoNonLu();
    }

    public List<ContactDto> findDtoNonTraite() {
        return repository.findDtoNonTraite();
    }
}
