package fabnum.metiis.services.statistique;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.domain.activite.TypeActivite;
import fabnum.metiis.domain.nouveaute.Nouveaute;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import fabnum.metiis.dto.statistique.StatistiqueUtilisateurDTO;
import fabnum.metiis.dto.tools.Taux;
import fabnum.metiis.repository.nouveaute.NouveauteRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.activite.TypeActiviteService;
import fabnum.metiis.services.disponibilite.DisponibiliteService;
import fabnum.metiis.services.rh.UtilisateurService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class StatistiqueService extends AbstarctService<NouveauteRepository, Nouveaute, Long> {


    private UtilisateurService utilisateurService;
    private TypeActiviteService typeActiviteService;
    private DisponibiliteService disponibiliteService;
    private ActiviteService activiteService;

    public StatistiqueService(NouveauteRepository repository,
                              UtilisateurService utilisateurService,
                              TypeActiviteService typeActiviteService,
                              DisponibiliteService disponibiliteService,
                              ActiviteService activiteService) {

        super(repository);
        this.utilisateurService = utilisateurService;
        this.activiteService = activiteService;
        this.typeActiviteService = typeActiviteService;
        this.disponibiliteService = disponibiliteService;
    }


    public void readAll(String login) {
        UtilisateurDto utilisateur = utilisateurService.findDtoOnlyByUserName(login);
        repository.readAll(utilisateur.getId());
    }

    public StatistiqueUtilisateurDTO statistiqueUtilisateur(String dateDebut, String dateFin, Long idUtilisateur) {
        StatistiqueUtilisateurDTO statistiqueUtilisateurDTO = new StatistiqueUtilisateurDTO();
        List<Long> nbActiviteRealise = new ArrayList<>();
        List<Long> avancementRealise = new ArrayList<>();
        List<Long> nbActiviteConvoque = new ArrayList<>();
        List<Long> avancementConvoque = new ArrayList<>();
        List<ActiviteDto> dernierActivite = new ArrayList<>();

        statistiqueUtilisateurDTO.setJourDisponible(disponibiliteService.compterDisponiblite(dateDebut, dateFin, idUtilisateur));

        statistiqueUtilisateurDTO.setJourIndisponible(disponibiliteService.compterIndisponiblite(dateDebut, dateFin, idUtilisateur));

        statistiqueUtilisateurDTO.setJourNonRenseigne(calculJourNonRenseigne(dateDebut, dateFin, statistiqueUtilisateurDTO.getJourDisponible(), statistiqueUtilisateurDTO.getJourIndisponible()));

        statistiqueUtilisateurDTO.setActivitesRealises(activiteService.findDTOAllActiviteRealise(dateDebut, dateFin, idUtilisateur));

        statistiqueUtilisateurDTO.setActivitesConvoques(activiteService.findDTOAllActiviteConvoque(dateDebut, dateFin, idUtilisateur));

        statistiqueUtilisateurDTO.setNbCandidature(activiteService.compterCandidature(dateDebut, dateFin, idUtilisateur));

        statistiqueUtilisateurDTO.setNbConvocation(activiteService.compterConvoque(dateDebut, dateFin, idUtilisateur));

        statistiqueUtilisateurDTO.setJourEnActivite(compterJourEnActivite(statistiqueUtilisateurDTO.getActivitesRealises()));


        // diver formation mission preparation soutien
        for (TypeActivite ta : typeActiviteService.findAllOrder()) {
            nbActiviteRealise.add(activiteService.compterActiviteRealise(dateDebut, dateFin, idUtilisateur, ta.getId()));
            avancementRealise.add(calculerAvacement(statistiqueUtilisateurDTO.getActivitesRealises().size(),
                    activiteService.compterActiviteRealise(dateDebut, dateFin, idUtilisateur, ta.getId())));

            nbActiviteConvoque.add(activiteService.compterActiviteConvoque(dateDebut, dateFin, idUtilisateur, ta.getId()));
            avancementConvoque.add(calculerAvacement(statistiqueUtilisateurDTO.getActivitesConvoques().size(),
                    activiteService.compterActiviteConvoque(dateDebut, dateFin, idUtilisateur, ta.getId())));

            dernierActivite.add(activiteService.findLastActivite(idUtilisateur, ta.getId()));
        }
        statistiqueUtilisateurDTO.setActivitesRealiser(nbActiviteRealise);
        statistiqueUtilisateurDTO.setAvancementActiviteRealise(avancementRealise);
        statistiqueUtilisateurDTO.setActivitesConvoque(nbActiviteConvoque);
        statistiqueUtilisateurDTO.setAvancementActiviteConvoque(avancementConvoque);
        statistiqueUtilisateurDTO.setDerniereActivites(dernierActivite);

        return statistiqueUtilisateurDTO;
    }

    private Long calculerAvacement(int total, Long nombre) {
        Taux taux = new Taux();
        taux.addTotal(total);
        taux.add(nombre);
        return taux.pourcentage().longValue();
    }

    private Long compterJourEnActivite(List<ActiviteDto> activiteDtos) {
        Long jourTotal = 0L;
        for (ActiviteDto a : activiteDtos) {
            SuperDate debut = new SuperDate(a.getDateDebut());
            SuperDate fin = new SuperDate(a.getDateFin());
            jourTotal = jourTotal + debut.compterNombreDeJourDebutInclus(fin);
        }
        return jourTotal;
    }

    private Long calculJourNonRenseigne(String dateDebut, String dateFin, Long jourDispo, Long jourIndispo) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut);
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin);
        Long jourTotal = debut.compterNombreDeJourDebutInclus(fin);
        jourTotal = jourTotal - jourDispo - jourIndispo;
        return jourTotal;
    }
}
