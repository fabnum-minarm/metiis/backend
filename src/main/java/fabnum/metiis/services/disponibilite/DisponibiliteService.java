package fabnum.metiis.services.disponibilite;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.disponibilite.Disponibilite;
import fabnum.metiis.domain.disponibilite.TypeDisponibilite;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.activite.*;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.dto.disponibilite.DisponibiliteOrganismeDto;
import fabnum.metiis.dto.disponibilite.DisponibiliteSurDuree;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.repository.disponibilite.DisponibiliteRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.activite.ParticiperService;
import fabnum.metiis.services.exceptions.ServiceException;
import fabnum.metiis.services.rh.CorpsService;
import fabnum.metiis.services.rh.UtilisateurService;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class DisponibiliteService extends AbstarctService<DisponibiliteRepository, Disponibilite, Long> {

    private UtilisateurService utilisateurService;
    private TypeDisponibiliteService typeDisponibiliteService;
    private CorpsService corpsService;
    private ParticiperService participerService;
    private ActiviteService activiteService;

    public DisponibiliteService(UtilisateurService utilisateurService,
                                TypeDisponibiliteService typeDisponibiliteService,
                                CorpsService corpsService,
                                @Lazy ParticiperService participerService,
                                @Lazy ActiviteService activiteService,
                                DisponibiliteRepository repository) {
        super(repository);
        this.utilisateurService = utilisateurService;
        this.corpsService = corpsService;
        this.typeDisponibiliteService = typeDisponibiliteService;
        this.participerService = participerService;
        this.activiteService = activiteService;
    }

    public List<Disponibilite> findListBeetween(Long idUtilisateur, Instant debut, Instant fin) {
        return repository.findListBeetween(idUtilisateur, debut, fin);
    }

    public List<Disponibilite> findListEstDisponibleBeetween(Long idUtilisateur, Instant debut, Instant fin, Boolean estDisponible) {
        return repository.findListEstDisponibleBeetween(idUtilisateur, debut, fin, estDisponible);
    }

    public List<DisponibiliteDto> createOrUpdate(DisponibiliteDto disponibilite, Long idUtilisateur) {
        Utilisateur utilisateur = utilisateurService.findById(idUtilisateur);
        return createOrUpdate(disponibilite, utilisateur);
    }

    public List<DisponibiliteDto> createOrUpdate(DisponibiliteDto disponibilite, String currentUsername) {
        Utilisateur utilisateur = utilisateurService.findByUserName(currentUsername);
        testerParticipeAUneActivitePasDispo(disponibilite, currentUsername);
        return createOrUpdate(disponibilite, utilisateur);
    }

    public void testerParticipeAUneActivitePasDispo(DisponibiliteDto disponibilite, String currentUsername) {
        TypeDisponibilite typeDisponibilite = typeDisponibiliteService.findById(disponibilite.getTypeDisponibilite().getId());
        List<ActiviteDto> activiteDtos = activiteService.findDtoActiviteWithUtilisateurParticipeNonDispo(disponibilite, currentUsername);
        if (!typeDisponibilite.getEstDisponible() && !activiteDtos.isEmpty()) {
            throw new ServiceException(Tools.message("dispo.participe.activite"));
        }
    }

    private List<DisponibiliteDto> createOrUpdate(DisponibiliteDto disponibiliteDto, Utilisateur utilisateur) {
        //long begin = Tools.currentTimeMillis();
        MetiisValidation.verifier(disponibiliteDto, ContextEnum.CREATE_DISPONIBILITE);

        DisponibiliteDate dates = DisponibiliteDate.getInstance(disponibiliteDto);

        if (disponibiliteDto.getCommentaire() != null && disponibiliteDto.getCommentaire().trim() == "") {
            disponibiliteDto.setCommentaire(null);
        }

        if (dates.debut().estApres(dates.fin())) {
            throw new ServiceException(Tools.message("dispo.date.incoherente"));
        }

        TypeDisponibilite typeDisponibilite = typeDisponibiliteService.findById(disponibiliteDto.getTypeDisponibilite().getId());

        deleteOldDisponibilite(utilisateur, dates.debut(), dates.fin());

        List<Disponibilite> disponibilites = new ArrayList<>();

        SuperDate startDispo = dates.debut().copie();
        while (startDispo.estAvantOuEgale(dates.fin())) {
            Disponibilite disponibilite = getDisponibilite(disponibiliteDto, utilisateur, typeDisponibilite, startDispo);
            disponibilites.add(disponibilite);
        }
        repository.saveAll(disponibilites);

        //Tools.duration(begin, "createOrUpdate " + disponibilites.size() + " dispo : ");

        return repository.findDtoListBeetweenUsingUtilisateur(utilisateur.getId(), dates.debutToInstant(), dates.finToInstant());
    }

    private void deleteOldDisponibilite(Utilisateur utilisateur, SuperDate debut, SuperDate fin) {
        repository.deleteListBeetween(utilisateur.getId(), debut.instant(), fin.instant());
    }

    public void deleteDisponibilite(Long idUtilisateur, String debut, String fin) {
        DisponibiliteDate dates = DisponibiliteDate.getInstance(debut, fin);
        repository.deleteListBeetween(idUtilisateur, dates.debutToInstant(), dates.finToInstant());
    }

    private Disponibilite getDisponibilite(DisponibiliteDto disponibiliteDto, Utilisateur utilisateur,
                                           TypeDisponibilite typeDisponibilite,
                                           SuperDate startDispo) {

        Disponibilite disponibilite = new Disponibilite();
        disponibilite.setCommentaire(disponibiliteDto.getCommentaire());
        disponibilite.setUtilisateur(utilisateur);
        disponibilite.setTypeDisponibilite(typeDisponibilite);
        disponibilite.setDateDebut(startDispo.copie().debutDeLaJournee().instant());
        disponibilite.setDateFin(startDispo.copie().finDeLaJournee().instant());
        startDispo.jourSuivant();

        return disponibilite;
    }

    public List<DisponibiliteDto> getListByIdUtilisateur(Long idUtilisateur) {
        return repository.findAllUsingIdUtilisateur(idUtilisateur);
    }

    public List<DisponibiliteDto> getListByIdUtilisateurBeetween(Long idUtilisateur, String dateDebut, String dateFin) {
        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);
        return repository.findDtoListBeetweenUsingUtilisateur(idUtilisateur, dates.debutToInstant(), dates.finToInstant());
    }

    public List<DisponibiliteDto> getListByUtilisateurConnecterBeetween(SuperDate debut, SuperDate fin) {
        var utilisateurConnecter = utilisateurService.findDtoByUserName(SecurityUtils.getCurrentUserLogin());
        DisponibiliteDate dates = DisponibiliteDate.getInstance(debut, fin);
        return repository.findDtoListBeetweenUsingUtilisateur(utilisateurConnecter.getId(), dates.debutToInstant(), dates.finToInstant());

    }

    public List<DisponibiliteDto> getListCommentaireByIdUtilisateurBeetween(Long idUtilisateur, String dateDebut, String dateFin) {

        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);
        List<DisponibiliteDto> disponibiliteDtos = repository.findDtoListBeetweenUsingUtilisateur(idUtilisateur, dates.debutToInstant(), dates.finToInstant());
        retirerCommantaireVide(disponibiliteDtos);

        return getCommentairesDisponibilite(disponibiliteDtos);
    }

    /**
     * Permet de recupérer les {@link Disponibilite} avec le même commentaire
     * sur une periode.
     *
     * @param disponibiliteAFusionner .
     * @return .
     */
    private List<DisponibiliteDto> getCommentairesDisponibilite(List<DisponibiliteDto> disponibiliteAFusionner) {
        List<DisponibiliteDto> disponibiliteDtos = new ArrayList<>();
        DisponibiliteDto dispoCumule = null;
        for (DisponibiliteDto dispoDto : disponibiliteAFusionner) {
            SuperDate superDateDebut = new SuperDate(dispoDto.getDateDebut());
            if (dispoCumule != null) {
                //si le type de dispo change :
                // - ajout dans liste
                // - un nouvelle objet
                if (!dispoDto.getTypeDisponibilite().equals(dispoCumule.getTypeDisponibilite())) {
                    dispoCumule = dispoDto;
                    disponibiliteDtos.add(dispoCumule);
                }
                // si c'est le meme type
                else {
                    if (!Objects.equals(dispoDto.getCommentaire(), dispoCumule.getCommentaire())) {
                        dispoCumule = dispoDto;
                        disponibiliteDtos.add(dispoCumule);
                    } //le meme commentaire on cumul la periode
                    else {
                        //si il y a un trous dans les dates (14 , 15  , 17 , 18 )
                        if (superDateDebut.nombreDeJourEntre(new SuperDate(dispoCumule.getDateFin())) == 1) {
                            dispoCumule = dispoDto;
                            disponibiliteDtos.add(dispoCumule);
                        } else {
                            dispoCumule.setDateFin(dispoDto.getDateFin());
                        }
                    }
                }
            } else {
                dispoCumule = dispoDto;
                disponibiliteDtos.add(dispoCumule);
            }
        }
        return disponibiliteDtos;
    }

    private void retirerCommantaireVide(List<DisponibiliteDto> disponibilites) {
        disponibilites.removeIf(disponibilite -> StringUtils.isEmpty(disponibilite.getCommentaire()));
    }


    public List<DisponibiliteOrganismeDto> getListResumeByIdOrganismeBeetween(Long idOrganisme, String
            dateDebut, String dateFin) {

        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);

        List<CorpsDto> corps = corpsService.findDtoAll();

        List<DisponibiliteDto> disposExistantes = repository.findDtoListEstDisponibleBeetweenUsingOrganisme(idOrganisme, dates.debutToInstant(), dates.finToInstant(), true);
        List<IndisponibiliteActiviteDto> indisponibiliteActivites = participerService.getIndisponibliteActivite(idOrganisme, dates.debutToInstant(), dates.finToInstant());

        List<DisponibiliteOrganismeDto> disponibilitesOrganismes = createAllDisponbilite(dates, corps, indisponibiliteActivites);

        disposExistantes.forEach(dispoExistante -> {
            DisponibiliteOrganismeDto disponibiliteOrganisme = find(disponibilitesOrganismes, dispoExistante);
            disponibiliteOrganisme.addDisponibilite(dispoExistante);
        });

        return disponibilitesOrganismes;
    }

    public List<DisponibiliteDto> getListByIdOrganismeBeetween(Long idOrganisme, SuperDate dateDebut, SuperDate
            dateFin) {
        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);

        return repository.findDtoListEstDisponibleBeetweenUsingOrganisme(idOrganisme, dates.debutToInstant(), dates.finToInstant(), true);
    }

    private List<DisponibiliteOrganismeDto> createAllDisponbilite(DisponibiliteDate
                                                                          dates, List<CorpsDto> corps, List<IndisponibiliteActiviteDto> indisponibiliteActivites) {

        List<DisponibiliteOrganismeDto> retour = new ArrayList<>();
        SuperDate startDispo = dates.debut().copie();


        while (startDispo.estAvantOuEgale(dates.fin())) {
            DisponibiliteOrganismeDto disponibilite = new DisponibiliteOrganismeDto(startDispo.copie().debutDeLaJournee(), startDispo.copie().finDeLaJournee(), corps, indisponibiliteActivites);
            retour.add(disponibilite);
            startDispo.jourSuivant();
        }

        return retour;
    }

    private DisponibiliteOrganismeDto find(List<DisponibiliteOrganismeDto> liste, DisponibiliteDto search) {
        Optional<DisponibiliteOrganismeDto> trouver = Optional.empty();
        for (DisponibiliteOrganismeDto disponibiliteDto : liste) {
            if (disponibiliteDto.equalsDate(search)) {
                trouver = Optional.of(disponibiliteDto);
                break;
            }
        }
        return Tools.getValueFromOptionalOrNotFoundException(trouver);
    }

    public List<MoisActivite> getDispoAnnuel(Long idUtilisateur, String dateDebut) {
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut).debutDeLaJournee().jourDuMois(1).moisSuivant(-3);
        SuperDate fin = debut.copie().moisSuivant(12).debutDeLaJournee();

        DisponibiliteDate dates = DisponibiliteDate.getInstance(debut, fin);
        List<ActiviteSimpleDto> activites = new ArrayList<>();
        List<DisponibiliteDto> disponibilites = repository.findDtoListBeetweenUsingUtilisateur(idUtilisateur, dates.debutToInstant(), dates.finToInstant());
        SuperDate increment = debut.copie();

        List<MoisActivite> resultat = creationMois(fin, increment);

        fin = fin.finDeLaSemaine();
        increment = debut.copie().debutDeLaSemaine();

        List<SemaineActivite> semaines = createSemaine(fin, increment, resultat);

        semaines.forEach(semaineActivite -> semaineActivite.add(activites, disponibilites));


        return resultat;
    }

    @NotNull
    private List<SemaineActivite> createSemaine(SuperDate fin, SuperDate increment, List<MoisActivite> resultat) {
        List<SemaineActivite> semaines = new ArrayList<>();

        while (increment.estAvantOuEgale(fin)) {
            SemaineActivite semaine = new SemaineActivite(increment);
            increment.jourSuivant(7);
            resultat.forEach(moisActivite -> moisActivite.add(semaine));
            semaines.add(semaine);
        }
        return semaines;
    }

    private List<MoisActivite> creationMois(SuperDate fin, SuperDate increment) {
        List<MoisActivite> listMois = new ArrayList<>();
        while (!increment.estEgaleJMA(fin)) {
            MoisActivite mois = new MoisActivite(increment);
            listMois.add(mois);
            increment.moisSuivant();
        }
        return listMois;
    }

    public Long compterDisponiblite(String dateDebut, String dateFin, Long idUtilisateur) {
        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);
        return repository.countDisponibilite(dates.debutToInstant(), dates.finToInstant(), idUtilisateur);
    }

    public Long compterIndisponiblite(String dateDebut, String dateFin, Long idUtilisateur) {
        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);
        return repository.countIndisponibilite(dates.debutToInstant(), dates.finToInstant(), idUtilisateur);
    }

    public DisponibiliteSurDuree disponibiliteUtilisateur(Long idUtilisateur, String dateDebut, String dateFin) {
        DisponibiliteDate dates = DisponibiliteDate.getInstance(dateDebut, dateFin);
        DisponibiliteSurDuree disponibiliteSurDuree = new DisponibiliteSurDuree();
        List<Disponibilite> disponibilites = repository.findListAllDisponibleBeetween(idUtilisateur, dates.debutToInstant(), dates.finToInstant());
        SuperDate debut = new SuperDate(dates.debutToInstant());
        SuperDate fin = new SuperDate(dates.finToInstant());

        disponibiliteSurDuree.setUtilisateur(utilisateurService.findDtoById(idUtilisateur));

        if (!disponibilites.isEmpty()) {
            if (disponibilites.size() == debut.compterNombreDeJourDebutInclus(fin)) {
                disponibiliteSurDuree.setEstRenseigner(true);
                for (Disponibilite d : disponibilites) {
                    if (d.getTypeDisponibilite().getEstDisponible()) {
                        disponibiliteSurDuree.setEstDisponible(d.getTypeDisponibilite().getEstDisponible());
                    } else {
                        disponibiliteSurDuree.setEstDisponible(d.getTypeDisponibilite().getEstDisponible());
                        break;
                    }
                }
            } else {
                for (Disponibilite d : disponibilites) {
                    if (!d.getTypeDisponibilite().getEstDisponible()) {
                        disponibiliteSurDuree.setEstDisponible(d.getTypeDisponibilite().getEstDisponible());
                        disponibiliteSurDuree.setEstRenseigner(true);
                        break;
                    }
                }
            }
        }
        return disponibiliteSurDuree;
    }
}
