package fabnum.metiis.services.disponibilite;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.disponibilite.TypeDisponibilite;
import fabnum.metiis.dto.disponibilite.TypeDisponibiliteDto;
import fabnum.metiis.repository.disponibilite.TypeDisponibiliteRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TypeDisponibiliteService extends AbstarctService<TypeDisponibiliteRepository, TypeDisponibilite, Long> {

    public TypeDisponibiliteService(TypeDisponibiliteRepository repository) {
        this.repository = repository;
    }

    @Cacheable("TypeDisponibilite")
    @Override
    public TypeDisponibilite findById(Long aLong) {
        return super.findById(aLong);
    }

    public Page<TypeDisponibiliteDto> findDtoPageableAll(Pageable pageable) {
        return repository.findDtoPageableAll(pageable);
    }

    public List<TypeDisponibiliteDto> findDtoAll() {
        return repository.findDtoAll();
    }

    public TypeDisponibiliteDto findDtoByIdOnly(Long id) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(id), getEntityClassName());
    }

    public TypeDisponibiliteDto creerFromDto(TypeDisponibiliteDto typeDisponibiliteDto) {

        MetiisValidation.verifier(typeDisponibiliteDto, ContextEnum.CREATE_TYPE_DISPONIBILITE);
        existCode(null, typeDisponibiliteDto.getCode());

        TypeDisponibilite typeDisponibilite = new TypeDisponibilite();

        remplirEntity(typeDisponibilite, typeDisponibiliteDto);

        saveOrUpdate(typeDisponibilite);

        return findDtoByIdOnly(typeDisponibilite.getId());
    }

    @CacheEvict(value = "TypeDisponibilite", allEntries = true)
    public TypeDisponibiliteDto modifier(Long idTypeDisponibiliteDto, TypeDisponibiliteDto typeDisponibiliteDto) {

        MetiisValidation.verifier(typeDisponibiliteDto, ContextEnum.UPDTAE_TYPE_DISPONIBILITE);
        existCode(idTypeDisponibiliteDto, typeDisponibiliteDto.getCode());

        TypeDisponibilite typeDisponibilite = findById(idTypeDisponibiliteDto);

        remplirEntity(typeDisponibilite, typeDisponibiliteDto);

        saveOrUpdate(typeDisponibilite);

        return findDtoByIdOnly(typeDisponibilite.getId());
    }

    @CacheEvict(value = "TypeDisponibilite", allEntries = true)
    @Override
    public void delete(Long id) {
        super.delete(id);
    }

    private void existCode(Long id, String code) {
        if (id != null) {
            testExist(repository.findByIdNotAndCode(id, code), "typeDisponibilite.code.exist");
        } else {
            testExist(repository.findByCode(code), "typeDisponibilite.code.exist");
        }
    }

    private void remplirEntity(TypeDisponibilite typeDisponibilite, TypeDisponibiliteDto typeDisponibiliteDto) {
        typeDisponibilite.setCode(typeDisponibiliteDto.getCode());
        typeDisponibilite.setLibelle(typeDisponibiliteDto.getLibelle());
        typeDisponibilite.setEstDisponible(typeDisponibiliteDto.getEstDisponible());
    }
}
