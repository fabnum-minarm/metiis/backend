package fabnum.metiis.services.disponibilite;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;

import java.time.Instant;

/**
 * Regroupe les traitement récurerrent sur les Dates des disponibilité.
 */
public class DisponibiliteDate {
    private SuperDate debut;
    private SuperDate fin;

    private DisponibiliteDate(String dateDebut, String dateFin) {
        debut = SuperDate.createFromIsoDateTime(dateDebut);
        fin = SuperDate.createFromIsoDateTime(dateFin);
        initHeure();
    }

    private DisponibiliteDate(Instant dateDebut, Instant dateFin) {
        debut = new SuperDate(dateDebut);
        fin = new SuperDate(dateFin);
        initHeure();
    }

    public DisponibiliteDate(SuperDate dateDebut, SuperDate dateFin) {
        debut = new SuperDate(dateDebut);
        fin = new SuperDate(dateFin);
        initHeure();
    }

    private void initHeure() {
        debut.debutDeLaJournee();
        fin.finDeLaJournee();
    }

    /**
     * From Iso DateTime
     *
     * @param dateDebut FromIsoDateTime
     * @param dateFin   FromIsoDateTime
     * @return new Instance
     */
    public static DisponibiliteDate getInstance(String dateDebut, String dateFin) {
        return new DisponibiliteDate(dateDebut, dateFin);
    }

    public static DisponibiliteDate getInstance(SuperDate dateDebut, SuperDate dateFin) {
        return new DisponibiliteDate(dateDebut, dateFin);
    }


    /**
     * Form Dto
     *
     * @param disponibilite Dto
     * @return new Instance
     */
    public static DisponibiliteDate getInstance(DisponibiliteDto disponibilite) {
        return new DisponibiliteDate(disponibilite.getDateDebut(), disponibilite.getDateFin());
    }

    /**
     * Form Instant
     *
     * @param dateDebut Instant
     * @param dateFin   Instant
     * @return new Instance
     */
    public static DisponibiliteDate getInstance(Instant dateDebut, Instant dateFin) {
        return new DisponibiliteDate(dateDebut, dateFin);
    }

    public SuperDate debut() {
        return debut;
    }

    public SuperDate fin() {
        return fin;
    }

    public Instant debutToInstant() {
        return debut.instant();
    }

    public Instant finToInstant() {
        return fin.instant();
    }
}
