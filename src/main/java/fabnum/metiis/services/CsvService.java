package fabnum.metiis.services;

import com.opencsv.CSVWriter;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.csv.DisponibiliteCompleteCsv;
import fabnum.metiis.csv.DisponibiliteCsv;
import fabnum.metiis.csv.ParticipantCsv;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.EtapeStatutDto;
import fabnum.metiis.dto.activite.UtilisateurActiviteDetailsDto;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.dto.rh.GroupePersonnaliserDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.disponibilite.DisponibiliteService;
import fabnum.metiis.services.rh.GroupePersonnaliserService;
import fabnum.metiis.services.rh.OrganismeService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.comparators.FixedOrderComparator;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class CsvService {
    private ActiviteService activiteService;
    private DisponibiliteService disponibiliteService;
    private OrganismeService organismeService;
    private UtilisateurService utilisateurService;
    private GroupePersonnaliserService groupePersonnaliserService;

    public void exportDisponibiliteComplete(Long idOrganisme, String dateDebut, String dateFin, Long idCorps, HttpServletResponse response) {

        OrganismeDto organismeDto = getOrganismeWithParent(idOrganisme);
        createFileDispo(dateDebut, dateFin, response, organismeDto, "disponiblite-entre");

        ListFilterCorps filterCorps = activiteService.listePersonnelDisponiblePourUneActiviteFiltrer(idOrganisme, dateDebut, dateFin, idCorps);

        try {
            //create a csv writer
            StatefulBeanToCsv<DisponibiliteCompleteCsv> writer = getCsvBean(response, DisponibiliteCompleteCsv.class, DisponibiliteCompleteCsv.getOrdre());

            //write all users to csv file
            List<DisponibiliteCompleteCsv> utilisateurActiviteDetailsDtos = getUtilisateurCsvs(filterCorps, organismeDto, dateDebut, dateFin);

            writer.write(utilisateurActiviteDetailsDtos);
        } catch (Exception ignored) {

        }
    }

    public void exportParticipants(Long idActivite, Long idEtape, Long idGroupe, Long idEtapeStatut, HttpServletResponse response) {

        EtapeStatutDto etapeStatutDto = Tools.getValueFromOptionalOrNotFoundException(activiteService.listParticipantByEtape(idActivite, idEtape, idGroupe)
                .stream()
                .filter(etapeStatutDto1 -> etapeStatutDto1.getId().equals(idEtapeStatut))
                .findFirst());

        ActiviteDto activiteDto = activiteService.findDtoByIdOnly(idActivite);
        createFileParticipants(activiteDto, etapeStatutDto, response);
        ListFilterCorps filterCorps = etapeStatutDto.getParticipants();

        try {
            //create a csv writer
            StatefulBeanToCsv<ParticipantCsv> writer = getCsvBean(response, ParticipantCsv.class, ParticipantCsv.getOrdre());

            //write all users to csv file
            List<ParticipantCsv> utilisateurActiviteDetailsDtos = getParticipantCsvs(activiteDto, etapeStatutDto, filterCorps);

            writer.write(utilisateurActiviteDetailsDtos);
        } catch (Exception ignored) {

        }
    }

    private void createFileParticipants(ActiviteDto activiteDto, EtapeStatutDto etapeStatutDto, HttpServletResponse response) {
        String filename = getFileName(activiteDto, etapeStatutDto, "participants");
        createFile(filename, response);
    }

    private void createFile(String filename, HttpServletResponse response) {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");
    }

    private String getFileName(ActiviteDto activiteDto, EtapeStatutDto etapeStatutDto, String fileName) {
        String separator = "-";
        String nameActivity = activiteDto.getNom().replace(" ", separator).toLowerCase();
        String etapeStatut = etapeStatutDto.getStatut().getCode();
        SuperDate date = new SuperDate();

        return date.toStringAAAAMMDD() + separator + nameActivity + separator + fileName + separator + etapeStatut + ".csv ";
    }

    private <E> StatefulBeanToCsv<E> getCsvBean(HttpServletResponse response, Class<E> eClass, String[] ordre) throws IOException {

        HeaderColumnNameMappingStrategy<E> mappingStrategy = new HeaderColumnNameMappingStrategy<>();
        mappingStrategy.setType(eClass);
        mappingStrategy.setColumnOrderOnWrite(new FixedOrderComparator<>(ordre));


        return new StatefulBeanToCsvBuilder<E>(response.getWriter())
                .withMappingStrategy(mappingStrategy)
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(';')
                .withOrderedResults(true)
                .build();
    }


    public void exportDisponibilite(Long idOrganisme, String dateDebut, String dateFin, HttpServletResponse response) {

        OrganismeDto organismeDto = getOrganismeWithParent(idOrganisme);

        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut);
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin);

        createFileDispo(dateDebut, dateFin, response, organismeDto, "disponiblite");

        List<DisponibiliteDto> disponibiliteDtos = disponibiliteService.getListByIdOrganismeBeetween(idOrganisme, debut, fin);

        try {
            StatefulBeanToCsv<DisponibiliteCsv> writer = getCsvBean(response, DisponibiliteCsv.class, DisponibiliteCsv.getOrdre());
            List<DisponibiliteCsv> disponibiliteCsvs = getDisponibilitesCsv(disponibiliteDtos, organismeDto);
            writer.write(disponibiliteCsvs);
        } catch (Exception ignored) {
            //ignored.printStackTrace();
        }
    }

    private List<DisponibiliteCsv> getDisponibilitesCsv(List<DisponibiliteDto> disponibilitesDtos, OrganismeDto organismeDto) {
        return disponibilitesDtos.stream()
                .map(disponibiliteDtos -> new DisponibiliteCsv(disponibiliteDtos, organismeDto))
                .collect(Collectors.toList());
    }

    private void createFileDispo(String dateDebut, String dateFin, HttpServletResponse response, OrganismeDto organismeDto, String disponiblite) {
        String filename = getFileName(organismeDto, dateDebut, dateFin, disponiblite);

        createFile(filename, response);
    }

    private List<ParticipantCsv> getParticipantCsvs(ActiviteDto activiteDto, EtapeStatutDto etapeStatutDto, ListFilterCorps filterCorps) {
        return filterCorps.getFiltrer()
                .stream()
                .filter(UtilisateurActiviteDetailsDto.class::isInstance)
                .map(UtilisateurActiviteDetailsDto.class::cast)
                .map(utilisateurActiviteDetailsDto -> {
                    UtilisateurCompletDto utilisateur = utilisateurService.findDtoByUserName(SecurityUtils.getCurrentUserLogin());
                    List<GroupePersonnaliserDto> groupes = groupePersonnaliserService.findAllDtoByIdProprietaireWithUtilisateurs(utilisateur.getId());
                    return new ParticipantCsv(activiteDto, etapeStatutDto, utilisateurActiviteDetailsDto.getUtilisateur(), groupes);
                })
                .collect(Collectors.toList());
    }

    private List<DisponibiliteCompleteCsv> getUtilisateurCsvs(ListFilterCorps filterCorps, OrganismeDto organismeDto, String dateDebut, String dateFin) {
        return filterCorps.getFiltrer()
                .stream()
                .filter(UtilisateurActiviteDetailsDto.class::isInstance)
                .map(UtilisateurActiviteDetailsDto.class::cast)
                .map(utilisateurActiviteDetailsDto -> {
                    List<DisponibiliteDto> commentaires = disponibiliteService
                            .getListCommentaireByIdUtilisateurBeetween(utilisateurActiviteDetailsDto.getUtilisateur().getId(), dateDebut, dateFin);
                    List<UtilisateurActiviteDetailsDto> coms = new ArrayList<>();
                    for (DisponibiliteDto commentaire : commentaires) {
                        UtilisateurActiviteDetailsDto com = new UtilisateurActiviteDetailsDto();
                        com.setCommentaire(commentaire.getCommentaire());
                        com.setDateValidation(commentaire.getDateDebut());
                        coms.add(com);
                    }
                    utilisateurActiviteDetailsDto.setCommentaires(coms);
                    return new DisponibiliteCompleteCsv(utilisateurActiviteDetailsDto, organismeDto);
                })
                .collect(Collectors.toList());
    }

    private OrganismeDto getOrganismeWithParent(Long idOrganisme) {
        OrganismeDto organismeDto = organismeService.findDtoById(idOrganisme);
        if (organismeDto.getIdparent() != null) {
            OrganismeDto parent = organismeService.findDtoById(organismeDto.getIdparent());
            organismeDto.setOrganismeParent(parent);
        }
        return organismeDto;
    }

    private String getFileName(OrganismeDto organismeDto, String dateDebut, String dateFin, String fileName) {
        String separator = "-";
        String organimse = organismeDto.getFullNameWithParent(separator);
        SuperDate date = new SuperDate();
        SuperDate debut = SuperDate.createFromIsoDateTime(dateDebut);
        SuperDate fin = SuperDate.createFromIsoDateTime(dateFin);
        return date.toStringAAAAMMDD() + separator + organimse + separator + fileName + separator + debut.toStringAAAAMMDD() + separator + fin.toStringAAAAMMDD() + ".csv ";
    }


}
