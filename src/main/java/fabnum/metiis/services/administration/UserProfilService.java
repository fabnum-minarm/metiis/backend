package fabnum.metiis.services.administration;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.administration.UserProfil;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.administration.UserProfilDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.repository.administration.UserProfilRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.exceptions.ServiceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
@Transactional
public class UserProfilService extends AbstarctService<UserProfilRepository, UserProfil, Long> {

    private ProfilService profilService;


    public UserProfilService(ProfilService profilService,
                             UserProfilRepository repository) {
        super(repository);
        this.profilService = profilService;
    }

    public void creerUserProfil(UserProfilDto userProfilDto, User user) {

        MetiisValidation.verifier(userProfilDto, ContextEnum.CREATE_USER_PROFIL);

        UserProfil userProfil = new UserProfil();

        Profil profil = profilService.findById(userProfilDto.getProfil().getId());

        userProfil.setSelected(userProfilDto.getSelected());
        userProfil.setProfil(profil);
        userProfil.setUser(user);
        saveOrUpdate(userProfil);
    }

    public List<UserProfilDto> findDtoAllByIdUtilisateur(Long idUtilisateur) {
        return repository.findDtoAllUsingIdUtilisateur(idUtilisateur);
    }

    public void deleteByUser(User user) {
        repository.deleteAllByUserId(user.getId());
    }

    public void modifierProfil(Utilisateur utilisateur, UtilisateurCompletDto utilisateurDto) {
        MetiisValidation.verifierCollection(utilisateurDto.getUserProfils(), ContextEnum.PATCH_UTILISATEUR);
        verifierAuMoinUnProfil(utilisateurDto.getUserProfils());
        List<Profil> profilsCreableParUtilisateur = profilService.profilsCreableParUtilisateur(SecurityUtils.getCurrentUserLogin());
        Set<Profil> profilsExistantsAGarder = getListProfilExistantNonCreableAGarder(utilisateurDto.getId(), profilsCreableParUtilisateur);
        if (profilsExistantsAGarder.stream().anyMatch(profil -> profil.getCode().equals("super_employeur"))) {
            profilsCreableParUtilisateur.remove(profilsCreableParUtilisateur.stream()
                    .filter(profil -> profil.getCode().equals("employeur")).findAny().orElse(null));
        }
        Set<Profil> aCreer = findProfilEnCommun(profilsCreableParUtilisateur, utilisateurDto);

        aCreer.addAll(profilsExistantsAGarder);

        User user = utilisateur.getUser();
        deleteByUser(user);
        rempliUserProfil(user, aCreer);
    }

    public void verifierAuMoinUnProfil(List<UserProfilDto> userProfilDtos) {
        if (userProfilDtos.isEmpty()) {
            throw new ServiceException(Tools.message("utilisateur.missing.profil"));
        }
    }

    /**
     * Un utilisateur ne peut pas forcement crée tout les profils, il ne faut donc pas supprimer ces profils à l'utilisateur.
     * Ex : si un profil Employeur ne peut pas crée de profil admin, il ne faut pas qu'il puissent le supprimer.
     *
     * @param idUtilisateur                .
     * @param profilsCreableParUtilisateur .
     * @return profils à garder.
     */
    private Set<Profil> getListProfilExistantNonCreableAGarder(Long idUtilisateur, List<Profil> profilsCreableParUtilisateur) {
        List<UserProfil> userProfilsExistants = repository.findAllUsingIdUtilisateur(idUtilisateur);
        List<Profil> profilsExistants = userProfilsExistants.stream().map(UserProfil::getProfil).collect(Collectors.toList());
        return profilsExistants.stream().filter(Predicate.not(profilsCreableParUtilisateur::contains)).collect(Collectors.toSet());
    }

    private Set<Profil> findProfilEnCommun(List<Profil> profilsCreableParUtilisateur, UtilisateurCompletDto utilisateurDto) {
        Set<Long> idsProfils = utilisateurDto.getUserProfils().stream().map(userProfilDto -> userProfilDto.getProfil().getId()).collect(Collectors.toSet());
        return profilsCreableParUtilisateur.stream().filter(profil -> idsProfils.contains(profil.getId())).collect(Collectors.toSet());

    }

    public void rempliUserProfil(User user, Collection<Profil> profils) {
        Boolean selected = Boolean.TRUE;
        for (Profil profil : profils) {
            UserProfil userProfil = new UserProfil();
            userProfil.setUser(user);
            userProfil.setProfil(profil);
            userProfil.setSelected(selected);
            saveOrUpdate(userProfil);
            selected = Boolean.FALSE;
        }
    }

    public void rempliUserProfil(User user, UtilisateurCompletDto utilisateurCompletDto) {
        for (UserProfilDto userProfilDto : utilisateurCompletDto.getUserProfils()) {
            UserProfil userProfil = new UserProfil();
            userProfil.setUser(user);
            userProfil.setProfil(profilService.findById(userProfilDto.getProfil().getId()));
            userProfil.setSelected(userProfilDto.getSelected());
            saveOrUpdate(userProfil);
        }
    }

}