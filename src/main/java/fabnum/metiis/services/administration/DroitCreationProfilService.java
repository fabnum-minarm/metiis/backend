package fabnum.metiis.services.administration;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.administration.DroitCreationProfil;
import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.dto.administration.DroitCreationProfilDto;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.repository.administration.DroitCreationProfilRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DroitCreationProfilService extends AbstarctService<DroitCreationProfilRepository, DroitCreationProfil, Long> {

    private ProfilService profilService;

    public DroitCreationProfilService(ProfilService profilService,
                                      DroitCreationProfilRepository repository) {
        super(repository);
        this.profilService = profilService;
    }

    public List<DroitCreationProfilDto> findAllDto() {
        return repository.findDtoAll();
    }

    public DroitCreationProfilDto creer(DroitCreationProfilDto droitCreationProfilDto) {
        MetiisValidation.verifier(droitCreationProfilDto, ContextEnum.DROIT_CREATION_PROFIL);

        testExistance(droitCreationProfilDto);

        DroitCreationProfil droitCreationProfil = new DroitCreationProfil();

        Profil createur = profilService.findById(droitCreationProfilDto.getProfilCreateur().getId());
        Profil cree = profilService.findById(droitCreationProfilDto.getProfilCree().getId());

        droitCreationProfil.setProfilCreateur(createur);
        droitCreationProfil.setProfilCree(cree);

        saveOrUpdate(droitCreationProfil);

        return findDtoById(droitCreationProfil.getId());
    }

    private void testExistance(DroitCreationProfilDto droitCreationProfilDto) {
        testExist(repository.findByCreateurAndCree(droitCreationProfilDto.getProfilCreateur().getId(),
                droitCreationProfilDto.getProfilCree().getId()), "droitCreationProfil.exist");
    }

    public DroitCreationProfilDto findDtoById(Long idDroitCreationProfil) {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idDroitCreationProfil), getEntityClassName());
    }

    public List<DroitCreationProfilDto> findAllCreateurDto(Long idCreateur) {
        return repository.findAllCreateurDto(idCreateur);
    }

    public List<ProfilDto> allProfilWithDroit() {
        List<ProfilDto> profils = profilService.findDtoAllSimple();
        List<DroitCreationProfilDto> droitCreationProfilDtos = repository.findDtoAll();
        profils.forEach(profilDto -> profilDto.createAllDroit(profils, droitCreationProfilDtos));
        return profils;
    }

    public void suprimer(Long idCreateur, Long idCree) {
        DroitCreationProfil droitCreationProfil = Tools.getValueFromOptionalOrNotFoundException(repository.findByCreateurAndCree(idCreateur, idCree), getEntityClassName());
        this.delete(droitCreationProfil);
    }


}
