package fabnum.metiis.services.administration;

import fabnum.metiis.config.ApplicationProperties;
import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.administration.ChangePasswordDto;
import fabnum.metiis.dto.administration.UserCompteActifDto;
import fabnum.metiis.dto.rh.ConfirmationInscriptionEmployeDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.exceptions.ConflictException;
import fabnum.metiis.exceptions.NotFoundException;
import fabnum.metiis.filter.FilterSpecificationBuilder;
import fabnum.metiis.repository.administration.UserRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.exceptions.ServiceException;
import fabnum.metiis.services.mail.MailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service transactionnel permettant la gestion des users.
 */
@Service
@Transactional
@AllArgsConstructor
@Slf4j
public class UserService {

    /**
     * Repository gerant la persistance des users
     */
    private UserRepository userRepository;

    private UserProfilService userProfilService;

    private MailService mailService;


    /**
     * Objet permettant la gestion de l'encodage des mots de passes
     */
    private PasswordEncoder passwordEncoder;


    /**
     * Propriétés propres à l'application
     */
    private ApplicationProperties appProps;

    /**
     * Permet de rechercher l'ensemble des users.
     *
     * @return List
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Permet de rechercher l'ensemble des users.
     *
     * @param filters {@link String} : critère de recherche/filtrage.
     * @return List
     */
    public List<User> findAll(String filters) {
        FilterSpecificationBuilder fsb = new FilterSpecificationBuilder(filters);
        return userRepository.findAll(fsb.build());
    }

    /**
     * Permet de rechercher l'ensemble des users.
     *
     * @param filters {@link String} : critère de recherche/filtrage.
     * @return List
     */
    public Page<User> findAll(String filters, Pageable pageable) {
        FilterSpecificationBuilder fsb = new FilterSpecificationBuilder(filters);
        return userRepository.findAll(fsb.build(), pageable);
    }

    /**
     * Permet de rechercher un user via son identifiant.
     *
     * @param id {@link Long}
     * @return Renvoi une exception dans le cas où le user n'existe pas (erreur 404, doc Cadre API)
     */
    public User findById(Long id) {
        return userRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    /**
     * Permet la suppression d'un user
     *
     * @param id {@link Long} : identifiant de l'utilisateur à supprimer.
     */
    public void delete(Long id) {
        User u = findById(id);

        if (u.getUsername().equals(SecurityUtils.getCurrentUserLogin())) {
            throw new ConflictException(Tools.message("user.delete.himself"));
        }

        userRepository.deleteById(id);
    }

    /**
     * Permet de rechercher les user liés à un profil via son identifiant.
     *
     * @param id {@link Long} : identifiant du profil.
     * @return List
     */
    public List<User> findByProfil(Long id) {
        return userRepository.findByUserProfilProfilId(id);
    }


    /**
     * Permet de gérer le blocage du user en cas d'erreurs successive lors d'un tentative de connexion.
     *
     * @param username {@link String} : le username de connexion
     * @return {@link Boolean} : faux si le user est bloqué.
     */
    public boolean manageCredential(String username, boolean error) {
        //boolean returnValue = true;
        if (appProps.getPassword().getAttempts() >= 0) {
            User user = userRepository.findByUsername(username).orElse(null);
            if (user != null) {
                if (error) {
                    user.setWrongCredential(user.getWrongCredential() + 1);
                    if (user.getWrongCredential() >= appProps.getPassword().getAttempts()) {
                        //returnValue = true;
                        envoieMailTentativeConnexion(user);
                    }
                } else {
                    if (user.getWrongCredential() > 0) {
                        updateAccountNonLockedAndResetWrongCrendials(user);
                    }
                    user.setLastConnexionDate(Instant.now());
                }
            }
        }
        return true;
    }

    private void updateAccountNonLockedAndResetWrongCrendials(User user) {
        user.setAccountNonLocked(true);
    }

    private void envoieMailTentativeConnexion(User user) {
        if (user.getWrongCredential() == appProps.getPassword().getAttempts()) {
            mailService.envoiyerMailTentativeDeConnection(user);
        }
    }

    /**
     * Permet de récupérer les préférences d'un user connecté.
     *
     * @return Map
     */
    public Map<String, String> getPreferences() {
        return userRepository.getByUsernameIgnoreCase(SecurityUtils.getCurrentUserLogin()).orElseThrow(NotFoundException::new).getPreferences();
    }

    /**
     * Permet de modifier les préférences d'un user connecté.
     *
     * @param prefs {@link Map} : préférences à enregistrer.
     * @return Map
     */
    public Map<String, String> updatePreferences(Map<String, String> prefs) {
        User u = userRepository.findByUsername(SecurityUtils.getCurrentUserLogin()).orElseThrow(NotFoundException::new);
        u.setPreferences(prefs);
        userRepository.save(u);
        return getPreferences();
    }

    public void verifieUserName(String login) {
        Optional<User> optionalUser = userRepository.findByUsername(login);
        optionalUser.ifPresent(p -> {
            ConflictException ex = new ConflictException(Tools.message("user.username.exist"));
            ex.addHeader(HttpHeaders.LOCATION, p.getId());
            throw ex;
        });
    }

    public void verifieUserNameExist(long idUser, String login) {
        if (userRepository.existsByIdNotAndUsernameIgnoreCase(idUser, login)) {
            ConflictException ex = new ConflictException(Tools.message("user.username.exist"));
            ex.addHeader(HttpHeaders.LOCATION, idUser);
            throw ex;
        }
    }


    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    public User createFromUtilisateurCompletDto(UtilisateurCompletDto utilisateurCompletDto) {

        User user = userRepository.save(
                User.builder()
                        .username(utilisateurCompletDto.getUsername())
                        .password(encodePassword(utilisateurCompletDto.getPassword()))
                        .lastname(utilisateurCompletDto.getNom())
                        .firstname(utilisateurCompletDto.getPrenom())
                        .email(utilisateurCompletDto.getEmail())
                        .build()
        );
        user.setEnabled(utilisateurCompletDto.getEnabled());
        user.setAccountNonLocked(utilisateurCompletDto.getAccountNonLocked());
        userProfilService.rempliUserProfil(user, utilisateurCompletDto);
        return user;
    }

    public User modifyFromUtilisateurCompletDto(User user, UtilisateurCompletDto utilisateurCompletDto) {
        if (Tools.testerNulliteString(utilisateurCompletDto.getPassword())) {
            user.setPassword(encodePassword(utilisateurCompletDto.getPassword()));
        }
        user.setAccountNonLocked(utilisateurCompletDto.getAccountNonLocked());
        modifierInfosPersos(user, utilisateurCompletDto);
        userProfilService.deleteByUser(user);
        userProfilService.rempliUserProfil(user, utilisateurCompletDto);
        return user;
    }

    public void modifierInfosPersos(User user, UtilisateurCompletDto utilisateurCompletDto) {
        user.setUsername(utilisateurCompletDto.getEmail());
        user.setLastname(utilisateurCompletDto.getNom());
        user.setFirstname(utilisateurCompletDto.getPrenom());
        user.setEmail(utilisateurCompletDto.getEmail());
        userRepository.save(user);
    }

    public void modifierInfosPersos(User user, String email) {
        user.setUsername(email);
        user.setEmail(email);
        userRepository.save(user);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void modifierUser(UtilisateurCompletDto utilisateurCompletDto, Utilisateur utilisateur) {
        User user = findById(utilisateur.getUser().getId());

        user.setEnabled(utilisateurCompletDto.getEnabled());

        userRepository.save(user);
    }

    public User findByIdUtilisateur(Long idUtilisateur) {
        return Tools.getValueFromOptionalOrNotFoundException(userRepository.findByIdUtilisateur(idUtilisateur), User.class.getSimpleName());
    }

    public boolean valider(Long idUtilisateur, Boolean enabled) {
        User user = findByIdUtilisateur(idUtilisateur);
        if (user.isEnabled() != enabled) {
            user.setEnabled(enabled);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public void debloquer(Long idUtilisateur, Boolean accountNonLocked) {
        User user = findByIdUtilisateur(idUtilisateur);
        user.setAccountNonLocked(accountNonLocked);
        userRepository.save(user);
    }

    public Page<UserCompteActifDto> getComptesActifs(Pageable pageable) {
        return userRepository.findComptesActifs(pageable);
    }

    public User createFromUtilisateur(Utilisateur utilisateur, List<Profil> profils) {
        User user = new User();
        user.setUsername(utilisateur.getEmail());
        user.setLastname(utilisateur.getNom());
        user.setFirstname(utilisateur.getPrenom());
        user.setEmail(utilisateur.getEmail());
        user.setPassword("");
        userRepository.save(user);
        userProfilService.rempliUserProfil(user, profils);
        return user;
    }

    public void confirmerInscription(Utilisateur utilisateur, ConfirmationInscriptionEmployeDto confirmationInscriptionEmploye) {
        User user = utilisateur.getUser();
        user.setLastname(utilisateur.getNom());
        user.setFirstname(utilisateur.getPrenom());
        user.setEnabled(Boolean.TRUE); // son compte est automatiquement validé.
        user.setPassword(encodePassword(confirmationInscriptionEmploye.getPassword()));
        userRepository.save(user);
    }

    public void changerMotDePasse(User user, String password) {
        user.setPassword(encodePassword(password));
        userRepository.save(user);
    }

    public void verifierAncienPassword(String passwordBddEncoder, ChangePasswordDto changePassword) {
        if (!passwordEncoder.matches(changePassword.getPasswordCourant(), passwordBddEncoder)) {
            throw new ServiceException(Tools.message("password.bad"));
        }

        Tools.verifierAncienPassword(changePassword.getPasswordCourant(), changePassword.getPassword());
    }
}