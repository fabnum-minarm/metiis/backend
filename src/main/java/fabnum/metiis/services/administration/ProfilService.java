package fabnum.metiis.services.administration;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.MetiisValidation;
import fabnum.metiis.domain.administration.Profil;
import fabnum.metiis.domain.rh.TypeOrganisme;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.exceptions.NotFoundException;
import fabnum.metiis.repository.administration.ProfilRepository;
import fabnum.metiis.repository.administration.UserRepository;
import fabnum.metiis.services.abstraction.AbstarctService;
import fabnum.metiis.services.exceptions.ServiceException;
import fabnum.metiis.services.rh.TypeOrganismeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service transactionnel permettant la gestion des profils.
 */
@Service
@Transactional
public class ProfilService extends AbstarctService<ProfilRepository, Profil, Long> {


    /**
     * Repository gerant la persistance des users
     */
    private UserRepository userRepository;
    private TypeOrganismeService typeOrganismeService;

    /**
     * je n'ai pas trouvé de moyen d'utilisé longbok pour generer ce constructeur et sans lui le service plante.
     *
     * @param repository repository
     */
    public ProfilService(UserRepository userRepository, ProfilRepository repository, TypeOrganismeService typeOrganismeService) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.typeOrganismeService = typeOrganismeService;
    }


    /**
     * Suppression d'un profil.
     *
     * @param id {@link Long} : identifiant technique du profil à supprimer.
     */
    @Override
    public void delete(Long id) {
        if (!repository.existsById(id)) {
            throw new NotFoundException();
        }
        // On vérifie que le code est bien renseigné.
        if (userRepository.existsUsersByUserProfilProfilId(id)) {
            throw new ServiceException(Tools.message("profil.users"));
        }

        repository.deleteById(id);
    }


    public Profil getDefault() {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDefault());
    }

    public ProfilDto getDefaultDto() {
        return Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(getDefault().getId()), getEntityClassName());
    }

    public ProfilDto getDefaultDtoWithRoles() {
        ProfilDto profil = getDefaultDto();
        rempliRole(profil);
        return profil;
    }

    /**
     * Permet de valider un profil
     *
     * @param profil {@link Profil} : profil à tester.
     */
    @Override
    protected void checkBeforeSaveOrUpdate(Profil profil) {
        super.checkBeforeSaveOrUpdate(profil);
        testExist(repository.findByIdNotAndCode(Tools.getValueFromOptional(profil.getId()), profil.getCode()), "profil.code.exist");
        // On vérifie qu'au moins un rôle est bien renseigné.
        if (profil.getRoles().isEmpty()) {
            throw new ServiceException(Tools.message("profil.missing.role"));
        }
    }


    public ProfilDto findDtoByIdOnly(Long idProfil) {
        ProfilDto profilDto = Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingId(idProfil), getEntityClassName());
        rempliRole(profilDto);
        return profilDto;
    }

    public ProfilDto findDtoByCode(String codeProfil) {
        ProfilDto profilDto = Tools.getValueFromOptionalOrNotFoundException(repository.findDtoUsingCode(codeProfil), getEntityClassName());
        rempliRole(profilDto);
        return profilDto;
    }

    public List<ProfilDto> findDtoAllSimple() {
        return repository.findDtoAll();
    }

    public List<ProfilDto> findDtoAll() {
        List<ProfilDto> profilDtos = repository.findDtoAll();
        profilDtos.forEach(this::rempliRole);
        return profilDtos;
    }


    public List<Profil> findListByDtos(List<ProfilDto> profils) {
        List<Long> idsProfils = Tools.getListIdDto(profils);
        return findByIds(idsProfils);
    }

    public Page<ProfilDto> findDtoAll(Pageable pageable) {
        Page<ProfilDto> profilDtos = repository.findDtoAll(pageable);
        profilDtos.forEach(this::rempliRole);
        return profilDtos;
    }

    public ProfilDto creerFromDto(ProfilDto profilDto) {
        MetiisValidation.verifier(profilDto, ContextEnum.CREATE_PROFIL);
        Profil profil = new Profil();
        rempliProfil(profil, profilDto);
        if (profil.getIsdefault()) {
            traitementProfilParDefaut(profil);
        }
        saveOrUpdate(profil);
        return findDtoByIdOnly(profil.getId());
    }

    public ProfilDto modifierFromDto(Long idProfil, ProfilDto profilDto) {
        MetiisValidation.verifier(profilDto, ContextEnum.UPDATE_PROFIL);
        Profil profil = findById(idProfil);
        rempliProfil(profil, profilDto);
        if (profil.getIsdefault()) {
            traitementProfilParDefaut(profil);
        }
        saveOrUpdate(profil);
        return findDtoByIdOnly(profil.getId());
    }

    private void rempliProfil(Profil profil, ProfilDto profilDto) {
        profil.setCode(profilDto.getCode());
        profil.setDescription(profilDto.getDescription());
        profil.setIsdefault(profilDto.getIsDefault());
        profil.setRoles(profilDto.getRoles());
        profil.setOrdre(profilDto.getOrdre());
        TypeOrganisme typeOrganisme = null;
        if (profilDto.getTypeOrganisme().getId() != null) {
            typeOrganisme = typeOrganismeService.findById(profilDto.getTypeOrganisme().getId());
        }
        profil.setTypeOrganisme(typeOrganisme);
    }

    private void rempliRole(ProfilDto profilDto) {
        profilDto.setRoles(repository.findRolesUsingIdProfil(profilDto.getId()));
    }

    private void traitementProfilParDefaut(Profil profil) {
        Optional<Profil> optionalAcienProfilParDefaut = repository.findDefault();
        if (optionalAcienProfilParDefaut.isPresent()) {
            Profil ancienProfilParDefaut = optionalAcienProfilParDefaut.get();
            if (!ancienProfilParDefaut.getId().equals(profil.getId())) {
                ancienProfilParDefaut.setIsdefault(false);
                saveOrUpdate(ancienProfilParDefaut);
            }
        }
    }

    public List<ProfilDto> profilsDtoCreableParUtilisateur(String username) {
        return repository.findListDtoCreableUsingUserName(username);
    }

    public List<Profil> profilsCreableParUtilisateur(String username) {
        return repository.findListCreableUsingUserName(username);
    }


}