package fabnum.metiis.csv;

import com.opencsv.bean.CsvBindByName;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class UtilisateurCsv {

    public static String[] getOrdre() {
        return new String[]{"CATEGORIE", "NOM", "PRENOM", "TELEPHONE", "EMAIL", "UNITE"};
    }


    @CsvBindByName(column = "CATEGORIE")
    protected String categorie;

    @CsvBindByName(column = "NOM")
    protected String nom;

    @CsvBindByName(column = "PRENOM")
    protected String prenom;

    @CsvBindByName(column = "TELEPHONE")
    protected String telephone;

    @CsvBindByName(column = "EMAIL")
    protected String email;

    @CsvBindByName(column = "UNITE")
    protected String unite;

    public UtilisateurCsv(UtilisateurDto utilisateurDto, OrganismeDto organismeDto) {
        categorie = utilisateurDto.getCorps().getCode();
        nom = utilisateurDto.getNom();
        prenom = utilisateurDto.getPrenom();
        email = utilisateurDto.getEmail();
        telephone = utilisateurDto.getTelephone();
        unite = organismeDto.getFullNameWithParent("/");
    }

}
