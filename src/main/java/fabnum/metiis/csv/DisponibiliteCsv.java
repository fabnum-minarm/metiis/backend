package fabnum.metiis.csv;

import com.opencsv.bean.CsvBindByName;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class DisponibiliteCsv extends UtilisateurCsv {


    public static String[] getOrdre() {
        return new String[]{"DATE", "CATEGORIE", "NOM", "PRENOM", "TELEPHONE", "EMAIL", "UNITE", "COMMENTAIRE"};
    }

    @CsvBindByName(column = "DATE")
    private String date;

    @CsvBindByName(column = "COMMENTAIRE")
    private String commentaire;

    public DisponibiliteCsv(DisponibiliteDto disponibiliteDtos, OrganismeDto organismeDto) {
        super(disponibiliteDtos.getUtilisateur(), organismeDto);
        SuperDate debut = new SuperDate(disponibiliteDtos.getDateDebut());
        date = debut.toStringDDMMAAAA();
        commentaire = disponibiliteDtos.getCommentaire();
    }
}
