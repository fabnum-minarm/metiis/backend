package fabnum.metiis.csv;

import com.opencsv.bean.CsvBindByName;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.activite.UtilisateurActiviteDetailsDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class DisponibiliteCompleteCsv extends UtilisateurCsv {


    public static String[] getOrdre() {
        return new String[]{"CATEGORIE", "NOM", "PRENOM", "TELEPHONE", "EMAIL", "UNITE", "COMMENTAIRE"};
    }

    @CsvBindByName(column = "COMMENTAIRE")
    private String commentaire;

    public DisponibiliteCompleteCsv(UtilisateurActiviteDetailsDto utilisateurActiviteDetailsDto, OrganismeDto organismeDto) {
        super(utilisateurActiviteDetailsDto.getUtilisateur(), organismeDto);
        commentaire = "";
        for (UtilisateurActiviteDetailsDto com : utilisateurActiviteDetailsDto.getCommentaires()) {
            SuperDate debut = new SuperDate(com.getDateValidation());
            commentaire += debut.toStringDDMMAAAA() + ": «" + com.getCommentaire() + "» ";
        }
    }
}
