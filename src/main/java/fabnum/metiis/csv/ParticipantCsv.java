package fabnum.metiis.csv;

import com.opencsv.bean.CsvBindByName;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.EtapeStatutDto;
import fabnum.metiis.dto.activite.UtilisateurActiviteDetailsDto;
import fabnum.metiis.dto.rh.GroupePersonnaliserDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import fabnum.metiis.dto.rh.WithCorpsDto;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.ListIterator;

@Getter
@NoArgsConstructor
public class ParticipantCsv extends UtilisateurCsv {

    public static String[] getOrdre() {
        return new String[]{"CATEGORIE", "NOM", "PRENOM", "TELEPHONE", "EMAIL", "UNITE", "ACTIVITE", "DEBUT", "FIN", "STATUT", "GROUPES",
                "COMMENTAIRE CANDIDAT", "COMMENTAIRE RETENU", "COMMENTAIRE CONVOQUE", "COMMENTAIRE ACTIVITE REALISEE"};
    }

    @CsvBindByName(column = "ACTIVITE")
    private String activite;

    @CsvBindByName(column = "DEBUT")
    private String debut;

    @CsvBindByName(column = "FIN")
    private String fin;

    @CsvBindByName(column = "STATUT")
    private String statut;

    @CsvBindByName(column = "GROUPES")
    private String groupes = "";

    @CsvBindByName(column = "COMMENTAIRE CANDIDAT")
    private String cCandidat = "";

    @CsvBindByName(column = "COMMENTAIRE RETENU")
    private String cRetenu = "";

    @CsvBindByName(column = "COMMENTAIRE CONVOQUE")
    private String cConvoque = "";

    @CsvBindByName(column = "COMMENTAIRE ACTIVITE REALISEE")
    private String cRealise = "";

    public ParticipantCsv(ActiviteDto activiteDto, EtapeStatutDto etapeStatutDto, UtilisateurDto utilisateurDto, List<GroupePersonnaliserDto> groupesPersonnaliser) {
        super(utilisateurDto, activiteDto.getOrganisme());
        activite = activiteDto.getNom();
        SuperDate ddebut = new SuperDate(activiteDto.getDateDebut());
        debut = ddebut.toStringDDMMAAAA();
        SuperDate dfin = new SuperDate(activiteDto.getDateFin());
        fin = dfin.toStringDDMMAAAA();
        statut = etapeStatutDto.getStatut().getNomSingulier();
        for (GroupePersonnaliserDto groupe : groupesPersonnaliser) {
            if (groupe.containsUtilisteur(utilisateurDto.getId())) {
                groupes += "[" + groupe.getNom() + "] ";
            }
        }

        List<WithCorpsDto> detailsDtos = etapeStatutDto.getParticipants().getAll();
        for (WithCorpsDto u : detailsDtos) {
            UtilisateurActiviteDetailsDto utilisateurActiviteDetailsDto = (UtilisateurActiviteDetailsDto) u;
            if (((UtilisateurActiviteDetailsDto) u).getUtilisateur().getId().equals(utilisateurDto.getId())) {
                List<UtilisateurActiviteDetailsDto> commentaires = utilisateurActiviteDetailsDto.getCommentaires();
                if (commentaires != null) {
                    ListIterator<UtilisateurActiviteDetailsDto> iterator = commentaires.listIterator(commentaires.size());
                    while (iterator.hasPrevious()) {
                        UtilisateurActiviteDetailsDto commentaire = iterator.previous();
                        String monCom = commentaire.getRedacteur().getCodePosteRo() + " "
                                + commentaire.getRedacteur().getNom() + " "
                                + commentaire.getRedacteur().getPrenom() +
                                ": «" + commentaire.getCommentaire() + "» ";
                        switch (commentaire.getStatut().getCode()) {
                            case "candidats":
                                cCandidat += monCom;
                                break;
                            case "retenus":
                                cRetenu += monCom;
                                break;
                            case "convoques":
                                cConvoque += monCom;
                                break;
                            case "non-convoques":
                                cConvoque += monCom;
                                break;
                            case "realises":
                                cRealise += monCom;
                                break;
                            case "non-realises":
                                cRealise += monCom;
                                break;
                        }
                    }
                }
            }
        }
    }
}
