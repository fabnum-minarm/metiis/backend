package fabnum.metiis.config;

import lombok.Getter;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class EmailValidation {

    @Getter
    private Set<String> valid = new HashSet<>();

    @Getter
    private Set<String> invalid = new HashSet<>();

    public EmailValidation(String emailsSeparatedBySemicolon) {
        if (emailsSeparatedBySemicolon != null) {
            List<String> mots = Tools.getEmail(emailsSeparatedBySemicolon);
            mots.forEach(email -> {
                try {
                    InternetAddress internetAddress = new InternetAddress(email);
                    internetAddress.validate();
                    valid.add(internetAddress.getAddress());
                } catch (AddressException ex) {
                    invalid.add(email);
                }
            });
        }
    }

}
