package fabnum.metiis.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Propriétés pour l'application.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Getter
@Setter
public class ApplicationProperties {

    private final Mail mail = new Mail();

    private final Password password = new Password();

    private final Front front = new Front();

    @Getter
    @Setter
    public static class Mail {
        private String from = "noreply@metiis.fr";

        private boolean enabled = true;
    }

    /**
     * nombre d'essai sur le mot de passe
     * Attention si modification -> modifier le ficher .yml
     */
    @Getter
    @Setter
    public static class Password {
        private int attempts = -1;
    }

    @Getter
    @Setter
    public static class Front {
        private String url = "";
    }
}
