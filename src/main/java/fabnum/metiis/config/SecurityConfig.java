package fabnum.metiis.config;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.security.jwt.JwtConfigurer;
import fabnum.metiis.security.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true) // Permet d'activer la securité des methode via role
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //@formatter:off
        http
                .headers()
                .contentSecurityPolicy("script-src 'self'")
                .and()
                .frameOptions().deny()
                .and()
                .httpBasic().disable()
                .csrf().disable()

                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()

                .authorizeRequests()
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                .antMatchers("/actuator/**").hasAuthority(Roles.ROLE_TECH.name())
                .antMatchers("/**/logs/**").permitAll()
                .antMatchers("/**/editions/**").permitAll()
                .antMatchers("/**/auth/signin").permitAll()

                .antMatchers("/**/type-organisme/with-first-child").permitAll()
                .antMatchers("/**/organisme/**/enfants").permitAll()
                .antMatchers("/**/organisme/type-organisme/**").permitAll()
                .antMatchers("/**/corps/all/**").permitAll()
                .antMatchers("/**/grade/corps/**").permitAll()
                .antMatchers(HttpMethod.POST, "/**/contacts").permitAll()

                .antMatchers("/**/utilisateur/confirmation-inscription/*").permitAll()
                .antMatchers(HttpMethod.POST, "/**/utilisateur/confirmation-inscription").permitAll()
                .antMatchers(HttpMethod.POST, "/**/utilisateur/demande-nouveau-mdp").permitAll()
                .antMatchers("/**/utilisateur/changement-mdp/*").permitAll()
                .antMatchers(HttpMethod.POST, "/**/utilisateur/changement-mdp").permitAll()
                .antMatchers("/**/utilisateur/infos-persos/*").permitAll()
                .antMatchers(
                        HttpMethod.GET,
                        "/v2/api-docs",
                        "/swagger-resources/**",
                        "/swagger-ui/**",
                        "/webjars/**",
                        "favicon.ico"
                ).permitAll()
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
        //@formatter:on
    }


}

