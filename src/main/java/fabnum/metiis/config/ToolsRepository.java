package fabnum.metiis.config;

import fabnum.metiis.services.exceptions.ServiceException;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Table;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ToolsRepository {


    /**
     * Méthode permettant de recupérer le nom sql de la table associé à son Entité.
     *
     * @param className : Class<? extends BaseEntity>
     * @return nomTable
     */
    public static String getNomTable(Class<?> className) {
        Table table = className.getAnnotation(Table.class);
        return table.name();
    }


    /**
     * Retoune une liste d'objet à partir d'une query (généralement SQL) construit la liste avec un constructeur prenant un Object[] en parametre.
     */
    public static <T> List<T> convertir(List<?> objets, Class<T> classe) {
        return objets.stream()
                .map(Object[].class::cast)
                .map(objects -> {
                    try {
                        return createNewInstance(classe, objects);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    public static <T> Optional<T> convertir(Optional<?> optional, Class<T> classe) {
        Optional<T> resultat = Optional.empty();
        if (optional.isEmpty()) {
            return resultat;
        }
        try {
            Object[] object = (Object[]) optional.get();
            return Optional.of(createNewInstance(classe, object));
        } catch (Exception e) {
            throw new ServiceException("convertir erreur");
        }
    }

    private static <T> T createNewInstance(Class<T> classe, Object[] tupleSQL) throws Exception {
        //On fait ca pour que new instance comprennent bien qu'il sagit d'1 seul param contenant une liste de param.
        //Sinon ca plante !
        Object[] param = new Object[1];
        param[0] = tupleSQL;

        return classe.getConstructor(Object[].class).newInstance(param);
    }

    /**
     * Transforme le resultat d'un count sql en Long.
     *
     * @param objects  :
     * @param position :
     * @return Long
     */
    public static Long sqlCountToLong(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        return ((Number) objects[position]).longValue();
    }

    /**
     * Transforme le resultat d'un sum sql en Long.
     *
     * @param objects  :
     * @param position :
     * @return Long
     */
    public static Long sqlSumToLong(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        return ((Number) objects[position]).longValue();
    }

    /**
     * Transforme le resultat d'un numéric sql en Integer (déclaration en Integer dans le mapping hibernate).
     *
     * @param objects  :
     * @param position :
     * @return Integer
     */
    public static Integer sqlInteger(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        return ((Number) objects[position]).intValue();
    }

    /**
     * Transforme le resultat d'un numéric sql en Long (déclaration en Long dans le mapping hibernate), retourne null si null.
     *
     * @param objects  :
     * @param position :
     * @return Long
     */
    public static Long sqlLong(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        return sqlLong(objects[position]);
    }

    public static Long sqlLong(Object object) {
        if (object == null) {
            return null;
        }
        return ((Number) object).longValue();
    }

    public static Float sqlFloat(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        return ((Number) objects[position]).floatValue();
    }

    /**
     * Transforme le resultat d'un String sql en string (déclaration en String dans le mapping hibernate), retourne null si null.
     *
     * @param objects  :
     * @param position :
     * @return String
     */
    public static String sqlString(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        return objects[position].toString();
    }

    /**
     * Transforme le resultat d'un String sql en string (déclaration en String dans le mapping hibernate), retourne "" si null.
     *
     * @param objects  :
     * @param position :
     * @return String
     */
    public static String sqlStringVide(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return "";
        }
        return objects[position].toString();
    }

    /**
     * Transforme le resultat d'un String sql en string (déclaration en String dans le mapping hibernate), retourne null si null. trim le texte si non null.
     *
     * @param objects  :
     * @param position :
     * @return String
     */
    public static String sqlStringWithTrim(Object[] objects, int position) {
        String texte = sqlString(objects, position);
        if (texte != null) {
            texte = texte.trim();
        }
        return texte;
    }

    public static Instant sqlDate(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        if (objects[position] instanceof BigInteger) {
            if (objects[position].equals(0)) {
                return null;
            }
        }
        if (objects[position] instanceof Timestamp) {
            return ((Timestamp) objects[position]).toInstant();
        }
        return (Instant) objects[position];
    }

    @Nullable
    public static Boolean sqlBoolean(Object[] objects, int position) {
        if (isNotValideTab(objects, position)) {
            return null;
        }
        return (Boolean) objects[position];
    }

    private static boolean isNotValideTab(Object[] objects, int position) {
        return objects == null || position >= objects.length || objects[position] == null;
    }


}
