package fabnum.metiis.config.checkBefore;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Sert a verifier,  avant la suppression d'une entity, si celle ci n'est pas rattachée à d'autre données :<br/>
 * par exemple : <i> ne pas supprimer un grade si celui ci est deja affecté à un utilisateur</i>.<br/>
 * Se place uniquement sur des {@link java.util.Collection} ({@link java.util.Set}). <br/>
 * Est utilisé par {@link MetiisBeforeDelete} via {@link fabnum.metiis.services.abstraction.AbstarctService}  dans la methode checkBeforeDelete.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CheckBeforeDelete {

    String message();
}
