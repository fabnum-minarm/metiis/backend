package fabnum.metiis.config.checkBefore;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Sert a tronquer un champs texte si sa taille est trop grande<br/>
 * En complement de @{@link javax.persistence.Column} et son attribut size
 * Est utilisé par {@link MetiisBeforeSaveOrUpdate} via {@link fabnum.metiis.services.abstraction.AbstarctService}
 * dans la methode checkBeforeSaveOrUpdate. Mais il faut pour cela impérativement utilisé la méthode saveOrUpdate.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TruncateIfTooLong {

}
