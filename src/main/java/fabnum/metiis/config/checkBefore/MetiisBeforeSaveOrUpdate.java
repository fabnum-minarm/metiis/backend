package fabnum.metiis.config.checkBefore;

import fabnum.metiis.config.Tools;
import fabnum.metiis.services.exceptions.ServiceException;

import javax.persistence.Column;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utilise les annotation {@link TruncateIfTooLong} & @{@link Column}
 * afin de tronquer automatiquement les chaines de charactere si celle ci dépasse la taille prévu par le lengh de @Colum.
 */
public class MetiisBeforeSaveOrUpdate {

    public static <T> void verifier(T entity) {
        List<Field> fields = fields(entity);
        fields.forEach(field -> {
            verifierTruncateIfTooLong(field, entity);
            verifierMinMax(field, entity);
        });
    }

    private static <T> void verifierMinMax(Field field, T objet) {
        MinMax minMax = field.getAnnotation(MinMax.class);

        if (minMax != null) {
            bornerNombre(field, objet, minMax);
        }
    }

    private static <T> void bornerNombre(Field field, T objet, MinMax minMax) {
        try {
            Object value = get(field, objet);
            if (value != null) {
                if (value instanceof Number) {
                    Number nombre = (Number) value;
                    set(field, objet, Tools.bornerNombre(nombre, minMax.min(), minMax.max()));
                } else {
                    throw new ServiceException("MinMax ne prend pas en compte de genre de champs <" + objet.getClass().getSimpleName() + "." + field.getName() + ">");
                }
            }
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException ignored) {
        }
    }

    private static <T> void verifierTruncateIfTooLong(Field field, T objet) {
        TruncateIfTooLong truncateIfTooLong = field.getAnnotation(TruncateIfTooLong.class);

        if (truncateIfTooLong != null) {
            Column column = field.getAnnotation(Column.class);
            if (column != null) {
                tuncateStringIfTooLong(column, field, objet);
            }
        }
    }

    private static <T> void tuncateStringIfTooLong(Column column, Field field, T objet) {
        try {
            Object value = get(field, objet);
            if (value != null) {
                if (value instanceof String) {
                    String texte = (String) value;
                    if (texte.length() > column.length()) {
                        set(field, objet, texte.substring(0, column.length()));
                    }
                } else {
                    throw new ServiceException("TruncateIfTooLong ne prend pas en compte de genre de champs <" + objet.getClass().getSimpleName() + "." + field.getName() + ">");
                }
            }
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException ignored) {
        }
    }


    private static <T> Object get(Field field, T objet) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        PropertyDescriptor pd = new PropertyDescriptor(field.getName(), objet.getClass());
        Method getter = pd.getReadMethod();
        return invoke(getter, objet);
    }

    private static <T> Object set(Field field, T objet, Object value) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        PropertyDescriptor pd = new PropertyDescriptor(field.getName(), objet.getClass());
        Method setter = pd.getWriteMethod();
        return invoke(setter, objet, value);
    }

    private static <T> Object invoke(Method methode, T objet, Object... params) throws InvocationTargetException, IllegalAccessException {
        return methode.invoke(objet, params);
    }

    private static <T> List<Field> fields(T objet) {
        Class classe = objet.getClass();
        List<Field> fields = new ArrayList<>(toListField(classe));
        Class superClasse = classe.getSuperclass();
        while (superClasse != null) {
            fields.addAll(toListField(superClasse));
            superClasse = superClasse.getSuperclass();
        }
        return fields;

    }

    private static List<Field> toListField(Class classe) {
        return Arrays.asList(classe.getDeclaredFields());
    }

}
