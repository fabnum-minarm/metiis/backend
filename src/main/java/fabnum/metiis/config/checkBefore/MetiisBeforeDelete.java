package fabnum.metiis.config.checkBefore;

import fabnum.metiis.config.Tools;
import fabnum.metiis.services.exceptions.ServiceConstraintException;
import fabnum.metiis.services.exceptions.ServiceException;
import org.springframework.util.CollectionUtils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Utilise les annotation {@link CheckBeforeDelete} afin de verifier si l'entity est attaché à d'autre données qui pourrait empecher sa suppression.
 */
public class MetiisBeforeDelete {

    public static <T> void verifier(T entity) {
        List<Field> fields = fields(entity);
        fields.forEach(field -> verifier(field, entity));
    }


    private static <T> void verifier(Field fieldOrMethode, T objet) {
        CheckBeforeDelete annotation = fieldOrMethode.getAnnotation(CheckBeforeDelete.class);

        if (annotation != null) {
            verifierCheckBeforeDelete(annotation, fieldOrMethode, objet);
        }
    }

    private static <T> void verifierCheckBeforeDelete(CheckBeforeDelete annotation, Field field, T objet) {
        try {
            verifierValue(annotation, get(field, objet));
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException ignored) {
        }
    }

    private static void verifierValue(CheckBeforeDelete annotation, Object value) {
        if (value instanceof Collection) {
            if (!CollectionUtils.isEmpty((Collection) value)) {
                throw new ServiceConstraintException(Tools.message(annotation.message()));
            }
        } else {
            throw new ServiceException("CheckBeforeDelete ne prend pas en compte de genre de champs");
        }
    }

    private static <T> Object get(Field field, T objet) throws IntrospectionException, InvocationTargetException, IllegalAccessException {
        PropertyDescriptor pd = new PropertyDescriptor(field.getName(), objet.getClass());
        Method getter = pd.getReadMethod();
        return invoke(getter, objet);
    }

    private static <T> Object invoke(Method methode, T objet) throws InvocationTargetException, IllegalAccessException {
        return methode.invoke(objet);
    }

    private static <T> List<Field> fields(T objet) {
        Class classe = objet.getClass();
        List<Field> fields = new ArrayList<>(toListField(classe));
        Class superClasse = classe.getSuperclass();
        while (superClasse != null) {
            fields.addAll(toListField(superClasse));
            superClasse = superClasse.getSuperclass();
        }
        return fields;

    }

    private static List<Field> toListField(Class classe) {
        return Arrays.asList(classe.getDeclaredFields());
    }

}
