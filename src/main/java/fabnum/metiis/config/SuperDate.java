package fabnum.metiis.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.i18n.LocaleContextHolder;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class SuperDate {
    private LocalDateTime date;

    public SuperDate() {
        date = LocalDateTime.now();
    }

    public SuperDate(LocalDateTime date) {
        this.date = date;
    }

    public SuperDate(Instant instant) {
        date = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    public SuperDate(SuperDate superDate) {
        date = superDate.date;
    }

    public static SuperDate createFromJJMMAAA(String dateFormatter) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate date = LocalDate.parse(dateFormatter, formatter);
        return new SuperDate(date.atStartOfDay());
    }

    public static SuperDate createFromIsoDateTime(String dateFormatter) {
        LocalDateTime date = LocalDateTime.parse(dateFormatter, DateTimeFormatter.ISO_DATE_TIME);
        return new SuperDate(date);
    }

    public static SuperDate createFromIsoDate(String dateFormatter) {
        LocalDate date = LocalDate.parse(dateFormatter, DateTimeFormatter.ISO_DATE);
        return new SuperDate(date.atStartOfDay());
    }

    public static Instant now() {
        return Instant.now();
    }

    public SuperDate debutDeLaJournee() {
        date = date.toLocalDate().atTime(LocalTime.MIN);
        return this;
    }

    public SuperDate debutDeLaSemaine() {
        date = date.with(WeekFields.of(Locale.FRANCE).dayOfWeek(), 1);
        return this;
    }

    public SuperDate finDeLaSemaine() {
        date = date.with(WeekFields.of(Locale.FRANCE).dayOfWeek(), 7);
        return this;
    }

    public Integer getJour() {
        return date.getDayOfMonth();
    }

    public Integer getMois() {
        return date.getMonthValue();
    }

    public Integer getAnnee() {
        return date.getYear();
    }

    public SuperDate finDeLaJournee() {
        // ne pas utilisé  date = date.toLocalDate().atTime(LocalTime.MAX) les dates sont ensuite mal comprise par posgresql;
        date = date.withHour(23).withMinute(59).withSecond(59).withNano(0);
        return this;
    }

    public Instant instant() {
        return date.toInstant(ZoneOffset.UTC);
    }

    public LocalDateTime localDateTime() {
        return date;
    }

    public SuperDate copie() {
        return new SuperDate(date);
    }

    public boolean estAvant(SuperDate superDate) {
        return date.isBefore(superDate.date);
    }

    public boolean estEgale(SuperDate superDate) {
        return date.isEqual(superDate.date);
    }

    public boolean estEgaleJMA(SuperDate superDate) {
        return getJour().equals(superDate.getJour()) && getMois().equals(superDate.getMois()) && getAnnee().equals(superDate.getAnnee());
    }

    public boolean estEgaleJMA(Instant date) {
        SuperDate superDate = new SuperDate(date);
        return estEgaleJMA(superDate);
    }

    public boolean estAvantOuEgale(SuperDate superDate) {
        return estAvant(superDate) || date.isEqual(superDate.date);
    }

    public boolean estApres(SuperDate superDate) {
        return date.isAfter(superDate.date);
    }


    public boolean estApresOuEgale(SuperDate superDate) {
        return date.isAfter(superDate.date) || date.isEqual(superDate.date);
    }

    /**
     * Ex : Si diff du 16/09 au 17/09 renvoie 1, et du 16/09 au 16/09 revoie 0.
     *
     * @param fin .
     * @return nombre.
     */
    public long nombreDeJourEntre(SuperDate fin) {
        if (date.isBefore(fin.date)) {
            return ChronoUnit.DAYS.between(date, fin.date);
        }
        return ChronoUnit.DAYS.between(fin.date, date);
    }

    /**
     * Ex : Si diff du 16/09 au 17/09 renvoie 2, et du 16/09 au 16/09 revoie 1.
     *
     * @param fin .
     * @return nombre.
     */
    public long compterNombreDeJourDebutInclus(SuperDate fin) {
        return nombreDeJourEntre(fin) + 1;
    }


    public SuperDate jourSuivant() {
        return jourSuivant(1);
    }

    public SuperDate jourSuivant(long jours) {
        date = date.plusDays(jours);
        return this;
    }

    public SuperDate jourPrecedent(long jours) {
        date = date.minusDays(jours);
        return this;
    }

    public SuperDate jourDuMois(int jour) {
        date = date.withDayOfMonth(jour);
        return this;
    }

    public SuperDate moisSuivant() {
        return moisSuivant(1);
    }

    public SuperDate moisSuivant(int mois) {
        date = date.plusMonths(mois);
        return this;
    }

    public SuperDate anneePrecedente() {
        date = date.plusYears(-1);
        return this;
    }

    public SuperDate anneeSuivante() {
        date = date.plusYears(1);
        return this;
    }


    /**
     * format yyyy-MM-dd
     *
     * @return yyyy-MM-dd
     */
    public String toStringAAAAMMDD() {
        String pattern = "yyyy-MM-dd";
        DateTimeFormatter format = getDateTimeFormatter(pattern);
        return format.format(date);
    }

    public String toStringDDLLLLAAAA() {
        String pattern = "dd LLLL yyyy";
        DateTimeFormatter format = getDateTimeFormatter(pattern);
        return format.format(date);
    }

    public String toStringLLLL() {
        String pattern = "LLLL";
        DateTimeFormatter format = getDateTimeFormatter(pattern);
        return format.format(date);
    }


    /**
     * format dd/MM/yyyy.
     *
     * @return dd/MM/yyyy
     */
    public String toStringDDMMAAAA() {
        String pattern = "dd/MM/yyyy";
        DateTimeFormatter format = getDateTimeFormatter(pattern);
        return format.format(date);
    }


    @NotNull
    private DateTimeFormatter getDateTimeFormatter(String pattern) {
        return DateTimeFormatter.ofPattern(pattern, LocaleContextHolder.getLocale());
    }

    /**
     * 8/23/16
     *
     * @return dd/MM/yy
     */
    @Override
    public String toString() {
        return DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).format(date);
    }

    public static void main(String[] args) {

        SuperDate debut = SuperDate.createFromIsoDate("2020-09-23").debutDeLaSemaine();
        System.out.println(debut);

        /*SuperDate fin = SuperDate.createFromJJMMAAA("19/04/1982").debutDeLaJournee();
        SuperDate debut = SuperDate.createFromIsoDate("1982-10-16").debutDeLaJournee();
        SuperDate isoDT = SuperDate.createFromIsoDateTime("1982-10-26T16:00:00Z");
        System.out.println(isoDT.instant());

        System.out.println(debut.compterNombreDeJourDebutInclus(fin));

        List<SuperDate> dates = new ArrayList<>();
        SuperDate startDispo = debut.copie();
        while (startDispo.estAvantOuEgale(fin)) {
            SuperDate debut2 = startDispo.copie();
            dates.add(debut2);
            System.out.println("debut " + debut2.instant());
            SuperDate fin2 = debut2.copie().finDeLaJournee();
            System.out.println("fin " + fin2.instant());
            dates.add(fin2);

            startDispo.jourSuivant();
        }

        dates.forEach(System.out::println);*/
    }


}
