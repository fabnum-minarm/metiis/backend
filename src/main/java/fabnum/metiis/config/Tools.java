package fabnum.metiis.config;

import fabnum.metiis.domain.abstractions.AbstractEntity;
import fabnum.metiis.domain.abstractions.AbstractEntityIdLong;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.IPasswords;
import fabnum.metiis.exceptions.ConflictException;
import fabnum.metiis.exceptions.NotFoundException;
import fabnum.metiis.services.exceptions.ServiceException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;

import java.security.SecureRandom;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Classe utilitaire de l'application (toutes les méthodes devront être
 * statiques).
 */
@Controller
public final class Tools {

    private static MessageSource source;

    public final static String PWD_PATTERN = "(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[^a-zA-Z\\d])^.{12,}$";

    @Autowired
    private Tools(MessageSource messageSource) {
        if (Tools.source == null) {
            Tools.source = messageSource;
        }
    }

    /**
     * Récupération d'un message applicatif via un bundle (cf application.yml).
     *
     * @param code   : la clé dans le fichier des messages,
     * @param params : les paramètres potentiels à apposer dans la valeur.
     * @return {@link String} : le message ou la valeur de la clé si elle n'est pas trouvée.
     */
    public static String message(String code, Object... params) {
        return source.getMessage(code, params, code, Locale.FRANCE);
    }

    /**
     * Méthode de génération du mot de passe.
     *
     * @return {@link String}
     */
    public static String generatePassword() {

        // List des caractères formant le mot de passe généré.
        List<Character> allChars = new ArrayList<>();

        // Création de l'objet gérant l'aléatoire.
        Random rand = new SecureRandom();

        // Récupération de majuscules.
        IntStream upperLetters = rand.ints(randomizePasswordElementSize(), 65, 90);
        // Récupération de minuscules.
        IntStream lowerLetters = rand.ints(randomizePasswordElementSize(), 97, 122);
        // Récupération de chiffres.
        IntStream numbers = rand.ints(randomizePasswordElementSize(), 48, 57);
        // Récupération de caractères spéciaux.
        IntStream specials = rand.ints(randomizePasswordElementSize(), 33, 47);

        // On va transformer le IntStream en stream de caractère (mapToObjet) et on va ajouter chaque occurence dans allChars (forEach).
        upperLetters.mapToObj(i -> ((char) i)).forEach(allChars::add);
        lowerLetters.mapToObj(i -> ((char) i)).forEach(allChars::add);
        numbers.mapToObj(i -> ((char) i)).forEach(allChars::add);
        specials.mapToObj(i -> ((char) i)).forEach(allChars::add);

        // On va mélanger la liste
        Collections.shuffle(allChars);

        // Au final la liste va être récupérer dans un stringBuilder, et chaque caractère y sera ajouter (collect).
        return allChars.stream().collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();
    }

    /**
     * @param min {@link Long} : borne minimale
     * @param max {@link Long} : borne maximale
     * @return {@link Long} : un nombre parmi les bornes passées en paramètre.
     */
    public static long ramdomizeLong(long min, long max) {
        return new SecureRandom().longs(1, min, max).findFirst().orElse(min);
    }

    /**
     * Méthode donnant aléatoirement un nombre d'occurence à fournir pour un élément de la génération du password (entre 3 et 5).
     * Elle permettant de générer un mot de passe allant de 12 à 20 caractères.
     *
     * @return {@link Long}
     */
    private static long randomizePasswordElementSize() {
        return ramdomizeLong(3, 5);
    }


    public static Long getValueFromOptional(Long value) {
        return Optional.ofNullable(value).orElse(Long.MIN_VALUE);
    }

    public static <T> T getValueFromOptionalOrNotFoundException(Optional<T> optional) {
        return optional.orElseThrow(NotFoundException::new);
    }

    public static <T> T getValueFromOptionalOrNotFoundException(Optional<T> optional, String classeName) {
        return optional.orElseThrow(() -> new NotFoundException(classeName));
    }

    /**
     * Teste si un champs texte est null, renvoie une exception avec le message errorMessageKey comme clé.
     *
     * @param value           .
     * @param errorMessageKey .
     */
    public static <T> void testerNullite(T value, String errorMessageKey) {
        testerNulliterGenerique(value, errorMessageKey);
    }

    private static <T> void testerNulliterGenerique(T value, String errorMessageKey) {
        if (Optional.ofNullable(value).isEmpty()) {
            throw new ServiceException(Tools.message(errorMessageKey));
        }
    }

    public static void testerNulliteString(String value, String errorMessageKey) {
        testerNulliterGenerique(value, errorMessageKey);
        if (value.isBlank()) {
            throw new ServiceException(Tools.message(errorMessageKey));
        }
    }

    /**
     * ToLowerCase + evite les null pointer.
     *
     * @param texte .
     * @return texte
     */
    public static String toLowerCase(String texte) {
        if (texte == null) {
            return null;
        }
        return texte.toLowerCase();
    }


    public static boolean testerNulliteString(String value) {
        try {
            testerNulliteString(value, "");
            return true;
        } catch (ServiceException e) {
            return false;
        }
    }

    public static void testerNulliteNumber(Number value, String errorMessageKey) {
        testerNulliteNumber(value, errorMessageKey, 0L);
    }

    public static void testerNulliteNumber(Number value, String errorMessageKey, Long nombre) {
        testerNulliterGenerique(value, errorMessageKey);
        if (value.longValue() <= nombre) {
            throw new ServiceException(Tools.message(errorMessageKey));
        }
    }

    public static <T> void testerNulliterCollection(Collection<T> value, String errorMessageKey) {
        testerNulliterGenerique(value, errorMessageKey);
        if (CollectionUtils.isEmpty(value)) {
            throw new ServiceException(Tools.message(errorMessageKey));
        }
    }

    public static <E extends AbstractEntity> void testerExistance(Optional<E> optionalEntity, String errorMessageKey) {
        optionalEntity.ifPresent(p -> {
            ConflictException ex = new ConflictException(Tools.message(errorMessageKey));
            ex.addHeader(HttpHeaders.LOCATION, p.getId());
            throw ex;
        });
    }

    public static <E extends AbstractEntityIdLong> List<Long> getListId(Collection<E> entitys) {
        List<Long> retour = new ArrayList<>(entitys.size());
        if (!CollectionUtils.isEmpty(entitys)) {
            retour = entitys.stream().map(AbstractEntityIdLong::getId).collect(Collectors.toList());
        }

        return retour;
    }

    public static <E extends AbstractLongDto> List<Long> getListIdDto(Collection<E> dtos) {
        List<Long> retour = new ArrayList<>();
        if (!CollectionUtils.isEmpty(dtos)) {
            retour = dtos.stream().map(AbstractLongDto::getId).collect(Collectors.toList());
        }
        return retour;
    }

    /**
     * pour calculer des durée traitement.
     */
    public static long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    /**
     * pour calculer des durée de traitement. à n'utilisé qu'en dev.
     *
     * @param start   .
     * @param message .
     */
    public static void duration(Long start, String message) {
        long end = currentTimeMillis();
        System.out.println("time " + message + " : " + (end - start) + "ms");
    }

    /**
     * @param nombre   .
     * @param borneMin .
     * @param borneMax .
     * @return nombre borné
     */
    public static Number bornerNombre(Number nombre, long borneMin, long borneMax) {
        testerNulliteNumber(nombre, "default.tools.borner.null");
        if (borneMin > borneMax) {
            throw new ServiceException(Tools.message("default.tools.borner.conflit"));
        }

        Number retour = nombre;
        if (nombre.longValue() < borneMin) {
            retour = borneMin;
        } else if (nombre.longValue() > borneMax) {
            retour = borneMax;
        }
        return retour;
    }

    private static void verifierEgaliteMotDePasse(String password, String passwordConfirmation) {
        verifierEgaliteMotDePasse(password, passwordConfirmation, "password.confirm.error");
    }

    private static void verifierEgaliteMotDePasse(String password, String passwordConfirmation, String message) {
        if (!password.equals(passwordConfirmation)) {
            throw new ServiceException(Tools.message(message));
        }
    }

    private static void verifierFormatMotDePasse(String password) {
        if (!password.matches(Tools.PWD_PATTERN)) {
            throw new ServiceException(Tools.message("password.pattern.error"));
        }
    }

    public static void verifierFormatEtEgaliterMotDePasse(IPasswords passwords) {
        verifierFormatMotDePasse(passwords.getPassword());
        verifierEgaliteMotDePasse(passwords.getPassword(), passwords.getPasswordconfirmation());
    }

    public static void verifierAncienPassword(String ancienMotDePasse, String nouveauMotDePasse) {
        verifierNouveauMotDePasseDifferentAncien(ancienMotDePasse, nouveauMotDePasse);
    }

    private static void verifierNouveauMotDePasseDifferentAncien(String currentPassword, String password) {
        if (password.equals(currentPassword)) {
            throw new ServiceException(Tools.message("password.error.same"));
        }
    }

    public static void verifierFormatEtEgaliterMotDePasse(String password, String passwordConfirmation) {
        verifierFormatMotDePasse(password);
        verifierEgaliteMotDePasse(password, passwordConfirmation);
    }

    public static List<String> getEmail(String emailsSeparatedBySemicolon) {
        return List.of(emailsSeparatedBySemicolon.split(";")).stream().map(String::trim).collect(Collectors.toList());
    }

    public static boolean hasExpired(Instant datePrescription) {
        return Instant.now().isAfter(datePrescription);
    }

    public static boolean search(String texte, String search) {
        return StringUtils.stripAccents(texte).trim().toLowerCase().contains(search);
    }

    public static String getFirstLetterMajuscule(String texte) {
        if (!StringUtils.isEmpty(texte)) {
            return texte.substring(0, 1).toUpperCase();
        }
        return "";
    }


}