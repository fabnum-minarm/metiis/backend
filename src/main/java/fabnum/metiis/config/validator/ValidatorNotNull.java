package fabnum.metiis.config.validator;

import java.lang.annotation.*;

/**
 * Permet de tester si un champs ou une méthode n'est pas null.<br/>
 * <ul>
 *     <li>Pour un {@link String} test aussi si le champs n'est pas vide ou blanc (suite d'espace)<br/></li>
 *     <li>Pour un {@link Long} test si la valeur est > 0 (pour les id Notamment}<br/></li>
 *     <li>Pour une {@link java.util.Collection} test si elle n'est pas vide<br/></li>
 * </ul>
 * Renvoie le message d'erreur si la valeur est incorrecte.
 * <p>
 * Le(s) contexte(s) permette de verifier les champs en fonction de différent cas (create, update , ...)
 * Utiliser {@link MetiisValidation} pour tester un objet.
 */
@Repeatable(ListValidatorNotNull.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidatorNotNull {
    ContextEnum[] contexte();

    String message();

    NumberTestEnum numberTest() default NumberTestEnum.SUP_0;


}
