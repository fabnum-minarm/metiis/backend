package fabnum.metiis.config.validator;

public enum NumberTestEnum {
    /**
     * Test si le nombre est strictement supérieur à 0.
     */
    SUP_0(0L),

    /**
     * Test si le nombre est supérieur ou égale à 0.
     */
    SUP_OU_EGALE_0(-1L),

    ;

    private long valeur;

    NumberTestEnum(long valeur) {
        this.valeur = valeur;
    }

    public long getValeur() {
        return valeur;
    }
}
