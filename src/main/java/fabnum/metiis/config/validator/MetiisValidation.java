package fabnum.metiis.config.validator;

import fabnum.metiis.config.Tools;
import fabnum.metiis.services.exceptions.ServiceException;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MetiisValidation {

    /**
     * Vérifie objetATester : chaque attribut / methode pourtant l'annotation @{@link ValidatorNotNull} sera controllé.
     * Verifie également les attribut pourtant l'annotation @{@link ValidatorChild}, il testera alors pour cette objet
     * chaque attribut / methode pourtant l'annotation @{@link ValidatorNotNull}.
     *
     * @param objetATester .
     * @param contexte     .
     * @param <T>          .
     */
    public static <T> void verifier(T objetATester, ContextEnum contexte) {
        if (objetATester == null) {
            throw new ServiceException("MetiisValidation : L'objet à tester est null; Contexte : " + contexte.name());
        }
        List<AccessibleObject> fieldsAndMethodes = fields(objetATester);
        fieldsAndMethodes.addAll(methods(objetATester));
        fieldsAndMethodes.forEach(accessibleObject -> verifier(accessibleObject, contexte, objetATester));
    }

    public static <T> void verifierCollection(Collection<T> objetsATeste, ContextEnum contexte) {
        if (objetsATeste == null) {
            throw new ServiceException("MetiisValidation : la liste d'objet à tester est null; Contexte : " + contexte.name());
        }
        objetsATeste.forEach(objetATester -> verifier(objetATester, contexte));
    }


    private static <T> void verifier(AccessibleObject fieldOrMethode, ContextEnum contexte, T objet) {
        verifierValidatorNotNull(fieldOrMethode, contexte, objet);
        verifierListeValidatorNotNull(fieldOrMethode, contexte, objet);

        if (fieldOrMethode instanceof Field) {
            verifierValidatorChild((Field) fieldOrMethode, contexte, objet);
        }

    }

    private static <T> void verifierListeValidatorNotNull(AccessibleObject fieldOrMethode, ContextEnum contexte, T objet) {
        ListValidatorNotNull annotation = fieldOrMethode.getAnnotation(ListValidatorNotNull.class);

        if (annotation != null) {
            List<ValidatorNotNull> validators = toList(annotation);
            validators.forEach(validatorNotNull -> verifierValidatorNotNull(validatorNotNull, fieldOrMethode, contexte, objet));
        }
    }

    private static <T> void verifierValidatorNotNull(AccessibleObject fieldOrMethode, ContextEnum contexte, T objet) {
        ValidatorNotNull annotation = fieldOrMethode.getAnnotation(ValidatorNotNull.class);

        if (annotation != null) {
            verifierValidatorNotNull(annotation, fieldOrMethode, contexte, objet);
        }
    }


    private static <T> void verifierValidatorChild(Field field, ContextEnum contexte, T objet) {
        ValidatorChild annotation = field.getAnnotation(ValidatorChild.class);

        if (annotation != null) {
            List<ContextEnum> contextes = toList(annotation);
            try {
                if (contextes.contains(contexte)) {
                    Object objetATester = get(field, objet);
                    if (objetATester == null) {
                        throw new ServiceException("MetiisValidation :  <" + objet.getClass().getSimpleName() + "." + field.getName() + "> est null ; Contexte : " + contexte.name());
                    }
                    verifier(objetATester, contexte);
                }
            } catch (IntrospectionException | InvocationTargetException | IllegalAccessException ignored) {
            }
        }
    }

    private static <T> void verifierValidatorNotNull(ValidatorNotNull annotation, AccessibleObject fieldOrMethode, ContextEnum contexte, T objet) {
        List<ContextEnum> contextes = toList(annotation);
        try {
            if (contextes.contains(contexte)) {
                verifierValue(annotation, get(fieldOrMethode, objet));
            }
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException ignored) {
        }
    }

    private static <T> Object invoke(Method methode, T objet) throws InvocationTargetException, IllegalAccessException {
        return methode.invoke(objet);
    }

    private static void verifierValue(ValidatorNotNull annotation, Object value) {
        if (value instanceof String) {
            Tools.testerNulliteString((String) value, annotation.message());
        } else if (value instanceof Long) {
            Tools.testerNulliteNumber((Number) value, annotation.message(), annotation.numberTest().getValeur());
        } else if (value instanceof Collection) {
            Tools.testerNulliterCollection((Collection) value, annotation.message());
        } else {
            Tools.testerNullite(value, annotation.message());
        }
    }

    private static <T> List<AccessibleObject> methods(T objet) {
        Class classe = objet.getClass();
        List<AccessibleObject> methods = new ArrayList<>(toListMethode(classe));
        Class superClasse = classe.getSuperclass();

        while (superClasse != null) {
            methods.addAll(toListMethode(superClasse));
            superClasse = superClasse.getSuperclass();
        }
        return methods;
    }

    private static List<Method> toListMethode(Class classe) {
        return Arrays.asList(classe.getDeclaredMethods());
    }

    private static <T> Object get(AccessibleObject fieldOrMethode, T objet) throws IntrospectionException, InvocationTargetException, IllegalAccessException {

        if (fieldOrMethode instanceof Field) {
            Field field = (Field) fieldOrMethode;
            PropertyDescriptor pd = new PropertyDescriptor(field.getName(), objet.getClass());
            Method getter = pd.getReadMethod();
            return invoke(getter, objet);
        } else if (fieldOrMethode instanceof Method) {
            return invoke((Method) fieldOrMethode, objet);
        }
        return null;
    }

    private static <T> List<AccessibleObject> fields(T objet) {
        Class classe = objet.getClass();
        List<AccessibleObject> fields = new ArrayList<>(toListField(classe));
        Class superClasse = classe.getSuperclass();
        while (superClasse != null) {
            fields.addAll(toListField(superClasse));
            superClasse = superClasse.getSuperclass();
        }
        return fields;

    }

    private static List<ContextEnum> toList(ValidatorChild annotation) {
        return Arrays.asList(annotation.contexte());
    }

    private static List<ContextEnum> toList(ValidatorNotNull annotation) {
        return Arrays.asList(annotation.contexte());
    }

    private static List<ValidatorNotNull> toList(ListValidatorNotNull annotation) {
        return Arrays.asList(annotation.value());
    }

    private static List<Field> toListField(Class classe) {
        return Arrays.asList(classe.getDeclaredFields());
    }

}
