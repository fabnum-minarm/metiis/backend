package fabnum.metiis.exceptions;

import fabnum.metiis.exceptions.superClasses.CadreApiException;
import org.springframework.http.HttpStatus;

/**
 * Exception permettant de determiner quand une ressource lue est introuvable.
 * Confer documentation Cadre API.
 */
public class NotFoundException extends CadreApiException {
    private static final long serialVersionUID = 1L;

    public NotFoundException() {
        super(HttpStatus.NOT_FOUND, "default.notfound.error");
    }


    public NotFoundException(String classeName) {
        super(HttpStatus.NOT_FOUND, "default.notfound.error.classename", classeName);
    }
}
