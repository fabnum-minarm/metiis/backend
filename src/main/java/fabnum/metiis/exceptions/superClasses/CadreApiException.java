package fabnum.metiis.exceptions.superClasses;

import fabnum.metiis.config.Tools;
import fabnum.metiis.dto.tools.InfoServiceDto;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;


public abstract class CadreApiException extends RuntimeException {

    private static final long serialVersionUID = -6623353487395541715L;

    @Getter
    private final HttpStatus status;

    @Getter
    private final String formatMessage;

    @Getter
    private List<Object> params = new ArrayList<>();

    public CadreApiException(HttpStatus status, String formatMessage, Object... params) {
        this.status = status;
        this.formatMessage = formatMessage;
        this.params = List.of(params);
    }

    public CadreApiException(HttpStatus status, String formatMessage) {
        this.status = status;
        this.formatMessage = formatMessage;
    }

    public ResponseEntity<Object> createResponse() {
        InfoServiceDto dto = InfoServiceDto.ko(status, Tools.message(formatMessage, params));
        return new ResponseEntity<>(dto, dto.getStatus());
    }
}
