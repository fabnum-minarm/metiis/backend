package fabnum.metiis.exceptions.superClasses;

import fabnum.metiis.config.Tools;
import fabnum.metiis.dto.tools.InfoServiceDto;
import lombok.Getter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;


public abstract class CadreApiWithHeadersException extends CadreApiException {

    @Getter
    private Map<String, String> headers;

    public CadreApiWithHeadersException(HttpStatus status, String formatMessage) {
        super(status, formatMessage);
        this.headers = new HashMap<>();
    }

    public void addHeader(String key, Object value) {
        if(key != null && value != null) {
            this.headers.put(key, value.toString());
        }
    }

    @Override
    public ResponseEntity createResponse() {
        InfoServiceDto dto = InfoServiceDto.ko(this.getStatus(), Tools.message(this.getFormatMessage()));
        HttpHeaders headers = new HttpHeaders();

        this.headers.forEach(headers::add);
        return new ResponseEntity<>(dto, headers,dto.getStatus());
    }
}
