package fabnum.metiis.exceptions;

import fabnum.metiis.exceptions.superClasses.CadreApiWithHeadersException;
import org.springframework.http.HttpStatus;

/**
 * Exception permettant de determiner quand une ressource possède un identifiant unique déjà référencé dans une autre occurence.
 * Confer documentation Cadre API.
 */
public class ConflictException extends CadreApiWithHeadersException {
    private static final long serialVersionUID = 1L;

    public ConflictException() {
        super(HttpStatus.CONFLICT, "default.conflit.error");
    }

    public ConflictException(String formatMessage) {
        super(HttpStatus.CONFLICT, formatMessage);
    }
}
