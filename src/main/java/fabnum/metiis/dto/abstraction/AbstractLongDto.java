package fabnum.metiis.dto.abstraction;

import fabnum.metiis.domain.abstractions.AbstractEntity;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class AbstractLongDto implements AbstractEntity<Long> {

    protected static final long serialVersionUID = 3340249735911849145L;

    @EqualsAndHashCode.Include
    protected Long id;

}
