package fabnum.metiis.dto.audit;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.dto.rh.UtilisateurDto;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class CreatedUpdatedByDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant createdDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant lastModifiedDate;

    private String createdByUsername;
    private String lastModifiedByUsername;

    private UtilisateurDto createdBy;
    private UtilisateurDto updatedBy;

    /* " t.createdDate , t.lastModifiedDate , t.createdBy , t.lastModifiedBy ," +
             " utc.id ,  utc.nom , utc.prenom  ," +
             " utu.id ,  utu.nom , utu.prenom " +*/
    public CreatedUpdatedByDto(Instant createdDate, Instant lastModifiedDate, String createdByUsername, String lastModifiedByUsername,
                               Long idUtilisateurCreateur, String nomCreateur, String prenomCreateur,
                               Long idUtilisateurLastUpdate, String nomLastUpdate, String prenomLastUpdate
    ) {

        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
        this.createdByUsername = createdByUsername;
        this.lastModifiedByUsername = lastModifiedByUsername;

        if (idUtilisateurCreateur != null) {
            createdBy = new UtilisateurDto(idUtilisateurCreateur, nomCreateur, prenomCreateur);
        }
        if (idUtilisateurLastUpdate != null) {
            updatedBy = new UtilisateurDto(idUtilisateurLastUpdate, nomLastUpdate, prenomLastUpdate);
        }
    }

}
