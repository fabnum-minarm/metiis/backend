package fabnum.metiis.dto.notification;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
@NoArgsConstructor
public class NotificationDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.activite.cree")
    private Boolean notifAppActiviteCree;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.activite.modif")
    private Boolean notifAppActiviteModif;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.activite.rappel")
    private Boolean notifAppActiviteRappel;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.activite.supp")
    private Boolean notifAppActiviteSupp;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.candidat.retenu")
    private Boolean notifAppCandidatRetenu;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.candidat.nonretenu")
    private Boolean notifAppCandidatNonRetenu;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.candidat.convoque")
    private Boolean notifAppCandidatConvoque;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.candidat.nonconvoque")
    private Boolean notifAppCandidatNonConvoque;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.partage.activite.unite")
    private Boolean notifAppPartageActiviteAutreUnite;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.cree.activite.unite")
    private Boolean notifAppCreaActiviteAutreUnite;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.modif.activite.unite")
    private Boolean notifAppModifActivitePartage;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.app.missing.supp.activite.unite")
    private Boolean notifAppSuppActivitePartage;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.activite.cree")
    private Boolean notifMailActiviteCree;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.activite.modif")
    private Boolean notifMailActiviteModif;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.activite.rappel")
    private Boolean notifMailActiviteRappel;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.activite.supp")
    private Boolean notifMailActiviteSupp;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.candidat.retenu")
    private Boolean notifMailCandidatRetenu;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.candidat.nonretenu")
    private Boolean notifMailCandidatNonRetenu;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.candidat.convoque")
    private Boolean notifMailCandidatConvoque;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.candidat.nonconvoque")
    private Boolean notifMailCandidatNonConvoque;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.partage.activite.unite")
    private Boolean notifMailPartageActiviteAutreUnite;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.cree.activite.unite")
    private Boolean notifMailCreaActiviteAutreUnite;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.modif.activite.unite")
    private Boolean notifMailModifActivitePartage;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_NOTIF, ContextEnum.UPDATE_NOTIF}, message = "notification.mail.missing.supp.activite.unite")
    private Boolean notifMailSuppActivitePartage;

    private Long idUtilistateur;

    private Long idTypeActivite;

    public NotificationDto(Long id){ super(id); }

    public NotificationDto(Long id, Boolean notifAppActiviteCree, Boolean notifAppActiviteModif,
                           Boolean notifAppActiviteRappel, Boolean notifAppActiviteSupp,
                           Boolean notifAppCandidatRetenu, Boolean notifAppCandidatNonRetenu,
                           Boolean notifAppCandidatConvoque, Boolean notifAppCandidatNonConvoque,
                           Boolean notifAppPartageActiviteAutreUnite, Boolean notifAppCreaActiviteAutreUnite,
                           Boolean notifAppModifActivitePartage, Boolean notifAppSuppActivitePartage,
                           Boolean notifMailActiviteCree, Boolean notifMailActiviteModif,
                           Boolean notifMailActiviteRappel, Boolean notifMailActiviteSupp,
                           Boolean notifMailCandidatRetenu, Boolean notifMailCandidatNonRetenu,
                           Boolean notifMailCandidatConvoque, Boolean notifMailCandidatNonConvoque,
                           Boolean notifMailPartageActiviteAutreUnite, Boolean notifMailCreaActiviteAutreUnite,
                           Boolean notifMailModifActivitePartage, Boolean notifMailSuppActivitePartage,
                           Long idUtilisateur, Long idTypeActivite) {
        super(id);
        this.notifAppActiviteCree = notifAppActiviteCree;
        this.notifAppActiviteModif = notifAppActiviteModif;
        this.notifAppActiviteRappel = notifAppActiviteRappel;
        this.notifAppActiviteSupp = notifAppActiviteSupp;
        this.notifAppCandidatRetenu = notifAppCandidatRetenu;
        this.notifAppCandidatNonRetenu = notifAppCandidatNonRetenu;
        this.notifAppCandidatConvoque = notifAppCandidatConvoque;
        this.notifAppCandidatNonConvoque = notifAppCandidatNonConvoque;
        this.notifAppPartageActiviteAutreUnite = notifAppPartageActiviteAutreUnite;
        this.notifAppCreaActiviteAutreUnite = notifAppCreaActiviteAutreUnite;
        this.notifAppModifActivitePartage = notifAppModifActivitePartage;
        this.notifAppSuppActivitePartage = notifAppSuppActivitePartage;
        this.notifMailActiviteCree = notifMailActiviteCree;
        this.notifMailActiviteModif = notifMailActiviteModif;
        this.notifMailActiviteRappel = notifMailActiviteRappel;
        this.notifMailActiviteSupp = notifMailActiviteSupp;
        this.notifMailCandidatRetenu = notifMailCandidatRetenu;
        this.notifMailCandidatNonRetenu = notifMailCandidatNonRetenu;
        this.notifMailCandidatConvoque = notifMailCandidatConvoque;
        this.notifMailCandidatNonConvoque = notifMailCandidatNonConvoque;
        this.notifMailPartageActiviteAutreUnite = notifMailPartageActiviteAutreUnite;
        this.notifMailCreaActiviteAutreUnite = notifMailCreaActiviteAutreUnite;
        this.notifMailModifActivitePartage = notifMailModifActivitePartage;
        this.notifMailSuppActivitePartage = notifMailSuppActivitePartage;
        this.idTypeActivite = idTypeActivite;
        this.idUtilistateur = idUtilisateur;
    }
}
