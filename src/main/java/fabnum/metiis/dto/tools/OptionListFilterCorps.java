package fabnum.metiis.dto.tools;

import lombok.Getter;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


public final class OptionListFilterCorps {

    @Getter
    private Set<Long> idsCorps = new HashSet<>();

    @Getter
    private Set<Long> idsGroupes = new HashSet<>();

    @Getter
    private String search;

    private OptionListFilterCorps() {

    }

    public static OptionListFilterCorps byCorps(Long... idCorps) {
        return new OptionListFilterCorps().andAddCorps(idCorps);
    }

    public static OptionListFilterCorps byGroupe(Long... idGroupePersonnaliser) {
        return new OptionListFilterCorps().andAddGroupes(idGroupePersonnaliser);
    }

    public OptionListFilterCorps andAddGroupes(Long... idGroupePersonnaliser) {
        if (idGroupePersonnaliser != null) {
            idsGroupes.addAll(Arrays.stream(idGroupePersonnaliser).filter(Objects::nonNull).collect(Collectors.toUnmodifiableSet()));
        }
        return this;
    }

    public OptionListFilterCorps andSearch(String search) {
        this.search = search;
        return this;
    }

    public OptionListFilterCorps andAddCorps(Long... idCorps) {
        if (idCorps != null) {
            idsCorps.addAll(Arrays.stream(idCorps).filter(Objects::nonNull).collect(Collectors.toUnmodifiableSet()));
        }
        return this;
    }

    public boolean hasCorps() {
        return !CollectionUtils.isEmpty(idsCorps);
    }

    public boolean hasGroupes() {
        return !CollectionUtils.isEmpty(idsGroupes);
    }
}
