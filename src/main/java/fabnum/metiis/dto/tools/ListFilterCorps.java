package fabnum.metiis.dto.tools;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.config.Tools;
import fabnum.metiis.dto.rh.CorpsCountDto;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.dto.rh.WithCorpsDto;
import fabnum.metiis.dto.rh.WithIdUtilisateur;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public class ListFilterCorps implements Serializable {
    private static final long serialVersionUID = -8599307069234756942L;

    @JsonIgnore
    private final Set<Long> idsCorpsFiltre = new HashSet<>();

    @JsonIgnore
    private Set<Long> idsUtilisateurFiltre = new HashSet<>();

    @Getter
    @JsonIgnore
    private List<WithCorpsDto> all = new ArrayList<>();

    @Getter
    private final List<CorpsCountDto> corps = new ArrayList<>();

    @Getter
    private final List<WithCorpsDto> filtrer = new ArrayList<>();

    @JsonIgnore
    @Setter
    private List<WithIdUtilisateur> filtreGroupe = new ArrayList<>();

    @JsonIgnore
    private String search;


    public ListFilterCorps(List<CorpsDto> corps, OptionListFilterCorps option) {

        if (option.hasCorps()) {
            idsCorpsFiltre.addAll(option.getIdsCorps());
        }
        if (corps != null) {
            this.corps.addAll(corps.stream().map(CorpsCountDto::new).collect(Collectors.toList()));
        }
        addFiltreSearch(option.getSearch());
    }

    public ListFilterCorps addFiltreSearch(String search) {
        if (Tools.testerNulliteString(search)) {
            this.search = StringUtils.stripAccents(search).toLowerCase().trim();
            if (!CollectionUtils.isEmpty(all)) {
                filtrerEtCompter();
            }
        }
        return this;
    }

    public ListFilterCorps addFiltreGroupe(List<? extends WithIdUtilisateur> withIdUtilisateurs) {
        if (!CollectionUtils.isEmpty(withIdUtilisateurs)) {
            filtreGroupe = new ArrayList<>(withIdUtilisateurs);
            idsUtilisateurFiltre = filtreGroupe.stream().map(WithIdUtilisateur::getIdUtilisateur).collect(Collectors.toSet());

            if (!CollectionUtils.isEmpty(all)) {
                filtrerEtCompter();
            }
        }
        return this;
    }

    public ListFilterCorps add(List<? extends WithCorpsDto> withCorpsDtoList) {
        if (!CollectionUtils.isEmpty(withCorpsDtoList)) {
            all = new ArrayList<>(withCorpsDtoList);
            filtrerEtCompter();
        }
        return this;
    }

    private void filtrerEtCompter() {
        all.forEach(withCorpsDto -> {
            if (withCorpsDto.getCorpsDto() != null) {
                compter(withCorpsDto);
                filtrer(withCorpsDto);
            }
        });
    }

    private void compter(WithCorpsDto withCorpsDto) {
        corps.forEach(corpsCount -> corpsCount.compter(withCorpsDto));
    }

    private void filtrer(WithCorpsDto withCorpsDto) {
        if (filtrerCorps(withCorpsDto) && filtrerUtilisateur(withCorpsDto) && filtrerSearch(withCorpsDto)) {
            filtrer.add(withCorpsDto);
        }
    }

    private boolean filtrerSearch(WithCorpsDto withCorpsDto) {
        if (!Tools.testerNulliteString(search)) {
            return true;
        }
        return withCorpsDto.anyMatch(search);
    }

    private boolean filtrerCorps(WithCorpsDto withCorpsDto) {
        return idsCorpsFiltre.isEmpty() || idsCorpsFiltre.contains(withCorpsDto.getCorpsDto().getId());
    }

    private boolean filtrerUtilisateur(WithCorpsDto withCorpsDto) {
        return idsUtilisateurFiltre.isEmpty() || idsUtilisateurFiltre.contains(withCorpsDto.getIdUtilisateur());
    }

    @JsonProperty("fullsize")
    public int fullSize() {
        return all.size();
    }
}
