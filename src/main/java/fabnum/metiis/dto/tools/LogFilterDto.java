package fabnum.metiis.dto.tools;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.slf4j.event.Level;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Map;

/**
 * Dto permettant de filtrer les résultats des logs.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogFilterDto implements Serializable {

    private static final long serialVersionUID = -2727076052277562352L;

    /* Nom de la variable correspondant au niveau de log dans un fichier. */
    public final static String LEVEL_NAME = "level";

    /* Nom de la variable correspondant à la date du log dans un fichier. */
    public final static String TIMESTAMP_NAME = "timestamp";

    /* Format de la date du log dans un fichier. */
    public final static String TIMESTAMP_PATTERN = "yyyy-MM-dd' 'HH:mm:ss,SSS";

    /* Niveau du log recherché. */
    private Level level;

    /* Date de début de la plage de recherchée.  */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startTime;

    /* Date de fin de la plage de recherchée.  */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime endTime;

    /* Triage décroissant des élément retournés.  */
    @Builder.Default
    private boolean descending = true;

    /* Récupération des archives plutôt que le fichier en cours pour la recherche.  */
    @Builder.Default
    private boolean archives = false;

    /**
     * Comparateur permettant de trier les logs retournés via leur rubrique LogFilterDto.TIMESTAMP_NAME
     */
    public static class LogMapComparator implements Comparator<Map<String, Object>>, Serializable {

        private static final long serialVersionUID = 7397602450079107155L;
        /**
         * Sens de comparaison.
         */
        private boolean ascending;

        public LogMapComparator() {
            this(true);
        }

        /**
         * @param pAscending {@link Boolean} : sens de comparaison (fait une inversion complète).
         */
        public LogMapComparator(boolean pAscending) {
            ascending = pAscending;
        }

        @Override
        public int compare(Map<String, Object> arg0, Map<String, Object> arg1) {
            return arg0.get(TIMESTAMP_NAME).toString().compareTo(arg1.get(TIMESTAMP_NAME).toString()) * (ascending ? 1 : -1);
        }
    }
}
