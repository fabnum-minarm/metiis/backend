package fabnum.metiis.dto.tools;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
public class Taux implements Serializable {

    private static final long serialVersionUID = -8278111017490262698L;
    
    @Getter
    private Long total = 0L;

    @Setter
    @Getter
    private Long nombre = 0L;

    public Taux(Long total) {
        this.total = total;
    }

    @JsonProperty("pourcentage")
    public Double pourcentage() {
        if (total.equals(0L)) {
            return 0D;
        }
        return (double) nombre * 100 / (double) total;
    }

    public void addTotal(long nombreTotal) {
        total += nombreTotal;
    }

    public void add(long nombre) {
        this.nombre += nombre;
    }

}
