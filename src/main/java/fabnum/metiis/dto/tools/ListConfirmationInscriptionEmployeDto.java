package fabnum.metiis.dto.tools;

import fabnum.metiis.config.Tools;
import fabnum.metiis.dto.rh.ConfirmationInscriptionEmployeDto;
import lombok.Getter;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ListConfirmationInscriptionEmployeDto {

    private List<ConfirmationInscriptionEmployeDto> envoyees = new ArrayList<>();
    private List<ConfirmationInscriptionEmployeDto> expirees = new ArrayList<>();


    public ListConfirmationInscriptionEmployeDto(List<ConfirmationInscriptionEmployeDto> all) {
        if (!CollectionUtils.isEmpty(all)) {
            init(all);
        }
    }

    private void init(List<ConfirmationInscriptionEmployeDto> all) {
        all.forEach(confirmationInscriptionEmployeDto -> {
            if (Tools.hasExpired(confirmationInscriptionEmployeDto.getPrecription())) {
                expirees.add(confirmationInscriptionEmployeDto);
            } else {
                envoyees.add(confirmationInscriptionEmployeDto);
            }
        });
    }
}
