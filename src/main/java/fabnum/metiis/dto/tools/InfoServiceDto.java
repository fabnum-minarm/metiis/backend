package fabnum.metiis.dto.tools;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * Dto des informations retournées par un service ou un controller.
 */
@Getter
@JsonPropertyOrder({"timestamp", "status", "message"})
public final class InfoServiceDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Date de l'erreur.
     */
    public final LocalDateTime timestamp = LocalDateTime.now();

    /**
     * Status de la réponse causé par l'erreur.
     */
    @JsonIgnore
    public HttpStatus status = HttpStatus.NOT_FOUND;

    /**
     * Code du status.
     */
    @JsonProperty("status")
    public int statusCode = HttpStatus.NOT_FOUND.value();

    /**
     * Message.
     */
    @Setter
    public String message;

    /**
     * Détails.
     */
    @Setter
    public String details;

    /**
     * Permet de modifier la valeur du status et de réindexer le code.
     *
     * @param pStatus {@link HttpStatus}
     */
    public void setStatus(@NonNull HttpStatus pStatus) {
        status = pStatus;
        statusCode = status.value();
    }

    /**
     * Permet d'accéder en static à un message d'information.
     *
     * @param pMessage {@link String}
     * @return {@link InfoServiceDto}
     */
    public static InfoServiceDto ok(String pMessage) {

        return ok(pMessage, pMessage);
    }

    /**
     * Permet d'accéder en static à un message d'information.
     *
     * @param pMessage {@link String}
     * @param pDetails {@link String}
     * @return {@link InfoServiceDto}
     */
    public static InfoServiceDto ok(String pMessage, String pDetails) {
        InfoServiceDto dto = new InfoServiceDto();
        dto.setStatus(HttpStatus.OK);
        dto.setMessage(pMessage);
        dto.setDetails(pDetails);

        return dto;
    }

    /**
     * Permet d'accéder en static à un message d'erreur'.
     *
     * @param pStatut  {@link HttpStatus}
     * @param pMessage {@link String}
     * @return {@link InfoServiceDto}
     */
    public static InfoServiceDto ko(HttpStatus pStatut, String pMessage) {
        return ko(pStatut, pMessage, pMessage);
    }

    /**
     * Permet d'accéder en static à un message d'erreur'.
     *
     * @param pStatut  {@link HttpStatus}
     * @param pMessage {@link String}
     * @param pDetails {@link String}
     * @return {@link InfoServiceDto}
     */
    public static InfoServiceDto ko(HttpStatus pStatut, String pMessage, String pDetails) {
        InfoServiceDto dto = new InfoServiceDto();
        dto.setStatus(pStatut);
        dto.setMessage(pMessage);
        dto.setDetails(pDetails);

        return dto;
    }
}