package fabnum.metiis.dto.tools;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class AuthoritiesJsonDeserializer extends JsonDeserializer<Collection<GrantedAuthority>> {

    private final static String NODE_NAME = "authority";

    @Override
    public Collection<GrantedAuthority> deserialize(JsonParser parser, DeserializationContext ctx)
			throws IOException, JsonProcessingException {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        JsonNode root = parser.getCodec().readTree(parser);

        root.forEach(n -> {
            if(n.has(NODE_NAME)){
                authorities.add(new SimpleGrantedAuthority(n.get(NODE_NAME).asText()));
            }
        });
		return authorities;
	}
}