package fabnum.metiis.dto.rh;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GradeDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = ContextEnum.CREATE_GRADE, message = "grade.missing.code")
    private String code;

    @ValidatorNotNull(contexte = ContextEnum.CREATE_GRADE, message = "grade.missing.libelle")
    private String libelle;

    @ValidatorNotNull(contexte = ContextEnum.CREATE_GRADE, message = "grade.missing.ordre")
    private Long ordre;

    @ValidatorChild(contexte = ContextEnum.CREATE_GRADE)
    private CorpsDto corps;

    public GradeDto(Long id, String code, String libelle, Long ordre,
                    Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps
    ) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.ordre = ordre;
        this.corps = new CorpsDto(idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);
    }
}
