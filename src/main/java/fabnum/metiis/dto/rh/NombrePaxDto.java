package fabnum.metiis.dto.rh;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class NombrePaxDto {

    public List<Long> nombrePax = new ArrayList<>();

    public NombrePaxDto(List<Long> nombrePax) {
        this.nombrePax = nombrePax;
    }

}
