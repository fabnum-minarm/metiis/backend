package fabnum.metiis.dto.rh;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class OrganismePartageDto extends OrganismeDto {

    private String commentaire;

}
