package fabnum.metiis.dto.rh;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CorpsDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_CORPS, ContextEnum.UPDATE_CORPS}, message = "corps.missing.letter")
    protected String lettre;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_CORPS, ContextEnum.UPDATE_CORPS}, message = "corps.missing.code")
    protected String code;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_CORPS, ContextEnum.UPDATE_CORPS}, message = "corps.missing.libelle")
    protected String libelle;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_CORPS, ContextEnum.UPDATE_CORPS}, message = "corps.missing.ordre")
    protected Long ordre;

    public CorpsDto(Long id) {
        super(id);
    }

    public CorpsDto(Long id, String lettre, String code, String libelle, Long ordre) {
        super(id);
        this.lettre = lettre;
        this.code = code;
        this.libelle = libelle;
        this.ordre = ordre;
    }

    @ValidatorNotNull(contexte = ContextEnum.CREATE_EFFECTIF,
            message = "effectif.missing.corps")
    @ValidatorNotNull(contexte = {ContextEnum.CREATE_GRADE, ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR},
            message = "grade.missing.corps")
    @Override
    public Long getId() {
        return super.getId();
    }
}
