package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface WithIdUtilisateur {

    @JsonIgnore
    Long getIdUtilisateur();
}
