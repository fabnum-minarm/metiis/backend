package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.ToolsRepository;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class OrganismeDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ORGANISME, ContextEnum.UPDATE_ORGANISME}, message = "organisme.missing.code")
    private String code;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ORGANISME, ContextEnum.UPDATE_ORGANISME}, message = "organisme.missing.libelle")
    private String libelle;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ORGANISME, ContextEnum.UPDATE_ORGANISME}, message = "organisme.missing.credo")
    private String credo;

    private Long idTypeOrganisme;
    private String libelleTypeOrganisme;
    private Boolean typeOrganismePourAffectation;
    private OrganismeDto organismeParent;

    private Long idparent;
    private String codeParent;
    private String libelleParent;

    @ApiModelProperty(hidden = true)
    private List<UtilisateurCompletDto> contacts;

    public OrganismeDto(Long id) {
        super(id);
    }

    public OrganismeDto(Long id, String code, String libelle, String codeParent, String libelleParent) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.codeParent = codeParent;
        this.libelleParent = libelleParent;
    }

    public OrganismeDto(Long id, String code, String libelle, String credo,
                        Long idTypeOrganisme, String libelleTypeOrganisme, Boolean pourAffectation,
                        Long idparent, String codeParent, String libelleParent) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.credo = credo;
        this.idTypeOrganisme = idTypeOrganisme;
        this.libelleTypeOrganisme = libelleTypeOrganisme;
        this.typeOrganismePourAffectation = pourAffectation;
        this.idparent = idparent;
        this.codeParent = codeParent;
        this.libelleParent = libelleParent;
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public OrganismeDto(Object[] objects) {
        int i = -1;
        id = ToolsRepository.sqlLong(objects, ++i);
        code = ToolsRepository.sqlString(objects, ++i);
        libelle = ToolsRepository.sqlString(objects, ++i);
        credo = ToolsRepository.sqlString(objects, ++i);
        idTypeOrganisme = ToolsRepository.sqlLong(objects, ++i);
        libelleTypeOrganisme = ToolsRepository.sqlString(objects, ++i);
        idparent = ToolsRepository.sqlCountToLong(objects, ++i);
        codeParent = ToolsRepository.sqlString(objects, ++i);
        libelleParent = ToolsRepository.sqlString(objects, ++i);
    }

    public OrganismeDto(Long id, String code, String libelle, String credo,
                        Long idparent, String codeParent, String libelleParent) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.credo = credo;
        this.idparent = idparent;
        this.codeParent = codeParent;
        this.libelleParent = libelleParent;
    }


    public OrganismeDto(Long id, String code, String libelle) {
        super(id);
        this.code = code;
        this.libelle = libelle;
    }

    @ValidatorNotNull(contexte = {
            ContextEnum.CREATE_ACTIVITE, ContextEnum.UPDATE_ACTIVITE,
            ContextEnum.UPDATE_UTILISATEUR, ContextEnum.UPDATE_AFFECTATION,
            ContextEnum.INSCRIPTION_TOKEN
    },
            message = "activite.missing.organisme")
    @Override
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public String getFullNameWithParent(String separator) {
        String fullName = getCode();
        if (getIdparent() != null) {
            fullName = codeParent + separator + fullName;
            if (organismeParent.getIdparent() != null) {
                fullName = organismeParent.getCodeParent() + separator + fullName;
            }
        }
        return fullName;
    }
}
