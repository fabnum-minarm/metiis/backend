package fabnum.metiis.dto.rh;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class TypeOrganismeDtoWithOrganisme extends TypeOrganismeDto {


    private List<OrganismeDto> organismes = new ArrayList<>();

    public TypeOrganismeDtoWithOrganisme(TypeOrganismeDto typeOrganismeDto) {
        super(typeOrganismeDto.getId(), typeOrganismeDto.code, typeOrganismeDto.libelle,
                typeOrganismeDto.ordre, typeOrganismeDto.pourAffectation, typeOrganismeDto.idTypeOrganismeParent);
    }

}
