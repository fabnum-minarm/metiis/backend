package fabnum.metiis.dto.rh;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.administration.ProfilDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InscriptionByEmployeurDto {
    @ValidatorNotNull(contexte = ContextEnum.INSCRIPTION_TOKEN, message = "invitation.emails.missing")
    private String emails;

    @ValidatorNotNull(contexte = ContextEnum.INSCRIPTION_TOKEN, message = "invitation.profils.missing")
    private List<ProfilDto> profils;

    @ValidatorChild(contexte = ContextEnum.INSCRIPTION_TOKEN)
    private OrganismeDto organismeAffectation;

    private PosteRoDto posteRo;

}
