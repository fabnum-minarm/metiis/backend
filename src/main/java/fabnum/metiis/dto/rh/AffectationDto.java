package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class AffectationDto extends AbstractLongDto {

    @ValidatorChild(contexte = {ContextEnum.UPDATE_AFFECTATION})
    private OrganismeDto organisme;

    private PosteRoDto posteRo;

    @JsonIgnore
    private Long idUtilistateur;

    private Instant dateAffectation;

    @ValidatorNotNull(contexte = {ContextEnum.CLOTURER_AFFECTATION}, message = "affectation.cloturer.missing")
    private Instant dateFinAffectation;


    public AffectationDto(Long id, Long idUtilistateur, Instant dateAffectation, Instant dateFinAffectation,
                          Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo,
                          Long idOrganisme, String codeOrganisme, String libelleOrganisme, String credoOrganisme,
                          Long idParent, String codeParentOrganisme, String libelleParentOrganisme) {
        super(id);
        this.idUtilistateur = idUtilistateur;
        this.dateAffectation = dateAffectation;
        this.dateFinAffectation = dateFinAffectation;

        organisme = new OrganismeDto(idOrganisme, codeOrganisme, libelleOrganisme, credoOrganisme,
                idParent, codeParentOrganisme, libelleParentOrganisme);
        posteRo = new PosteRoDto(idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo);
    }

    public AffectationDto(Long id, Long idUtilistateur, Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo) {
        super(id);
        this.idUtilistateur = idUtilistateur;
        posteRo = new PosteRoDto(idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo);
    }

    public AffectationDto() {
        this.organisme = new OrganismeDto();
    }


    @ValidatorNotNull(contexte = {ContextEnum.UPDATE_UTILISATEUR, ContextEnum.UPDATE_AFFECTATION}, message = "affectation.missing")
    @Override
    public Long getId() {
        return super.getId();
    }

}
