package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.dto.administration.UserProfilDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Getter
@Setter
@NoArgsConstructor
public class UtilisateurCompletDto extends UtilisateurDto implements IPasswords {


    private static final long serialVersionUID = 5277042886968807793L;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR},
            message = "utilisateur.missing.password")
    private String password;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR},
            message = "utilisateur.missing.confirmation")
    private String passwordconfirmation;

    private String newEmail;
    private String newTelephone;

    private List<UserProfilDto> userProfils = new ArrayList<>();

    private ProfilDto profilCourant;

    @ValidatorChild(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR, ContextEnum.PATCH_UTILISATEUR, ContextEnum.CLOTURER_AFFECTATION})
    private AffectationDto affectation;

    @JsonIgnore
    private Long idUser;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR, ContextEnum.VALIDER, ContextEnum.CLOTURER_AFFECTATION},
            message = "user.enabled")
    private Boolean enabled;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR, ContextEnum.DEBLOQUER},
            message = "user.enabled")
    private Boolean accountNonLocked;

    private List<GroupePersonnaliserDto> groupes = new ArrayList<>();


    public UtilisateurCompletDto(Long id, String nom, String prenom, String email, String telephone, String newEmail, String newTelephone,
                                 Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
                                 Boolean enabled, Boolean accountNonLocked, String username,
                                 Long idAffectation, Instant dateAffectation, Instant dateFinAffectation,
                                 Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo,
                                 Long idOrganismeDto, String codeOrganismeDto, String libelleOrganismeDto, String credoOrganisme,
                                 Long idParent, String codeParentOrganismeDto, String libelleParentOrganismeDto,
                                 Long idProfil, String codeProfil, String descriptionProfil, Boolean isDefault) {
        super(id, nom, prenom);
        this.email = email;
        this.telephone = telephone;
        this.newEmail = newEmail;
        this.newTelephone = newTelephone;

        corps = new CorpsDto(idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);

        affectation = new AffectationDto(idAffectation, id, dateAffectation, dateFinAffectation,
                idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo,
                idOrganismeDto, codeOrganismeDto, libelleOrganismeDto, credoOrganisme,
                idParent, codeParentOrganismeDto, libelleParentOrganismeDto);

        profilCourant = new ProfilDto(idProfil, codeProfil, descriptionProfil, isDefault);

        this.username = username;
        this.enabled = enabled;
        this.accountNonLocked = accountNonLocked;
    }

    public UtilisateurCompletDto(Long id, String nom, String prenom, String email, String telephone,
                                 Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
                                 Boolean enabled, Boolean accountNonLocked, String username) {
        super(id, nom, prenom);
        this.email = email;
        this.telephone = telephone;

        corps = new CorpsDto(idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);

        this.username = username;
        this.enabled = enabled;
        this.accountNonLocked = accountNonLocked;
        this.affectation = new AffectationDto();
    }

    public UtilisateurCompletDto(Long id, String nom, String prenom,
                                 Long idCorps, String codeCorps, Long ordreCorps,
                                 Long idAffectation,
                                 Long idPosteRo, String codePosteRo, Long ordrePosteRo) {

        super(id, nom, prenom, idCorps, codeCorps, ordreCorps);
        affectation = new AffectationDto(idAffectation, id, idPosteRo, codePosteRo, null, ordrePosteRo);
    }

    public UtilisateurCompletDto(Long id, String nom, String prenom, String email, String telephone,
                                 Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCoprs,
                                 Long idAffectation,
                                 Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo) {
        super(id, nom, prenom);
        this.email = email;
        this.telephone = telephone;

        corps = new CorpsDto(idCorps, lettreCorps, codeCorps, libelleCorps, ordreCoprs);

        affectation = new AffectationDto(idAffectation, id,
                idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo);
    }

    @ValidatorNotNull(contexte = {ContextEnum.VALIDER, ContextEnum.DEBLOQUER, ContextEnum.PATCH_UTILISATEUR}, message = "utilisateur.missing")
    @Override
    public Long getId() {
        return super.getId();
    }


    @JsonProperty("codePosteRo")
    public String getCodePosteRo() {
        if (affectation != null && affectation.getPosteRo() != null && affectation.getPosteRo().getCode() != null) {
            return affectation.getPosteRo().getCode();
        }
        return "";

    }

    public void addGroupePersonnaliser(List<GroupePersonnaliserDto> groupePersonnalisers) {
        if (!CollectionUtils.isEmpty(groupePersonnalisers)) {
            groupes = groupePersonnalisers
                    .stream()
                    .filter(groupePersonnaliserDto -> groupePersonnaliserDto.containsUtilisteur(id))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public boolean anyMatch(String search) {
        if (super.anyMatch(search)) {
            return true;
        }
        String codePoste = getCodePosteRo();
        if (Tools.testerNulliteString(codePoste)) {
            if (Tools.search(codePoste + " " + nom + " " + prenom, search)) {
                return true;
            }
            return Tools.search(codePoste + " " + prenom + " " + nom, search);
        }
        return false;
    }
}
