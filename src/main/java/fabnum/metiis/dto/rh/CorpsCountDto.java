package fabnum.metiis.dto.rh;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CorpsCountDto extends CorpsDto {

    private Long count;

    public CorpsCountDto(CorpsDto corpsDto) {
        super(corpsDto.getId(), corpsDto.lettre, corpsDto.code, corpsDto.libelle, corpsDto.ordre);
        count = 0L;
    }

    public void compter(WithCorpsDto withCorpsDto) {
        if (equals(withCorpsDto.getCorpsDto())) {
            ++count;
        }
    }

}
