package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class UtilisateurDto extends AbstractLongDto implements WithCorpsDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR, ContextEnum.UPDATE_INFOS_PERSOS},
            message = "utilisateur.missing.nom")
    protected String nom;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR, ContextEnum.UPDATE_INFOS_PERSOS},
            message = "utilisateur.missing.prenom")
    protected String prenom;

    @ValidatorChild(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR})
    protected CorpsDto corps;

    protected String username;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.UPDATE_UTILISATEUR, ContextEnum.UPDATE_INFOS_PERSOS},
            message = "utilisateur.missing.email")
    protected String email;
    protected String telephone;

    @JsonProperty("initial")
    public String getInitial() {
        return Tools.getFirstLetterMajuscule(nom) + Tools.getFirstLetterMajuscule(prenom);
    }

    public UtilisateurDto(Long id, String nom, String prenom,
                          Long idCorps, String codeCorps, Long ordreCoprs
    ) {
        super(id);
        this.nom = nom;
        this.prenom = prenom;
        corps = new CorpsDto(idCorps, null, codeCorps, null, ordreCoprs);
    }

    public UtilisateurDto(Long id, String nom, String prenom,
                          Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCoprs
    ) {
        super(id);
        this.nom = nom;
        this.prenom = prenom;
        corps = new CorpsDto(idCorps, lettreCorps, codeCorps, libelleCorps, ordreCoprs);
    }

    public UtilisateurDto(Long id, String nom, String prenom, String email, String telephone,
                          Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCoprs
    ) {
        this(id, nom, prenom,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCoprs);
        this.telephone = telephone;
        this.email = email;

    }


    public UtilisateurDto(Long id) {
        super(id);
        corps = new CorpsDto();
    }

    public UtilisateurDto(Long id, String nom, String prenom) {
        super(id);
        this.nom = nom;
        this.prenom = prenom;
    }


    public UtilisateurDto(UtilisateurCompletDto utilisateurCompletDto) {
        id = utilisateurCompletDto.id;
        nom = utilisateurCompletDto.nom;
        prenom = utilisateurCompletDto.prenom;
        corps = utilisateurCompletDto.corps;
        email = utilisateurCompletDto.email;
        telephone = utilisateurCompletDto.telephone;
        username = utilisateurCompletDto.username;
    }


    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ALERTE}, message = "alerte.missing.destinataire")
    @ValidatorNotNull(contexte = {ContextEnum.CREATE_GROUPE}, message = "utilisateur.missing")
    @ValidatorNotNull(contexte = {ContextEnum.CREATE_GROUPE_DETAILS}, message = "utilisateur.missing")
    @ValidatorNotNull(contexte = {ContextEnum.CHOISIR}, message = "activite.missing.validateur")
    @ValidatorNotNull(contexte = {ContextEnum.CHOISIR_PERSONNE}, message = "activite.missing.choix.utilisateur")
    @ValidatorNotNull(contexte = {ContextEnum.CREATE_EVALUATION}, message = "evaluer.missing.utilisateur")
    @Override
    public Long getId() {
        return super.getId();
    }


    @Override
    public Long getIdUtilisateur() {
        return id;
    }

    @Override
    public CorpsDto getCorpsDto() {
        return corps;
    }

    @Override
    public boolean anyMatch(String search) {
        if (Tools.search(prenom + " " + nom, search)) {
            return true;
        }
        return Tools.search(nom + " " + prenom, search);
    }
}
