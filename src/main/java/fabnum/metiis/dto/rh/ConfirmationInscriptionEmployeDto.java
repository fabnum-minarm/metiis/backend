package fabnum.metiis.dto.rh;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
public class ConfirmationInscriptionEmployeDto extends ChangementMotDePasseDto {

    private List<OrganismeDto> organismes;

    private OrganismeDto organisme;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR},
            message = "utilisateur.missing.nom")
    protected String nom;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR},
            message = "utilisateur.missing.prenom")
    protected String prenom;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR},
            message = "utilisateur.missing.telephone")
    private String telephone;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR},
            message = "utilisateur.missing.accord")
    private Boolean accordInscription;

    @ValidatorChild(contexte = {ContextEnum.CREATE_UTILISATEUR})
    private CorpsDto corps;

    private UtilisateurDto createur;


    /*"   " tu.token , tu.prescription , " +
            " ut.email , o.id , " +
            " o.id ,  o.code , o.libelle , o.credo ," +
            " op.id , op.code , op.libelle ," +
            " utc.id , utc.nom , utc.prenom " + +*/
    public ConfirmationInscriptionEmployeDto(String token, Instant precription,
                                             String email,
                                             Long idOrganisme, String codeOrganisme, String libelleOrganisme, String credoOrganisme,
                                             Long idParent, String codeParentOrganisme, String libelleParentOrganisme,
                                             Long idUtilisateurCreateur, String nomCreateur, String prenomCreateur) {
        super(token, email, precription);
        organisme = new OrganismeDto(idOrganisme, codeOrganisme, libelleOrganisme, credoOrganisme,
                idParent, codeParentOrganisme, libelleParentOrganisme);
        createur = new UtilisateurDto(idUtilisateurCreateur, nomCreateur, prenomCreateur);

    }
}
