package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class ChangementMotDePasseDto implements IPasswords {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.NEW_PWD, ContextEnum.RENEW_INSCRIPTION},
            message = "token.missing")
    protected String token;

    protected String email;

    protected Instant precription;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.NEW_PWD},
            message = "utilisateur.missing.password")
    protected String password;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_UTILISATEUR, ContextEnum.NEW_PWD},
            message = "utilisateur.missing.confirmation")
    protected String passwordconfirmation;

    public ChangementMotDePasseDto(String token, String email, Instant precription) {
        this.token = token;
        this.precription = precription;
        this.email = email;
    }

    @JsonProperty("hasExpired")
    public Boolean hasExpired() {
        return Tools.hasExpired(precription);
    }
}
