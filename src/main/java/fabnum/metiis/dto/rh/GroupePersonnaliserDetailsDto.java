package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
@NoArgsConstructor
public class GroupePersonnaliserDetailsDto extends AbstractLongDto implements WithIdUtilisateur {

    @JsonIgnore
    private Long idGroupe;

    @ValidatorChild(contexte = ContextEnum.CREATE_GROUPE_DETAILS)
    private UtilisateurCompletDto utilisateur;

    //  " gpd.id ," +
    //            " gp.id , " +
    //             " ut.id ,  ut.nom , ut.prenom , ut.email , ut.telephone , " +
    //            " c.id ,  c.lettre , c.code ,  c.libelle , c.ordre , " +
    //            " u.enabled , u.accountNonLocked , u.username , " +
    //            " af.id , af.dateAffectation , " +
    //            " pr.id , pr.code , pr.libelle , pr.ordre , " +
    //            " o.id ,  o.code , o.libelle , o.credo ," +
    //            " op.id , op.code , op.libelle ," +
    //            " pf.id , pf.code , pf.description , pf.isdefault " +
    @SuppressWarnings({"UnusedDeclaration"})
    public GroupePersonnaliserDetailsDto(Long id,
                                         Long idGroupe,
                                         Long idUtilisateur, String nom, String prenom, String email, String telephone, String newEmail, String newTelephone,
                                         Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
                                         Boolean enabled, Boolean accountNonLocked, String username,
                                         Long idAffectation, Instant dateAffectation, Instant dateFinAffectation,
                                         Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo,
                                         Long idOrganismeDto, String codeOrganismeDto, String libelleOrganismeDto, String credoOrganisme,
                                         Long idParent, String codeParentOrganismeDto, String libelleParentOrganismeDto,
                                         Long idProfil, String codeProfil, String descriptionProfil, Boolean isDefault) {
        super(id);
        this.idGroupe = idGroupe;
        utilisateur = new UtilisateurCompletDto(idUtilisateur, nom, prenom, email, telephone, newEmail, newTelephone,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps,
                enabled, accountNonLocked, username,
                idAffectation, dateAffectation, dateFinAffectation,
                idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo,
                idOrganismeDto, codeOrganismeDto, libelleOrganismeDto, credoOrganisme,
                idParent, codeParentOrganismeDto, libelleParentOrganismeDto,
                idProfil, codeProfil, descriptionProfil, isDefault);
    }

    @Override
    public Long getIdUtilisateur() {
        return utilisateur.getIdUtilisateur();
    }
}
