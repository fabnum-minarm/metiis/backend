package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.config.EmailValidation;
import lombok.Getter;

import java.util.Collection;
import java.util.Set;


public class CompteRenduInvitation {
    private final Integer demandes;
    private Integer creations = 0;
    private Integer suppressions = 0;

    @Getter
    private Set<String> fauxEmails;

    public CompteRenduInvitation(EmailValidation validation) {
        demandes = validation.getValid().size();
        fauxEmails = validation.getInvalid();
    }

    public <T> CompteRenduInvitation(Collection<T> collection) {
        demandes = collection.size();
    }

    public void incrementeCree() {
        creations++;
    }

    public void incrementeSupprime() {
        suppressions++;
    }

    @JsonProperty("rejets")
    public Integer rejets() {
        return demandes() - creations();
    }

    public Integer demandes() {
        return demandes;
    }

    @JsonProperty("creations")
    public Integer creations() {
        return creations;
    }

    @JsonProperty("suppressions")
    public Integer suppressions() {
        return suppressions;
    }

}
