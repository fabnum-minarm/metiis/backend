package fabnum.metiis.dto.rh;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TypeOrganismeDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_ORGANISME, ContextEnum.UPDATE_TYPE_ORGANISME}, message = "type-organisme.missing.code")
    protected String code;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_ORGANISME, ContextEnum.UPDATE_TYPE_ORGANISME}, message = "type-organisme.missing.libelle")
    protected String libelle;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_ORGANISME, ContextEnum.UPDATE_TYPE_ORGANISME}, message = "type-organisme.missing.ordre")
    protected Long ordre;

    protected Boolean pourAffectation = false;
    protected Long idTypeOrganismeParent;

    public TypeOrganismeDto(Long id, String code, String libelle, Long ordre, Boolean pourAffectation, Long idTypeOrganismeParent) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.ordre = ordre;
        this.pourAffectation = pourAffectation;
        this.idTypeOrganismeParent = idTypeOrganismeParent;
    }

    public TypeOrganismeDto(Long id, String code, String libelle) {
        super(id);
        this.code = code;
        this.libelle = libelle;
    }

    @Override
    public Long getId() {
        return super.getId();
    }
}
