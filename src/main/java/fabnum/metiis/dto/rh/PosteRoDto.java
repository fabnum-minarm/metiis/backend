package fabnum.metiis.dto.rh;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class PosteRoDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE, ContextEnum.UPDATE}, message = "posteRo.missing.code")
    private String code;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE, ContextEnum.UPDATE}, message = "posteRo.missing.libelle")
    private String libelle;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE, ContextEnum.UPDATE}, message = "posteRo.missing.ordre")
    private Long ordre;

    public PosteRoDto(Long id, String code, String libelle, Long ordre) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.ordre = ordre;
    }

    @ValidatorNotNull(contexte = {ContextEnum.UPDATE_UTILISATEUR, ContextEnum.UPDATE_AFFECTATION, ContextEnum.PATCH_UTILISATEUR},
            message = "posteRo.missing")
    @Override
    public Long getId() {
        return super.getId();
    }

}
