package fabnum.metiis.dto.rh;

public interface IPasswords {

    String getPassword();

    String getPasswordconfirmation();
}
