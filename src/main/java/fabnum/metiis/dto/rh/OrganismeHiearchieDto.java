package fabnum.metiis.dto.rh;

import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class OrganismeHiearchieDto extends AbstractLongDto {

    private String code;

    private String libelle;

    private String credo;

    private Long idTypeOrganisme;
    private Long idparent;
    private String libelleTypeOrganisme;

    private List<OrganismeHiearchieDto> organismesEnfants;

    private Boolean isOpen = false;
    private Boolean selected = false;

    public OrganismeHiearchieDto(Long id) {
        super(id);
    }

    public OrganismeHiearchieDto(Long id, String code, String libelle, String credo,
                                 Long idTypeOrganisme, String libelleTypeOrganisme, Long idparent) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.credo = credo;
        this.idTypeOrganisme = idTypeOrganisme;
        this.libelleTypeOrganisme = libelleTypeOrganisme;
        this.idparent = idparent;
    }

    public OrganismeHiearchieDto(OrganismeDto organismeDto) {
        super(organismeDto.getId());
        this.code = organismeDto.getCode();
        this.libelle = organismeDto.getLibelle();
        this.credo = organismeDto.getCredo();
        this.idTypeOrganisme = organismeDto.getIdTypeOrganisme();
        this.libelleTypeOrganisme = organismeDto.getLibelleTypeOrganisme();
        this.idparent = organismeDto.getIdparent();
    }

}
