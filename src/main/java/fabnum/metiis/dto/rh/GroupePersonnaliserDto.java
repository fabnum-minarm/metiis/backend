package fabnum.metiis.dto.rh;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
public class GroupePersonnaliserDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = ContextEnum.CREATE_GROUPE, message = "groupe.missing.nom")
    private String nom;

    @ValidatorChild(contexte = ContextEnum.CREATE_GROUPE)
    private UtilisateurDto proprietaire;

    private List<GroupePersonnaliserDetailsDto> groupesPersonnaliserDetails = new ArrayList<>();

    @SuppressWarnings({"UnusedDeclaration"})
    public GroupePersonnaliserDto(Long id, String nom,
                                  Long idUtilisateur, String nomUtilisateur, String prenom, String email, String telephone,
                                  Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps) {
        super(id);
        this.nom = nom;
        proprietaire = new UtilisateurDto(idUtilisateur, nomUtilisateur, prenom, email, telephone,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);
    }


    public void addGroupeDetails(List<GroupePersonnaliserDetailsDto> groupesDetails) {
        groupesPersonnaliserDetails = groupesDetails
                .stream()
                .filter(groupePersonnaliserDetailsDto -> groupePersonnaliserDetailsDto.getIdGroupe().equals(id))
                .collect(Collectors.toList());
    }

    public boolean containsUtilisteur(Long idUtilisateur) {
        return groupesPersonnaliserDetails
                .stream()
                .anyMatch(groupePersonnaliserDetailsDto -> groupePersonnaliserDetailsDto.getUtilisateur().getId().equals(idUtilisateur));
    }
}
