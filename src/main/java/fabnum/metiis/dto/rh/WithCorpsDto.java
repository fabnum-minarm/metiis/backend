package fabnum.metiis.dto.rh;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface WithCorpsDto extends WithIdUtilisateur {

    @JsonIgnore
    CorpsDto getCorpsDto();

    @JsonIgnore
    boolean anyMatch(String search);

}
