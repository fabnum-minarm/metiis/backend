package fabnum.metiis.dto.contacter;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
public class ContactDto extends AbstractLongDto {

    private static final long serialVersionUID = 5791307967585281498L;
    
    @ValidatorNotNull(contexte = {ContextEnum.CONTACT}, message = "contact.nom.missing")
    private String nom;

    @ValidatorNotNull(contexte = {ContextEnum.CONTACT}, message = "contact.prenom.missing")
    private String prenom;

    @ValidatorNotNull(contexte = {ContextEnum.CONTACT}, message = "contact.email.missing")
    private String email;

    @ValidatorNotNull(contexte = {ContextEnum.CONTACT}, message = "contact.objet.missing")
    private String objet;

    @ValidatorNotNull(contexte = {ContextEnum.CONTACT}, message = "contact.message.missing")
    private String message;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateCreation;

    private Boolean lu;

    private Boolean traite;

    public ContactDto(Long id, String nom, String prenom, String email, String objet, String message, Instant dateCreation, Boolean lu, Boolean traite) {
        super(id);
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.objet = objet;
        this.message = message;
        this.dateCreation = dateCreation;
        this.lu = lu;
        this.traite = traite;
    }

}
