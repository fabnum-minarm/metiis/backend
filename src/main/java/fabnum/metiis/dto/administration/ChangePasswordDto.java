package fabnum.metiis.dto.administration;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.rh.IPasswords;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordDto implements Serializable, IPasswords {

    private static final long serialVersionUID = 1L;

    @ValidatorNotNull(contexte = {ContextEnum.NEW_PWD},
            message = "utilisateur.missing.old.password")
    private String passwordCourant;


    @ValidatorNotNull(contexte = {ContextEnum.NEW_PWD},
            message = "utilisateur.missing.password")
    private String password;

    @ValidatorNotNull(contexte = {ContextEnum.NEW_PWD},
            message = "utilisateur.missing.confirmation")
    private String passwordconfirmation;

    @Override
    public String toString() {
        return "ChangePasswordDto{" +
                "passwordCourant='" + passwordCourant + '\'' +
                ", password='" + password + '\'' +
                ", passwordconfirmation='" + passwordconfirmation + '\'' +
                '}';
    }
}