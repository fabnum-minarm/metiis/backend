package fabnum.metiis.dto.administration;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.TypeOrganismeDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Getter
@Setter
@NoArgsConstructor
public class ProfilDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_PROFIL, ContextEnum.UPDATE_PROFIL},
            message = "profil.missing.code")
    private String code;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_PROFIL, ContextEnum.UPDATE_PROFIL},
            message = "profil.missing.description")
    private String description;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_PROFIL, ContextEnum.UPDATE_PROFIL},
            message = "profil.missing.description")
    private Boolean isDefault;

    private Long ordre;

    private TypeOrganismeDto typeOrganisme;

    public Set<Roles> roles = new HashSet<>();

    public List<DroitCreationProfilDto> droitsCreationsProfilDto = new ArrayList<>();

    public ProfilDto(Long id, String code, String description, Boolean isDefault) {
        super(id);
        this.code = code;
        this.description = description;
        this.isDefault = isDefault;
    }

    public ProfilDto(Long id, String code, String description, Boolean isDefault, Long ordre,
                     Long idTypeOrganisme, String codeTypeOrganisme, String libelleTypeOrganisme) {
        super(id);
        this.code = code;
        this.description = description;
        this.isDefault = isDefault;
        this.ordre = ordre;
        this.typeOrganisme = new TypeOrganismeDto(idTypeOrganisme, codeTypeOrganisme, libelleTypeOrganisme);
    }

    public ProfilDto(Long id, String code, String description, Boolean isDefault,
                     Long idTypeOrganisme, String codeTypeOrganisme, String libelleTypeOrganisme) {
        super(id);
        this.code = code;
        this.description = description;
        this.isDefault = isDefault;
        this.typeOrganisme = new TypeOrganismeDto(idTypeOrganisme, codeTypeOrganisme, libelleTypeOrganisme);
    }

    public ProfilDto(ProfilDto profil) {
        this(profil.id, profil.code, profil.description, profil.isDefault, profil.ordre,
                profil.typeOrganisme.getId(), profil.typeOrganisme.getCode(), profil.typeOrganisme.getLibelle());
    }

    @ValidatorNotNull(contexte = {
            ContextEnum.UPDATE_UTILISATEUR,
            ContextEnum.DROIT_CREATION_PROFIL,
            ContextEnum.PATCH_UTILISATEUR
    },
            message = "profil.missing")
    @Override
    public Long getId() {
        return super.getId();
    }


    public void createAllDroit(List<ProfilDto> profils, List<DroitCreationProfilDto> droitCreationProfilDtos) {
        profils.forEach(profil -> droitsCreationsProfilDto.add(new DroitCreationProfilDto(this, profil)));
        droitCreationProfilDtos.forEach(this::addDroitCreationProfil);
    }

    private void addDroitCreationProfil(DroitCreationProfilDto droitAAjouter) {
        droitsCreationsProfilDto.forEach(
                droitCreationProfil -> droitCreationProfil.updateDroit(droitAAjouter)
        );
    }
}
