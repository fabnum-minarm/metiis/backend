package fabnum.metiis.dto.administration;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DroitCreationProfilDto extends AbstractLongDto {

    @ValidatorChild(contexte = ContextEnum.DROIT_CREATION_PROFIL)
    private ProfilDto profilCreateur;

    @ValidatorChild(contexte = ContextEnum.DROIT_CREATION_PROFIL)
    private ProfilDto profilCree;

    /* Utile pour le js. */
    private Boolean exist;

    public DroitCreationProfilDto(Long id,
                                  Long idProfilCreateur, String codeProfilCreateur, String descriptionProfilCreateur, Boolean isDefaultProfilCreateur,
                                  Long idProfilCree, String codeProfilCree, String descriptionProfilCree, Boolean isDefaultProfilCree) {
        super(id);
        profilCreateur = new ProfilDto(idProfilCreateur, codeProfilCreateur, descriptionProfilCreateur, isDefaultProfilCreateur);
        profilCree = new ProfilDto(idProfilCree, codeProfilCree, descriptionProfilCree, isDefaultProfilCree);
        exist = id != null;
    }

    public DroitCreationProfilDto(ProfilDto profilCreateur, ProfilDto profilCree) {
        this.profilCreateur = new ProfilDto(profilCreateur);
        this.profilCree = new ProfilDto(profilCree);
        exist = Boolean.FALSE;
    }

    public void updateDroit(DroitCreationProfilDto droitAAjouter) {
        if (profilCree.equals(droitAAjouter.profilCree) && profilCreateur.equals(droitAAjouter.profilCreateur)) {
            id = droitAAjouter.id;
            exist = Boolean.TRUE;
        }
    }
}

