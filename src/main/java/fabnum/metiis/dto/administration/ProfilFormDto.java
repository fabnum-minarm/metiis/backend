package fabnum.metiis.dto.administration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProfilFormDto implements Serializable {
    
    private static final long serialVersionUID = -2727076052277562352L;

    private String code;
    
    private String description;

    List<String> roles = new ArrayList<>();
}
