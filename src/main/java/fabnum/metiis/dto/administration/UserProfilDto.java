package fabnum.metiis.dto.administration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class UserProfilDto extends AbstractLongDto {

    @JsonIgnore
    private Long idUser;

    @JsonIgnore
    private Long idUtilisateur;

    @ValidatorChild(contexte = {ContextEnum.PATCH_UTILISATEUR})
    private ProfilDto profil;

    private Boolean selected;

    /*
      " up.id ,  up.selected , " +
              " u.id , " +
              " ut.id , " +
              " p.id , p.code , p.libelle , p.isdefault" +
    */
    @SuppressWarnings({"UnusedDeclaration"})
    public UserProfilDto(Long id, Boolean selected,
                         Long idUtilisateur,
                         Long idUser,
                         Long idProfil, String codeProfil, String descriptionProfil, Boolean isDefault,
                         Long idTypeOrganisme, String codeTypeOrganisme, String libelleTypeOrganisme) {
        super(id);
        this.idUser = idUser;
        this.idUtilisateur = idUtilisateur;
        this.selected = selected;

        profil = new ProfilDto(idProfil, codeProfil, descriptionProfil, isDefault,
                idTypeOrganisme, codeTypeOrganisme, libelleTypeOrganisme);
    }

    @Override
    public Long getId() {
        return super.getId();
    }

}
