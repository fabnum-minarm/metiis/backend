package fabnum.metiis.dto.administration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserCompteActifDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;

    private String nom;

    private String prenom;

    private String email;

    private Instant dateActif;

}