package fabnum.metiis.dto.sondage;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class ReponseSondageDto extends AbstractLongDto {

    private UtilisateurCompletDto utilisateur;

    private String reponse;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant createdDate;

    @SuppressWarnings({"UnusedDeclaration"})
    public ReponseSondageDto(Long idUtilisateur, String nom, String prenom, String email, String telephone, String newEmail, String newTelephone,
                             Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
                             Boolean enabled, Boolean accountNonLocked, String username,
                             Long idAffectation, Instant dateAffectation, Instant dateFinAffectation,
                             Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo,
                             Long idOrganismeDto, String codeOrganismeDto, String libelleOrganismeDto, String credoOrganisme,
                             Long idParent, String codeParentOrganismeDto, String libelleParentOrganismeDto,
                             Long idProfil, String codeProfil, String descriptionProfil, Boolean isDefault,
                             String reponse, Instant createdDate) {
        this.utilisateur = new UtilisateurCompletDto(idUtilisateur, nom, prenom, email, telephone, newEmail, newTelephone,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps,
                enabled, accountNonLocked, username,
                idAffectation, dateAffectation, dateFinAffectation,
                idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo,
                idOrganismeDto, codeOrganismeDto, libelleOrganismeDto, credoOrganisme,
                idParent, codeParentOrganismeDto, libelleParentOrganismeDto,
                idProfil, codeProfil, descriptionProfil, isDefault);
        this.reponse = reponse;
        this.createdDate = createdDate;
    }
}
