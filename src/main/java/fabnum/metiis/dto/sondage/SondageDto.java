package fabnum.metiis.dto.sondage;

import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class SondageDto extends AbstractLongDto {

    private String question;

    private String reponse;

    @SuppressWarnings({"UnusedDeclaration"})
    public SondageDto(Long id, String question) {
        super(id);
        this.question = question;

    }
}
