package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.dto.rh.WithCorpsDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class UtilisateurActiviteDetailsDto implements WithCorpsDto {

    @JsonIgnore
    private Long idActivite;

    @ValidatorChild(contexte = ContextEnum.CHOISIR_PERSONNE)
    private UtilisateurCompletDto utilisateur;

    private StatutDto statut;

    private ChoixDto choix;

    private String commentaire;

    private UtilisateurCompletDto redacteur;

    private Instant dateValidation;

    private List<ActiviteDto> autresActivites;

    @ApiModelProperty(hidden = true)
    private List<UtilisateurActiviteDetailsDto> commentaires;


    /*     " ut.id ,  ut.nom , ut.prenom , ut.email , ut.telephone , " +
            " c.id ,  c.lettre , c.code ,  c.libelle , c.ordre , " +
            " ap.commentaire , ap.dateValidation ,  " +
            " s.id , s.code , s.nom , s.nomSingulier , s.action , " +
            " sd.id , sd.code , sd.disponibilite , " +
            " a.id , " +
            " af.id , " +
             " pr.id , pr.code , pr.libelle , pr.ordre , " +
            " uv.id ,  uv.nom , uv.prenom , " +
            " cuv.id ,  cuv.lettre , cuv.code ,  cuv.libelle , cuv.ordre  "
              " afv.id , " +
            " prv.id , prv.code , prv.libelle , prv.ordre , " +*/
    @SuppressWarnings({"UnusedDeclaration"})
    public UtilisateurActiviteDetailsDto(Long idUtilisateur, String nom, String prenom, String email, String telephone,
                                         Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
                                         String commentaire, Instant dateValidation,
                                         Long idChoix, String codeChoix, String nomChoix, String lettreChoix, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
                                         Long idStatut, String codeStatut, String nomStatut, String nomSingulierStatut, String actionStatut,
                                         Long idStatutDisponibilite, String codeStatutDisponibilite, Boolean disponibilite,
                                         Long idActivite,
                                         Long idAffectation,
                                         Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo,
                                         Long idUtilisateurValidateur, String nomValidateur, String prenomValidateur,
                                         Long idCorpsValidateur, String lettreCorpsValidateur, String codeCorpsValidateur, String libelleCorpsValidateur, Long ordreCorpsValidateur,
                                         Long idAffectationValidateur,
                                         Long idPosteRoValidateur, String codePosteRoValidateur, String libellePosteRoValidateur, Long ordrePosteRoValidateur
    ) {
        this.idActivite = idActivite;
        utilisateur = new UtilisateurCompletDto(idUtilisateur, nom, prenom, email, telephone,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps,
                idAffectation,
                idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo);

        this.commentaire = commentaire;
        this.dateValidation = dateValidation;

        choix = new ChoixDto(idChoix, codeChoix, nomChoix, lettreChoix, actionPrincipale, actionSuppression, canActiviteWithOtherActivite);
        statut = new StatutDto(idStatut, codeStatut, nomStatut, nomSingulierStatut, actionStatut, idStatutDisponibilite, codeStatutDisponibilite, disponibilite);

        if (idUtilisateurValidateur != null) {
            redacteur = new UtilisateurCompletDto(idUtilisateurValidateur, nomValidateur, prenomValidateur, null, null,
                    idCorpsValidateur, lettreCorpsValidateur, codeCorpsValidateur, libelleCorpsValidateur, ordreCorpsValidateur,
                    idAffectationValidateur,
                    idPosteRoValidateur, codePosteRoValidateur, libellePosteRoValidateur, ordrePosteRoValidateur);
        }

    }

    public UtilisateurActiviteDetailsDto(UtilisateurCompletDto utilisateurCompletDto) {
        utilisateur = utilisateurCompletDto;
    }


    @Override
    public Long getIdUtilisateur() {
        Long idUtilisateur = null;
        if (utilisateur != null) {
            idUtilisateur = utilisateur.getIdUtilisateur();
        }
        return idUtilisateur;
    }

    @Override
    @JsonIgnore
    public CorpsDto getCorpsDto() {
        CorpsDto corpsDto = null;
        if (utilisateur != null) {
            corpsDto = utilisateur.getCorpsDto();
        }
        return corpsDto;
    }

    @Override
    public boolean anyMatch(String search) {
        if (utilisateur != null) {
            return utilisateur.anyMatch(search);
        }
        return false;
    }


}
