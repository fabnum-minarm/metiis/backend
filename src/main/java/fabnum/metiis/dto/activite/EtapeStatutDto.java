package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class EtapeStatutDto extends AbstractLongDto implements Comparable<EtapeStatutDto> {

    @JsonIgnore
    private Long idEtape;

    private StatutDto statut;

    private Long ordreDansGroupe;

    private List<ChoixEtapeStatutDto> choixEtapeStatuts;

    private ListFilterCorps participants;

    /*
       " sgs.id , sgs.ordreDansGroupe,    " +
            " gs.id , " +
            " s.id , s.code , s.nom " +
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public EtapeStatutDto(Long id, Long ordreDansGroupe,
                          Long idEtape,
                          Long idStatut, String codeStatut, String nomStatut, String nomSingulierStatut, String actionStatut,
                          Long idStatutDisponibilite, String codeStatutDisponibilite, Boolean disponibilite) {
        super(id);
        this.ordreDansGroupe = ordreDansGroupe;
        this.idEtape = idEtape;

        statut = new StatutDto(idStatut, codeStatut, nomStatut, nomSingulierStatut, actionStatut, idStatutDisponibilite, codeStatutDisponibilite, disponibilite);
    }

    @Override
    public int compareTo(EtapeStatutDto etapeStatutDto) {
        return ordreDansGroupe.compareTo(etapeStatutDto.ordreDansGroupe);
    }

    public void remplirChoix(List<ChoixEtapeStatutDto> listChoix) {
        choixEtapeStatuts = new ArrayList<>();
        if (listChoix != null) {
            listChoix.forEach(this::add);
        }
        Collections.sort(choixEtapeStatuts);
    }

    private void add(ChoixEtapeStatutDto choixEtapeStatutDto) {
        if (equalsEtapeStatut(choixEtapeStatutDto)) {
            choixEtapeStatuts.add(choixEtapeStatutDto);
        }
    }

    private boolean equalsEtapeStatut(ChoixEtapeStatutDto choixEtapeStatutDto) {
        return id.equals(choixEtapeStatutDto.getIdEtapeStatut());
    }

    @JsonProperty("countParticipants")
    public int countParticipants() {
        int count = 0;
        if (participants != null) {
            count = participants.fullSize();
        }
        return count;
    }

}
