package fabnum.metiis.dto.activite;

import lombok.Getter;

@Getter
public class PositionActivite {

    private Integer position = 0;
    private ActiviteSimpleDto activite;

    public PositionActivite(ActiviteSimpleDto activite) {
        this.activite = activite;
    }

    public PositionActivite(int position) {
        this.position = position;
    }

    public boolean equalsActivite(ActiviteSimpleDto activite) {
        return this.activite.getId().equals(activite.getId());
    }

    public void setPosition(int position) {
        if (this.position.equals(0)) {
            this.position = position;
        }
    }

    @Override
    public String toString() {
        return "PositionActivite{" +
                "" + position + " - activite :" + activite.getId() +
                '}';
    }
}
