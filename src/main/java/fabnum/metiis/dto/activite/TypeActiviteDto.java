package fabnum.metiis.dto.activite;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TypeActiviteDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_ACTIVITE, ContextEnum.UPDATE_TYPE_ACTIVITE},
            message = "typeActivite.missing.code")
    private String code;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_ACTIVITE, ContextEnum.UPDATE_TYPE_ACTIVITE},
            message = "typeActivite.missing.libelle")
    private String libelle;

    private String codeCss;
    private String codeCssIcone;

    private Long ordre;

    public TypeActiviteDto(Long id, String code, String libelle, String codeCss, String codeCssIcone, Long ordre) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.codeCss = codeCss;
        this.codeCssIcone = codeCssIcone;
        this.ordre = ordre;
    }

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ACTIVITE, ContextEnum.UPDATE_ACTIVITE_PARTIEL, ContextEnum.UPDATE_ACTIVITE},
            message = "activite.missing.typeactivite")
    @Override
    public Long getId() {
        return super.getId();
    }
}
