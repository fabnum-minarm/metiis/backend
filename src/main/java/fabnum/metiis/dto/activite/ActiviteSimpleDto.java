package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class ActiviteSimpleDto extends AbstractLongDto {

    private static final long serialVersionUID = 2495428595558864113L;

    private String nom;

    private String description;

    private Long idParent;

    @JsonIgnore
    private SuperDate debut;

    @JsonIgnore
    private SuperDate fin;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateDebut;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateFin;

    private TypeActiviteDto typeActivite;

    public ActiviteSimpleDto(
            Long id, String nom, String description, Instant dateDebut, Instant dateFin, Long idParent,
            Long idTypeActivite, String codeTypeActivite, String libelleTypeActivite, String codeCssTypeActivite, String codeCssIconeTypeActivite, Long ordreTypeActivite
    ) {
        super(id);
        this.nom = nom;
        this.description = description;
        debut = new SuperDate(dateDebut);
        fin = new SuperDate(dateFin);
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.idParent = idParent;
        typeActivite = new TypeActiviteDto(idTypeActivite, codeTypeActivite, libelleTypeActivite, codeCssTypeActivite, codeCssIconeTypeActivite, ordreTypeActivite);
    }


}
