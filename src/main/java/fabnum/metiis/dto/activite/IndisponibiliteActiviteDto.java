package fabnum.metiis.dto.activite;

import lombok.Getter;
import lombok.Setter;

import java.time.Instant;


@Setter
@Getter
public class IndisponibiliteActiviteDto {
    private Long idActivite;

    private Instant dateDebut;
    private Instant dateFin;

    private Long idParticiper;
    private Long idUtilisateur;

    /*  " a.id , a.dateDebut , a.dateFin " +
              " p.id , " +
              " ut.id ," +*/
    public IndisponibiliteActiviteDto(
            Long idActivite, Instant dateDebut, Instant dateFin,
            Long idParticiper, Long idUtilisateur
    ) {
        this.idActivite = idActivite;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.idParticiper = idParticiper;
        this.idUtilisateur = idUtilisateur;
    }

    /**
     * dateDebut <=  :fin " +
     * AND dateFin >= :debut ")
     *
     * @param debut Instant
     * @param fin   Instant
     * @return boolean
     */
    public boolean estEntre(Instant debut, Instant fin) {
        return (dateDebut.isBefore(fin) || dateDebut.equals(debut)) && (dateFin.equals(debut) || dateFin.isAfter(debut));
    }

    @Override
    public String toString() {
        return "IndisponibiliteActiviteDto{" +
                "idActivite=" + idActivite +
                ", dateDebut=" + dateDebut +
                ", dateFin=" + dateFin +
                ", idParticiper=" + idParticiper +
                ", idUtilisateur=" + idUtilisateur +
                '}';
    }
}
