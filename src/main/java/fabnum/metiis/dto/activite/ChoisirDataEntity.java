package fabnum.metiis.dto.activite;

import fabnum.metiis.domain.activite.*;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.alerte.ParticipationDataEntity;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class ChoisirDataEntity {

    private final Activite activite;
    private final Utilisateur validateur;
    private final Choix choix;
    private final List<ChoixEtapeActionDto> etapesActions;

    private final List<ParticipationDataEntity> dataForAlertes = new ArrayList<>();

    private ParticipationDataEntity current;

    public ChoisirDataEntity(Activite activite, Utilisateur validateur, Choix choix, List<ChoixEtapeActionDto> etapesActions) {
        this.activite = activite;
        this.validateur = validateur;
        this.choix = choix;
        this.etapesActions = etapesActions;
    }

    public void newCurrentParticipationData() {
        current = new ParticipationDataEntity();
        dataForAlertes.add(current);
    }

    public void addCurrentParticiper(Participer participer) {
        current.setParticiper(participer);
    }

    public void addCurrentAvancementParticipation(AvancementParticipation avancementParticipation) {
        current.setAvancementParticipation(avancementParticipation);
    }

    public void addCurrentAvancementsParticipationsDto(List<AvancementParticipationDto> avancementsParticipations) {
        current.setAvancementsParticipationsDto(avancementsParticipations);
    }

    public Statut getStatut() {
        return choix.getStatut();
    }
}
