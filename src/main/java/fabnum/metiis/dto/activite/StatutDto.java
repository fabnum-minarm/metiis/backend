package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class StatutDto extends AbstractLongDto {

    private static final long serialVersionUID = -4343741698932918028L;

    protected String code;

    protected String nom;

    protected String nomSingulier;

    @JsonIgnore
    protected String action;

    @JsonIgnore
    protected Boolean forVisu;

    @JsonIgnore
    protected Boolean forCompletion;

    @JsonIgnore
    protected Boolean forDetails;

    @JsonIgnore
    protected StatutDisponibiliteDto statutDisponibiliteDto;


    public StatutDto(Long id, String code, String nom, String nomSingulier, String action) {
        super(id);
        this.code = code;
        this.nom = nom;
        this.nomSingulier = nomSingulier;
        this.action = action;
    }

    public StatutDto(Long id, String code, String nomSingulier) {
        super(id);
        this.code = code;
        this.nomSingulier = nomSingulier;
    }
    /*
        " s.id , s.code , s.nom  , "+
        " sd.id , sd.code , sd.disponibilite "+
     */

    public StatutDto(Long id, String code, String nom, String nomSingulier, String action,
                     Long idStatutDisponibilite, String codeStatutDisponibilite, Boolean disponibilite) {
        this(id, code, nom, nomSingulier, action);
        if (idStatutDisponibilite != null) {
            statutDisponibiliteDto = new StatutDisponibiliteDto(idStatutDisponibilite, codeStatutDisponibilite, disponibilite);
        }
    }

    @Nullable
    public Boolean afficherDispoIndispo() {
        if (statutDisponibiliteDto != null) {
            return statutDisponibiliteDto.getDisponibilite();
        }
        return null;
    }
}
