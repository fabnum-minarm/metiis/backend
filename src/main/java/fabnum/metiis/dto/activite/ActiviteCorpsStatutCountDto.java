package fabnum.metiis.dto.activite;

import lombok.Getter;

@Getter
public class ActiviteCorpsStatutCountDto {


    private Long idActivite;

    private Long idCoprs;

    private Long idStatut;

    private Long countUtilisateur;

    /*
      " a.id , " +
              " c.id ," +
              " s.id ," +
              " count(ut.id)  "
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public ActiviteCorpsStatutCountDto(
            Long idActivite,
            Long idCoprs,
            Long idStatut,
            Long count) {

        this.idActivite = idActivite;
        this.idCoprs = idCoprs;
        this.idStatut = idStatut;
        countUtilisateur = count;
    }
}
