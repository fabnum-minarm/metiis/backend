package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.NumberTestEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.dto.tools.Taux;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EffectifDto extends AbstractLongDto {

    @JsonIgnore
    private Long idActivite;

    @ValidatorChild(contexte = ContextEnum.CREATE_EFFECTIF)
    private CorpsDto corps;

    @ValidatorNotNull(contexte = ContextEnum.CREATE_EFFECTIF, numberTest = NumberTestEnum.SUP_OU_EGALE_0,
            message = "effectif.nombre.sup.egale.zero")
    private Long nombre;

    private Taux avancement;

    @JsonIgnore
    private StatutAvancementActivite avancementActivite;

    @SuppressWarnings({"UnusedDeclaration"})
    public EffectifDto(Long id, Long nombre,
                       Long idActivite,
                       Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps) {
        super(id);
        this.idActivite = idActivite;
        corps = new CorpsDto(idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);
        this.nombre = nombre;
        avancement = new Taux(nombre);
    }

    public void setAvancementTotale(StatutAvancementActivite avancementActivite) {
        this.avancementActivite = avancementActivite;
        this.avancementActivite.addTotale(nombre);
    }

    public void ajouterAvancement(ActiviteCorpsStatutCountDto activiteCorpsStatutCountDto) {
        if (equalsCorps(activiteCorpsStatutCountDto)) {
            avancement.add(activiteCorpsStatutCountDto.getCountUtilisateur());
            avancementActivite.addUtilisateur(activiteCorpsStatutCountDto.getCountUtilisateur());
        }
    }

    private boolean equalsCorps(ActiviteCorpsStatutCountDto activiteCorpsStatutCountDto) {
        return corps.getId().equals(activiteCorpsStatutCountDto.getIdCoprs());
    }
}
