package fabnum.metiis.dto.activite;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ListActiviteDisponibiliteUtilisateurFiltreDto {
    private final SuperDate debut;
    private final SuperDate fin;
    private final Long idCorps;

    private final List<ActiviteDisponibiliteUtilisateurFiltreDto> disponibilites = new ArrayList<>();

    private final Set<Long> idsUtilisateurAvecDesDisponibilite = new TreeSet<>();

    private List<UtilisateurCompletDto> utilisateursDeLOrganisme;

    private List<Long> idsUtilisateursDisponiblesPourActivite = new ArrayList<>();
    private List<Long> idsUtilisateursNonDisponiblesPourActivite = new ArrayList<>();

    private List<UtilisateurCompletDto> participantsActiviteNonDisponible;

    public ListActiviteDisponibiliteUtilisateurFiltreDto(SuperDate debut, SuperDate fin, Long idCorps) {
        this.debut = new SuperDate(debut);
        this.fin = new SuperDate(fin);
        this.idCorps = idCorps;
        init();
    }

    private void init() {
        SuperDate start = debut.copie().debutDeLaJournee();
        while (start.estAvantOuEgale(fin)) {
            disponibilites.add(new ActiviteDisponibiliteUtilisateurFiltreDto(start.copie().debutDeLaJournee(), start.copie().finDeLaJournee()));
            start.jourSuivant();
        }
    }

    public ListActiviteDisponibiliteUtilisateurFiltreDto addDisponibilite(List<DisponibiliteDto> disponibiliteDtos) {
        disponibiliteDtos.forEach(disponibiliteDto -> {
                    ActiviteDisponibiliteUtilisateurFiltreDto dispoActivite = find(disponibiliteDto);
                    add(dispoActivite, disponibiliteDto);
                }
        );

        return this;
    }

    public List<Long> getIdsUtilisateurDispoAllDays() {
        return idsUtilisateursDisponiblesPourActivite;
    }

    public List<Long> getIdsUtilisateursNonDisponiblesPourActivite() {
        return idsUtilisateursNonDisponiblesPourActivite;
    }

    public ListActiviteDisponibiliteUtilisateurFiltreDto filtrer() {

        List<Long> idsUtilisateurDisponibleToutLesJour = calculerUtilisateurDispoAllDay();
        //System.out.println("dispo tous les jours  : " + idsUtilisateurDisponibleToutLesJour.size());


        List<Long> idsUtilisateurParticipantDejaAUneActivite = Tools.getListIdDto(participantsActiviteNonDisponible);
        //System.out.println("participant à une activité  : " + idsUtilisateurParticipantDejaAUneActivite.size());

        List<Long> idsUtilisateurOrganisme = Tools.getListIdDto(utilisateursDeLOrganisme);
        //System.out.println("unité  : " + idsUtilisateurParticipantDejaAUneActivite.size());

        calculerDisponibilite(idsUtilisateurDisponibleToutLesJour, idsUtilisateurParticipantDejaAUneActivite);
        calculerIndisponibilite(idsUtilisateurOrganisme, idsUtilisateurDisponibleToutLesJour, idsUtilisateurParticipantDejaAUneActivite);

        return this;
    }

    private void calculerDisponibilite(List<Long> idsUtilisateurDispoibleToutLesJour, List<Long> idsUtilisateurParticipantDejaAUneActivite) {
        idsUtilisateursDisponiblesPourActivite = new ArrayList<>();
        idsUtilisateursDisponiblesPourActivite.addAll(idsUtilisateurDispoibleToutLesJour);
        //System.out.println("dispo pour la periode  : " + idsUtilisateursDisponiblesPourActivite.size());

        idsUtilisateursDisponiblesPourActivite.removeAll(idsUtilisateurParticipantDejaAUneActivite);

        //System.out.println("dispo pour la periode sans les participation  : " + idsUtilisateursDisponiblesPourActivite.size());

    }

    private void calculerIndisponibilite(List<Long> idsUtilisateurOrganisme, List<Long> idsUtilisateurDisponibleToutLesJour, List<Long> idsUtilisateurParticipantDejaAUneActivite) {
        idsUtilisateursNonDisponiblesPourActivite = new ArrayList<>();
        idsUtilisateursNonDisponiblesPourActivite.addAll(idsUtilisateurOrganisme);
        //System.out.println("indispo pour la periode (toute l'unité)  : " + idsUtilisateursNonDisponiblesPourActivite.size());

        idsUtilisateursNonDisponiblesPourActivite.removeAll(idsUtilisateurDisponibleToutLesJour);
        //System.out.println("indispo pour la periode (sans les dispo tout les jours)  : " + idsUtilisateursNonDisponiblesPourActivite.size());

        idsUtilisateursNonDisponiblesPourActivite.removeAll(idsUtilisateurParticipantDejaAUneActivite);
        //System.out.println("indispo pour la periode (sans participants)  : " + idsUtilisateursNonDisponiblesPourActivite.size());
    }


    private List<Long> calculerUtilisateurDispoAllDay() {
        List<Long> idsUtilisateursDisponiblesAllDays = new ArrayList<>();

        for (Long idUtilisateur : idsUtilisateurAvecDesDisponibilite) {

            boolean presentToutLesJours = true;

            for (ActiviteDisponibiliteUtilisateurFiltreDto disponibilite : disponibilites) {
                if (disponibilite.nonPresentCeJour(idUtilisateur)) {
                    presentToutLesJours = false;
                    break;
                }
            }

            if (presentToutLesJours) {
                idsUtilisateursDisponiblesAllDays.add(idUtilisateur);
            }
        }

        return idsUtilisateursDisponiblesAllDays;

    }

    private void add(ActiviteDisponibiliteUtilisateurFiltreDto dispoActivite, DisponibiliteDto disponibiliteDto) {
        if (dispoActivite != null) {
            if (idCorps != null) {
                if (!disponibiliteDto.getCorps().getId().equals(idCorps)) {
                    disponibiliteDto = null;
                }
            }
            if (disponibiliteDto != null) {
                dispoActivite.add(disponibiliteDto);
                idsUtilisateurAvecDesDisponibilite.add(disponibiliteDto.getUtilisateur().getId());
                //System.out.println("disponibilites journaliere mise à jour  : " + dispoActivite);
            }
        }
    }

    private ActiviteDisponibiliteUtilisateurFiltreDto find(DisponibiliteDto disponibiliteDto) {
        for (ActiviteDisponibiliteUtilisateurFiltreDto dispoActivite : disponibilites) {
            if (dispoActivite.equalsDate(disponibiliteDto)) {
                return dispoActivite;
            }
        }
        return null;
    }

    public ListActiviteDisponibiliteUtilisateurFiltreDto addUtilisateurParticipantsActiviteNonDisponible(List<UtilisateurCompletDto> participantsActivite) {
        participantsActiviteNonDisponible = participantsActivite;
        return this;
    }

    public ListActiviteDisponibiliteUtilisateurFiltreDto addUtilisateurOrganisme(List<UtilisateurCompletDto> utilisateursDeLUnite) {
        utilisateursDeLOrganisme = utilisateursDeLUnite;
        return this;
    }
}
