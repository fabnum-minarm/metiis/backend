package fabnum.metiis.dto.activite;

import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.disponibilite.TypeDisponibiliteDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ChoixDto extends AbstractLongDto {

    protected String code;

    protected String nom;

    protected String lettre;

    private Boolean actionPrincipale = Boolean.FALSE;

    private Boolean actionSuppression = Boolean.FALSE;

    private Boolean canActiviteWithOtherActivite = Boolean.FALSE;

    private StatutDto statut;

    private TypeDisponibiliteDto typeDisponibilite;

    private TypeEtatChoixDto typeEtatChoix;


    public ChoixDto(Long id, String code, String nom, String lettre, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite) {
        super(id);
        this.code = code;
        this.nom = nom;
        this.lettre = lettre;
        this.actionPrincipale = actionPrincipale;
        this.actionSuppression = actionSuppression;
        this.canActiviteWithOtherActivite = canActiviteWithOtherActivite;
    }

    public ChoixDto(Long id, String code, String nom, String lettre, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
                    Long idStatut, String codeStatut, String nomSingulier,
                    Long idTypeDispo, String codeTypeDispo, String libelleTypeDispo, Boolean estDisponible) {
        this(id, code, nom, lettre, actionPrincipale, actionSuppression, canActiviteWithOtherActivite);
        statut = new StatutDto(idStatut, codeStatut, nomSingulier);
        typeDisponibilite = new TypeDisponibiliteDto(idTypeDispo, codeTypeDispo, libelleTypeDispo, estDisponible);

    }

    public ChoixDto(Long id, String code, String nom, String lettre, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
                    Long idTypeDispo, String codeTypeDispo, String libelleTypeDispo, Boolean estDisponible) {
        this(id, code, nom, lettre, actionPrincipale, actionSuppression, canActiviteWithOtherActivite);
        typeDisponibilite = new TypeDisponibiliteDto(idTypeDispo, codeTypeDispo, libelleTypeDispo, estDisponible);

    }


}
