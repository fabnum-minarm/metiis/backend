package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@Getter
@Setter
public class RechercheParticipantDto {

    private UtilisateurCompletDto utilisateur;
    private Long countJourDisponible;
    private Long dureeActivite;

    private EtapeDto etape;
    private StatutDto statut;

    @JsonIgnore
    private Long idParticiper;

    @JsonIgnore
    private Long idActivite;

    @JsonIgnore
    private List<EtapeStatutDto> etapeStatutWithDisponibilite;

    /*
    * " a.id , " +
            " p.id , " +
            " c.id , c.code , c.ordre , " +
            " ut.id , ut.nom , ut.prenom , " +
            " s.id , s.code , s.nom , s.nomSingulier , s.action, " +
            " e.id , " +
            " af.id , " +
            " pr.id , pr.code ,  pr.ordre , " +
            " count(d.id)  " +
    * */
    @SuppressWarnings({"UnusedDeclaration"})
    public RechercheParticipantDto(Long idActivite,
                                   Long idParticiper,
                                   Long idCorps, String codeCorps, Long ordreCorps,
                                   Long idUtilisateur, String nom, String prenom,
                                   Long idStatut, String codeStatut, String nomStatut, String nomSingulierStatut, String actionStatut,
                                   Long idEtape,
                                   Long idAffectation,
                                   Long idPosteRo, String codePosteRo, Long ordrePosteRo,
                                   Long countDisponibilite) {

        this.idActivite = idActivite;
        utilisateur = new UtilisateurCompletDto(idUtilisateur, nom, prenom, idCorps, codeCorps, ordreCorps, idAffectation, idPosteRo, codePosteRo, ordrePosteRo);
        etape = new EtapeDto(idEtape);
        this.idParticiper = idParticiper;
        statut = new StatutDto(idStatut, codeStatut, nomStatut, nomSingulierStatut, actionStatut);
        countJourDisponible = countDisponibilite;
    }

    @Nullable
    @JsonProperty("isDisponible")
    public Boolean isDisponible() {
        if (idParticiper != null) {
            return null;
        }
        return countJourDisponible.equals(dureeActivite);
    }

    public void setEtapeStatutWithDisponibilite(List<EtapeStatutDto> etapeStatutWithDisponibilite) {
        this.etapeStatutWithDisponibilite = etapeStatutWithDisponibilite;
        majEtapeAndStatut();
    }

    private void majEtapeAndStatut() {
        if (idParticiper == null) {
            for (EtapeStatutDto etapeStatutDto : etapeStatutWithDisponibilite) {
                if (etapeStatutDto.getStatut().getStatutDisponibiliteDto().getDisponibilite().equals(isDisponible())) {
                    etape.setId(etapeStatutDto.getIdEtape());
                    statut = etapeStatutDto.getStatut();
                    break;
                }
            }
        }
    }


}
