package fabnum.metiis.dto.activite;

import fabnum.metiis.dto.tools.Taux;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


@NoArgsConstructor
public class StatutAvancementActivite implements Serializable {

    private static final long serialVersionUID = -6214711188101370535L;

    @Getter
    @Setter
    private StatutDto statut;

    @Getter
    private final Taux taux = new Taux();


    public void addTotale(Long nombre) {
        taux.addTotal(nombre);
    }

    public void addUtilisateur(Long countUtilisateur) {
        taux.add(countUtilisateur);
    }
}
