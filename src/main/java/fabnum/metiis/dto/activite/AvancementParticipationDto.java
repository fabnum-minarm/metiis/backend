package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class AvancementParticipationDto extends AbstractLongDto implements Comparable<AvancementParticipationDto> {

    private String commentaire;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateValidation;

    private Boolean estRealiser = false;

    @JsonIgnore
    private Long idParticiper;

    private ChoixDto choix;

    private EtapeDto etape;

    private UtilisateurDto validateur;

    /*
    " ap.id , ap.commentaire , ap.dateValidation , ap.estRealiser ,  " +
    " p.id , " +
    " e.id , e.code , e.nom e.nomPluriel , e.ordre , " +

    " ch.id , ch.code , ch.nom , ch.actionPrincipale " +
    " s.id , s.code , s.nom  , " +
    " tec.id , tec.code , tec.nom  ,  tec.finAvancement " +

    " ut.id , ut.nom , ut.prenom , " +
    " c.id , c.lettre , c.code , c.libelle , c.ordre " +
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public AvancementParticipationDto(Long id, String commentaire, Instant dateValidation, Boolean estRealiser,
                                      Long idParticiper,
                                      Long idEtape, String codeEtape, String nomEtape, String nomPlurielEtape, Long ordreEtape, String descriptionEtape, String descriptionPlurielEtape,

                                      Long idChoix, String codeChoix, String nomChoix, String lettreChoix, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
                                      Long idStatut, String codeStatut, String nomStatut, String nomSingulierStatut, String actionStatut,
                                      Long idStatutDisponibilite, String codeStatutDisponibilite, Boolean disponibilite,
                                      Long idTypeEtatChoix, String codeTypeEtatChoix, String nomTypeEtatChoix, Boolean finAvancement,

                                      Long idUtilisateur, String nom, String prenom,
                                      Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps) {
        super(id);

        this.commentaire = commentaire;
        this.dateValidation = dateValidation;
        this.estRealiser = estRealiser;

        this.idParticiper = idParticiper;

        etape = new EtapeDto(idEtape, codeEtape, nomEtape, nomPlurielEtape, ordreEtape, descriptionEtape, descriptionPlurielEtape);

        choix = new ChoixDto(idChoix, codeChoix, nomChoix, lettreChoix, actionPrincipale, actionSuppression, canActiviteWithOtherActivite);
        choix.setStatut(new StatutDto(idStatut, codeStatut, nomStatut, nomSingulierStatut, actionStatut, idStatutDisponibilite, codeStatutDisponibilite, disponibilite));
        choix.setTypeEtatChoix(new TypeEtatChoixDto(idTypeEtatChoix, codeTypeEtatChoix, nomTypeEtatChoix, finAvancement));

        validateur = new UtilisateurDto(idUtilisateur, nom, prenom,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public AvancementParticipationDto(
            Long idEtape, String codeEtape, String nomEtape, String nomPlurielEtape, Long ordreEtape, String descriptionEtape, String descriptionPlurielEtape
    ) {
        etape = new EtapeDto(idEtape, codeEtape, nomEtape, nomPlurielEtape, ordreEtape, descriptionEtape, descriptionPlurielEtape);

    }

    public AvancementParticipationDto(Long id, String commentaire, Instant dateValidation, Boolean estRealiser,
                                      Long idParticiper,
                                      Long idChoix, String codeChoix, String nomChoix, String lettreChoix, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
                                      Long idStatut, String codeStatut, String nomSingulier,
                                      Long idTypeDispo, String codeTypeDispo, String libelleTypeDispo, Boolean estDisponible,
                                      Long idEtape, String codeEtape, String nomEtape, String nomPlurielEtape, Long ordreEtape, String descriptionEtape, String descriptionPlurielEtape) {
        super(id);

        this.commentaire = commentaire;
        this.dateValidation = dateValidation;
        this.estRealiser = estRealiser;

        this.idParticiper = idParticiper;
        if (this.idParticiper != null) {
            choix = new ChoixDto(idChoix, codeChoix, nomChoix, lettreChoix, actionPrincipale, actionSuppression, canActiviteWithOtherActivite,
                    idStatut, codeStatut, nomSingulier,
                    idTypeDispo, codeTypeDispo, libelleTypeDispo, estDisponible);
        }
        etape = new EtapeDto(idEtape, codeEtape, nomEtape, nomPlurielEtape, ordreEtape, descriptionEtape, descriptionPlurielEtape);

    }

    public boolean estFinit() {
        return choix != null && choix.getId() != null
                && choix.getTypeEtatChoix() != null && choix.getTypeEtatChoix().getId() != null
                && choix.getTypeEtatChoix().getFinAvancement();
    }

    public boolean equalsEtape(Long idEtape) {
        return etape != null && etape.getId() != null && etape.getId().equals(idEtape);
    }

    public boolean isBefore(AvancementParticipationDto avancementParticipation) {
        return etape.getOrdre() < avancementParticipation.etape.getOrdre();
    }

    @Override
    public String toString() {
        return "AvancementParticipationDto{" +
                "estRealiser=" + estRealiser +
                ", idParticiper=" + idParticiper +
                ", etape=" + etape +
                ", id=" + id +
                '}';
    }

    @Override
    public int compareTo(@NotNull AvancementParticipationDto o) {
        return etape.getOrdre().compareTo(o.etape.getOrdre());
    }
}
