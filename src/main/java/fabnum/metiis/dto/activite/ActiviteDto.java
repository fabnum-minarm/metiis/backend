package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import fabnum.metiis.interfaces.activite.IActivite;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ActiviteDto extends AbstractLongDto implements IActivite {

    private static final long serialVersionUID = -4624380080353680961L;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ACTIVITE, ContextEnum.UPDATE_ACTIVITE_PARTIEL, ContextEnum.UPDATE_ACTIVITE},
            message = "activite.missing.nom")
    private String nom;

    private String description;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ACTIVITE, ContextEnum.UPDATE_ACTIVITE},
            message = "activite.missing.debut")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateDebut;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ACTIVITE, ContextEnum.UPDATE_ACTIVITE},
            message = "activite.missing.fin")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateFin;

    @ValidatorChild(contexte = {ContextEnum.CREATE_ACTIVITE, ContextEnum.UPDATE_ACTIVITE_PARTIEL, ContextEnum.UPDATE_ACTIVITE})
    private TypeActiviteDto typeActivite;

    @ValidatorChild(contexte = {ContextEnum.CREATE_ACTIVITE, ContextEnum.UPDATE_ACTIVITE})
    private OrganismeDto organisme;

    private UtilisateurDto utilisateur;

    private Long idParent;

    private String token;

    private List<EffectifDto> effectifs = new ArrayList<>();

    private StatutAvancementActivite avancement = new StatutAvancementActivite();

    private List<ParticiperDto> participants = new ArrayList<>();

    @ApiModelProperty(hidden = true)
    private List<UtilisateurActiviteDetailsDto> participantsGroupe;

    private Boolean estDispo;

    private Boolean envoyerMailCreation;

    private Long rappelNbJourAvantDebut;
    private Instant dateEnvoieRappel;
    private Boolean rappelInApp;
    private Boolean rappelEmail;

    private Long rappelParticipationNbJourAvantDebut;
    private Instant dateEnvoieRappelParticipation;
    private Boolean rappelParticipationInApp;
    private Boolean rappelParticipationEmail;

    public ActiviteDto(Long id) {
        super(id);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public ActiviteDto(
            Long id, String token, String nom, String description, Instant dateDebut, Instant dateFin,
            Boolean envoyerMailCreation,
            Long rappelNbJourAvantDebut, Instant dateEnvoieRappel,
            Boolean rappelInApp, Boolean rappelEmail,
            Long rappelParticipationNbJourAvantDebut, Instant dateEnvoieRappelParticipation,
            Boolean rappelParticipationInApp, Boolean rappelParticipationEmail,

            Long idTypeActivite, String codeTypeActivite, String libelleTypeActivite, String codeCssTypeActivite, String codeCssIconeTypeActivite, Long ordreTypeActivite,

            Long idUtilisateur,
            Long idOrganisme, String codeOrganisme, String libelleOrganisme,
            Long idParent) {

        this(id, nom, description, dateDebut, dateFin);
        this.token = token;
        this.envoyerMailCreation = envoyerMailCreation;

        this.rappelNbJourAvantDebut = rappelNbJourAvantDebut;
        this.dateEnvoieRappel = dateEnvoieRappel;
        this.rappelInApp = rappelInApp;
        this.rappelEmail = rappelEmail;

        this.rappelParticipationNbJourAvantDebut = rappelParticipationNbJourAvantDebut;
        this.dateEnvoieRappelParticipation = dateEnvoieRappelParticipation;
        this.rappelParticipationInApp = rappelParticipationInApp;
        this.rappelParticipationEmail = rappelParticipationEmail;

        this.idParent = idParent;

        typeActivite = new TypeActiviteDto(idTypeActivite, codeTypeActivite, libelleTypeActivite, codeCssTypeActivite, codeCssIconeTypeActivite, ordreTypeActivite);
        utilisateur = new UtilisateurDto(idUtilisateur);
        organisme = new OrganismeDto(idOrganisme, codeOrganisme, libelleOrganisme);
    }

    public ActiviteDto(Long id, String nom, String description, Instant dateDebut, Instant dateFin) {
        super(id);
        this.nom = nom;
        this.description = description;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
    }

    public ActiviteDto(
            Long id, String token, String nom, String description, Instant dateDebut, Instant dateFin,
            Long idTypeActivite, String codeTypeActivite, String libelleTypeActivite, String codeCssTypeActivite, String codeCssIconeTypeActivite, Long ordreTypeActivite) {
        this(id, nom, description, dateDebut, dateFin);
        this.token = token;
        typeActivite = new TypeActiviteDto(
                idTypeActivite,
                codeTypeActivite,
                libelleTypeActivite,
                codeCssTypeActivite,
                codeCssIconeTypeActivite,
                ordreTypeActivite);
    }

    public ActiviteDto(
            Long id, String token, String nom, String description, Instant dateDebut, Instant dateFin,
            Long idTypeActivite, String codeTypeActivite, String libelleTypeActivite, String codeCssTypeActivite, String codeCssIconeTypeActivite, Long ordreTypeActivite,
            Long idOrganisme, String codeOrganisme, String libelleOrganisme) {
        this(id, nom, description, dateDebut, dateFin);
        this.token = token;
        typeActivite = new TypeActiviteDto(
                idTypeActivite,
                codeTypeActivite,
                libelleTypeActivite,
                codeCssTypeActivite,
                codeCssIconeTypeActivite,
                ordreTypeActivite);
        organisme = new OrganismeDto(idOrganisme, codeOrganisme, libelleOrganisme);
    }

    public void add(EffectifDto effectif) {
        if (id.equals(effectif.getIdActivite())) {
            effectif.setAvancementTotale(avancement);
            effectifs.add(effectif);
        }
    }


    @ValidatorNotNull(contexte = ContextEnum.CHOISIR, message = "activite.missing")
    @Override
    public Long getId() {
        return super.getId();
    }


    @Override
    public String toString() {
        return "Activite : " + dateDebut + " > " + dateFin + " " + nom;
    }

    public void setEffectifs(List<EffectifDto> effectifs) {
        this.effectifs = effectifs;
        this.effectifs.forEach(effectif -> effectif.setAvancementTotale(avancement));
    }

    public void ajouterCompletion(List<ActiviteCorpsStatutCountDto> countCompletion) {
        if (!CollectionUtils.isEmpty(countCompletion)) {
            countCompletion.forEach(this::addToEffectif);
        }
    }

    private void addToEffectif(ActiviteCorpsStatutCountDto activiteCorpsStatutCountDto) {
        if (equalsActivite(activiteCorpsStatutCountDto)) {
            if (!CollectionUtils.isEmpty(effectifs)) {
                effectifs.forEach(effectif -> effectif.ajouterAvancement(activiteCorpsStatutCountDto));
            }
        }
    }

    private boolean equalsActivite(ActiviteCorpsStatutCountDto activiteCorpsStatutCountDto) {
        return id.equals(activiteCorpsStatutCountDto.getIdActivite());
    }

    public void setStatutCompletion(StatutDto statut) {
        avancement.setStatut(statut);
    }

    public void addParticipants(List<ParticiperDto> participations) {
        if (!CollectionUtils.isEmpty(participations)) {
            participations.forEach(this::addParticipant);
        }
    }

    public void addParticipant(ParticiperDto participer) {
        if (id.equals(participer.getIdActivite())) {
            participants.add(participer);
        }
    }
}
