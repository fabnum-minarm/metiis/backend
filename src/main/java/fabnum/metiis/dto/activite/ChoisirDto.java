package fabnum.metiis.dto.activite;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.dto.rh.UtilisateurDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ChoisirDto {

    @ValidatorChild(contexte = ContextEnum.CHOISIR)
    private ActiviteDto activite;

    @ValidatorChild(contexte = ContextEnum.CHOISIR)
    private ChoixDto choix;

    @ValidatorChild(contexte = ContextEnum.CHOISIR)
    private UtilisateurDto validateur;

    private List<UtilisateurActiviteDetailsDto> selectionnees;
}
