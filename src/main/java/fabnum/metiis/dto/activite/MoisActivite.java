package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fabnum.metiis.config.SuperDate;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({"mois", "numero", "semaines"})
@Getter
public class MoisActivite {

    @JsonIgnore
    @Setter
    private SuperDate date;

    @JsonIgnore
    private String annee = "";

    @JsonProperty("mois")
    public String mois() {
        return date.toStringLLLL() + annee;
    }

    @JsonProperty("numero")
    public Integer numero() {
        return date.getMois();
    }

    private final List<SemaineActivite> semaines = new ArrayList<>();


    public MoisActivite(SuperDate date) {
        this.date = date.copie();
        if (this.date.copie().moisSuivant(-1).getAnnee() < this.date.getAnnee()) {
            annee = " " + date.getAnnee();
        }

    }

    public void add(SemaineActivite semaine) {
        if (sameMonth(semaine.getFinSemaine()) || sameMonth(semaine.getDebutSemaine())) {
            semaines.add(semaine);
        }
    }

    private boolean sameMonth(SuperDate date) {
        return date.getMois().equals(this.date.getMois()) && date.getAnnee().equals(this.date.getAnnee());
    }
}
