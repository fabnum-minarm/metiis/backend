package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import fabnum.metiis.config.SuperDate;
import lombok.Getter;

@Getter
public class JourPositionActivite {

    @JsonIgnore
    private final PositionActivite positionActivite;
    private final boolean debut;
    private final boolean fin;

    public JourPositionActivite(PositionActivite positionActivite, SuperDate jour) {
        this.positionActivite = positionActivite;
        debut = (jour.estEgaleJMA(positionActivite.getActivite().getDebut()));
        fin = (jour.estEgaleJMA(positionActivite.getActivite().getFin()));
    }

    public JourPositionActivite(int position) {
        positionActivite = new PositionActivite(position);
        debut = false;
        fin = false;
    }

    @JsonProperty("position")
    public Integer getPosition() {
        return positionActivite.getPosition();
    }

    @JsonProperty("activite")
    public ActiviteSimpleDto getActivite() {
        return positionActivite.getActivite();
    }
   

    public void setPosition(int position) {
        positionActivite.setPosition(position);
    }
}
