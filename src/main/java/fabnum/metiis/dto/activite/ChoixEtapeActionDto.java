package fabnum.metiis.dto.activite;


import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ChoixEtapeActionDto extends AbstractLongDto {
    private EtapeDto etape;

    private ChoixDto choix;

    private EtapeDto etapeARemplir;

    private EtapeDto etapeSuivante;

    @SuppressWarnings({"UnusedDeclaration"})
    public ChoixEtapeActionDto(
            Long id,
            Long idEtape, String codeEtape, String nomEtape, String nomPlurielEtape,
            Long ordreEtape, String descriptionEtape, String descriptionPlurielEtape,

            Long idChoix, String codeChoix, String nomChoix, String lettreChoix, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
            Long idTypeDispo, String codeTypeDispo, String libelleTypeDispo, Boolean estDisponible,

            Long idEtapeARemplir, String codeEtapeARemplir, String nomEtapeARemplir,
            String nomPlurielEtapeARemplir, Long ordreEtapeARemplir, String descriptionEtapeARemplir, String descriptionPlurielEtapeARemplir,

            Long idEtapeSuivante, String codeEtapeSuivante, String nomEtapeSuivante,
            String nomPlurielEtapeSuivante, Long ordreEtapeSuivante, String descriptionEtapeSuivante, String descriptionPlurielEtapeSuivante) {

        super(id);

        etape = new EtapeDto(idEtape, codeEtape, nomEtape, nomPlurielEtape, ordreEtape, descriptionEtape, descriptionPlurielEtape);
        choix = new ChoixDto(idChoix, codeChoix, nomChoix, lettreChoix, actionPrincipale, actionSuppression, canActiviteWithOtherActivite,
                idTypeDispo, codeTypeDispo, libelleTypeDispo, estDisponible);
        
        etapeARemplir = new EtapeDto(idEtapeARemplir, codeEtapeARemplir, nomEtapeARemplir, nomPlurielEtapeARemplir, ordreEtapeARemplir, descriptionEtapeARemplir, descriptionPlurielEtapeARemplir);

        if (idEtapeSuivante != null) {
            etapeSuivante = new EtapeDto(idEtapeSuivante, codeEtapeSuivante, nomEtapeSuivante, nomPlurielEtapeSuivante, ordreEtapeSuivante, descriptionEtapeSuivante, descriptionPlurielEtapeSuivante);
        }
    }

    public boolean equalsEtapeChoix(Long idEtape, Long idChoix) {
        return etape.getId().equals(idEtape) && choix.getId().equals(idChoix);
    }

    @Override
    public String toString() {
        return "ChoixEtapeActionDto{" +
                "etape=" + etape +
                ", choix=" + choix.getId() +
                ", etapeARemplir=" + etapeARemplir.getId() +
                ", etapeSuivante=" + etapeSuivante +
                ", id=" + id +
                '}';
    }
}
