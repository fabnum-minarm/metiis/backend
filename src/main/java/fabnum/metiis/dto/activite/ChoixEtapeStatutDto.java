package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ChoixEtapeStatutDto extends AbstractLongDto implements Comparable<ChoixEtapeStatutDto> {

    @JsonIgnore
    private Long idEtapeStatut;

    private ChoixDto choix;

    private Long ordreDansGroupe;

    /*
         " cgs.id , cgs.ordreDansGroupe,    " +
            " gs.id , " +
            " c.id , c.code , c.nom , c.actionPrincipale " +
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public ChoixEtapeStatutDto(Long id, Long ordreDansGroupe,
                               Long idEtapeStatut,
                               Long idChoix, String codeChoix, String nomChoix, String lettreChoix, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
                               Long idTypeDispo, String codeTypeDispo, String libelleTypeDispo, Boolean estDisponible) {
        super(id);
        this.ordreDansGroupe = ordreDansGroupe;
        this.idEtapeStatut = idEtapeStatut;

        choix = new ChoixDto(idChoix, codeChoix, nomChoix, lettreChoix, actionPrincipale, actionSuppression, canActiviteWithOtherActivite,
                idTypeDispo, codeTypeDispo, libelleTypeDispo, estDisponible);
    }

    @Override
    public int compareTo(ChoixEtapeStatutDto statutGroupeStatutDto) {
        return ordreDansGroupe.compareTo(statutGroupeStatutDto.ordreDansGroupe);
    }


}
