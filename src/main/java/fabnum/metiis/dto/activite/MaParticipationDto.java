package fabnum.metiis.dto.activite;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class MaParticipationDto {
    private ParticiperDto participer;
    private List<ChoixEtapeActionDto> mesActions = new ArrayList<>();
}
