package fabnum.metiis.dto.activite;

import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StatutDisponibiliteDto extends AbstractLongDto {

    private String code;

    private Boolean disponibilite;

    public StatutDisponibiliteDto(Long id, String code, Boolean disponibilite) {
        super(id);
        this.code = code;
        this.disponibilite = disponibilite;
    }

}
