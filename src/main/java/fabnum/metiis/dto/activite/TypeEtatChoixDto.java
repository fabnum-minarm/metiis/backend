package fabnum.metiis.dto.activite;

import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TypeEtatChoixDto extends AbstractLongDto {

    private String code;

    private String nom;

    private Boolean finAvancement;

    public TypeEtatChoixDto(Long id, String code, String nom, Boolean finAvancement) {
        super(id);
        this.nom = nom;
        this.code = code;
        this.finAvancement = finAvancement;
    }
}
