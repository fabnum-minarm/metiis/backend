package fabnum.metiis.dto.activite;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class EtapeDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = ContextEnum.UPDATE_ETAPE, message = "etape.missing.code")
    private String code;

    @ValidatorNotNull(contexte = ContextEnum.UPDATE_ETAPE, message = "etape.missing.nom")
    private String nom;

    @ValidatorNotNull(contexte = ContextEnum.UPDATE_ETAPE, message = "etape.missing.nompluriel")
    private String nomPluriel;

    @ValidatorNotNull(contexte = ContextEnum.UPDATE_ETAPE, message = "etape.missing.ordre")
    private Long ordre;

    private String description;

    private String descriptionPluriel;

    private List<EtapeStatutDto> etapesStatuts;

    public EtapeDto(Long id) {
        super(id);
    }

    public EtapeDto(Long id, String code, String nom, String nomPluriel, Long ordre, String description, String descriptionPluriel) {
        super(id);
        this.nom = nom;
        this.nomPluriel = nomPluriel;
        this.code = code;
        this.ordre = ordre;
        this.description = description;
        this.descriptionPluriel = descriptionPluriel;
    }

    public void remplirStatut(List<EtapeStatutDto> listeStatut) {
        etapesStatuts = new ArrayList<>();
        if (listeStatut != null) {
            listeStatut.forEach(this::add);
        }
        Collections.sort(etapesStatuts);
    }


    private void add(EtapeStatutDto etapeStatutDto) {
        if (equalsEtape(etapeStatutDto)) {
            etapesStatuts.add(etapeStatutDto);
        }
    }

    private boolean equalsEtape(EtapeStatutDto etapeStatutDto) {
        return id.equals(etapeStatutDto.getIdEtape());
    }


    @Override
    public String toString() {
        return "EtapeDto{" +
                "code='" + code + '\'' +
                ", ordre=" + ordre +
                ", id=" + id +
                '}';
    }
}
