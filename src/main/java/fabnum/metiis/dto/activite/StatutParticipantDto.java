package fabnum.metiis.dto.activite;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class StatutParticipantDto {
    private StatutDto statut;
    private List<UtilisateurActiviteDetailsDto> participants;

    public StatutParticipantDto(StatutDto statut) {
        this.statut = statut;
    }
}
