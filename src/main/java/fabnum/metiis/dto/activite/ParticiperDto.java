package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ParticiperDto extends AbstractLongDto {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateInscription;

    private Long idActivite;

    //@ValidatorChild(contexte = ContextEnum.CHOISIR)
    //private ActiviteDto activite;

    private AvancementParticipationDto dernierAvancement;

    private UtilisateurDto utilisateur;

    private List<AvancementParticipationDto> avancements;

    /*
    " p.id , p.dateInscription ,  " +
            " a.id , " +
            " ut.id , ut.nom , ut.prenom , " +
            " c.id , c.lettre , c.code , c.libelle , c.ordre , " +
            " dap.id , dap.commentaire , dap.dateValidation , dap.estRealiser , " +
            " e.id , e.code , e.nom , e.nomPluriel, e.ordre  " +
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public ParticiperDto(
            Long id, Instant dateInscription,
            Long idActivite,
            Long idUtilisateur, String nom, String prenom,
            Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
            Long idAvancementParticipation, String commentaire, Instant dateValidation, Boolean estRealiser,
            Long idChoix, String codeChoix, String nomChoix, String lettreChoix, Boolean actionPrincipale, Boolean actionSuppression, Boolean canActiviteWithOtherActivite,
            Long idStatut, String codeStatut, String nomSingulier,
            Long idTypeDispo, String codeTypeDispo, String libelleTypeDispo, Boolean estDisponible,
            Long idEtape, String codeEtape, String nomEtape, String nomPlurielEtape, Long ordreEtape, String descriptionEtape, String descriptionPlurielEtape
    ) {
        super(id);
        this.dateInscription = dateInscription;
        this.idActivite = idActivite;
        //this.activite = new ActiviteDto(idActivite);
        utilisateur = new UtilisateurDto(idUtilisateur, nom, prenom,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);

        if (idAvancementParticipation != null) {
            dernierAvancement = new AvancementParticipationDto(
                    idAvancementParticipation, commentaire, dateValidation, estRealiser,
                    id,
                    idChoix, codeChoix, nomChoix, lettreChoix, actionPrincipale, actionSuppression, canActiviteWithOtherActivite,
                    idStatut, codeStatut, nomSingulier,
                    idTypeDispo, codeTypeDispo, libelleTypeDispo, estDisponible,
                    idEtape, codeEtape, nomEtape, nomPlurielEtape, ordreEtape, descriptionEtape, descriptionPlurielEtape);
        }
    }

    public ParticiperDto(Long idActivite, Long idUtilisateur) {
        this.idActivite = idActivite;
        utilisateur = new UtilisateurDto(idUtilisateur);
    }
}
