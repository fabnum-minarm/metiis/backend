package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@JsonPropertyOrder({"jour", "mois", "today", "activites"})
@Getter
public class ListJourneeActiviteDisponibilite {

    @JsonIgnore
    private final SuperDate jour;

    private final List<JourPositionActivite> activites = new ArrayList<>();

    private DisponibiliteDto disponibilite;

    @JsonIgnore
    private Set<Integer> positions;

    public ListJourneeActiviteDisponibilite(SuperDate jour) {
        this.jour = jour;
    }

    @JsonProperty("jour")
    public Integer getJour() {
        return jour.getJour();
    }

    @JsonProperty("mois")
    public Integer getMois() {
        return jour.getMois();
    }

    @JsonProperty("today")
    public boolean today() {
        return jour.estEgaleJMA(new SuperDate());
    }

    public void add(PositionActivite positionActivite) {
        if (jour.estApresOuEgale(positionActivite.getActivite().getDebut())
                && jour.estAvantOuEgale(positionActivite.getActivite().getFin())) {
            activites.add(new JourPositionActivite(positionActivite, jour));
        }
    }

    public void add(DisponibiliteDto disponibilite) {
        if (jour.estEgaleJMA(disponibilite.getDateDebut())) {
            this.disponibilite = disponibilite;
        }
    }

    public void calculerPosition() {

        int position = 1;

        definirPositionExistante();

        trierActivites();

        for (JourPositionActivite activite : activites) {
            if (activite.getPosition().equals(0)) {
                while (positions.contains(position)) {
                    position++;
                }
                activite.setPosition(position);
                positions.add(position);
            }
        }

        trierActivites();
    }

    private void trierActivites() {
        activites.sort(Comparator.comparing(JourPositionActivite::getPosition));
    }

    private void definirPositionExistante() {
        positions = activites.stream()
                .map(JourPositionActivite::getPosition)
                .collect(Collectors.toSet());
    }

    public void remplirDeVide(Integer max) {
        for (int i = 1; i <= max; ++i) {
            if (!positions.contains(i)) {
                activites.add(new JourPositionActivite(i));
                positions.add(i);
            }
        }
        trierActivites();
    }

    @JsonIgnore
    public Integer getMaxPosition() {
        return positions.stream().max(Integer::compareTo).orElse(0);
    }

    @Override
    public String toString() {
        return jour.toStringAAAAMMDD();
    }


}
