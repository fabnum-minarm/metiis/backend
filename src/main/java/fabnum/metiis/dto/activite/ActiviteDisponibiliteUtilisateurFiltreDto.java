package fabnum.metiis.dto.activite;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;

import java.util.ArrayList;
import java.util.List;


public class ActiviteDisponibiliteUtilisateurFiltreDto {
    private final SuperDate debut;
    private final SuperDate fin;

    private final List<DisponibiliteDto> disponibilites = new ArrayList<>();
    private final List<Long> idsUtilisateursDisponible = new ArrayList<>();

    public ActiviteDisponibiliteUtilisateurFiltreDto(SuperDate start, SuperDate fin) {
        debut = new SuperDate(start);
        this.fin = new SuperDate(fin);
    }

    public void add(DisponibiliteDto disponibiliteDto) {
        disponibilites.add(disponibiliteDto);

        if (disponibiliteDto.getTypeDisponibilite().getEstDisponible()) {
            idsUtilisateursDisponible.add(disponibiliteDto.getUtilisateur().getId());
        }
    }

    public boolean equalsDate(DisponibiliteDto disponibiliteDto) {
        return debut.estEgaleJMA(disponibiliteDto.getDateDebut()) && fin.estEgaleJMA(disponibiliteDto.getDateFin());
    }

    public boolean nonPresentCeJour(Long idUtilisateur) {
        return !idsUtilisateursDisponible.contains(idUtilisateur);
    }

    @Override
    public String toString() {
        return "ADUF : " + debut + " > " + fin + " " + disponibilites.size() + " / " + idsUtilisateursDisponible;
    }
}
