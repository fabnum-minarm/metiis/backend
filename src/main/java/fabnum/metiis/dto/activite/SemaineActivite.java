package fabnum.metiis.dto.activite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Getter
public class SemaineActivite {

    @JsonIgnore
    private final SuperDate debutSemaine;

    @JsonIgnore
    private final SuperDate finSemaine;

    @JsonIgnore
    private final List<PositionActivite> positionActivites = new ArrayList<>();
    private final List<ListJourneeActiviteDisponibilite> jours = new ArrayList<>();


    public SemaineActivite(SuperDate debut) {
        debutSemaine = debut.copie().debutDeLaSemaine();
        finSemaine = debutSemaine.copie().finDeLaSemaine();

        var increment = debut.copie();
        while (increment.estAvantOuEgale(finSemaine)) {
            jours.add(new ListJourneeActiviteDisponibilite(increment.copie()));
            increment.jourSuivant();
        }
    }

    public void add(List<ActiviteSimpleDto> activites, List<DisponibiliteDto> disponibilites) {
        activites.forEach(this::add);
        disponibilites.forEach(this::add);
        calculerPosition();
        remplirDeVide();
    }

    private void remplirDeVide() {
        Integer max = jours.stream().mapToInt(ListJourneeActiviteDisponibilite::getMaxPosition).max().orElse(0);

        jours.forEach(listJourneeActivite -> listJourneeActivite.remplirDeVide(max));
    }

    private void calculerPosition() {
        jours.forEach(ListJourneeActiviteDisponibilite::calculerPosition);
    }

    private void add(ActiviteSimpleDto activite) {
        if (isBeetweenDate(activite)) {
            PositionActivite positionActivite = addOrGet(activite);
            jours.forEach(listJourneeActivite -> listJourneeActivite.add(positionActivite));
        }
    }

    private void add(DisponibiliteDto disponibilites) {
        jours.forEach(listJourneeActivite -> listJourneeActivite.add(disponibilites));
    }

    //" AND a.dateDebut <=  :fin " +
    //            " AND a.dateFin >= :debut " +
    private boolean isBeetweenDate(ActiviteSimpleDto activite) {
        return activite.getDebut().estAvantOuEgale(finSemaine) && activite.getFin().estApresOuEgale(debutSemaine);
    }

    private PositionActivite addOrGet(ActiviteSimpleDto activite) {
        Optional<PositionActivite> positionActiviteOptional = positionActivites
                .stream()
                .filter(positionActivite -> positionActivite.equalsActivite(activite))
                .findFirst();

        PositionActivite retour;
        if (positionActiviteOptional.isEmpty()) {
            retour = new PositionActivite(activite);
            positionActivites.add(retour);
        } else {
            retour = positionActiviteOptional.get();
        }
        return retour;
    }


}
