package fabnum.metiis.dto.evaluation;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class EvaluerDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_EVALUATION},
            message = "evaluer.missing.noteUtilisation")
    private Long noteUtilisation;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_EVALUATION},
            message = "evaluer.missing.noteBesoin")
    private Long noteBesoin;

    private String commentaire;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateCreation;

    @ValidatorChild(contexte = {ContextEnum.CREATE_EVALUATION})
    private UtilisateurDto utilisateur;

    @SuppressWarnings({"UnusedDeclaration"})
    public EvaluerDto(Long id, Instant dateCreation, Long noteUtilisation, Long noteBesoin, String commentaire,
                      Long idUtilisateur, String nom, String prenom) {
        super(id);
        this.noteUtilisation = noteUtilisation;
        this.dateCreation = dateCreation;
        this.noteBesoin = noteBesoin;
        this.commentaire = commentaire;
        utilisateur = new UtilisateurDto(idUtilisateur, nom, prenom);

    }

}
