package fabnum.metiis.dto.disponibilite;

import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class DisponibiliteSurDuree {

    public boolean estDisponible;

    public boolean estRenseigner;

    public UtilisateurCompletDto utilisateur;

}
