package fabnum.metiis.dto.disponibilite.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@NoArgsConstructor
@Data
public class DisponibiliteFormAjouterDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE},
            message = "dispo.missing.debut")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateDebut;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE},
            message = "dispo.missing.fin")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateFin;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE},
            message = "typedisponibilite.missing")
    private Long idTypeDisponibilite;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE},
            message = "dispo.missing.utilisateur")
    private Long idUtilisateur;

    private String commentaire;
}
