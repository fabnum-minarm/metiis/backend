package fabnum.metiis.dto.disponibilite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.CorpsDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CorpsDisponibiliteDto extends AbstractLongDto {

    private String code;

    @JsonIgnore
    private String libelle;

    @JsonIgnore
    private String lettre;

    @JsonIgnore
    private Long ordre;

    @Setter(AccessLevel.NONE)
    private Long nombre;

    public CorpsDisponibiliteDto(CorpsDto corps) {
        this.id = corps.getId();
        this.code = corps.getCode();
        this.libelle = corps.getLibelle();
        this.lettre = corps.getLettre();
        this.ordre = corps.getOrdre();
        nombre = 0L;
    }

    public boolean equalsCorps(DisponibiliteDto disponibilite) {
        return id.equals(disponibilite.getCorps().getId());
    }

    public void incremente() {
        nombre++;
    }

}
