package fabnum.metiis.dto.disponibilite;

import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TypeDisponibiliteDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_DISPONIBILITE, ContextEnum.UPDTAE_TYPE_DISPONIBILITE},
            message = "typeDisponibilite.missing.code")
    private String code;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_DISPONIBILITE, ContextEnum.UPDTAE_TYPE_DISPONIBILITE},
            message = "typeDisponibilite.missing.libelle")
    private String libelle;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_TYPE_DISPONIBILITE, ContextEnum.UPDTAE_TYPE_DISPONIBILITE},
            message = "typeDisponibilite.missing.estDispo")
    private Boolean estDisponible;

    public TypeDisponibiliteDto(Long id, String code, String libelle, Boolean estDisponible) {
        super(id);
        this.code = code;
        this.libelle = libelle;
        this.estDisponible = estDisponible;
    }

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_DISPONIBILITE},
            message = "typedisponibilite.missing")
    @Override
    public Long getId() {
        return super.getId();
    }
}
