package fabnum.metiis.dto.disponibilite;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.activite.IndisponibiliteActiviteDto;
import fabnum.metiis.dto.rh.CorpsDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Setter
@Getter
@NoArgsConstructor
public class DisponibiliteOrganismeDto extends AbstractLongDto {
    private static final long serialVersionUID = -4845028942846011635L;
    private Instant dateDebut;
    private Instant dateFin;

    @JsonIgnore
    private SuperDate debut;
    @JsonIgnore
    private SuperDate fin;

    private List<CorpsDisponibiliteDto> effectifs = new ArrayList<>();

    private List<DisponibiliteDto> details = new ArrayList<>();

    private Set<Long> idUtilisateurInActivite = new HashSet<>();


    public DisponibiliteOrganismeDto(SuperDate debut, SuperDate fin, List<CorpsDto> corps, List<IndisponibiliteActiviteDto> indisponibiliteActivites) {
        dateDebut = debut.instant();
        dateFin = fin.instant();
        this.debut = debut;
        this.fin = fin;
        createEffectif(corps);
        remplirIndispo(indisponibiliteActivites);
    }

    private void remplirIndispo(List<IndisponibiliteActiviteDto> indisponibiliteActivites) {
        idUtilisateurInActivite = indisponibiliteActivites.stream()
                .filter(indisponibiliteActiviteDto -> indisponibiliteActiviteDto.estEntre(dateDebut, dateFin))
                .map(IndisponibiliteActiviteDto::getIdUtilisateur)
                .collect(Collectors.toSet());
    }

    public boolean equalsDate(DisponibiliteDto disponibiliteDto) {
        return debut.estEgaleJMA(disponibiliteDto.getDateDebut()) && fin.estEgaleJMA(disponibiliteDto.getDateFin());
    }

    public void addDisponibilite(DisponibiliteDto disponibiliteDto) {
        if (utilisateurNotInActivite(disponibiliteDto)) {
            details.add(disponibiliteDto);
            for (CorpsDisponibiliteDto corpsDisponibilite : effectifs) {
                if (corpsDisponibilite.equalsCorps(disponibiliteDto)) {
                    corpsDisponibilite.incremente();
                }
            }
        }
    }

    private boolean utilisateurNotInActivite(DisponibiliteDto disponibiliteDto) {
        return !idUtilisateurInActivite.contains(disponibiliteDto.getUtilisateur().getId());
    }

    private void createEffectif(List<CorpsDto> corpsDtos) {
        effectifs = new ArrayList<>();
        corpsDtos.forEach(corps ->
                effectifs.add(new CorpsDisponibiliteDto(corps))
        );
    }
}
