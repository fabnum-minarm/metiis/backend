package fabnum.metiis.dto.disponibilite;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Setter
@Getter
@NoArgsConstructor
public class DisponibiliteDto extends AbstractLongDto {

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_DISPONIBILITE},
            message = "dispo.missing.debut")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateDebut;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_DISPONIBILITE},
            message = "dispo.missing.fin")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateFin;

    private UtilisateurCompletDto utilisateur;

    @ValidatorChild(contexte = {ContextEnum.CREATE_DISPONIBILITE})
    private TypeDisponibiliteDto typeDisponibilite;

    private String commentaire;

    @JsonIgnore
    private CorpsDto corps;

    /* d.id ," +
               " d.dateDebut , d.dateFin , d.commentaire , " +
               " ut.id ,  ut.nom , ut.prenom , ut.email , ut.telephone , " +
               " g.id ,  g.code ,  g.libelle , g.ordre ,  " +
               " c.id ,  c.lettre , c.code ,  c.libelle , c.ordre , " +
               " td.id , td.code , td.libelle , td.estDisponible , " +
                " af.id , " +
                " pr.id , pr.code , pr.libelle , pr.ordre  " +
                " ) " */
    public DisponibiliteDto(Long id, Instant dateDebut, Instant dateFin, String commentaire,
                            Long idUtilisateur, String nom, String prenom, String email, String telephone,
                            Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
                            Long idTypeDisponibilite, String codeTypeDisponibilite, String libelleTypeDisponibilite, Boolean estDisponible,
                            Long idAffectation,
                            Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo
    ) {
        super(id);
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.commentaire = commentaire;
        utilisateur = new UtilisateurCompletDto(idUtilisateur, nom, prenom, email, telephone,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps,
                idAffectation,
                idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo);

        typeDisponibilite = new TypeDisponibiliteDto(idTypeDisponibilite, codeTypeDisponibilite, libelleTypeDisponibilite, estDisponible);

        corps = new CorpsDto(idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps);
    }


    @Override
    public String toString() {
        return "Dispo : " + dateDebut + " > " + dateFin + " " + utilisateur.getId() + " / " + typeDisponibilite.getCode();
    }

}
