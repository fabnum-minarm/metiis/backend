package fabnum.metiis.dto.nouveaute;

import fabnum.metiis.dto.abstraction.AbstractLongDto;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NouveauteDto extends AbstractLongDto {

    private static final long serialVersionUID = 2806206666634542262L;

    private String date;

    private String nom;
    
    public NouveauteDto(Long id, String date, String nom) {
        super(id);
        this.date = date;
        this.nom = nom;
    }
}
