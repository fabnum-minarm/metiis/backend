package fabnum.metiis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Deprecated
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDto implements Serializable {

    private static final long serialVersionUID = -2727076052277562352L;

    private String username;

    private String password;

    private String confirm;

    private String lastname;

    private String firstname;

    private String email;
}
