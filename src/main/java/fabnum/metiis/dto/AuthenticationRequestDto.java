package fabnum.metiis.dto;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorNotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationRequestDto implements Serializable {
    private static final long serialVersionUID = -3126962187291509725L;

    @ValidatorNotNull(contexte = ContextEnum.NEW_PWD, message = "utilisateur.missing.email")
    private String username;
    private String password;

    public void setUsername(String username) {
        this.username = Tools.toLowerCase(username);
    }
}
