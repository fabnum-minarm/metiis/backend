package fabnum.metiis.dto.statistique;

import fabnum.metiis.dto.activite.ActiviteDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class StatistiqueUtilisateurDTO {

    private Long jourDisponible;

    private Long jourIndisponible;

    private Long jourNonRenseigne;

    private Long jourEnActivite;

    private Long nbCandidature;

    private Long nbConvocation;

    private List<Long> activitesRealiser;

    private List<Long> avancementActiviteRealise;

    private List<Long> activitesConvoque;

    private List<Long> avancementActiviteConvoque;

    private List<ActiviteDto> activitesRealises;

    private List<ActiviteDto> activitesConvoques;

    private List<ActiviteDto> derniereActivites;

}
