package fabnum.metiis.dto.statistique;

import fabnum.metiis.config.SuperDate;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.tools.Taux;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstratctStatistiqueDispoCount extends AbstractLongDto {

    protected Long nbDisponibilite;
    protected Long nbJours;

    protected Taux remplissageDispo;


    public void calculNbJour(SuperDate debut, SuperDate fin) {
        nbJours = debut.compterNombreDeJourDebutInclus(fin);
        remplissageDispo = new Taux(nbJours * nombreUtilisateurs());
        remplissageDispo.setNombre(nbDisponibilite);
    }

    protected abstract int nombreUtilisateurs();
}
