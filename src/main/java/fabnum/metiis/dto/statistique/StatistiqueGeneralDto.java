package fabnum.metiis.dto.statistique;

import fabnum.metiis.config.ToolsRepository;
import lombok.Getter;


@Getter
public class StatistiqueGeneralDto {

    private final Long utilisateurs;
    private final Long invitations;
    private final Long activites;
    private final Long disponibilites;
    private final Long participations;
    private final Long organismes;

    @SuppressWarnings({"UnusedDeclaration"})
    public StatistiqueGeneralDto(Object[] objects) {
        int i = -1;
        utilisateurs = ToolsRepository.sqlLong(objects, ++i);
        invitations = ToolsRepository.sqlLong(objects, ++i);
        activites = ToolsRepository.sqlLong(objects, ++i);
        disponibilites = ToolsRepository.sqlLong(objects, ++i);
        participations = ToolsRepository.sqlLong(objects, ++i);
        organismes = ToolsRepository.sqlLong(objects, ++i);
    }


}
