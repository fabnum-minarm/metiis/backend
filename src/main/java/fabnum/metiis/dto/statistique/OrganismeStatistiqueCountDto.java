package fabnum.metiis.dto.statistique;

import fabnum.metiis.config.ToolsRepository;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class OrganismeStatistiqueCountDto extends AbstratctStatistiqueDispoCount {

    private String code;
    private String codeParent;
    private Long nbUtilisateur;
    private Long nbActivites;
    private Long nbTokenInvitation;
    private Long nbDisponibiliteOk;


    //select o.id , o.code , op.code , nbutilisateur , coalesce(nbdispo , 0) nbdispo
    @SuppressWarnings({"UnusedDeclaration"})
    public OrganismeStatistiqueCountDto(Object[] objects) {
        int i = -1;
        id = ToolsRepository.sqlLong(objects, ++i);
        code = ToolsRepository.sqlString(objects, ++i);
        codeParent = ToolsRepository.sqlString(objects, ++i);
        nbUtilisateur = ToolsRepository.sqlCountToLong(objects, ++i);
        nbDisponibilite = ToolsRepository.sqlCountToLong(objects, ++i);
        nbActivites = ToolsRepository.sqlCountToLong(objects, ++i);
        nbDisponibiliteOk = ToolsRepository.sqlCountToLong(objects, ++i);
        nbTokenInvitation = ToolsRepository.sqlCountToLong(objects, ++i);
    }

    @Override
    protected int nombreUtilisateurs() {
        return nbUtilisateur.intValue();
    }
}
