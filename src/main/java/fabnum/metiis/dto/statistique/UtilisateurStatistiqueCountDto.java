package fabnum.metiis.dto.statistique;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.config.ToolsRepository;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;


@Getter
@Setter
@NoArgsConstructor
public class UtilisateurStatistiqueCountDto extends AbstratctStatistiqueDispoCount {

    private static final long serialVersionUID = -6778761642920907138L;

    private UtilisateurCompletDto utilisateur;

    private Long nbActivitesCree;

    private Long nbParticipation;

    private Long nbDisponibiliteOk;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant derniereConnexion;

    private OrganismeDto affectation;

    public UtilisateurStatistiqueCountDto(Long id, String nom, String prenom, String email, String telephone, String newEmail, String newTelephone,
                                          Long idCorps, String lettreCorps, String codeCorps, String libelleCorps, Long ordreCorps,
                                          Boolean enabled, Boolean accountNonLocked, String username,
                                          Long idAffectation, Instant dateAffectation, Instant dateFinAffectation,
                                          Long idPosteRo, String codePosteRo, String libellePosteRo, Long ordrePosteRo,
                                          Long idOrganismeDto, String codeOrganismeDto, String libelleOrganismeDto, String credoOrganisme,
                                          Long idParent, String codeParentOrganismeDto, String libelleParentOrganismeDto,
                                          Long idProfil, String codeProfil, String descriptionProfil, Boolean isDefault, Instant lastConnexionDate) {
        utilisateur = new UtilisateurCompletDto(id, nom, prenom, email, telephone, newEmail, newTelephone,
                idCorps, lettreCorps, codeCorps, libelleCorps, ordreCorps,
                enabled, accountNonLocked, username,
                idAffectation, dateAffectation, dateFinAffectation,
                idPosteRo, codePosteRo, libellePosteRo, ordrePosteRo,
                idOrganismeDto, codeOrganismeDto, libelleOrganismeDto, credoOrganisme,
                idParent, codeParentOrganismeDto, libelleParentOrganismeDto,
                idProfil, codeProfil, descriptionProfil, isDefault);
        derniereConnexion = lastConnexionDate;

    }

    /*    ut.id  as utid ,  ut.nom , ut.prenom ,  " +
            " c.id as cid , c.code as ccode, c.ordre  as cordre, " +
            " af.id as afid , " +
            " pr.id as pid , pr.code  as prcode, pr.ordre as prordre , " +
            " u.last_connexion_date , coalesce(dispo.nbdispo , 0) nbdispo , coalesce(activite.nbactivite , 0) nbactivite , coalesce(participer.nbparticipation , 0) nbparticipation ;*/
    @SuppressWarnings({"UnusedDeclaration"})
    public UtilisateurStatistiqueCountDto(Object[] objects) {
        int i = -1;
        id = ToolsRepository.sqlLong(objects, ++i);


        utilisateur = new UtilisateurCompletDto(id, ToolsRepository.sqlString(objects, ++i), ToolsRepository.sqlString(objects, ++i),
                ToolsRepository.sqlLong(objects, ++i), ToolsRepository.sqlString(objects, ++i), ToolsRepository.sqlLong(objects, ++i),
                ToolsRepository.sqlLong(objects, ++i),
                ToolsRepository.sqlLong(objects, ++i), ToolsRepository.sqlString(objects, ++i), ToolsRepository.sqlLong(objects, ++i));


        derniereConnexion = ToolsRepository.sqlDate(objects, ++i);
        nbDisponibilite = ToolsRepository.sqlCountToLong(objects, ++i);
        nbActivitesCree = ToolsRepository.sqlCountToLong(objects, ++i);
        nbParticipation = ToolsRepository.sqlCountToLong(objects, ++i);
        nbDisponibiliteOk = ToolsRepository.sqlCountToLong(objects, ++i);
    }

    @Override
    protected int nombreUtilisateurs() {
        return 1;
    }
}
