package fabnum.metiis.dto.alerte;

import com.fasterxml.jackson.annotation.JsonFormat;
import fabnum.metiis.config.validator.ContextEnum;
import fabnum.metiis.config.validator.ValidatorChild;
import fabnum.metiis.config.validator.ValidatorNotNull;
import fabnum.metiis.dto.abstraction.AbstractLongDto;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.TypeActiviteDto;
import fabnum.metiis.dto.rh.UtilisateurDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
public class AlerteDto extends AbstractLongDto {

    private static final long serialVersionUID = 5946275210792067401L;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ALERTE}, message = "alerte.missing.dateAlerte")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Instant dateAlerte;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ALERTE}, message = "alerte.missing.lu")
    private Boolean lu = Boolean.FALSE;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ALERTE}, message = "alerte.missing.titre")
    private String titre;

    @ValidatorNotNull(contexte = {ContextEnum.CREATE_ALERTE}, message = "alerte.missing.description")
    private String description;

    private String commentaire;

    private UtilisateurDto emetteur;

    @ValidatorChild(contexte = {ContextEnum.CREATE_ALERTE})
    private UtilisateurDto destinataire;

    private ActiviteDto activite;

    private TypeActiviteDto typeActivite;

    private Boolean partage = Boolean.FALSE;

    @SuppressWarnings({"UnusedDeclaration"})
    public AlerteDto(Long id, Instant dateAlerte, Boolean lu, String titre, String description, String commentaire, Boolean partage,
                     Long idEmetteur, String nomEmetteur, String prenomEmetteur,
                     Long idDestinataire, String nomDestinataire, String prenomDestinataire,
                     Long idActivite, String token, String nomActivite, String descriptionActivite, Instant dateDebutActivite, Instant dateFinActivite,
                     Long idTypeActivite, String codeTypeActivite, String libelleTypeActivite, String codeCssTypeActivite, String codeCssIconeTypeActivite, Long ordreTypeActivite) {
        super(id);
        this.dateAlerte = dateAlerte;
        this.lu = lu;
        this.titre = titre;
        this.description = description;
        this.commentaire = commentaire;
        this.partage = partage;
        emetteur = new UtilisateurDto(idEmetteur, nomEmetteur, prenomEmetteur);
        destinataire = new UtilisateurDto(idDestinataire, nomDestinataire, prenomDestinataire);
        activite = new ActiviteDto(idActivite, token, nomActivite, descriptionActivite, dateDebutActivite, dateFinActivite,
                idTypeActivite, codeTypeActivite, libelleTypeActivite, codeCssTypeActivite, codeCssIconeTypeActivite, ordreTypeActivite);

        typeActivite = new TypeActiviteDto(idTypeActivite, codeTypeActivite, libelleTypeActivite, codeCssTypeActivite, codeCssIconeTypeActivite, ordreTypeActivite);
    }


}