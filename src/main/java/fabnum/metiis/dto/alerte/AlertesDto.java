package fabnum.metiis.dto.alerte;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class AlertesDto {

    @Getter
    private final Long nonLue;

    @Getter
    private final Integer size;

    @Getter
    private final List<DateListAlerteDto> content = new ArrayList<>();


    public AlertesDto(List<AlerteDto> alertes, Long nonLue) {
        this.nonLue = nonLue;
        size = alertes.size();
        init(alertes);
    }

    private void init(List<AlerteDto> alertesDtos) {
        alertesDtos.forEach(alerte -> {
                    DateListAlerteDto dateListAlerteDto = new DateListAlerteDto(alerte);
                    int index = content.indexOf(dateListAlerteDto);
                    if (index < 0) {
                        content.add(dateListAlerteDto);
                    } else {
                        content.get(index).add(alerte);
                    }
                }
        );
    }
}
