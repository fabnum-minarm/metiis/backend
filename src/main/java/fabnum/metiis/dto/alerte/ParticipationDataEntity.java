package fabnum.metiis.dto.alerte;

import fabnum.metiis.domain.activite.AvancementParticipation;
import fabnum.metiis.domain.activite.Participer;
import fabnum.metiis.dto.activite.AvancementParticipationDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class ParticipationDataEntity {

    private Participer participer;

    private AvancementParticipation avancementParticipation;

    private List<AvancementParticipationDto> avancementsParticipationsDto;

    public boolean haveAvancementParticipation() {
        return Optional.ofNullable(avancementParticipation).isPresent();
    }

}
