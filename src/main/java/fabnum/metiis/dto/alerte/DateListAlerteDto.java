package fabnum.metiis.dto.alerte;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import fabnum.metiis.config.SuperDate;
import fabnum.metiis.config.Tools;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonPropertyOrder({"date", "alertes"})
public class DateListAlerteDto {

    @JsonIgnore
    private final SuperDate date;

    @Getter
    private final List<AlerteDto> alertes = new ArrayList<>();

    public DateListAlerteDto(AlerteDto alerteDto) {
        date = getInstant(alerteDto);
        alertes.add(alerteDto);
    }

    private SuperDate getInstant(AlerteDto alerteDto) {
        return new SuperDate(alerteDto.getDateAlerte()).debutDeLaJournee();
    }


    @JsonProperty("date")
    public String getDate() {
        SuperDate today = new SuperDate().debutDeLaJournee();
        if (today.instant().equals(date.instant())) {
            return Tools.message("default.today");
        }
        return date.toStringDDLLLLAAAA();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DateListAlerteDto)) return false;
        DateListAlerteDto that = (DateListAlerteDto) o;
        return date.estEgale(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }

    public void add(AlerteDto alerte) {
        if (date.estEgale(getInstant(alerte))) {
            alertes.add(alerte);
        }
    }
}
