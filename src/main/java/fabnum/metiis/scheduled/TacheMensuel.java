package fabnum.metiis.scheduled;

import fabnum.metiis.services.alerte.AlerteService;
import fabnum.metiis.services.rh.TokenUtilisateurService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Job Mensuel<br/>
 * Help pour le cron :
 * <ul>
 * <li><b>1</b> second</li>
 * <li><b>2</b> minute</li>
 * <li><b>3</b> hour</li>
 * <li><b>4</b> day of month</li>
 * <li><b>5</b> month</li>
 * <li><b>6</b> day(s) of week</li>
 * <br/>
 * <li><b>*</b> all</li>
 * <li><b>?</b> any</li>
 * </ul>
 */
@Service
@Transactional
@AllArgsConstructor
public class TacheMensuel {

    private TokenUtilisateurService tokenUtilisateurService;
    private AlerteService alerteService;
    private UtilisateurService utilisateurService;

    /**
     * On supprime les tokens de renouvellement de password périmés.
     * On supprime les tokens de modification d'infos perso périmés.
     * On garde les token inscription pour pouvoir renvoyé une invitation.
     * Tout les 1er du mois à 0h00.
     */
    @Async
    @Scheduled(cron = "0 0 0 1 * ?")
    public void supprimeTokenPerimer() {
        tokenUtilisateurService.supprimerTokenRenewPasswordPerimer();
        utilisateurService.supprimerModifierInfosPersosPerime();
    }

    /**
     * On supprime les alertes lu de plus de 1 ans.
     * Tout les 2 du mois à 00h00.
     */
    @Async
    @Scheduled(cron = "0 0 0 2 * ?")
    public void supprimeAleteLuPerimer() {
        alerteService.supprimerAlertePerimer();
    }

}
