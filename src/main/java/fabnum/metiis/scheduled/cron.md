### Chiffre : signification ( 1 2 3 4 5 6 7)
* 1 : second
* 2 : minute
* 3 : hour 
* 4 : day-of-month
* 5 : month
* 6 : day-of-week
* 7 : year [optional]

### Valeur spécial
* \* tous