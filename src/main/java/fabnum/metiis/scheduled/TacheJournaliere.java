package fabnum.metiis.scheduled;

import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class TacheJournaliere {

    private ActiviteService activiteService;
    private UtilisateurService utilisateurService;

    /**
     * Envoie un rappel (mail / alerte ) pour les activites.
     * Tout les jours à 1h00.
     */
    @Async
    @Scheduled(cron = "0 0 1 * * ?")
    public void envoyerRappelActivite() {
        activiteService.envoyerRappelActivite();
        activiteService.envoyerRappelParticipationActivite();
        utilisateurService.supprimerInscriptionsPerimees();
    }
}
