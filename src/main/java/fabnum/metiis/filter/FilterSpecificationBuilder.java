package fabnum.metiis.filter;


import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * Permet la construction des filtrages pour rechercher des données.
 */
public class FilterSpecificationBuilder {

    /** Liste des critère de rechercher. */
    private final List<FilterCriteria> criterion;

    public FilterSpecificationBuilder(String search) {
        this.criterion = new ArrayList<>();
        this.digest(search);
    }

    /**
     * Permet de transformer une chîane de caractère en critères de filtrage
     * @param search {@link String}
     */
    public void digest(String search) {
        if(search == null) {
            return;
        }
        // Vérification des parties des critères de filtrage répondant au pattern d'un critère de filtrage.
        Matcher m = FilterCriteria.pattern().matcher(search);
        while (m.find()) {
            FilterCriteria c = FilterCriteria.builder()
                    .or("|".equals(m.group(1)))
                    .key(m.group(2))
                    .operation(FilterOperation.byValue(m.group(3)))
                    .value(m.group(4))
                    .pattern(m.group(5)).build();

            criterion.add(c);
        }
    }

    /**
     * Ajout d'un critère de filtre.
     * @param criteria FilterCriteria
     * @return FilterSpecificationBuilder
     */
    public FilterSpecificationBuilder with(FilterCriteria criteria) {
        criterion.add(criteria);
        return this;
    }

    /**
     * Ajout Ajout d'un critère de filtre.
     * @param key {@link String}
     * @param operation {@link String}
     * @param value {@link Object}
     * @param orPredicate boolean
     * @param pattern {@link String}
     * @return FilterSpecificationBuilder
     */
    public FilterSpecificationBuilder with(String key, String operation, String value, boolean orPredicate, String pattern) {
        return this.with(new FilterCriteria(key, FilterOperation.byValue(operation), value, orPredicate, pattern));
    }

    /**
     * Construction de des filtres à appliquer.
     * @return Specification
     */
    public Specification build() {
        if(criterion.isEmpty()) {
            return null;
        }

        List<Specification> specs = criterion.stream()
                .map(FilterSpecification::new)
                .collect(Collectors.toList());

        Specification result = specs.get(0);

        for (int i = 1; i < criterion.size(); i++) {
            if(criterion.get(i).isOr()) {
                result = Specification.where(result).or(specs.get(i));
            } else {
                result = Specification.where(result).and(specs.get(i));
            }
        }

        return result;
    }
}
