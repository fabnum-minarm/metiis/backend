package fabnum.metiis.filter;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import javax.persistence.metamodel.Bindable;
import java.util.Objects;
import java.util.Optional;

/**
 * Filtre pour rechercher des données.
 * @param <T>
 */
@AllArgsConstructor
class FilterSpecification<T> implements Specification<T> {

    /** Critère de recherche. */
    private FilterCriteria criteria;

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {

        // Récupération de la variable réelle à filtrer.
        Expression<String> path = this.getPath(builder, root, this.criteria.getKey(), this.criteria.getPattern());
        String value = Optional.ofNullable(criteria.getValue()).orElseGet(String::new).toLowerCase();
        // Vérification de l'opération à appliquer (par défaut le système fera l'égalité)
        Predicate predicate;
        switch (criteria.getOperation()) {
            case NE :
                predicate = builder.notEqual(path, value);
                break;
            case LK :
                predicate = builder.like(path, "%" + value + "%");
                break;
            case NLK :
                predicate = builder.notLike(path, "%" + value + "%");
                break;
            case LT :
                predicate = builder.lessThan(path, value);
                break;
            case LE :
                predicate = builder.lessThanOrEqualTo(path, value);
                break;
            case GT :
                predicate = builder.greaterThan(path, value);
                break;
            case GE :
                predicate = builder.greaterThanOrEqualTo(path, value);
                break;
            case SW :
                predicate = builder.like(path, value + "%");
                break;
            case NSW :
                predicate = builder.notLike(path, value + "%");
                break;
            case EW :
                predicate = builder.like(path, "%" + value);
                break;
            case NEW :
                predicate = builder.notLike(path, "%" + value);
                break;
            default: // Equals.
                predicate = builder.equal(path, value);
                break;
        }

        return predicate;
    }

    /**
     * Recherche du chemin réel (dans les entités) pour appliquer le filtre.
     * @param builder
     * @param root
     * @param key
     * @param pattern
     * @return
     */
    private Expression<String> getPath(CriteriaBuilder builder, Root<?> root, String key, String pattern) {
        Expression expr;
        Path<?> path = root;

        for (String s : key.split("\\.")) {
            Bindable<Object> model = path.get(s).getModel();
            if (path instanceof From && (
                    model == null || !Objects.equals(Bindable.BindableType.SINGULAR_ATTRIBUTE, model.getBindableType())
            )) {
                final Join existing = ((From<?,?>)path).getJoins().stream().filter(j -> Objects.equals(j.getAlias(), s)).findFirst()
                        .orElse(null);
                if (existing == null) {
                    path = ((From<?,?>)path).join(s);
                    path.alias(s);
                } else {
                    path = existing;
                }
            } else {
                path = path.get(s);
            }
        }

        if (!Objects.isNull(pattern)) {
            expr = builder.function("TO_CHAR", String.class, path,
                    builder.literal(pattern));
        } else {
            expr = path.as(String.class);
        }

        return builder.lower(expr);
    }
}
