package fabnum.metiis.filter;

import lombok.*;

import java.util.regex.Pattern;

/**
 * Critère de filtrage.
 */
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"key"})
@ToString
public class FilterCriteria {

    /**
     * Clé du critère de recherche
     */
    private String key;

    /**
     * Operation de recherche
     */
    private FilterOperation operation = FilterOperation.EQ;

    /**
     * valeur de recherche
     */
    private String value;

    /**
     * Determine si le critère est à ajouter dans une restriction en faisant un and ou un or.
     */
    private boolean or;

    /**
     * Pattern à utiliser.
     */
    private String pattern;

    /**
     * Pattern recherché pour déterminer un critère.
     *
     * @return Pattern
     */
    public static Pattern pattern() {
        final String pattern = String.format("(&|\\|)?([\\.\\w]+)(%s)([^&\\|:]+)?[:]?([^&\\|]+)?", FilterOperation.patterns());
        return Pattern.compile(pattern);
    }
}
