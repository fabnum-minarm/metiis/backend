package fabnum.metiis.filter;

import lombok.Getter;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Enumération des opréations disponibles pour les critères de filtrage.
 */
@Getter
public enum FilterOperation {

    EQ("="), // Equals.
    NE("!="), // Not equals.
    LK("%="), // Like (Contains).
    NLK("!%="), // Not like (Not contains).
    GT(">"), // Greater than.
    GE(">="), // Greater than or equals.
    LT("<"), // Leaster than.
    LE("<="), // Leaster than or equals.
    SW("^="), // Start with.
    NSW("!^="), // Not start with.
    EW("$="), // End with.
    NEW("!$="), // Not end with.
    ;

    /**
     * Correspondance de l'opération dans une chaîne de filtrage.
     */
    private String value;

    FilterOperation(String value) {
        this.value = value;
    }

    /**
     * Récupération via la value.
     *
     * @param value {@link String}
     * @return FilterOperation
     */
    public static FilterOperation byValue(String value) {
        return Arrays.stream(values())
                .filter(fo -> Objects.equals(fo.getValue(), value))
                .findFirst().orElse(null);
    }

    /**
     * Récupération de l'ensemble des patterns collectés en chaîne en caractères pour détecter les opérations.
     *
     * @return String
     */
    public static String patterns() {
        return Arrays.stream(values()).sorted(new FilterOperationSorter()).map(e -> Pattern.quote(e.getValue()))
                .collect(Collectors.joining("|"));
    }

    /**
     * Permet de trier les opérations de façon décroissante pour permmettre la bonne gestion des patterns.
     */
    private static class FilterOperationSorter implements Comparator<FilterOperation>, Serializable {

        private static final long serialVersionUID = -6197562057535696595L;

        @Override
        public int compare(FilterOperation o1, FilterOperation o2) {
            return o2.getValue().compareTo(o1.getValue());
        }
    }
}
