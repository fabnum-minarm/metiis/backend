package fabnum.metiis.interfaces.activite;

import java.time.Instant;

public interface IActivite {
    Long getId();

    Instant getDateFin();

    Instant getDateDebut();
}