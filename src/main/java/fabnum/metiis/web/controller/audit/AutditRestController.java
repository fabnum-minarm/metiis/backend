package fabnum.metiis.web.controller.audit;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.dto.audit.CreatedUpdatedByDto;
import fabnum.metiis.services.audit.AuditService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/v1/audit")
@AllArgsConstructor
public class AutditRestController {

    protected AuditService service;

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/utilisateur/{id}")
    public CreatedUpdatedByDto get(@PathVariable("id") Long idUtilisateur) {
        return service.getById(Utilisateur.class, idUtilisateur);
    }

}
