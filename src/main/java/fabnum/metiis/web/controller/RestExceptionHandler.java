package fabnum.metiis.web.controller;

import fabnum.metiis.config.Tools;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.exceptions.superClasses.CadreApiException;
import fabnum.metiis.security.jwt.InvalidJwtAuthenticationException;
import fabnum.metiis.services.exceptions.ServiceConstraintException;
import fabnum.metiis.services.exceptions.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.DataException;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.status;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {

    @ExceptionHandler(value = {InvalidJwtAuthenticationException.class})
    public ResponseEntity<Object> invalidJwtAuthentication() {
        log.debug("handling InvalidJwtAuthenticationException...");
        return status(HttpStatus.UNAUTHORIZED).build();
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleException(Exception ex) {
        log.debug("handling Exception...");
        LoggerFactory.getLogger("exception").error(ex.getMessage(), ex.getCause());
        InfoServiceDto dto = InfoServiceDto.ko(HttpStatus.INTERNAL_SERVER_ERROR, Tools.message("default.error.message"));
        return new ResponseEntity<>(dto, dto.getStatus());
    }

    @ExceptionHandler(value = {AccessDeniedException.class})
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
        log.debug("handling AccessDeniedException...");
        LoggerFactory.getLogger("exception").error(ex.getMessage(), ex.getCause());
        InfoServiceDto dto = InfoServiceDto.ko(HttpStatus.FORBIDDEN, Tools.message("default.access.denied"));
        return new ResponseEntity<>(dto, dto.getStatus());
    }


    @ExceptionHandler(value = {ServiceException.class})
    public ResponseEntity<Object> handleServiceException(ServiceException ex) {
        log.debug("handling handleServiceException...");
        InfoServiceDto dto = InfoServiceDto.ko(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        return new ResponseEntity<>(dto, dto.getStatus());
    }

    @ExceptionHandler(value = {CadreApiException.class})
    public ResponseEntity<Object> handleCadreApiException(CadreApiException ex) {
        log.debug("handling handleCadreApiException...");
        return ex.createResponse();
    }

    @ExceptionHandler(value = {TransactionSystemException.class})
    public ResponseEntity<Object> handleTransactionSystemException(TransactionSystemException ex) {
        log.debug("handling handleTransactionSystemException...");
        ResponseEntity<Object> response;

        if (ex.getCause().getClass().isAssignableFrom(RollbackException.class)) {
            response = this.handleRollbackException((RollbackException) ex.getCause());
        } else {
            response = this.handleException(ex);
        }

        return response;
    }

    @ExceptionHandler(value = {RollbackException.class})
    public ResponseEntity<Object> handleRollbackException(RollbackException ex) {
        log.debug("handling handleRollbackException...");
        ResponseEntity<Object> response;

        if (ex.getCause().getClass().isAssignableFrom(ConstraintViolationException.class)) {
            response = this.handleConstraintViolationException((ConstraintViolationException) ex.getCause());
        } else {
            response = this.handleException(ex);
        }

        return response;
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {
        log.debug("handling handleConstraintViolationException...");
        List<String> messages = ex.getConstraintViolations().stream()
                .map(c -> String.join(" : ", c.getPropertyPath().toString(), c.getMessage()))
                .collect(Collectors.toList());

        InfoServiceDto dto = InfoServiceDto.ko(HttpStatus.INTERNAL_SERVER_ERROR, String.join("<br/>", messages));
        return new ResponseEntity<>(dto, dto.getStatus());
    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex) {
        log.debug("handling handleDataIntegrityViolationException...");
        InfoServiceDto dto;
        if (ex.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
            dto = InfoServiceDto.ko(HttpStatus.CONFLICT, Tools.message("default.conflit.error"));
        } else if (ex.getCause() instanceof DataException) {
            dto = InfoServiceDto.ko(HttpStatus.CONFLICT, Tools.message("default.data.too.long.error"));
        } else {
            dto = InfoServiceDto.ko(HttpStatus.CONFLICT, ex.getMessage());
        }
        return new ResponseEntity<>(dto, dto.getStatus());
    }

    @ExceptionHandler(value = {ServiceConstraintException.class})
    public ResponseEntity<Object> handleServiceConstraintException(ServiceConstraintException ex) {
        log.debug("handling ServiceConstraintException...");
        InfoServiceDto dto = InfoServiceDto.ko(HttpStatus.CONFLICT, ex.getMessage());
        return new ResponseEntity<>(dto, dto.getStatus());
    }


    @ExceptionHandler(value = {ObjectOptimisticLockingFailureException.class})
    public ResponseEntity<Object> handleObjectOptimisticLockingFailureException(ObjectOptimisticLockingFailureException ex) {
        log.debug("handling handleObjectOptimisticLockingFailureException...");
        LoggerFactory.getLogger("exception").error(ex.getMessage(), ex.getCause());
        InfoServiceDto dto = InfoServiceDto.ko(HttpStatus.INTERNAL_SERVER_ERROR, Tools.message("default.lock.error"));

        return new ResponseEntity<>(dto, dto.getStatus());
    }

    @ExceptionHandler(value = {BadCredentialsException.class})
    public ResponseEntity<Object> handleBadCredentialsException(BadCredentialsException ex) {
        log.debug("handling handleBadCredentialsException...");
        InfoServiceDto dto = InfoServiceDto.ko(HttpStatus.FORBIDDEN, ex.getMessage());
        return new ResponseEntity<>(dto, dto.getStatus());
    }
}
