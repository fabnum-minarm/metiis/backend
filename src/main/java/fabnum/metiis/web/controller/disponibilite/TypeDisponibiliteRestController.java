package fabnum.metiis.web.controller.disponibilite;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.disponibilite.TypeDisponibiliteDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.disponibilite.TypeDisponibiliteService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/type-disponibilite")
@AllArgsConstructor
public class TypeDisponibiliteRestController {

    protected TypeDisponibiliteService typeDisponibiliteService;

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping
    public Page<TypeDisponibiliteDto> all(Pageable pageable) {
        return this.typeDisponibiliteService.findDtoPageableAll(pageable);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/all")
    public List<TypeDisponibiliteDto> all() {
        return this.typeDisponibiliteService.findDtoAll();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/{id}")
    public TypeDisponibiliteDto get(@PathVariable("id") Long id) {
        return this.typeDisponibiliteService.findDtoByIdOnly(id);
    }

    @Secured(Roles.Code.ADMIN)
    @PutMapping("/{id}")
    public TypeDisponibiliteDto modifier(@PathVariable("id") Long idTypeDisponibiliteDto, @RequestBody TypeDisponibiliteDto typeDisponibiliteDto) {
        return this.typeDisponibiliteService.modifier(idTypeDisponibiliteDto, typeDisponibiliteDto);
    }

    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idtypeDisponibiliteService) {
        this.typeDisponibiliteService.delete(idtypeDisponibiliteService);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.ADMIN})
    @PostMapping
    public TypeDisponibiliteDto creerFromDto(@RequestBody TypeDisponibiliteDto typeDisponibiliteDto) {
        return this.typeDisponibiliteService.creerFromDto(typeDisponibiliteDto);
    }

}
