package fabnum.metiis.web.controller.disponibilite;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.dto.disponibilite.DisponibiliteSurDuree;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.disponibilite.DisponibiliteService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/disponibilite")
@AllArgsConstructor
public class DisponibiliteRestController {

    private DisponibiliteService service;

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("liste-utilisateur/{id}")
    public List<DisponibiliteDto> rechercher(@PathVariable("id") Long idUtilisateur) {
        return service.getListByIdUtilisateur(idUtilisateur);
    }

    @Secured({Roles.Code.EMPLOYE})
    @PostMapping("utilisateur/{id}")
    public List<DisponibiliteDto> ajouter(
            @PathVariable("id") Long idUtilisateur,
            @RequestBody DisponibiliteDto disponibilite) {

        return service.createOrUpdate(disponibilite, idUtilisateur);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/{idUtilisateur}/liste-commentaire-disponibilite/entre/{dateDebut}/{dateFin}")
    public List<DisponibiliteDto> rechercherCommentaireDisponibilite(@PathVariable("idUtilisateur") Long idUtilisateur,
                                                                     @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                                                     @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin) {
        return service.getListCommentaireByIdUtilisateurBeetween(idUtilisateur, dateDebut, dateFin);
    }

    @Secured({Roles.Code.EMPLOYE})
    @DeleteMapping("/{idUtilisateur}/suprimer-disponiblilite/entre/{dateDebut}/{dateFin}")
    public InfoServiceDto delete(@PathVariable("idUtilisateur") Long idUtilisateur,
                                 @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                 @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin) {
        service.deleteDisponibilite(idUtilisateur, dateDebut, dateFin);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/{idUtilisateur}/disponibilite/entre/{dateDebut}/{dateFin}")
    public DisponibiliteSurDuree disponibliteEntre(@PathVariable("idUtilisateur") Long idUtilisateur,
                                                   @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                                   @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin) {
        return service.disponibiliteUtilisateur(idUtilisateur, dateDebut, dateFin);
    }


}
