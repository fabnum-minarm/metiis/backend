package fabnum.metiis.web.controller;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.dto.AuthenticationRequestDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.repository.administration.UserRepository;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.security.jwt.JwtTokenProvider;
import fabnum.metiis.services.administration.UserService;
import fabnum.metiis.services.rh.OrganismeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpCookie;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/v1/auth")
public class AuthController {

    private final static Logger LOG = LoggerFactory.getLogger("account");

    private AuthenticationManager authenticationManager;

    private UserService userService;

    private OrganismeService organismeService;

    private JwtTokenProvider jwtTokenProvider;

    private UserRepository userRepository;

    @Value("${security.jwt.token.expire-length:3600000}") // 1 heure par défaut.
    private long validityInMilliseconds;

    public AuthController(AuthenticationManager authenticationManager, UserService userService, JwtTokenProvider jwtTokenProvider, UserRepository userRepository, OrganismeService organismeService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.userRepository = userRepository;
        this.organismeService = organismeService;
    }

    @PostMapping("/signin")
    public ResponseEntity<Object> signin(@RequestBody AuthenticationRequestDto data) {
        try {
            data.setUsername(data.getUsername().trim());
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(data.getUsername(), data.getPassword()));

            User user = this.userService.findByUsername(data.getUsername())
                    .orElseThrow(() -> new UsernameNotFoundException("Username " + data.getUsername() + "not found"));
            this.userService.manageCredential(data.getUsername(), false);

            LOG.info(Tools.message("signin.log", SecurityUtils.getClientIp(), data.getUsername(), Tools.message("signin.ok")));

            return getUserTokenResponse(user);
        } catch (AuthenticationException e) {
            // Dans le cas nominal, on retourne le message d'erreur comme quoi le login et/ou le mot de passe sont incorectes.
            String message = Tools.message("signin.error");
            if (e instanceof BadCredentialsException) {
                // Dans le cas où le compte va être verrouillé, c'est dans cet unique cas de figure que le message change.
                if (!this.userService.manageCredential(data.getUsername(), true)) {
                    message = Tools.message("user.locked");
                }
            }

            LOG.warn(Tools.message("signin.log", SecurityUtils.getClientIp(), data.getUsername(), e.getMessage()));

            throw new BadCredentialsException(message);
        }
    }

    private HttpCookie getHttpCookie(String secureCookie) {
        return ResponseCookie
                .from("secureCookie", secureCookie)
                .maxAge((int) (this.validityInMilliseconds / 1000))
                .httpOnly(true)
                //.secure(true)
                .build();
    }

    private Map<Object, Object> createMapUser(User user, String token) {
        Map<Object, Object> model = new HashMap<>();
        model.put("username", user.getUsername());
        model.put("token", token);
        return model;
    }

    private Map<Object, Object> createMapUser(UtilisateurCompletDto utilisateurCompletDto, String token) {
        Map<Object, Object> model = new HashMap<>();
        model.put("token", token);
        model.put("utilisateur", utilisateurCompletDto);
        return model;
    }

    private Map<Object, Object> createMapOrganisme(OrganismeDto organismeDto, String token) {
        Map<Object, Object> model = new HashMap<>();
        model.put("token", token);
        model.put("organisme", organismeDto);
        return model;
    }

    @GetMapping("/renew")
    public ResponseEntity<Object> renew() {
        User user = currentUser();

        ResponseEntity<Object> responseEntity = getUserTokenResponse(user);
        LOG.info(Tools.message("renew.log", SecurityUtils.getClientIp(), user.getUsername(), Tools.message("renew.ok")));

        return responseEntity;
    }

    private ResponseEntity<Object> getUserTokenResponse(User user) {
        final String secureCookie = UUID.randomUUID().toString();
        final HttpCookie cookie = getHttpCookie(secureCookie);
        String token = jwtTokenProvider.createToken(user.getUsername(), organismeService.findAllOrganismesAllowed(user.getUsername()), user.getAuthorities(), secureCookie);
        Map<Object, Object> model = createMapUser(user, token);

        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString()).body(model);
    }

    public ResponseEntity<Object> getUtilisateurCompletDtoTokenResponse(UtilisateurCompletDto utilisateurCompletDto) {
        final String secureCookie = UUID.randomUUID().toString();
        final HttpCookie cookie = getHttpCookie(secureCookie);
        User user = userService.findByIdUtilisateur(utilisateurCompletDto.getId());
        String token = jwtTokenProvider.createToken(user.getUsername(), organismeService.findAllOrganismesAllowed(user.getUsername()), user.getAuthorities(), secureCookie);
        Map<Object, Object> model = createMapUser(utilisateurCompletDto, token);

        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString()).body(model);
    }

    public ResponseEntity<Object> getOrganismeDtoTokenResponse(OrganismeDto organismeDto) {
        final String secureCookie = UUID.randomUUID().toString();
        final HttpCookie cookie = getHttpCookie(secureCookie);
        User user = userService.findByUsername(SecurityUtils.getCurrentUserLogin()).get();
        String token = jwtTokenProvider.createToken(user.getUsername(), organismeService.findAllOrganismesAllowed(user.getUsername()), user.getAuthorities(), secureCookie);
        Map<Object, Object> model = createMapOrganisme(organismeDto, token);

        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString()).body(model);
    }

    @GetMapping("/logout")
    public InfoServiceDto logout() {
        LOG.info(Tools.message("logout.ok", SecurityUtils.getCurrentUserLogin()));
        return InfoServiceDto.ok(Tools.message("logout.ok", SecurityUtils.getCurrentUserLogin()));
    }

    @GetMapping("/me")
    public User currentUser() {
        return this.userRepository.findByUsername(SecurityUtils.getCurrentUserLogin()).orElse(null);
    }

    @GetMapping("/me/preferences")
    public Map<String, String> currentUserPreferences() {
        return this.userService.getPreferences();
    }

    @PatchMapping("/me/preferences")
    public Map<String, String> updateUserPreferences(@RequestBody Map<String, String> preferences) {
        return this.userService.updatePreferences(preferences);
    }


}
