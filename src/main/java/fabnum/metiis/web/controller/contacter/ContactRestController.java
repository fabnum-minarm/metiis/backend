package fabnum.metiis.web.controller.contacter;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.contacter.ContactDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.contacter.ContactService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/contacts")
@AllArgsConstructor
public class ContactRestController {

    private final ContactService contactService;

    //Doit être accessible à tous.
    @PostMapping
    public InfoServiceDto creer(@RequestBody ContactDto contactDto) {
        contactService.creer(contactDto);
        return InfoServiceDto.ok(Tools.message("default.succes"));
    }


    @Secured({Roles.Code.ADMIN})
    @GetMapping("/non-lus")
    public List<ContactDto> nonLuNonTraite() {
        return contactService.findDtoNonLu();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/non-traites")
    public List<ContactDto> nonTraite() {
        return contactService.findDtoNonTraite();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/old")
    public Page<ContactDto> old(Pageable pageable) {
        return contactService.findDtoOld(pageable);
    }

    @Secured({Roles.Code.ADMIN})
    @PatchMapping("/{id}/lire")
    public InfoServiceDto lu(@PathVariable("id") Long idContact) {
        contactService.lu(idContact);
        return InfoServiceDto.ok(Tools.message("default.succes"));
    }

    @Secured({Roles.Code.ADMIN})
    @PatchMapping("/{id}/traite")
    public InfoServiceDto traite(@PathVariable("id") Long idContact) {
        contactService.traite(idContact);
        return InfoServiceDto.ok(Tools.message("default.succes"));
    }

}
