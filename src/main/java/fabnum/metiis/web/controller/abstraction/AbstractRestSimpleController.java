package fabnum.metiis.web.controller.abstraction;

import fabnum.metiis.domain.abstractions.AbstractEntity;
import fabnum.metiis.services.abstraction.AbstarctService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRestSimpleController<S extends AbstarctService, E extends AbstractEntity, ID> {

    protected S service;

    /**
     * Récupération de l'ensemble des entity.
     * @return List
     */
    @GetMapping
    @SuppressWarnings("unchecked")
    public List<E> all() {
        return this.service.findAll();
    }

    /**
     * Recupere le corps via son id.
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @SuppressWarnings("unchecked")
    public E get(@PathVariable("id") ID id) {
        return (E) this.service.findById(id);
    }

}
