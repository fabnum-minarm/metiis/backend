package fabnum.metiis.web.controller.abstraction;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.abstractions.AbstractEntity;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.abstraction.AbstarctService;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@NoArgsConstructor
public abstract class AbstractRestSimpleCrudSimpleController<S extends AbstarctService, E extends AbstractEntity, ID> extends AbstractRestSimpleController<S, E, ID> {


    public AbstractRestSimpleCrudSimpleController(S service) {
        super(service);
    }

    /**
     * Permet la création.
     * @param entity {@link E} : l'entity à créer.
     * @return E : l'entity créé.
     */
    @PostMapping
    @SuppressWarnings("unchecked")
    public E create(@RequestBody E entity) {
        return (E) this.service.saveOrUpdate(entity);
    }

    /**
     * Permet de modifier.
     * @param id {@link ID} : l'identifiant à modifier.
     * @param entity {@link E} : l'entity à modifier.
     * @return Corps : le entity modifié.
     */
    @PatchMapping("/{id}")
    @SuppressWarnings("unchecked")
    public E update(@PathVariable("id") ID id, @RequestBody E entity) {
        return (E) this.service.update(id, entity);
    }

    /**
     * Permet de supprimer une entity
     * @param id {@link Long} : l'identifinat d'entity à supprimer.
     * @return InfoServiceDto
     */
    @DeleteMapping("/{id}")
    @SuppressWarnings("unchecked")
    public InfoServiceDto delete(@PathVariable("id") Long id) {
        this.service.delete(id);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    /**
     * Permet de supprimer plusieurs entity.
     * @param ids {@link List} : identifiants des entitys à supprimer.
     * @return InfoServiceDto
     */
    @DeleteMapping
    @SuppressWarnings("unchecked")
    public InfoServiceDto delete(@RequestBody List<Long> ids) {
        this.service.delete(ids);
        return InfoServiceDto.ok(Tools.message("default.delete.multiple"));
    }
}
