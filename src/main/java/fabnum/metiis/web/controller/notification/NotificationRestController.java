package fabnum.metiis.web.controller.notification;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.notification.NotificationDto;
import fabnum.metiis.services.notification.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping("/v1/notification")
@AllArgsConstructor
public class NotificationRestController {

    protected NotificationService service;

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/utilisateur/{idUtilisateur}")
    public List<NotificationDto> get(@PathVariable("idUtilisateur") Long idUtilisateur) {
        return service.findAllUsingIdUtilisateur(idUtilisateur);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @PutMapping("/modifier-notification/{idNotification}")
    public NotificationDto modifierFromDto (@PathVariable("idNotification") Long idNotification,
                                            @RequestBody NotificationDto notificationDto){
        return service.modifierFromDto(idNotification, notificationDto);
    }
}
