package fabnum.metiis.web.controller.evaluation;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.evaluation.EvaluerDto;
import fabnum.metiis.services.evaluation.EvaluerService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/evaluation")
@AllArgsConstructor
public class EvaluerRestController {

    private EvaluerService evaluerService;

    @Secured({Roles.Code.ADMIN})
    @GetMapping
    public Page<EvaluerDto> all(Pageable pageable) {
        return evaluerService.findDtoPageableAll(pageable);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/all")
    public List<EvaluerDto> all() {
        return evaluerService.findDtoAll();
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @PostMapping
    public EvaluerDto creer(@RequestBody EvaluerDto evaluerDto) {
        return evaluerService.creer(evaluerDto);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/moyenne-utilisation")
    public Double moyenneUtilisation() {
        return evaluerService.moyenneUtilisation();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/moyenne-besoin")
    public Double moyenneBesoin() {
        return evaluerService.moyenneBesoin();
    }

}
