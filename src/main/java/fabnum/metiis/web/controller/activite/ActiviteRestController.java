package fabnum.metiis.web.controller.activite;


import fabnum.metiis.config.Tools;
import fabnum.metiis.config.security.IsActivityAllowed;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.activite.*;
import fabnum.metiis.dto.rh.NombrePaxDto;
import fabnum.metiis.dto.rh.OrganismePartageDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.CsvService;
import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController()
@RequestMapping("/v1/activite")
@AllArgsConstructor
public class ActiviteRestController {

    @Getter
    private final ActiviteService service;
    private final CsvService csvService;
    private final UtilisateurService utilisateurService;

    @Secured({Roles.Code.EMPLOYEUR})
    @PostMapping
    public ActiviteDto ajouter(@RequestBody ActiviteDto activiteDto) {
        return service.creer(activiteDto, SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsActivityAllowed
    @PatchMapping("/{id}")
    public ActiviteDto modifierPartiellement(@PathVariable("id") Long idActivite, @RequestBody ActiviteDto activiteDto) {
        return service.modifierPartiellement(idActivite, activiteDto);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @IsActivityAllowed
    @PutMapping("/{id}/full")
    public ActiviteDto modifier(@PathVariable("id") Long idActivite, @RequestBody ActiviteDto activiteDto) {
        return service.modifier(idActivite, activiteDto);
    }


    @Secured(Roles.Code.EMPLOYEUR)
    @GetMapping("/{id}/disponibilite")
    public ListFilterCorps listePersonnelDisponiblePourUneActivite(@PathVariable("id") Long idActivite,
                                                                   @RequestParam(name = "idCorps", required = false) Long idCorps,
                                                                   @RequestParam(name = "idGroupe", required = false) Long idGroupePersonnaliser) {
        return service.listePersonnelDisponiblePourUneActiviteFiltrer(idActivite, idCorps, idGroupePersonnaliser);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{id}/statut/{idStatut}")
    public ListFilterCorps personnelsTrierParStatutPourUneActivite(@PathVariable("id") Long idActivite,
                                                                   @PathVariable("idStatut") Long idStatut,
                                                                   @RequestParam(name = "idCorps", required = false) Long idCorps,
                                                                   @RequestParam(name = "idGroupe", required = false) Long idGroupePersonnaliser) {
        return service.personnelsTrierParStatutPourUneActivite(idActivite, idStatut, idCorps, idGroupePersonnaliser);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{id}/details-participation-pop-up")
    public List<StatutParticipantDto> detailsParticipations(@PathVariable("id") Long idActivite) {
        return service.getListeForDetailsParticipation(idActivite);
    }


    @Secured(Roles.Code.EMPLOYEUR)
    @GetMapping("/{id}/commentaire/employeur")
    public List<UtilisateurActiviteDetailsDto> allCommentairesEmployeur(@PathVariable("id") Long idActivite) {
        return service.getListeCommentaire(idActivite);
    }

    @Secured(Roles.Code.EMPLOYE)
    @GetMapping("/{id}/commentaire/employe")
    public List<UtilisateurActiviteDetailsDto> allCommentairesEmploye(@PathVariable("id") Long idActivite) {

        return service.getListeCommentaire(idActivite, SecurityUtils.getCurrentUserLogin());
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @GetMapping("/{id}/commentaire/employe-only/{idUtilisateur}/etape/{idEtape}")
    public List<UtilisateurActiviteDetailsDto> commentairesDeLemploye(@PathVariable("id") Long idActivite,
                                                                      @PathVariable("idUtilisateur") Long idUtilisateur,
                                                                      @PathVariable("idEtape") Long idEtape) {

        return service.getListeCommentaireDeLemploye(idActivite, idUtilisateur, idEtape);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/{id}/commentaire/employe/{idUtilisateur}")
    public List<UtilisateurActiviteDetailsDto> allCommentairesEmploye(@PathVariable("id") Long idActivite,
                                                                      @PathVariable("idUtilisateur") Long idUtilisateur) {

        return service.getListeCommentaire(idActivite, idUtilisateur);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @IsActivityAllowed
    @GetMapping("/{id}")
    public ActiviteDto get(@PathVariable("id") Long idActivite) {
        return service.findDtoById(idActivite);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/{token}/from-parent")
    public ActiviteDto getFromParent(@PathVariable("token") String token) {
        return service.findDtoByIdFromParent(token);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsActivityAllowed
    @GetMapping("/{id}/parent")
    public ActiviteDto getParent(@PathVariable("id") Long idActivite) {
        return service.findDtoByIdParent(idActivite);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/{id}/etapes/{idEtape}/etape-statuts")
    public List<EtapeStatutDto> etapeStatuts(@PathVariable("id") Long idActivite,
                                             @PathVariable("idEtape") Long idEtape,
                                             @RequestParam(name = "idGroupe", required = false) Long idGroupe) {
        return service.listParticipantByEtape(idActivite, idEtape, idGroupe);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/{id}/etapes/{idEtape}/{idEtapeStatut}/export-participants")
    public void etapeStatuts(@PathVariable("id") Long idActivite,
                             @PathVariable("idEtape") Long idEtape,
                             @PathVariable("idEtapeStatut") Long idEtapeStatut,
                             @RequestParam(name = "idGroupe", required = false) Long idGroupe,
                             HttpServletResponse response) {
        csvService.exportParticipants(idActivite, idEtape, idGroupe, idEtapeStatut, response);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/{id}/recherche-participant")
    public List<RechercheParticipantDto> rechercherParticipants(@PathVariable("id") Long idActivite,
                                                                @RequestParam("search") String search) {
        return service.rechercherParticipants(idActivite, search);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsActivityAllowed
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idActivite, @RequestParam(name = "commentaire", required = false) String commentaire) {
        service.supprimer(idActivite, commentaire);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping
    public Page<ActiviteDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsActivityAllowed
    @GetMapping("/{id}/enfants")
    public List<ActiviteDto> getEnfants(@PathVariable("id") Long idActivite) {
        return service.getEnfants(idActivite);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsActivityAllowed
    @PostMapping("/{id}/partager")
    public ActiviteDto partager(@PathVariable("id") Long idActivite,
                                @RequestBody List<OrganismePartageDto> organismesDto) {
        return service.partager(idActivite, organismesDto, SecurityUtils.getCurrentUserLogin());
    }

    /**
     * Compte le nombre de pax par unite
     *
     * @param idOrganisme
     * @return
     */
    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/organisme/{idOrganisme}")
    public NombrePaxDto nombrePaxUnite(@PathVariable("idOrganisme") Long idOrganisme) {
        return service.compteurNombrePax(idOrganisme);
    }
}
