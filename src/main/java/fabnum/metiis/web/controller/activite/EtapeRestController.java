package fabnum.metiis.web.controller.activite;


import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.activite.EtapeDto;
import fabnum.metiis.services.activite.EtapeService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/etape")
@AllArgsConstructor
public class EtapeRestController {

    private EtapeService service;

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/employeur")
    public List<EtapeDto> allEmployeur() {
        return service.findDtoAllForEmployeurAction();
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/employe")
    public List<EtapeDto> allEmploye() {
        return service.findDtoAllForEmployer();
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping
    public List<EtapeDto> all() {
        return service.findDtoAll();
    }

    @Secured(Roles.Code.ADMIN)
    @PutMapping("/{id}")
    public EtapeDto modifier(@PathVariable("id") Long idEtape, @RequestBody EtapeDto etapeDto) {
        return service.modifier(idEtape, etapeDto);
    }

    @Secured(Roles.Code.ADMIN)
    @GetMapping("/{id}")
    public EtapeDto get(@PathVariable("id") Long id) {
        return service.findDtoByIdOnly(id);
    }

}
