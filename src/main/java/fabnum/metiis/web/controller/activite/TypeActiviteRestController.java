package fabnum.metiis.web.controller.activite;


import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.activite.TypeActiviteDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.activite.TypeActiviteService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/type-activite")
@AllArgsConstructor
public class TypeActiviteRestController {

    private TypeActiviteService service;

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/all")
    public List<TypeActiviteDto> all() {
        return service.findDtoAll();
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/all-order")
    public List<TypeActiviteDto> allOder() {
        return service.findDtoAllOder();
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/{id}")
    public TypeActiviteDto get(@PathVariable("id") Long id) {
        return service.findDtoByIdOnly(id);
    }

    @Secured({Roles.Code.ADMIN})
    @PostMapping
    public TypeActiviteDto creerFromDto(@RequestBody TypeActiviteDto typeActiviteDto) {
        return service.creerFromDto(typeActiviteDto);
    }

    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idTypeActivite) {
        service.delete(idTypeActivite);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.ADMIN})
    @PutMapping("/{id}")
    public TypeActiviteDto modifierFromDto(@PathVariable("id") Long idTypeActivite,
                                           @RequestBody TypeActiviteDto typeActiviteDto) {
        return service.modifierFromDto(idTypeActivite, typeActiviteDto);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping
    public Page<TypeActiviteDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }
}
