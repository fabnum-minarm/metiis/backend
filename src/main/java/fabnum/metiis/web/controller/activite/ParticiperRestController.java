package fabnum.metiis.web.controller.activite;


import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.activite.ChoisirDto;
import fabnum.metiis.dto.activite.IndisponibiliteActiviteDto;
import fabnum.metiis.dto.activite.ParticiperDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.activite.ParticiperService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/participer")
@AllArgsConstructor
public class ParticiperRestController {

    private final ParticiperService service;

    @Secured({Roles.Code.ADMIN})
    @PostMapping("/creation")
    public ParticiperDto creer(@RequestParam(name = "idActivite") Long idActivite,
                               @RequestParam(name = "idUtilisateur") Long idUtilisateur) {
        return service.creerParticipation(idActivite, idUtilisateur);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @PostMapping
    public InfoServiceDto participer(@RequestBody ChoisirDto choisir) {
        service.choisir(choisir);
        return InfoServiceDto.ok(Tools.message("default.succes"));
    }

    @Secured({Roles.Code.EMPLOYE})
    @PostMapping("/mon-choix")
    public InfoServiceDto maParticipation(@RequestBody ChoisirDto choisir) {
        service.choisirEmploye(choisir);
        return InfoServiceDto.ok(Tools.message("default.succes"));
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/{id}")
    public ParticiperDto get(@PathVariable("id") Long idParticiper) {
        return service.findDtoById(idParticiper);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/activite/{idActivite}/autres-activites-utilisateur/{idUtilisateur}")
    public List<IndisponibiliteActiviteDto> get(@PathVariable("idActivite") Long idActivite, @PathVariable("idUtilisateur") Long idUtilisateur) {
        return service.searchIndisponibiliteInOtherActivite(idActivite, idUtilisateur);
    }

    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idParticiper) {
        service.delete(idParticiper);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/activite/{idActivite}/utilisateur/{idUtilisateur}")
    public ParticiperDto getParticiaptionUtilisateurByActivite(@PathVariable("idActivite") Long idActivite,
                                                               @PathVariable("idUtilisateur") Long idUtilisateur) {
        return service.findDtoByIdActiviteAndIdUtilisateur(idActivite, idUtilisateur);
    }


}
