package fabnum.metiis.web.controller.nouveaute;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.nouveaute.NouveauteDto;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.nouveaute.NouveauteService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("/v1/nouveautes")
@AllArgsConstructor
public class NouveauteRestController {

    private final NouveauteService service;

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/me")
    public List<NouveauteDto> nouveautes() {
        return service.findListDtoByUtilisateurLogin(SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/read-all")
    public void readAll() {
        service.readAll(SecurityUtils.getCurrentUserLogin());
    }

}
