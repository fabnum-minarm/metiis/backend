package fabnum.metiis.web.controller.administration;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.administration.UserCompteActifDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.administration.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Controller REST permettant la gestion des utilisateurs.
 */
@RestController()
@RequestMapping("/v1/utilisateurs")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    /**
     * Récupération de l'ensemble des users.
     *
     * @return List
     */
    @Secured({Roles.Code.ADMIN})
    @GetMapping
    public List<User> all(@RequestParam(value = "filters", required = false) String filters) {
        return userService.findAll(filters);
    }

    /**
     * Récupération de l'ensemble des users.
     *
     * @param filters {@link String} : critère de recherche/filtrage.
     * @return List
     */
    @Secured({Roles.Code.ADMIN})
    @GetMapping("/lazy")
    public Page<User> all(@RequestParam(value = "filters", required = false) String filters, Pageable pageable) {
        return userService.findAll(filters, pageable);
    }

    /**
     * Permet de rechercher un utilisateur.
     *
     * @param id {@link Long} identifiant de l'utilisateur.
     * @return Renvoi une exception dans le cas où l'utilisateur n'existe pas (erreur 404, doc Cadre API)
     */
    @Secured({Roles.Code.ADMIN})
    @GetMapping("/{id}")
    public User get(@PathVariable("id") Long id) {
        return userService.findById(id);
    }


    /**
     * Pemret de supprimer un utilisateur.
     *
     * @param id {@link Long} : l'identifiant du user à supprimer.
     * @return InfoServiceDto
     */
    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long id) {
        userService.delete(id);

        return InfoServiceDto.ok(Tools.message("user.deleted"));
    }

    /**
     * Permet de récupérer l'ensemble des roles de l'application.
     *
     * @return tableau des roles.
     */
    @Secured({Roles.Code.ADMIN})
    @GetMapping("/roles")
    public Roles[] getRoles() {
        return Roles.values();
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/comptes-actifs")
    public Page<UserCompteActifDto> comptesActifs(Pageable pageable) {
        return userService.getComptesActifs(pageable);
    }
}
