package fabnum.metiis.web.controller.administration;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.administration.User;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.administration.ProfilService;
import fabnum.metiis.services.administration.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/v1/profil")
@AllArgsConstructor
public class ProfilController {

    private UserService userService;
    private ProfilService service;

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/{id}/utilisateurs")
    public List<User> allUserByProfil(@PathVariable("id") Long id) {
        return userService.findByProfil(id);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/full")
    public List<ProfilDto> full() {
        return service.findDtoAll();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping
    public Page<ProfilDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/{id}")
    public ProfilDto get(@PathVariable("id") Long id) {
        return service.findDtoByIdOnly(id);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/default")
    public ProfilDto getDefaultWithRoles() {
        return service.getDefaultDtoWithRoles();
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/default-simple")
    public ProfilDto getDefault() {
        return service.getDefaultDto();
    }

    @Secured(Roles.Code.ADMIN)
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idProfil) {
        service.delete(idProfil);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured(Roles.Code.ADMIN)
    @PostMapping
    public ProfilDto creerFromDto(@RequestBody ProfilDto profilDto) {
        return service.creerFromDto(profilDto);
    }

    @Secured(Roles.Code.ADMIN)
    @PutMapping("/{id}")
    public ProfilDto modifierFromDto(@PathVariable("id") Long idProfil,
                                     @RequestBody ProfilDto profilDto) {
        return service.modifierFromDto(idProfil, profilDto);
    }
}
