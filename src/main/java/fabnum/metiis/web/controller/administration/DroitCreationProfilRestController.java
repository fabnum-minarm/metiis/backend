package fabnum.metiis.web.controller.administration;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.administration.DroitCreationProfilDto;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.administration.DroitCreationProfilService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/droit-creation-profil")
@AllArgsConstructor
public class DroitCreationProfilRestController {

    private DroitCreationProfilService service;

    @Secured({Roles.Code.ADMIN})
    @GetMapping
    public List<DroitCreationProfilDto> all() {
        return service.findAllDto();
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/all-createur/{idCreateur}")
    public List<DroitCreationProfilDto> allCreateur(@PathVariable("idCreateur") Long idCreateur) {
        return service.findAllCreateurDto(idCreateur);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/all-profils-with-droit")
    public List<ProfilDto> allWithDroit() {
        return service.allProfilWithDroit();
    }

    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{idCreateur}/{idCree}")
    public InfoServiceDto delete(@PathVariable("idCreateur") Long idCreateur, @PathVariable("idCree") Long idCree) {
        service.suprimer(idCreateur, idCree);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.ADMIN})
    @PostMapping
    public DroitCreationProfilDto creer(@RequestBody DroitCreationProfilDto droitCreationProfilDto) {
        return service.creer(droitCreationProfilDto);
    }

}
