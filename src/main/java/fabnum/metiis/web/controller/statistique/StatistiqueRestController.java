package fabnum.metiis.web.controller.statistique;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.statistique.OrganismeStatistiqueCountDto;
import fabnum.metiis.dto.statistique.StatistiqueGeneralDto;
import fabnum.metiis.dto.statistique.StatistiqueUtilisateurDTO;
import fabnum.metiis.dto.statistique.UtilisateurStatistiqueCountDto;
import fabnum.metiis.services.rh.OrganismeService;
import fabnum.metiis.services.rh.UtilisateurService;
import fabnum.metiis.services.statistique.StatistiqueService;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/v1/statistiques")
@AllArgsConstructor
public class StatistiqueRestController {

    private final OrganismeService organismeService;

    private final UtilisateurService utilisateurService;

    private final StatistiqueService statistiqueService;

    @Secured({Roles.Code.ADMIN})
    @GetMapping("organisme/entre/{dateDebut}/{dateFin}")
    public List<OrganismeStatistiqueCountDto> statistiqueDipos(@PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                                               @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin
    ) {
        return organismeService.getStatistiqueOrganismeDispo(dateDebut, dateFin);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("organisme/{idOrganisme}/entre/{dateDebut}/{dateFin}")
    public List<UtilisateurStatistiqueCountDto> statistiqueDipos(
            @PathVariable("idOrganisme") Long idOrganisme,
            @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
            @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin
    ) {
        return utilisateurService.getStatistiqueByOrganisme(idOrganisme, dateDebut, dateFin);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("dernieres-connexions")
    public List<UtilisateurStatistiqueCountDto> dernieresConnexion() {
        return utilisateurService.dernieresConnexion();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("generales")
    public StatistiqueGeneralDto statistiqueDipos() {
        return utilisateurService.getStatistique();
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL, Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("utilisateur/{idUtilisateur}/entre/{dateDebut}/{dateFin}")
    public StatistiqueUtilisateurDTO statistiqueUtilisateur(
            @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
            @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin,
            @PathVariable("idUtilisateur") Long idUtilisateur) {
        return statistiqueService.statistiqueUtilisateur(dateDebut, dateFin, idUtilisateur);
    }
}
