package fabnum.metiis.web.controller.sondage;

import fabnum.metiis.constante.Order;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.sondage.ReponseSondageDto;
import fabnum.metiis.dto.sondage.SondageDto;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.sondage.SondageService;
import fabnum.metiis.services.sondage.SondageUtilisateurService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/v1/sondage")
@AllArgsConstructor
public class SondageRestController {

    private SondageUtilisateurService sondageUtilisateurService;
    private SondageService sondageService;

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR, Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @PostMapping
    public void creer(@RequestBody SondageDto sondageDto) {
        sondageUtilisateurService.creer(sondageDto, SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR, Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @GetMapping
    public SondageDto dernierSondage() {
        return sondageService.getDernierSondageDto();
    }

    @Secured(Roles.Code.ADMIN)
    @GetMapping("reponse-sondage")
    public Page<ReponseSondageDto> allDashboardSort(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(defaultValue = "ut.nom") String sortBy,
            @RequestParam(defaultValue = Order.ASC) String order,
            @RequestParam(defaultValue = "") String filtre,
            @RequestParam(defaultValue = "") Long idOrganisme,
            @RequestParam(name = "reponse", required = false) String reponse) {
        return sondageUtilisateurService.findDtoDashboardAll(idOrganisme, page, size, sortBy, order, filtre, reponse);
    }

    @Secured(Roles.Code.ADMIN)
    @GetMapping("reponses-possibles")
    public List<String> reponsesPossibles() {
        return sondageUtilisateurService.getDistintReponses();
    }
}
