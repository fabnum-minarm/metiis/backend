package fabnum.metiis.web.controller.tools;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.tools.LogFilterDto;
import fabnum.metiis.services.exceptions.ServiceException;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.springframework.http.ResponseEntity.ok;


/**
 * Service de manipulation des logs de l'application.
 */
@RestController()
@RequestMapping("/v1/logs")
public class LogsController {

    private final static String LOG_EXTENSION = ".log";

    @Value("${LOG_PATH:./logs}/")
    private String logsDir;

    @Value("${LOG_PATH:./logs}/archives/")
    private String archiveDir;

    /**
     * Méthode permettant de lister les nos des differents logs.
     *
     * @return {@link ResponseEntity} : réponse au format collection de string.
     * @throws IOException .
     */
    @Secured({Roles.Code.ADMIN})
    @GetMapping("/names")
    public ResponseEntity<Collection<String>> names() throws IOException {
        // Liste des noms des fichiers (sans extension) à retourner.
        List<String> names = new ArrayList<>();
        // Récupération des fichiers dans le .
        DirectoryStream<Path> paths = Files.newDirectoryStream(Paths.get(logsDir), "*".concat(LOG_EXTENSION));
        paths.forEach(path -> {
            // Recupération du nom dans le cas où le chemin représente un fichier.
            if (Files.isRegularFile(path)) {
                names.add(path.toFile().getName().replaceAll(LOG_EXTENSION, ""));
            }
        });
        paths.close();
        return ok(names);
    }

    /**
     * Méthode de récupération des niveaux de logs.
     *
     * @return {@link ResponseEntity} : réponse sous forme de tableau de {@link Level} (JSON : tableau de String correpondant au name() de chaque occurence).
     */
    @Secured({Roles.Code.ADMIN})
    @GetMapping("/levels")
    public ResponseEntity<Level[]> levels() {
        return ok(Level.values());
    }

    /**
     * Méthode de récupération des logs.
     *
     * @param name    {@link String} : nom du log (cf. LogsController.names()). Paramètre obligatoire.
     * @param filters {@link LogFilterDto} : Ensemble des filtres permmetant de restreindre la recherche des logs
     * @return {@link ResponseEntity} réponse sous forme de Collection de Map (JSON : tableau sous forme de clé/valeur).
     * @throws IOException .
     */
    @Secured({Roles.Code.ADMIN})
    @PostMapping("/{name}")
    public ResponseEntity<Collection<Map<String, Object>>> getLogs(@PathVariable(name = "name") String name,
                                                                   @RequestBody LogFilterDto filters) throws IOException {
        // Liste des noms des fichiers (sans extension) à retourner.
        List<Path> path = new ArrayList<>();
        List<Map<String, Object>> lines = new ArrayList<>();
        // Récupération des fichiers dans le .

        Path directory = Paths.get(filters.isArchives() ? archiveDir : logsDir);

        if (!directory.toFile().exists()) {
            if (!filters.isArchives()) {
                throw new ServiceException(Tools.message("log.dir.notExist"));
            }
        } else {
            DirectoryStream<Path> paths = Files.newDirectoryStream(directory, name.concat("*").concat(LOG_EXTENSION));
            paths.forEach(path::add);
            paths.close();

        }

        for (Path p : path) {
            if (Files.isRegularFile(p)) {
                for (String l : Files.readAllLines(p, Charset.defaultCharset())) {
                    Map<String, Object> json = JsonParserFactory.getJsonParser().parseMap(l);
                    if (Optional.ofNullable(filters.getLevel()).isPresent() &&
                            !filters.getLevel().name().equals(json.get(LogFilterDto.LEVEL_NAME))) {
                        continue;
                    }

                    LocalDateTime timestamp = LocalDateTime.parse(
                            json.get(LogFilterDto.TIMESTAMP_NAME).toString(),
                            DateTimeFormatter.ofPattern(LogFilterDto.TIMESTAMP_PATTERN));

                    if (Optional.ofNullable(filters.getStartTime()).isPresent() &&
                            !filters.getStartTime().isBefore(timestamp)) {
                        continue;
                    }

                    if (Optional.ofNullable(filters.getEndTime()).isPresent() &&
                            !filters.getEndTime().isAfter(timestamp)) {
                        continue;
                    }
                    json.replace(LogFilterDto.TIMESTAMP_NAME, timestamp.toInstant(ZoneOffset.UTC));
                    lines.add(json);
                }
            }
        }

        boolean descending = Optional.of(filters.isDescending()).orElse(true);
        lines.sort(new LogFilterDto.LogMapComparator(!descending));

        return ok(lines);
    }
}