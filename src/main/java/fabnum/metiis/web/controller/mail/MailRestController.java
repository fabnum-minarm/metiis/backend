package fabnum.metiis.web.controller.mail;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.domain.rh.Utilisateur;
import fabnum.metiis.services.mail.MailService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController()
@RequestMapping("/v1/mail")
@AllArgsConstructor
public class MailRestController {

    protected MailService service;
    protected UtilisateurService utilisateurService;

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/test1")
    public void sendTest1() {
        Utilisateur utilisateur = utilisateurService.findById(1L);
        //service.mail(utilisateur, HTMLMail.Object.BIENVENUE, HTMLMail.Message.COMPTE_ACTIVE, HTMLMail.Boutton.ACCEDER, "");
        service.envoyerMailConfirmationInscription(utilisateur);
        service.envoyerMailCompteBloque(utilisateur);
    }

}
