package fabnum.metiis.web.controller.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.security.IsUtilisateurAllowed;
import fabnum.metiis.constante.Order;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.AuthenticationRequestDto;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.IndisponibiliteActiviteDto;
import fabnum.metiis.dto.activite.MaParticipationDto;
import fabnum.metiis.dto.activite.MoisActivite;
import fabnum.metiis.dto.administration.ChangePasswordDto;
import fabnum.metiis.dto.administration.ProfilDto;
import fabnum.metiis.dto.alerte.AlertesDto;
import fabnum.metiis.dto.disponibilite.DisponibiliteDto;
import fabnum.metiis.dto.rh.*;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.activite.ParticiperService;
import fabnum.metiis.services.administration.ProfilService;
import fabnum.metiis.services.alerte.AlerteService;
import fabnum.metiis.services.disponibilite.DisponibiliteService;
import fabnum.metiis.services.rh.GroupePersonnaliserService;
import fabnum.metiis.services.rh.UtilisateurService;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController()
@RequestMapping("/v1/utilisateur")
@AllArgsConstructor
public class UtilisateurRestController {

    @Getter
    private final UtilisateurService service;

    private final DisponibiliteService disponibiliteService;

    private final ActiviteService activiteService;
    private final AlerteService alerteService;
    private final ParticiperService participerService;

    private final ProfilService profilService;

    private final GroupePersonnaliserService groupePersonnaliserService;

    @Secured(Roles.Code.ADMIN)
    @GetMapping("all")
    public List<UtilisateurCompletDto> all() {
        return service.findDtoAll();
    }

    @Secured(Roles.Code.ADMIN)
    @GetMapping
    public Page<UtilisateurCompletDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @PostMapping
    public UtilisateurCompletDto creerFromDto(@RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        return service.creerFromUtilisateurCompletDto(utilisateurCompletDto);
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @PatchMapping
    public UtilisateurCompletDto modifierPatiellement(@RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        return service.modifierPartiellement(utilisateurCompletDto);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @IsUtilisateurAllowed
    @PutMapping("/{idUtilisateur}")
    public UtilisateurCompletDto modifierfromUtilisateurCompletDto(@PathVariable("idUtilisateur") Long idUtilisateur,
                                                                   @RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        return service.modifierFromUtilisateurCompletDto(idUtilisateur, utilisateurCompletDto);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @IsUtilisateurAllowed
    @PutMapping("/cloturer/{idUtilisateur}")
    public UtilisateurCompletDto cloturerAffectationUtilisateurCompletDto(@PathVariable("idUtilisateur") Long idUtilisateur,
                                                                          @RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        return service.cloturerAffectationUtilisateurCompletDto(idUtilisateur, utilisateurCompletDto);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @PatchMapping("/modifier-infos-persos")
    public UtilisateurCompletDto modifierInfosPersos(@RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        return service.modifierInfosPersos(utilisateurCompletDto);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @PatchMapping("/annuler-modifier-infos-persos")
    public UtilisateurCompletDto annulerModifierInfosPersos() {
        return service.annulerModifierInfosPersos();
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @PatchMapping("/renvoyer-modifier-infos-persos")
    public UtilisateurCompletDto renvoyerModifierInfosPersos() {
        return service.renvoyerModifierInfosPersos();
    }

    //doit être accessible à tous.
    @GetMapping("/infos-persos/{token}")
    public ChangementMotDePasseDto getModifierInfosPerso(@PathVariable("token") String token) {
        return service.getModifInfosPerso(token);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @IsUtilisateurAllowed
    @DeleteMapping("/{idUtilisateur}")
    public InfoServiceDto delete(@PathVariable("idUtilisateur") Long idUtilisateur) {
        service.delete(idUtilisateur);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }


    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @IsUtilisateurAllowed
    @GetMapping("/{idUtilisateur}")
    public UtilisateurCompletDto get(@PathVariable("idUtilisateur") Long idUtilisateur) {
        return service.findDtoById(idUtilisateur);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsUtilisateurAllowed
    @GetMapping("/{idUtilisateur}/with-my-groupe")
    public UtilisateurCompletDto getWithMyGroupe(@PathVariable("idUtilisateur") Long idUtilisateur) {
        return service.findDtoByIdWithMyGroupe(idUtilisateur);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/me")
    public UtilisateurCompletDto currentUser() {
        return service.findDtoByUserName(SecurityUtils.getCurrentUserLogin());
    }


    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{idUtilisateur}/activite")
    public Page<ActiviteDto> activitesOrganismeByUtilisateur(@PathVariable("idUtilisateur") Long idUtilisateur,
                                                             @RequestParam(name = "idTypeActivite", required = false) List<Long> idsTypeActivite,
                                                             Pageable pageable) {

        return activiteService.findDtoAllByIdUtilisateur(idUtilisateur, idsTypeActivite, pageable);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/{idUtilisateur}/activite-participation")
    public Page<ActiviteDto> activitesParticipations(@PathVariable("idUtilisateur") Long idUtilisateur, Pageable pageable) {
        return activiteService.findDtoActiviteWithParticipation(idUtilisateur, pageable);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{id}/activite-participation/entre/{dateDebut}/{dateFin}")
    public Page<ActiviteDto> activitesParticipations(@PathVariable("id") Long idUtilisateur,
                                                     @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                                     @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin,
                                                     Pageable pageable) {
        return activiteService.findDtoActiviteWithParticipationEntre(idUtilisateur, dateDebut, dateFin, pageable);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{idUtilisateur}/disponibilite/entre/{dateDebut}/{dateFin}")
    public List<DisponibiliteDto> disponibiliteEntre(@PathVariable("idUtilisateur") Long idUtilisateur,
                                                     @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                                     @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin
    ) {

        return disponibiliteService.getListByIdUtilisateurBeetween(idUtilisateur, dateDebut, dateFin);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{idUtilisateur}/activite/entre/{dateDebut}/{dateFin}")
    public List<ActiviteDto> activitesEntre(@PathVariable("idUtilisateur") Long idUtilisateur,
                                            @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                            @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin,
                                            @RequestParam(name = "idTypeActivite", required = false) List<Long> idsTypeActivite,
                                            @RequestParam(name = "idGroupe", required = false) List<Long> idsGroupe,
                                            @RequestParam(name = "isMyActivities", required = false) Boolean isMyActivities,
                                            @RequestParam(name = "filtre", required = false) String filtre) {

        return activiteService.getListByIdUtilisateurBeetween(idUtilisateur, dateDebut, dateFin, idsTypeActivite, isMyActivities, idsGroupe, filtre);
    }

    @Secured(Roles.Code.EMPLOYE)
    @PostMapping("/disponibilite")
    public List<DisponibiliteDto> ajouterSesDisponibilites(
            @RequestBody DisponibiliteDto disponibilite) {

        return disponibiliteService.createOrUpdate(disponibilite, SecurityUtils.getCurrentUserLogin());
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @PostMapping("/valider-compte")
    public UtilisateurCompletDto validerCompte(@RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        return service.modifier(utilisateurCompletDto);
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @PostMapping("/valider")
    public InfoServiceDto valider(@RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        service.valider(utilisateurCompletDto);
        return selectMessageWithNomPrenom(utilisateurCompletDto,
                utilisateurCompletDto.getEnabled(),
                "user.account.actived",
                "user.account.disactived");

    }

    @Secured(Roles.Code.EMPLOYEUR)
    @PostMapping("/debloquer")
    public InfoServiceDto debloquer(@RequestBody UtilisateurCompletDto utilisateurCompletDto) {
        service.debloquer(utilisateurCompletDto);
        return selectMessageWithNomPrenom(utilisateurCompletDto,
                utilisateurCompletDto.getAccountNonLocked(),
                "user.account.unlocked", "user.account.locked"
        );
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @GetMapping("/creation-profils")
    public List<ProfilDto> creationProfils() {
        return profilService.profilsDtoCreableParUtilisateur(SecurityUtils.getCurrentUserLogin());
    }

    private InfoServiceDto selectMessageWithNomPrenom(UtilisateurCompletDto utilisateur, Boolean choix, String messageOk, String messageKo) {
        String message;
        if (choix) {
            message = messageOk;
        } else {
            message = messageKo;
        }
        return InfoServiceDto.ok(Tools.message(message, utilisateur.getNom(), utilisateur.getPrenom()));
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{idUtilisateur}/groupes")
    public List<GroupePersonnaliserDto> getGroupePersonnaliser(@PathVariable("idUtilisateur") Long idUtilisateur) {
        return groupePersonnaliserService.findAllDtoByIdProprietaireWithUtilisateurs(idUtilisateur);
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @PostMapping("/inscription-by-token")
    public CompteRenduInvitation inscriptionByToken(@RequestBody InscriptionByEmployeurDto inscriptionByEmployeurDto) {
        return service.inscriptionByToken(inscriptionByEmployeurDto, SecurityUtils.getCurrentUserLogin());
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @PostMapping("/renew-inscription-by-token")
    public CompteRenduInvitation renouvellementInscriptionByToken(@RequestBody List<ConfirmationInscriptionEmployeDto> confirmationInscriptionEmployeDtos) {
        return service.renouvellementInscriptionByToken(confirmationInscriptionEmployeDtos, SecurityUtils.getCurrentUserLogin());
    }

    @Secured(Roles.Code.EMPLOYEUR)
    @PostMapping("/supprimer-inscription")
    public CompteRenduInvitation supprimerInscription(@RequestBody List<ConfirmationInscriptionEmployeDto> confirmationInscriptionEmployeDtos) {
        return service.supprimerInscription(confirmationInscriptionEmployeDtos);
    }

    //doit être accessible à tous.
    @GetMapping("/confirmation-inscription/{token}")
    public ConfirmationInscriptionEmployeDto getConfirmationInscriptionEmploye(@PathVariable("token") String token) {
        return service.getConfirmationInscription(token);
    }

    //doit être accessible à tous.
    @PostMapping("/confirmation-inscription")
    public UtilisateurCompletDto postConfirmationInscriptionEmploye(@RequestBody ConfirmationInscriptionEmployeDto confirmationInscriptionEmploye) {
        return service.confirmerInscription(confirmationInscriptionEmploye);
    }

    //doit être accessible à tous.
    @PostMapping("/demande-nouveau-mdp")
    public void demandeNouveauMotDePasse(@RequestBody AuthenticationRequestDto authenticationRequestDto) {
        service.demandeNouveauMotDePasse(authenticationRequestDto);
    }

    //doit être accessible à tous.
    @GetMapping("/changement-mdp/{token}")
    public ChangementMotDePasseDto getChangementMotDePasse(@PathVariable("token") String token) {
        return service.getChangementMotDePasse(token);
    }

    //doit être accessible à tous.
    @PostMapping("/changement-mdp")
    public InfoServiceDto getChangementMotDePasse(@RequestBody ChangementMotDePasseDto confirmationInscriptionEmploye) {
        service.changerMotDePasse(confirmationInscriptionEmploye);
        return InfoServiceDto.ok(Tools.message("default.succes"));
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/alertes")
    public AlertesDto alerte(@RequestParam(name = "limit", required = false) Integer limit) {
        return alerteService.findDtoByUserNameWithNonLue(SecurityUtils.getCurrentUserLogin(), limit);
    }


    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @PutMapping("/lire-alertes")
    public void lireAlertes() {
        alerteService.lireAlertes(SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @PatchMapping("/changement-mpd-connecte")
    public InfoServiceDto changePassword(@RequestBody ChangePasswordDto changePassword) {
        service.changerMotDePasse(changePassword);
        return InfoServiceDto.ok(Tools.message("password.change", SecurityUtils.getCurrentUserLogin()));
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("{idUtilisateur}/organimes-parents-by-hierarchie")
    public List<OrganismeDto> organismesByEnfant(@PathVariable("idUtilisateur") Long idUtilisateur) {
        return service.findDtoListByEnfantOrderByHierarchieByUtilisateur(idUtilisateur);
    }


    @Secured(Roles.Code.ADMIN)
    @GetMapping("all-sort-dashboard")
    public Page<UtilisateurCompletDto> allDashboardSort(
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(defaultValue = "nom") String sortBy,
            @RequestParam(defaultValue = Order.ASC) String order,
            @RequestParam(defaultValue = "") String filtre,
            @RequestParam(name = "enabled", required = false) Boolean enabled) {
        return service.findDtoDashboardAll(page, size, sortBy, order, filtre, enabled);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/activites/{idActivite}/mes-autres-activites")
    public List<IndisponibiliteActiviteDto> mesAutresActivites(@PathVariable("idActivite") Long idActivite) {
        return participerService.searchMesIndisponibiliteInOtherActivite(idActivite);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/activites/{idActivite}/ma-participation")
    public MaParticipationDto getMaParticipationByActivite(@PathVariable("idActivite") Long idActivite) {
        return participerService.getMaParticipationByActivite(idActivite);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/{idUtilisateur}/disponibilites-annuel/{dateDebut}")
    public List<MoisActivite> entreOrganisme(@PathVariable("idUtilisateur") Long idUtilisateur,
                                             @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut) {

        return disponibiliteService.getDispoAnnuel(idUtilisateur, dateDebut);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR, Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @PatchMapping("/default-user-profil/")
    public UtilisateurCompletDto modifierUserProfil(
            @RequestBody UtilisateurCompletDto utilisateurDto) {
        return service.modifierUserProfil(utilisateurDto);
    }
}
