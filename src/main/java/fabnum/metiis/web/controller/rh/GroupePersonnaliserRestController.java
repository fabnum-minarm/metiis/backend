package fabnum.metiis.web.controller.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.rh.GroupePersonnaliserDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.services.rh.GroupePersonnaliserService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/v1/groupe-personnaliser")
@AllArgsConstructor
public class GroupePersonnaliserRestController {

    private GroupePersonnaliserService service;

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{id}")
    public GroupePersonnaliserDto get(@PathVariable("id") Long idGroupePersonnaliser) {
        return service.findDtoByIdWithUtilisateurs(idGroupePersonnaliser);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{id}/utilisateur-filter")
    public ListFilterCorps getUtilisateurGroupeFiltrer(@PathVariable("id") Long idGroupePersonnaliser,
                                                       @RequestParam(name = "idCorps", required = false) Long idCorps,
                                                       @RequestParam(name = "search", required = false) String search) {
        return service.findUtilisateurGroupeFiltrer(idGroupePersonnaliser, idCorps, search);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @PostMapping
    public GroupePersonnaliserDto creer(@RequestBody GroupePersonnaliserDto groupePersonnaliserDto) {
        return service.creerFromDto(groupePersonnaliserDto);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @PutMapping("/{id}")
    public GroupePersonnaliserDto modifier(@PathVariable("id") Long idGroupePersonnaliser, @RequestBody GroupePersonnaliserDto groupePersonnaliserDto) {
        return service.modifierFromDto(idGroupePersonnaliser, groupePersonnaliserDto);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idGroupePersonnaliser) {
        service.delete(idGroupePersonnaliser);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }


}
