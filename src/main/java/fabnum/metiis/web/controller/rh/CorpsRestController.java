package fabnum.metiis.web.controller.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.rh.CorpsDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.rh.CorpsService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController()
@RequestMapping("/v1/corps")
@AllArgsConstructor
public class CorpsRestController {

    protected CorpsService service;

    //doit être accessible à tous.
    @GetMapping("/all")
    public List<CorpsDto> all() {
        return service.findDtoAll();
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping("/{id}")
    public CorpsDto get(@PathVariable("id") Long id) {
        return service.findDtoById(id);
    }

    @Secured({Roles.Code.ADMIN})
    @PostMapping
    public CorpsDto creerFromDto(@RequestBody CorpsDto corpsDto) {
        return service.creerFromDto(corpsDto);
    }

    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idCorps) {
        service.delete(idCorps);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.ADMIN})
    @PutMapping("/{id}")
    public CorpsDto modifierFromDto(@PathVariable("id") Long idCorps,
                                    @RequestBody CorpsDto corpsDto) {
        return service.modifierFromDto(idCorps, corpsDto);
    }

    @Secured({Roles.Code.EMPLOYE, Roles.Code.EMPLOYEUR})
    @GetMapping
    public Page<CorpsDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }
}
