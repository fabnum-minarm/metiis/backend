package fabnum.metiis.web.controller.rh;

import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.rh.ConfirmationInscriptionEmployeDto;
import fabnum.metiis.dto.tools.ListConfirmationInscriptionEmployeDto;
import fabnum.metiis.services.rh.TokenUtilisateurService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/v1/token-utilisateur")
@AllArgsConstructor
public class TokenUtilisateurRestController {

    private TokenUtilisateurService service;

    @Secured(Roles.Code.ADMIN)
    @GetMapping("all")
    public ListConfirmationInscriptionEmployeDto all() {
        return service.getList();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("recherche-invitations")
    public List<ConfirmationInscriptionEmployeDto> rechercheInvitation(@RequestParam("search") String search) {
        return service.rechercheInvitation(search);
    }
}
