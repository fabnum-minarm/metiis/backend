package fabnum.metiis.web.controller.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.config.security.IsOrganismeAllowed;
import fabnum.metiis.constante.Order;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.activite.ActiviteDto;
import fabnum.metiis.dto.activite.MoisActivite;
import fabnum.metiis.dto.disponibilite.DisponibiliteOrganismeDto;
import fabnum.metiis.dto.rh.ConfirmationInscriptionEmployeDto;
import fabnum.metiis.dto.rh.OrganismeDto;
import fabnum.metiis.dto.rh.OrganismeHiearchieDto;
import fabnum.metiis.dto.rh.UtilisateurCompletDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.dto.tools.ListConfirmationInscriptionEmployeDto;
import fabnum.metiis.dto.tools.ListFilterCorps;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.CsvService;
import fabnum.metiis.services.activite.ActiviteService;
import fabnum.metiis.services.disponibilite.DisponibiliteService;
import fabnum.metiis.services.rh.OrganismeService;
import fabnum.metiis.services.rh.TokenUtilisateurService;
import fabnum.metiis.services.rh.UtilisateurService;
import fabnum.metiis.web.controller.AuthController;
import lombok.AllArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/v1/organisme")
@AllArgsConstructor
public class OrganismeRestController {

    private OrganismeService service;

    private ActiviteService activiteService;

    private DisponibiliteService disponibiliteService;

    private UtilisateurService utilisateurService;

    private CsvService csvService;

    private TokenUtilisateurService tokenUtilisateurService;

    private AuthController authController;

    @Secured({Roles.Code.ADMIN})
    @GetMapping("all")
    public List<OrganismeDto> all() {
        return service.findDtoAll();
    }


    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{idOrganisme}")
    public OrganismeDto get(@PathVariable("idOrganisme") Long idOrganisme) {
        return service.findDtoById(idOrganisme);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @IsOrganismeAllowed
    @GetMapping("/{idOrganisme}/activite/entre/{dateDebut}/{dateFin}")
    public List<ActiviteDto> entreOrganisme(@PathVariable("idOrganisme") Long idOrganisme,
                                            @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                            @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin,
                                            @RequestParam(name = "idTypeActivite", required = false) List<Long> idsTypeActivite,
                                            @RequestParam(name = "idGroupe", required = false) List<Long> idsGroupe,
                                            @RequestParam(name = "filtre", required = false) String filtre) {
        return activiteService.getListByIdOrganismeBeetween(idOrganisme, dateDebut, dateFin, idsTypeActivite, idsGroupe, filtre);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{idOrganisme}/activite-annuel/{dateDebut}")
    public List<MoisActivite> entreOrganisme(@PathVariable("idOrganisme") Long idOrganisme,
                                             @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                             @RequestParam(name = "idTypeActivite", required = false) List<Long> idsTypeActivite) {

        return activiteService.getActiviteEtDispoAnnuel(idOrganisme, dateDebut, idsTypeActivite);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @IsOrganismeAllowed
    @GetMapping("/{idOrganisme}/disponibilite/entre/{dateDebut}/{dateFin}")
    public List<DisponibiliteOrganismeDto> entreOrganisme(@PathVariable("idOrganisme") Long idOrganisme,
                                                          @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                                          @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin

    ) {
        return disponibiliteService.getListResumeByIdOrganismeBeetween(idOrganisme, dateDebut, dateFin);
    }


    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("/{idOrganisme}/disponibilite-complete/entre/{dateDebut}/{dateFin}")
    public ListFilterCorps listePersonnelDisponiblePourUneActivite(@PathVariable("idOrganisme") Long idOrganisme,
                                                                   @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                                                   @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin,
                                                                   @RequestParam(name = "idCorps", required = false) Long idCorps) {

        return activiteService.listePersonnelDisponiblePourUneActiviteFiltrer(idOrganisme, dateDebut, dateFin, idCorps);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("/{idOrganisme}/export-disponibilite-complete/entre/{dateDebut}/{dateFin}")
    public void exportDisponibiliteComplete(@PathVariable("idOrganisme") Long idOrganisme,
                                            @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                            @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin,
                                            @RequestParam(name = "idCorps", required = false) Long idCorps,
                                            HttpServletResponse response) {

        csvService.exportDisponibiliteComplete(idOrganisme, dateDebut, dateFin, idCorps, response);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("/{idOrganisme}/export-disponibilite/entre/{dateDebut}/{dateFin}")
    public void exportDisponibilite(@PathVariable("idOrganisme") Long idOrganisme,
                                    @PathVariable("dateDebut") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateDebut,
                                    @PathVariable("dateFin") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) String dateFin,
                                    HttpServletResponse response) {

        csvService.exportDisponibilite(idOrganisme, dateDebut, dateFin, response);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @IsOrganismeAllowed
    @GetMapping("/{idOrganisme}/activite")
    public Page<ActiviteDto> all(@PathVariable("idOrganisme") Long idOrganisme,
                                 @RequestParam(name = "idTypeActivite", required = false) List<Long> idsTypeActivite,
                                 Pageable pageable) {

        return activiteService.findDtoAllByIdOrganisme(idOrganisme, idsTypeActivite, pageable);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @PostMapping
    public ResponseEntity<Object> creerFromDto(@RequestBody OrganismeDto organismeDto) {
        organismeDto = service.creerFromDto(organismeDto);
        ResponseEntity<Object> responseEntity = authController.getOrganismeDtoTokenResponse(organismeDto);
        LoggerFactory.getLogger("account").info(Tools.message("renew.log", SecurityUtils.getClientIp(),
                SecurityUtils.getCurrentUserLogin(), Tools.message("renew.ok")));
        return responseEntity;
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @IsOrganismeAllowed
    @PutMapping("/{idOrganisme}")
    public OrganismeDto modifier(@PathVariable("idOrganisme") Long idOrganisme, @RequestBody OrganismeDto organismeDto) {
        return service.modifier(idOrganisme, organismeDto);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @IsOrganismeAllowed
    @DeleteMapping("/{idOrganisme}")
    public InfoServiceDto delete(@PathVariable("idOrganisme") Long idOrganisme) {
        service.delete(idOrganisme);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping
    public Page<OrganismeDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }

    @GetMapping("/type-organisme/{idTypeOrganisme}")
    public List<OrganismeDto> allByTypeOrganisme(@PathVariable("idTypeOrganisme") Long idTypeOrganisme) {
        return service.findDtoAllByTypeOrganisme(idTypeOrganisme);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("{idOrganisme}/enfants")
    public List<OrganismeDto> organismesByParent(@PathVariable("idOrganisme") Long idOrganismeParent) {
        return service.findDtoListByParent(idOrganismeParent);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @GetMapping("/enfants-by-type-organisme/{idTypeOrganisme}")
    public List<OrganismeDto> organismesByParentAndTypeOrganisme(@PathVariable("idTypeOrganisme") Long idTypeOrganisme) {
        return service.findDtoListByParentAndTypeOrganisme(SecurityUtils.getCurrentUserLogin(), idTypeOrganisme);
    }

    @Secured({Roles.Code.ADMIN_LOCAL})
    @GetMapping("/all-organismes-allowed")
    public List<OrganismeDto> organismesAllByParent() {
        return service.findDtoListAllByParent(SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("{idOrganisme}/parents-by-hierarchie")
    public List<OrganismeDto> organismesByEnfant(@PathVariable("idOrganisme") Long idOrganismeEnfant) {
        return service.findDtoListByEnfantOrderByHierarchie(idOrganismeEnfant);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/utilisateurs")
    public List<UtilisateurCompletDto> utilisateurParOrganisme(@PathVariable("idOrganisme") Long idOrganisme) {
        return utilisateurService.findDtoAllByOrganisme(idOrganisme);
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @GetMapping("{idOrganisme}/utilisateurs-sort")
    public Page<UtilisateurCompletDto> allSort(
            @PathVariable("idOrganisme") Long idOrganisme,
            @RequestParam(defaultValue = "0") Integer page,
            @RequestParam(defaultValue = "10") Integer size,
            @RequestParam(defaultValue = "nom") String sortBy,
            @RequestParam(defaultValue = Order.ASC) String order,
            @RequestParam(defaultValue = "") String filtre,
            @RequestParam(name = "enabled", required = false) Boolean enabled) {
        return utilisateurService.findDtoDashboardAll(idOrganisme, page, size, sortBy, order, filtre, enabled);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/utilisateurs-bloques")
    public ListFilterCorps utilisateurBloqueParOrganisme(@PathVariable("idOrganisme") Long idOrganisme, @RequestParam(name = "idCorps", required = false) Long idCorps) {
        return utilisateurService.findDtoAllBloqueByOrganisme(idOrganisme, idCorps);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/utilisateurs-a-valider")
    public ListFilterCorps utilisateurAValiderParOrganisme(@PathVariable("idOrganisme") Long idOrganisme, @RequestParam(name = "idCorps", required = false) Long idCorps) {
        return utilisateurService.findDtoAllAValiderByOrganisme(idOrganisme, idCorps);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/invitations")
    public ListConfirmationInscriptionEmployeDto getInvitationByOrganisme(@PathVariable("idOrganisme") Long idOrganisme) {
        return tokenUtilisateurService.getListByOrganisme(idOrganisme);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/recherche-invitations")
    public List<ConfirmationInscriptionEmployeDto> rechercheInvitation(@PathVariable("idOrganisme") Long idOrganisme, @RequestParam("search") String search) {
        return tokenUtilisateurService.rechercheInvitation(idOrganisme, search);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/utilisateurs-filter")
    public ListFilterCorps utilisateurFilterParOrganisme(@PathVariable("idOrganisme") Long idOrganisme,
                                                         @RequestParam(name = "idCorps", required = false) Long idCorps,
                                                         @RequestParam(name = "idGroupe", required = false) Long idGroupPersonaliser) {
        return utilisateurService.findFilterByOrganisme(idOrganisme, idCorps, idGroupPersonaliser);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/utilisateurs-filter/with-no-enabled")
    public ListFilterCorps utilisateurFilterParOrganismeWithNoEnabled(@PathVariable("idOrganisme") Long idOrganisme,
                                                                      @RequestParam(name = "idCorps", required = false) Long idCorps,
                                                                      @RequestParam(name = "idGroupe", required = false) Long idGroupPersonaliser) {
        return utilisateurService.findFilterByOrganismeWithNoEnabled(idOrganisme, idCorps, idGroupPersonaliser);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/utilisateurs-filter/with-my-groupe")
    public ListFilterCorps utilisateurFilterParOrganismeWithGroupe(@PathVariable("idOrganisme") Long idOrganisme,
                                                                   @RequestParam(name = "idCorps", required = false) Long idCorps,
                                                                   @RequestParam(name = "idGroupe", required = false) Long idGroupPersonaliser) {
        return utilisateurService.findFilterByOrganismeWithGroupe(idOrganisme, idCorps, idGroupPersonaliser);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/utilisateurs-filter/with-my-groupe/with-no-enabled")
    public ListFilterCorps utilisateurFilterParOrganismeWithGroupeAndNoEnabled(@PathVariable("idOrganisme") Long idOrganisme,
                                                                               @RequestParam(name = "idCorps", required = false) Long idCorps,
                                                                               @RequestParam(name = "idGroupe", required = false) Long idGroupPersonaliser) {
        return utilisateurService.findFilterByOrganismeWithGroupeAndNoEnabled(idOrganisme, idCorps, idGroupPersonaliser);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @IsOrganismeAllowed
    @GetMapping("{idOrganisme}/recherche-utilisateurs")
    public List<UtilisateurCompletDto> rechercheUnite(@PathVariable("idOrganisme") Long idOrganisme, @RequestParam("search") String search) {
        return utilisateurService.searchByOrganisme(idOrganisme, search);
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/organismes-affectable-allowed")
    public List<OrganismeDto> organismesAllowed() {
        return service.findAllOrganismesAffectableAllowed(SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/hiearchie")
    public List<OrganismeHiearchieDto> getOrganismeHiearchie() {
        return service.getAllOrganismeHiearchie(SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/organismes-allowed-partage-activite/{idActivite}")
    public List<OrganismeDto> organismesAllowedActivitePartage(@PathVariable("idActivite") Long idActivite) {
        return service.findAllOrganismesAllowedActivitePartage(idActivite);
    }
}
