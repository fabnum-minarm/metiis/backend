package fabnum.metiis.web.controller.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.rh.GradeDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.rh.GradeService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController()
@RequestMapping("/v1/grade")
@AllArgsConstructor
public class GradeRestController {

    private GradeService service;

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/all")
    public List<GradeDto> all() {
        return service.findDtoAll();
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/corps/{id}")
    public List<GradeDto> byCorps(@PathVariable("id") Long idCorps) {
        return service.findDtoAllByIdCoprs(idCorps);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping("/{id}")
    public GradeDto get(@PathVariable("id") Long id) {
        return service.findDtoByIdOnly(id);
    }

    @Secured({Roles.Code.ADMIN})
    @PostMapping
    public GradeDto creerFromDto(@RequestBody GradeDto gradeDto) {
        return service.creerFromDto(gradeDto);
    }

    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idGrade) {
        service.delete(idGrade);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.ADMIN})
    @PutMapping("/{id}")
    public GradeDto modifierFromDto(@PathVariable("id") Long idGrade,
                                    @RequestBody GradeDto gradeDto) {
        return service.modifierFromDto(idGrade, gradeDto);
    }

    @Secured({Roles.Code.ADMIN})
    @GetMapping
    public Page<GradeDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }
}
