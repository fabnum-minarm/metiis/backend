package fabnum.metiis.web.controller.rh;


import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.rh.PosteRoDto;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.services.rh.PosteRoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController()
@RequestMapping("/v1/poste-ro")
@AllArgsConstructor
public class PosteRoRestController {

    private PosteRoService service;

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{id}")
    public PosteRoDto get(@PathVariable("id") Long idPosteRo) {
        return service.findDtoById(idPosteRo);
    }

    @Secured({Roles.Code.EMPLOYEUR,})
    @PostMapping
    public PosteRoDto ajouter(@RequestBody PosteRoDto posteRo) {
        return service.creer(posteRo);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @PutMapping("/{id}")
    public PosteRoDto modifier(@PathVariable("id") Long idPosteRo, @RequestBody PosteRoDto posteRo) {
        return service.modifier(idPosteRo, posteRo);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idPosteRo) {
        service.delete(idPosteRo);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping
    public Page<PosteRoDto> all(Pageable pageable) {
        return service.findDtoAll(pageable);
    }

    @Secured({Roles.Code.EMPLOYEUR})
    @GetMapping("/full")
    public List<PosteRoDto> full() {
        return service.findDtoAll();
    }

}
