package fabnum.metiis.web.controller.rh;

import fabnum.metiis.config.Tools;
import fabnum.metiis.domain.enumerations.Roles;
import fabnum.metiis.dto.rh.TypeOrganismeDto;
import fabnum.metiis.dto.rh.TypeOrganismeDtoWithOrganisme;
import fabnum.metiis.dto.tools.InfoServiceDto;
import fabnum.metiis.security.SecurityUtils;
import fabnum.metiis.services.rh.OrganismeService;
import fabnum.metiis.services.rh.TypeOrganismeService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController()
@RequestMapping("/v1/type-organisme")
@AllArgsConstructor
public class TypeOrganismeRestController {

    protected TypeOrganismeService service;
    protected OrganismeService organismeService;

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping
    public List<TypeOrganismeDto> allDto() {
        return service.findAllDto();
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/with-first-child")
    public List<TypeOrganismeDtoWithOrganisme> allDtoWithFirsChildOrganisme() {
        return organismeService.findAllDtoTypeOrganismeWithFirsChildOrganisme();
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @GetMapping("/child-by-organisme/")
    public List<TypeOrganismeDto> allDtoByOrganismeParent() {
        return organismeService.findTypeOrganismeDtoAllByOrganismeParent(SecurityUtils.getCurrentUserLogin());
    }

    @Secured({Roles.Code.ADMIN, Roles.Code.ADMIN_LOCAL})
    @GetMapping("/type-organismes-allowed-by-profil/{codeProfil}")
    public List<TypeOrganismeDto> allDtoAllowed(@PathVariable("codeProfil") String codeProfil) {
        return organismeService.findTypeOrganismeAllowedByCodeProfil(codeProfil);
    }

    @Secured({Roles.Code.ADMIN})
    @PostMapping
    public TypeOrganismeDto creerFromDto(@RequestBody TypeOrganismeDto typeOrganismeDto) {
        return service.creerFromDto(typeOrganismeDto);
    }

    @Secured({Roles.Code.ADMIN})
    @DeleteMapping("/{id}")
    public InfoServiceDto delete(@PathVariable("id") Long idTypeOrganisme) {
        service.delete(idTypeOrganisme);
        return InfoServiceDto.ok(Tools.message("default.delete"));
    }

    @Secured({Roles.Code.EMPLOYEUR, Roles.Code.EMPLOYE})
    @GetMapping("/{id}")
    public TypeOrganismeDto get(@PathVariable("id") Long id) {
        return service.findDtoById(id);
    }

    @Secured({Roles.Code.ADMIN})
    @PutMapping("/{id}")
    public TypeOrganismeDto modifier(@PathVariable("id") Long idTypeOrganisme,
                                     @RequestBody TypeOrganismeDto typeOrganismeDto) {
        return service.modifierFromDto(idTypeOrganisme, typeOrganismeDto);
    }
}
