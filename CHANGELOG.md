# Changelog

## 1.9.1
### Fixes
* Mise à jour des librairies et suppression de code changelog [#321](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/321)

## 1.9.0
### Features
* Voir les effectifs d'un groupe sélectionné sur mon calendrier [#306](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/306)
### Fixes
* Adapter les stats aux personnels qui ont connus plusieurs affectations [#308](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/308)
* wording [#307](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/307)


## 1.8.0
### Features
* Ajout de sondages [#305](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/305)
### Fixes

## 1.7.0
### Features
* Partage d'activités inter-unités [#297](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/297)
### Fixes

## 1.6.0
### Features
* Annuler sa participation depuis "retenu" et recandidater après [#291](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/291)
* Ajout du script mvnw pour le déploiement [#290](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/290)
* Annuler sa candidature et candidater à nouveau [#282](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/282)
* Amélioration du dashboard organismes et utilisateurs [#277](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/277)
### Fixes
* bug recherche de participants [#293](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/293)
* bug à la modification des profils [#292](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/292)
* Annuler sa participation au stade "retenu" même si convoqué sur une seconde activité [#281](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/281)
* Seuls les employés sont visible dans gestion des participants [#279](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/279)

## 1.5.0
### Features
* API statistiques : ajout du nombre de convocations et du nombre de candidatures [#271](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/271)
* Autoriser les employeurs à modifier les noms/prénoms des pax [#270](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/270)
### Fixes
* Ajout d'un ordre pour les types d'activités [#272](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/272)
* Remettre à zéro les notifications en cas de changement d'unité [#269](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/269)

##1.4.0
### Features
* Rendre un pax actif/inactif pour les CDU [#258](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/258)
* Ajout d'un profil super employé pour les chefs de groupe [#252](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/252)
### Fixes
* Nous contacter [#263](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/263)
* fix les comptes temporaires des invitations supprimées n'étaient pas supprimées. [#262](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/262)
* Pas d'enregistrement des contacts en bdd si pas connecté [#255](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/255)


## 1.3.0
### Features
* Export des participants : ajout des groupes et des commentaires [#251](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/251)
* Mécanisme de changement d'e-mail [#247](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/247)
* statistiques personnel [#246](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/246)
* Détail d'un pax onglet activité : ajout d'un filtre 'Activités réalisées' [#243](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/243)
* Détail d'un pax onglet activité : ajout d'un filtre sur les dates [#243](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/243)
* Détail d'un pax onglet disponibilités : ajout d'un calendrier annuel [#243](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/243)
### Fixes
* Ajout des commentaires à l'export des disponibilités [#257](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/257)
* Mécanisme de changement d'e-mail [#256](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/256)
* export participants ajout groupes et commentaires [#254](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/254)
* Supprimer des invitations envoyées [#250](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/250)
* Exporter les commentaires des disponibilités [#249](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/249)
* wording [#245](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/245)
* Get activité : paramétrage rappel [#244](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/244)
* Gestion des utilisateurs invités mais non inscrit [#242](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/242)
* Ajouter les coordonnées de l'usager dans les mails reçus via le formulaire de contact. [#241](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/241)

## 1.2.0
### Features
* Pax retenu sur plusieurs activités [#225](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/225)
* Ajout d'un rappel de participation [#223](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/223)
* filtre des groupes sur les disponibilité [#215](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/215)
* filtre des groupes sur les pax [#215](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/215)
* Ajout d'un administrateur local [#178](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/178)
### Fixes
* API get activité [#238](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/238)
* fix message en français [#236](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/236)
* ActiviteSimpleDto : ajout des dates d'activité dans le json [#235](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/235)
* modification d'un utilisateur clôturé par super admin [#234](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/234)
* Sécurité accès utilisateurs [#233](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/233)
* pax retenu sur plusieurs activités [#232](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/232)
* compilation error [#231](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/231)
* Suppression en cascade des alertes à la suppression d'un utilisateur [#230](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/230)
* SSI : n'accéder qu'aux utilisateurs autorisés par son profil et son affectation [#229](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/229)
* SSI : n'accéder qu'aux activités autorisées par son profil et son affectation [#228](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/228)
* Validation des emails [#226](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/226)
* Messages en français [#224](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/224)
* Problèmes d'accents dans les messages [#221](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/221)
* Rédaction des mails [#220](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/220)
* Suppression des alertes en anglais [#219](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/219)
* Wording [#218](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/218)
* Pouvoir modifier un poste RO en champ vide [#217](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/217)
* Enlever le "autre" dans la liste des postes RO [#217](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/217)
* Mail de candidature est envoyé au candidat pas à l'employeur [#216](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/216)
* Ajout de la gestion des emails incorrectes [#214](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/214)

## 1.1.0
### Features
* ameliration des messages de contact [#208](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/208)
* Disponibilité calendrier annuelle [#207](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/207)
* Statistiques dernières connexions [#204](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/204)
* Information complémentaire sur la pop-up du calendrier [#203](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/203)
* rappel activité  [#201](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/201)
* Api : calendrier annuel [#199](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/199)
* ajout de statistiques [#198](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/198)
* Changement d'url de swagger : ~~/swagger-ui.html~~ en /swagger-ui/ [#196](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/196)
* Ajout de statistiques générales [#195](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/195)
* ameliorations des alertes [#192](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/192)
* Ajout de la gestions des nouveautés applicatives [#189](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/189)
* Mise en place de l'annulation d'une étape par l'employeur [#181](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/181)
* Verif de cumul d'activité en cours [#181](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/181)
* Pouvoir candidater / se desinscrire [#166](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/166)
* Maj Spring boot : [#154](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/154)
* Maj jackson-databind [#154](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/154)
* Maj tomcat-embed-core [#154](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/154)
* Maj jackson-dataformat-csv [#154](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/154)
### Fixes
* détails pop up en profil employé. [#211](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/211)
* envoi de mail aux personnes inscritent [#210](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/210)
* liste des commentaires utilisateur : les commentaires vides ne sont plus affichés [#209](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/209)
* Fix bug date disponibilité pouvant entrainé une erreur sur le calendrier / sur et sur les dispo a une activité [#207](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/207)
* Correction des mois en anglais sur un profil FR [#206](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/206)
* Remise en marche de swagger suite au mise à jour des librairies [#196](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/196)
## 1.0.3
### Features
* API : tous les enfants affectable d'un organisme quelque soit le niveau. [#186](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/186)
* Export csv des participants à une activité [#185](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/185)
* ajoute des commentaires des disponibilité  [#184](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/184)
* clôturer affectation [#168](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/168)
### Fixes
* owasp : deserialisation de données java [#188](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/188)
* spotbugs [#188](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/188)
* mcs [#188](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/188)
* fix constructeur [#187](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/187)
* Ajout de la date de création des messages [#183](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/183)
* Ordonner la position des Poste RO [#182](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/182)
  Correction du bug disponible -> non retenu [#180](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/180)
* impossibilité de saisir une date de fin supérieur à la date de début pour une activité [#179](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/179)
* espace retiré après l'identifiant de connexion [#177](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/177)
* email pour récupérer mot de passe [#175](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/175)
* Ajout du commentaire dans les alertes pour les participations. [#174](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/174)
* API modifier infos persos : envoi d'un nouveau token. [#173](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/173)
* ajout du filtre sur enabled  [#168](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/168)
* wording [#168](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/168)
## 1.0.2
### Features
* Alerte pour le créateur en cas de modification / ou en cas de suppression. [#169](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/169)
### Fixes
* Commentaires visible pour les employeurs (profil employeur uniquement) dans l'écran des participation. [#170](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/170)
* API Utilisateur modifierPatiellement : contrôle au moins un profil sélectionné. [#167](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/167)
## 1.0.1
### Features
* Renommage des API Statistique [#162](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/162)
* Ajout de statistiques organisme [#162](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/162)
* Création de statistiques utilisateurs [#162](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/162)
### Fixes
* Correction apostrophe avec variable [#163](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/163)
## 1.0.0
### Features
* Nouvelle api Liste d'organismes d'un utilisateur [#158](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/158)
* Rename api camelCase -> kebab-case [#158](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/158)
* Evaluation : note sur 10 [#157](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/157)
* Evaluation : date de création [#157](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/157)
* Statistique organismes (avec nombre d'activités) [#149](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/149)
* Création du profil super employeur. [#147](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/147)
* API modification de mes infos persos [#142](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/142)
* API organismes/ utilisateurs / groupes  [#141](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/141)
* commentaire sur la suppression [#140](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/140)
* Gestion des groupes [#139](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/139)
* changement nom Api Evaluer [#138](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/138)
* Ajout du filtre mes activité à l'API activite/entre utilisateur [#137](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/137)
* Ajout du filtre par nom à l'API activite/entre utilisateur [#137](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/137)
* Ajout du filtre par nom à l'API activite/entre organisme [#137](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/137)
* API droit création profil renommé [#136](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/136)
* Ordre poste au RO [#135](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/135)
* implementation de l'ordre dans l'entité utilisateur [#135](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/135)
* Edition profil par employeur [#135](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/135)
* Audit utilisateur (suivi du créate / update) [#135](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/135)
* Ecran invitations [#134](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/134)
* retrait du blocage de compte [#133](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/133)
* envois d'un mail à 5 tentatives infructueuse [#133](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/133)
* Mot de passe oublié [#132](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/132)
* Job de suppression de token périmée. [#132](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/132)
* Notification Partication Activites (alertes + mail) [#131](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/131)
* API de gestion des alertes [#130](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/130)
* Nouveau logo mails [#129](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/129)
* retrait de grade [#128](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/128)
* Ajout d'un champs estDispo dans ActiviteDao pourl'API des activités utilisateurs [#127](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/127)
* Inscription v2 : par invitation. [#124](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/124)
* Ajout idcorps dans utilisateur [#123](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/123)
* Gestion back des groupes [#122](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/122)
* Date dernière connexion [#122](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/122)
* possibilité d'envoyer des mail preécrie [#121](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/121)
* les dispo du calendrier prennent en compte les activités. [#120](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/120)
* Api mes participations [#120](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/120)
* Maj modèle de données et données. [#120](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/120)
  [#120](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/120)
* Gestion des participants [#118](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/118)
* Recherches participants [#118](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/118)
* Ajout tri par colonnes et filtre dans dashboard utilisateurs [#117](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/117)
* DashBoard Etape [#116](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/116)
* Formulaire de contact [#115](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/115)
* DashBorad message des contact [#115](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/115)
* Formulaire d'évaluation [#105](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/105)
* DashBorad message Evaluation [#105](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/105)
* API utilisateur
* API Utilisateur/activite/entre : on peut désormais voir ces participations
* Ajout d'une gestion simplifier des champs trop long via @TruncaceIfTooLong
* API Droit Création Profil
* Modification des Utilisateurs : Ajout de l'accord d'inscription
* API export CSV dispo
* API de recherche de la hiérarchie des organismes pour l'écran "informations personnelles"
* API liste des utilisateurs d'une unité
* Modification du password possible quand on modifie un utilisateur
* Ajout des initiales dans UtilisateurDto
* Rallongement de la taille mini des mots de passe à 12 caractères pour être en conformité avec le front
* Ajout d'une API pour récupérer les comptes validés/débloqués
* Ajout d'une API pour trier les utilisateurs
### Fix
* Nouvelles données postes au RO : requête qui marche également pour les tables vides. [#159](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/159)
* wording messages.properties [#155](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/155)
* wording mail tentative de connexion [#153](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/153)
* Correction droit API corps/all [#152](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/152)
* Titre alerte trop court [#151](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/151)
* Un employé ne doit pas pouvoir se mettre indisponible sur une date qui tombe pendant une activité sur laquelle il est déjà retenu ou convoqué [#150](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/150)
* Description des profils [#147](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/147)
* description non obligatoire [#146](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/146)
* fix email pour tous les environnement [#144](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/144)
* fix profil par defaut pour invitation [#144](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/144)
* Correction écran changement de mot de passe [#143](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/143)
* Inscription avec token [#132](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/132)
* Typo : API Activite commetaire -> commentaire [#126](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/126)
* Typo : Taux.totale -> Taux.total [#126](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/126)
* Listes des indispo [#120](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/120)
* Correction de données pour les choix de participation. [#118](https://gitlab.com/fabnum-minarm/metiis/backend/-/merge_requests/118)
* correction de la gestion des exceptions pour les champs trop long
* modélisation : modification de la propriété enfant de organisme
* optimisation de la méthode rempliRole de ProfilService
* correction création type disponibilité
* changement de séparateur dans les exports CSV
* correction modifier utilisateur
* Orthographe messages d'erreurs
* Modification du username via le dashboard
## 0.2.0
### Features
* API Liste d'organismes par type d'organisme.
* API Liste d'organismes en fonction du parent.
* API Type Organisme + 1er niveau d'organisme.
* les API pour l'inscription sont desormais public
* API grade / corps
* API profil
### Fix
* Correction d'un nom dans l'API Type Organisme
## 0.1.0
### Features
* Ajout du changelog
* API organisme
* API type-organisme
* API organisme
* API grade
* API corps
* API type-activite
* API Type-Disponiblite
### Fix
